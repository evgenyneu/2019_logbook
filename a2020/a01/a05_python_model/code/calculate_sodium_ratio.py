import pandas as pd
from cmdstanpy import CmdStanModel
from scipy.stats import zscore
import pickle
import os
import numpy as np
from tabulate import tabulate
import arviz as az
import seaborn as sns
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot
import math


def load_data(type):
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]

    # Standardize parameters
    zscores = zscore(df['na_over_h'].tolist(), ddof=1)
    uncertainties = (df['uncertainty'] / df['na_over_h'].std()).tolist()

    model_data = {
        'y': zscores,
        'uncertainties': uncertainties,
        'N': len(df)
    }

    return (model_data, df)

def calculate(type):
    """
    Parameters
    -----------

    type : str
        Stat type: 'AGB' or 'RGB'
    """

    model_data, df = load_data(type)
    data_dir = 'a2020/a01/a05_python_model/data'
    path_to_model = os.path.join(data_dir, f'samples_{type.lower()}.pkl')

    if os.path.exists(path_to_model):
        with open(path_to_model, 'rb') as input:
            samples = pickle.load(input)
    else:
        # Load model
        path = "a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan"
        model = CmdStanModel(stan_file=path)

        fit = model.sample(data=model_data, seed=11421,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=12000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        print(fit.summary())
        print(fit.diagnose())

        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        with open(path_to_model, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)

    # De-standardize parameters
    std = df['na_over_h'].std()
    mean = df['na_over_h'].mean()
    samples['mu.1'] = samples['mu.1'] * std + mean
    samples['mu.2'] = samples['mu.2'] * std + mean
    samples['sigma.1'] = samples['sigma.1'] * df['na_over_h'].std()
    samples['sigma.2'] = samples['sigma.2'] * df['na_over_h'].std()
    return samples


def sample_summary(df, show=True):
    rows = []

    for column in df:
        values = df[column].to_numpy()
        mean = round(df[column].mean(), 2)
        std = round(df[column].std(), 2)
        hpdi5 = np.round(az.hpd(values, credible_interval=0.05), 2)
        mode = hpdi5[0] + (hpdi5[1] - hpdi5[0]) / 2
        hpdi68 = np.round(az.hpd(values, credible_interval=0.68), 2)
        uncert_plus = hpdi68[1] - mode
        uncert_minus = mode - hpdi68[0]
        hpdi95 = np.round(az.hpd(values, credible_interval=0.95), 2)
        rows.append([column, mean, std, mode, uncert_plus,
                     uncert_minus, hpdi68, hpdi95])

    headers = ['Name', 'Mean', 'Std', 'Mode', '+', '-',
               '68% HPDI', '95% HPDI']

    if show:
        table = tabulate(rows, headers=headers, floatfmt=".2f", tablefmt="pipe")
        print(table)

    return pd.DataFrame(rows, columns=headers)


def plot_posterior(df, parameters=None, title=None):
    """
    Make histograms for the parameters from posterior destribution.

    Parameters
    -----------

    df : Panda's DataFrame

        Each column contains samples from posterior distribution.

    parameters : list of str

        Names of the parameters for plotting.
        If None, all parameters will be plotted.
    """
    if parameters is None:
        parameters = df.columns

    ncols = math.ceil(math.sqrt(len(parameters)))
    summary = sample_summary(df)
    nrows = ncols

    if (ncols**2 - len(parameters) >= ncols):
        nrows -= 1

    fig, ax = plt.subplots(nrows=nrows, ncols=ncols)

    if ncols == 1:
        ax = np.array(ax)

    axes = ax.flatten()

    for i, ax in enumerate(axes):
        if i >= len(parameters):
            break

        parameter = parameters[i]
        data = summary[summary['Name'] == parameter].iloc[0]
        sns.distplot(df[parameter], kde=False, norm_hist=True, ax=ax)

        sns.kdeplot(df[parameter], shade=False, clip=data['95% HPDI'],
                    label='95% HPDI', ax=ax, legend=None)

        sns.kdeplot(df[parameter], shade=False, clip=data['68% HPDI'],
                    label='68% HPDI', color='blue', ax=ax, legend=None)

        sns.kdeplot(df[parameter], shade=True, clip=data['68% HPDI'],
                    color='blue', label='_nolegend_', alpha=0.2,
                    ax=ax, legend=None)

        ax.axvline(x=data['Mean'], label='Mean', linewidth=1.5,
                   linestyle='dashed', color='#33AA66')

        ax.axvline(x=data['Mode'], label='Mode', linewidth=1.5,
                   color='#FF66AA')
        ax.set_xlabel(parameter)
        ax.set_xlim(np.percentile(df[parameter], [0.5, 99.5]))

    # Do not draw the axes for non-existing plots
    for ax in axes[len(parameters):]:
        ax.axis('off')

    if len(parameters) > 1:
        handles, labels = axes[0].get_legend_handles_labels()
        fig.legend(handles, labels, loc='lower right')
    else:
        axes[0].legend()

    if title is not None:
        fig.suptitle(title)
        plt.tight_layout(rect=[0, 0, 1, 0.95])
    else:
        plt.tight_layout()


if __name__ == '__main__':
    sns.set_style("ticks")

    df = calculate('RGB')
    print("RGB")
    print("")
    plot_posterior(df, title="Probability density for RGB stars")
    save_plot(plt=plt, suffix='rgb')

    print("\n\nAGB")
    print("")
    df = calculate('AGB')
    plot_posterior(df, title="Probability density for AGB stars")
    save_plot(plt=plt, suffix='agb')

    print('We are done')

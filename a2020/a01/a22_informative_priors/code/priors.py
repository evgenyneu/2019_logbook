import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from code_dope.python.plot.plot import save_plot
from generate_priors import generate_priors, distribution


def do_work():
    np.random.seed(seed=4231)
    sns.set(style="ticks")
    n = 300
    priors = generate_priors(n)
    mu1_samples = priors["mu1"]
    mu2_samples = priors["mu2"]
    sigma1_samples = priors["sigma1"]
    sigma2_samples = priors["sigma2"]
    p_samples = priors["p"]

    x = np.linspace(-10, 10, 500)

    result = [
        distribution(i, p=p_samples, mu1=mu1_samples, mu2=mu2_samples,
                     sigma1=sigma1_samples, sigma2=sigma2_samples)
        for i in x
    ]

    result = np.array(result)

    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    mean = df["na_over_h"].mean()
    std = df["na_over_h"].std()
    x_transformed = x * std + mean
    fig, ax = plt.subplots()

    for i in range(200):
        ax.plot(x_transformed, result[:, i], color="#00000011")

        ax.set_xlabel("Sodium abundance [Na/H]")
        ax.set_ylabel("Probability density")
        ax.set_ylim(0, 10)
        fig.tight_layout()

    save_plot(plt=fig)


if __name__ == '__main__':
    do_work()
    print('We are done')

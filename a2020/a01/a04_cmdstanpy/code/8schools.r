library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

schools_dat <- list(J = 8,
                    y = c(28,  8, -3,  7, -1,  1, 18, 12),
                    sigma = c(15, 10, 16, 11,  9, 11, 10, 18))

model_path = 'a2020/a01/a04_cmdstanpy/code/8schools.stan'
fit <- stan(file=model_path, data=schools_dat)

print(fit)

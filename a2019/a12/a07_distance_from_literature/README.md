## Distance form Harris catalog is 8.9 kps

From [Harris catalog](http://physwww.mcmaster.ca/~harris/mwgc.dat).

## Gaia Collaboration at al. 2018

Distance from [Gaia collaboration at al. 2018](https://arxiv.org/abs/1804.09381), the data used is from Harris 2010 as well.

The distance form the Sun is 8.89 ± 0.21 kpc.

Code: [a2019/a12/a07_distance_from_literature/code/distance.py](a2019/a12/a07_distance_from_literature/code/distance.py)

## Read about Gaia parallaxes

[Gaia Data Release 2. Using Gaia parallaxes](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...9L/abstract)

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def observed():
    data_dir = 'a2019/a12/a09_distance/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open stars data from FITS file
    fits_path = os.path.join(data_dir, '27oct10016.fibres.txt')
    fits_df = pd.read_fwf(fits_path)

    # add new colu ns
    df.insert(loc=1, column='observed', value=0)

    # Go over rows
    for index, star in df.iterrows():
        star_id = star['id']

        star_fits = fits_df.loc[(fits_df['Name'] == star_id)]

        if star_fits.shape[0] > 1:
            raise ValueError(f"Too many values {star_id}")

        if star_fits.shape[0] == 1:
            df.loc[index, 'observed'] = 1

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_observed.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    observed()
    print('We are done')

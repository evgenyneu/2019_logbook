## Save abundances from Chris

I have received a [CSV file](a2020/a07/a06_abundances_from_chris/data/elements_final.csv) from Chris with abundances he calculated for the stars. I want to add these abundances to the list of all stars.

Code: [a2020/a07/a06_abundances_from_chris/code/add_abundances_from_chris.py](a2020/a07/a06_abundances_from_chris/code/add_abundances_from_chris.py)

Output: [a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris.csv](a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris.csv)


## Mark AGB/RGB stars

I want to mark stars as RGB and AGB using based on the type. A new column 'agb_or_arb' is added containing either 'AGB' or 'RGB':

* Mark AGB if type is AGBS, BAGS or BAGB.

* Mark RGB if type is RGBS, BGBS.


Code: [a2020/a07/a06_abundances_from_chris/code/mark_agb_rgb_stars.py](a2020/a07/a06_abundances_from_chris/code/mark_agb_rgb_stars.py)

Output: [a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris_agb_or_arb.csv](a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris_agb_or_arb.csv)



## Plot old vs new values

I want to double check the I merge the data correctly by comparing the old and new values of effective temperature and iron abundances.

Code: [a2020/a07/a06_abundances_from_chris/code/plot_old_vs_new.py](a2020/a07/a06_abundances_from_chris/code/plot_old_vs_new.py)

### Old vs new Fe I abundances

<img src="a2020/a07/a06_abundances_from_chris/code/plots/plot_old_vs_new_fei_h.png" width="700" alt='Old vs new Fe I abundances'>

Figure 1: Old vs new Fe I abundances.


### Old vs new Fe II abundances

<img src="a2020/a07/a06_abundances_from_chris/code/plots/plot_old_vs_new_feii_h.png" width="700" alt='Old vs new Fe II abundances'>

Figure 2: Old vs new Fe II abundances.


### Old vs new effective temperature abundances

<img src="a2020/a07/a06_abundances_from_chris/code/plots/plot_old_vs_new_t_eff.png" width="700" alt='Old vs new effective temperature abundances'>

Figure 3: Old vs new effective temperature abundances.



## Plot CMD

I want to plot the CMD that only includes that stars that have measurements of temeprature and abundaces that Chris supplied.

Code: [a2020/a07/a06_abundances_from_chris/code/cmd_b_minus_k.py](a2020/a07/a06_abundances_from_chris/code/cmd_b_minus_k.py)

<img src="a2020/a07/a06_abundances_from_chris/code/plots/cmd_b_minus_k.png" width="700" alt='CMD for stars with measured abundances'>

Figure 4: Color magnitude diagram showing stars for which Chris measured chemical abundances.


## Remove old columns

I want to remove old columns containing temperature and iron abundances. I then save the CSV file to [data/NGC288/ngc288_stars.csv](data/NGC288/ngc288_stars.csv) and updated the documentation at [data/NGC288/ngc288_stars.md](data/NGC288/ngc288_stars.md).

Code: [a2020/a07/a06_abundances_from_chris/code/remove_columns.py](a2020/a07/a06_abundances_from_chris/code/remove_columns.py)

Output: [a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris_agb_or_rgb_columns_removed.csv](a2020/a07/a06_abundances_from_chris/data/ngc288_stars_with_abundances_from_chris_agb_or_rgb_columns_removed.csv)



## Extract data for stats

I want to calculate proportion of high sodium stars using Stan. In order to do this, I need to extract just three columns into a separate CSV file.

Code: [a2020/a07/a06_abundances_from_chris/code/abundances_for_stats.py](a2020/a07/a06_abundances_from_chris/code/abundances_for_stats.py)

Output: [a2020/a07/a06_abundances_from_chris/data/sodium.csv](a2020/a07/a06_abundances_from_chris/data/sodium.csv)

// Mixture of two Guassians

data {
  int<lower=1> n;           // Number of observations
  real y[n];                // Observed values
  real uncertainties[n];    // Uncertainties
  real sigma_stdev_prior;   // Spread for standard deviation of population
}

parameters {
  real<lower=0,upper=1> r;  // Mixing proportion
  ordered[2] mu;            // Locations of mixture components
  real<lower=0> sigma;      // Spread of mixture components
}

model {
  sigma ~ normal(0, sigma_stdev_prior);
  mu[1] ~ normal(0, 1);
  mu[2] ~ normal(0, 1);
  r ~ beta(2, 2);

  // Loop through observed values
  // and mix two Gaussian components accounting for uncertainty
  // of each measurment
  for (i in 1:n) {
    target += log_mix(1 - r,
      normal_lpdf(y[i] | mu[1], sqrt(sigma^2 + uncertainties[i]^2)),
      normal_lpdf(y[i] | mu[2], sqrt(sigma^2 + uncertainties[i]^2)));
  }
}

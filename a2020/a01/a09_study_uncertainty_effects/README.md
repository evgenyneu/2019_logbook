## Study effects of measurement uncertainty

I change the measurement uncertainties and see how it affects the posterior distributions of parameters. In the code, I multiply the actual measurement uncertainties by various factors (0, 0.5, 2, 4, 10) and plot the posterior distributions of parameters (shown on Figures 1 and 2).

We can see that from Figures 1 and 2 that measurement uncertainties smaller than actual values only slightly reduce the error bars our parameters, such as `p`. However, data with larger than actual uncertainties result in different mode estimate and substantially increased error bars.

Code: [/Users/evgenii/Documents/2019_summer_project/logbook/a2020/a01/a09_study_uncertainty_effects/code/uncertainty_effects.py](/Users/evgenii/Documents/2019_summer_project/logbook/a2020/a01/a09_study_uncertainty_effects/code/uncertainty_effects.py)


<img src="a2020/a01/a09_study_uncertainty_effects/code/plots/uncertainty_effects_RGB.png" alt='Posterior probability densities for RGB stars'>

Figure 1: Posterior distributions of parameters of the model for RGB stars corresponding to different measurement uncertainties. The markers are the modes (values of maximum probability). The error bars indicate 68% and 95% HPDI (highest posterior density interval).


<img src="a2020/a01/a09_study_uncertainty_effects/code/plots/uncertainty_effects_AGB.png" alt='Posterior probability densities for AGB stars'>

Figure 2: Posterior distributions of parameters of the model for AGB stars corresponding to different measurement uncertainties.

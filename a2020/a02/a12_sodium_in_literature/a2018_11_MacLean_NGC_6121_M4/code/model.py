import os
import pandas as pd
from paper.model.code.run_model import do_analysis, load_data


def convert_data_to_csv():
    data_dir = "a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/data"
    data_path = os.path.join(data_dir, "a2018_11_MacLean_NGC_6121_M4_table5.dat")
    df = pd.read_table(data_path)
    df_new = pd.DataFrame(columns=["Type", "A(Na)", "e_A(Na)"])

    for index, star in df.iterrows():
        star_id = star["ID"]
        star_type = star["Type"]
        na_over_h = star["logε(Na)"]
        e_na_over_h = star["rms.3"]

        if float(e_na_over_h) == 0:
            print(f"Zero Na uncertainty for star ID={star_id}")
            e_na_over_h = 0.01

        df_new.loc[star_id] = [star_type, na_over_h, e_na_over_h]

    # Standardise
    df_new['A(Na)_std'] = (df_new['A(Na)'] - df_new['A(Na)'].mean()) / df_new['A(Na)'].std()
    df_new['e_A(Na)_std'] = df_new['e_A(Na)'] / df_new['A(Na)'].std()

    csv_path = os.path.join(data_dir, "a2018_11_MacLean_NGC_6121_M4_table5.csv")
    df_new.to_csv(csv_path, index_label="ID")


def do_work():
    """Main function that does all the work"""

    convert_data_to_csv()
    data_path = "a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/data/a2018_11_MacLean_NGC_6121_M4_table5.csv"
    type_name = "Type"
    abundance_name = "A(Na)_std"
    uncertainty_name = "e_A(Na)_std"

    values_agb, uncert_agb = load_data('AGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    values_rgb, uncert_rgb = load_data('RGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    do_analysis(values_agb=values_agb, uncert_agb=uncert_agb,
                values_rgb=values_rgb, uncert_rgb=uncert_rgb,
                path="a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4",
                dir_name="model_info",
                study_name="NGC 6121 (M4) MacLean et al. 2018",
                xlabel="Sodium abundance A(Na) (std)",
                mu_prior=0,
                sigma_prior=1)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

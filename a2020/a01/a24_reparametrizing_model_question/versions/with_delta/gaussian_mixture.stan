// Guassian mixture model
data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed values
  real uncertainties[N]; // uncertainties
}
parameters {
  vector[N] y_true;          // true value
  real<lower=0,upper=1> p;   // mixing proportion
  real mu1;                  // location of 1st mixture component
  real<lower=0> sigma;       // spread of mixture components
  real delta;                // distance between two mixture components
}
transformed parameters {
  real mu2;                  // location of 2nd mixture component
  mu2 = mu1 + delta;
}
model {
  sigma ~ normal(0, 0.1);
  mu1 ~ normal(-1.4, 0.2);
  delta ~ normal(0.4, sqrt(0.2^2 + 0.2^2));
  p ~ beta(2, 2);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y_true[n] | mu1, sigma),
                      normal_lpdf(y_true[n] | mu2, sigma));
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}

# Analysing data from Campbell et al. 2013, NGC 6752

We ran the statistical model using data from [Campbell et al. 2013](https://arxiv.org/abs/1305.7090) paper for NGC 6752 globular cluster.

Code: [a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/code/a2013_06_campbell_ngc_6752.py](a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/code/a2013_06_campbell_ngc_6752.py)


## Observations

<img src="a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances from [Campbell et al. 2013](https://arxiv.org/abs/1305.7090), NGC 6752.


## Results


### Parameter distributions

<img src="a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     | r                        | mu.1                      | mu.2                      |
|:----|-------------------------:|--------------------------:|--------------------------:|
| AGB | $`0.05^{+0.69}_{-0.05}`$ | $`-0.08^{+0.04}_{-0.07}`$ | $`-0.04^{+0.05}_{-0.04}`$ |
| RGB | $`0.72^{+0.11}_{-0.12}`$ | $`-0.12^{+0.10}_{-0.07}`$ | $`0.41^{+0.04}_{-0.05}`$  |

### AGB failure rate

We calculated failure rate f=$`0.9^{+0.1}_{-0.9}`$, which is defined as proportion of SP2 stars that fail to reach AGB:

```
f = 1 - r_agb / r_rgb
```



## Analysis

### RGB stars

We calculated `r = 0.7 ± 0.1` for RGB stars, which agrees with `r=0.7` reported by [Campbell et at. 2013](https://arxiv.org/abs/1305.7090).

### AGB stars

For AGB sample, the Stan showed divergent transitions warning:

> 35 of 16000 (0.22%) transitions ended with a divergence

In addition, the distribution of parameter `r` was close to uniform (Fig. 3), which suggest that bimodal distribution is not a good model for AGB stars. This confirms what we can see on Fig. 1, that shows a unimodal distribution of values for AGB stars.

<img src="a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/images/distribution_r_agb.png" width="700" alt='Distribution of parameter r'>

Figure 3: Distribution of parameter `r`.

#### Distributions of mu1 and mu2

We can also see from Fig. 4 that distributions of mu1 and mu2 mostly overlap, and their difference of `0.06 ± 0.15` shows no significant difference. Therefore, the data are more consistent with unimodal distribution for AGB stars, which agrees with conclusion from [Campbell et al. 2013](https://arxiv.org/abs/1305.7090).

<img src="a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/images/mus_agb.png" width="700" alt='Distribution of parameter mu1 and mu2'>

Figure 4: Distributions of parameters `mu1` and `mu2`.


### AGB failure rate


The distribution of `f` is shown on Fig. 5. We can see that it is very uncertain. Since model suggests that there is single low-sodium AGB population, the failure rate of f=$`0.90^{+0.10}_{-0.90}`$ is consistent with Campbell et al. 2013.

<img src="a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752/images/agb_failure_rate.png" width="700" alt='AGB failure rate'>

Figure 5: Distribution of AGB failure rate.


## Conclusion

Our model suggests there is a single low-sodium population of AGB stars and two subpopulations of RGB stars with `r_RGB = 0.7 ± 0.1` and AGB avoidance rate f=$`0.90^{+0.10}_{-0.90}`$. These results agree with [Campbell et al. 2013](https://arxiv.org/abs/1305.7090).

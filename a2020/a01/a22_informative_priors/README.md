# Make informative priors

[Yesterday](a2020/a01/a20_plotting_priors) I plotted priors. Based on those plots, I make the following changes to the model priors:

1. Decrease the spread of the priors.
1. Make the spread of two population equal.

## Previous model (weak priors)

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma_1) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma_2) \\
\mu_1 \sim \textrm{Normal}(-1, 0.5) \\
\mu_2 \sim \textrm{Normal}(1, 0.5) \\
\sigma_1 \sim \textrm{LogNormal}(0, 1) \\
\sigma_2 \sim \textrm{LogNormal}(0, 1) \\
p \sim \textrm{Uniform}(0, 1).
```

Stan code: [a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan](a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan)

## New model (informative priors)

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma) \\
\mu_1 \sim \textrm{Normal}(-0.7, 0.5) \\
\mu_2 \sim \textrm{Normal}(0.7, 0.5) \\
\sigma \sim \textrm{HalfNoormal}(0, 0.3) \\
p \sim \textrm{Beta}(2, 2).
```

Stan code: [a2020/a01/a22_simulate_sodium_constraining_priors/code/stan_model/sodium_ratio_model.stan](a2020/a01/a22_simulate_sodium_constraining_priors/code/stan_model/sodium_ratio_model.stan)


## Plotting all priors

On Fig. 1 we plot multiple prior distributions of sodium abundances. We can see that the values of the priors are less spread out than the [weak priors](a2020/a01/a20_plotting_priors/code/plots/priors.png).

Code: [a2020/a01/a22_informative_priors/code/priors.py](a2020/a01/a22_informative_priors/code/priors.py)

<img src="a2020/a01/a22_informative_priors/code/plots/priors.png" width="700" alt='Prior distribution'>

Figure 1: Prior distributions of sodium abundances.


## Plotting individual priors

On Fig. 2 we plot some individual prior distributions. The distributions look more realistic than the ones from [weak priors](a2020/a01/a20_plotting_priors). Specifically, we no longer have distributions with large spreads.

Code: [a2020/a01/a22_informative_priors/code/priors_separate.py](a2020/a01/a22_informative_priors/code/priors_separate.py)

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__00.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__01.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__02.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__03.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__04.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__05.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__06.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__07.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__08.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__09.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__10.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__11.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__12.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__13.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__14.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__15.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__16.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__17.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__18.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a22_informative_priors/code/plots/priors_separate__19.png" width="400" alt='Prior distribution'>


Figure 2: Prior distributions of sodium abundances. The dotted and dashed lines show distributions of two populations, and the solid lines show the combined distribution mixed with parameter p.

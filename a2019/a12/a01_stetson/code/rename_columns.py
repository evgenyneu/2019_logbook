# Rename columns

import pandas as pd
import os


def rename():
    data_dir = 'a2019/a12/a01_stetson/data'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_stetson_update__yazan_id.csv')
    df = pd.read_csv(stars_path)

    # Rename columns
    df = df.rename(columns={'TYPE_tab1': 'type'})
    df = df.rename(columns=lambda x: x.lower())
    df = df.rename(columns=lambda x: x.replace('/', '_'))
    df = df.rename(columns=lambda x: x.replace(' ', '_'))

    # Save data frame
    filename = 'ngc288_stars_with_stetson_update__yazan_id_renamed.csv'
    output_path = os.path.join(data_dir, filename)
    df.to_csv(output_path, index=False)
    print(f'Successufully saved to {output_path}')


if __name__ == '__main__':
    rename()
    print('We are done!')

# Use values for UVBI from stetson's catalog for Cat=STE stars

import pandas as pd
import os


def update_magnitudes(df, stetson_df, star_id, star, index, star_stetson):
    if star['Cat'] != 'STE':
        return

    filters = ['U', 'V', 'B', 'I']

    for filter in filters:
        try:
            magnitude = float(star_stetson[f'{filter}mag_tab1'])
            uncertainty = float(star_stetson[f'e_{filter}mag_tab1'])
        except ValueError:
            print(f'Failed to read {filter} magnitude from {star_id}')
            df.loc[index, filter] = ''
            df.loc[index, f'u_{filter}'] = ''
            continue

        # Update magnitude and uncertainty
        df.loc[index, filter] = magnitude
        df.loc[index, f'u_{filter}'] = uncertainty


def stetson():
    data_dir = 'a2019/a12/a01_stetson/data'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open stetson cross match data
    stetson_path = os.path.join(data_dir, 'XMatch_stetson_0.5_arcsec.csv')
    stetson_df = pd.read_csv(stetson_path)

    # Insert new column
    df.insert(loc=df.columns.get_loc('ID_2MASS') + 1,
              column='id_stetson', value='')

    # Go over rows
    for index, star in df.iterrows():
        star_id = star['id']

        star_stetson = stetson_df.loc[stetson_df['id_tab2'] == star_id]

        if star_stetson.shape[0] != 1:
            print(f"Can't find star {star_id} in Stetson catalog")
            continue

        update_magnitudes(df=df, stetson_df=stetson_df, star_id=star_id,
                          star=star, index=index, star_stetson=star_stetson)

        stetson_id = star_stetson['Star_tab1'].values[0]
        df.loc[index, 'id_stetson'] = stetson_id


    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_stetson_update.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully saved to {output_path}')


if __name__ == '__main__':
    stetson()
    print('We are done!')

import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    plt.figure(figsize=(4.3, 3.2))
    data_dir = 'a2019/a12/a10_calculate_distance/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    columns = ['pm_ra', 'u_pm_ra', 'pm_de', 'u_pm_de']

    for column in columns:
        df = df[df[column] != ' ']  # Remove rows with missing uncertainties
        df[column] = pd.to_numeric(df[column])

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    pm_ra = stars_other_df['pm_ra']
    pm_de = stars_other_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='gray', marker='o', s=7, edgecolors='gray',
                label='Excluded')


    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    pm_ra = stars_agb_df['pm_ra']
    pm_de = stars_agb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    pm_ra = stars_rgb_df['pm_ra']
    pm_de = stars_rgb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    plt.axis('equal')
    plt.xlim(3, 5.5)
    plt.ylim(-6.5, -4.6)

    plt.legend()
    plt.xlabel(r'Proper motion in right ascension $\mu_\alpha \ \cos \delta$ [mas $a^{-1}$]')
    plt.ylabel('Proper motion in declination $\mu_\delta$ [mas $a^{-1}$]')

    # Descriptive statistics for selected stars
    # -----------

    selected_df = df.loc[df['selected'] == 1]

    pm_ra_mean = selected_df['pm_ra'].mean()
    pm_ra_std = selected_df['pm_ra'].std()
    print(f'Proper motion RA={pm_ra_mean:.3f}±{pm_ra_std:.3f}')

    pm_de_mean = selected_df['pm_de'].mean()
    pm_de_std = selected_df['pm_de'].std()
    print(f'Proper motion DE={pm_de_mean:.3f}±{pm_de_std:.3f}')

    # Draw a circle with radius
    radius = 0.25
    circle = plt.Circle((pm_ra_mean, pm_de_mean), radius, color='#7181ff',
                        fill=False, linewidth=1)

    ax = plt.gca()
    ax.add_artist(circle)

    # Error bars
    # --------------

    selected_df = df.loc[(df['selected'] == 1)]

    pm_ra = selected_df['pm_ra']
    pm_de = selected_df['pm_de']

    u_pm_ra = selected_df['u_pm_ra']
    u_pm_de = selected_df['u_pm_de']

    u_ra = u_pm_ra.median()
    u_de = u_pm_de.median()

    x_margin = 0.05 * abs(plt.xlim()[1] - plt.xlim()[0])
    y_margin = 0.05 * abs(plt.ylim()[0] - plt.ylim()[1])
    x_error_bar = plt.xlim()[1] - u_ra - x_margin
    y_error_bar = plt.ylim()[0] + u_de + y_margin

    plt.errorbar(x=[x_error_bar], y=[y_error_bar],
                 xerr=[u_ra], yerr=[u_de],
                 fmt='none', color='black', capsize=2,
                 ecolor='black', eflinewidth=1)

    # --------------

    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    plt.tight_layout(pad=0.05)
    save_plot(plt=plt, extensions=['png', 'pdf'])
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

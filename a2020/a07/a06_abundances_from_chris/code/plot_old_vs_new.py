import pandas as pd
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot
import os


def set_plot_style():
    """Set global style"""

    plt.rcParams['font.family'] = 'serif'

    TINY_SIZE = 15
    SMALL_SIZE = 18
    NORMAL_SIZE = 22
    LARGE_SIZE = 25

    # Title size
    plt.rcParams['axes.titlesize'] = LARGE_SIZE

    # Axes label size
    plt.rcParams['axes.labelsize'] = NORMAL_SIZE

    # Tick label size
    plt.rcParams['xtick.labelsize'] = TINY_SIZE
    plt.rcParams['ytick.labelsize'] = TINY_SIZE

    # Legend text size
    plt.rcParams['legend.fontsize'] = SMALL_SIZE

    plt.rcParams['font.size'] = NORMAL_SIZE
    plt.rcParams['legend.fontsize'] = NORMAL_SIZE

    # Grid color
    plt.rcParams['grid.color'] = '#cccccc'

    # Define plot size
    plt.rcParams['figure.figsize'] = [10, 10]

    # Marker size
    plt.rcParams['lines.markersize'] = 13


def plot_old_vs_new(df, col_old_name, col_new_name,
                    col_error_old_name=None,
                    col_error_new_name=None,
                    marker_color_type=dict(AGB='#0033ff', RGB='#ff0000'),
                    marker_type_type=dict(AGB='^', RGB='o'),
                    x_text_offset=100,
                    show_ids=True):

    fig, ax = plt.subplots(1, 1)

    # Drop rows with empty magnitudes
    df = df.dropna(subset=[col_new_name])

    for star_type in ['AGB', 'RGB']:
        df_type = df[df['agb_or_rgb'] == star_type]
        marker_color = marker_color_type[star_type]
        marker_type = marker_type_type[star_type]

        # Show scatter plots
        x_values = df_type[col_old_name]
        y_values = df_type[col_new_name]

        if col_error_old_name is not None:
            x_errors = df_type[col_error_old_name]
            y_errors = df_type[col_error_new_name]

            ax.errorbar(x_values, y_values, xerr=x_errors, yerr=y_errors,
                        fmt='none', ecolor=f"{marker_color}70")

        ax.scatter(x_values, y_values,
                   color=f"{marker_color}40",
                   marker=marker_type,
                   edgecolor=marker_color, zorder=2,
                   label=star_type)

        if show_ids:
            for x_value, y_value, no in zip(x_values, y_values, df_type["id"]):
                ax.text(x_value + x_text_offset, y_value, s=no,
                        verticalalignment='center', fontsize=14,
                        color=marker_color)

    # Plot diagonal
    xlim = ax.get_xlim()
    ax.plot(xlim, xlim, linestyle='dashed', color="gray", zorder=1)

    # Show grid
    ax.grid()

    # Show legend
    ax.legend()

    # Set plot labels
    ax.set_xlabel(f"Old {col_new_name}")
    ax.set_ylabel(f"New {col_new_name}")

    ax.set_aspect('equal', adjustable='box')
    fig.tight_layout()
    save_plot(plt=plt, suffix=col_old_name)


def plot():
    set_plot_style()
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open star data
    data_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb.csv')

    df = pd.read_csv(data_path)

    # Plot old vs new
    # ----------

    plot_old_vs_new(df=df, col_old_name="t_eff", col_new_name="Teff",
                    x_text_offset=50, show_ids=False)

    plot_old_vs_new(df=df, col_old_name="fei_h", col_new_name="FeI/H",
                    x_text_offset=0.01, show_ids=False,
                    col_error_old_name='u_fei_h',
                    col_error_new_name='errFeI/H')

    plot_old_vs_new(df=df, col_old_name="feii_h", col_new_name="FeII/H",
                    x_text_offset=0.01, show_ids=False,
                    col_error_old_name='u_feii_h',
                    col_error_new_name='errFeII/H')


if __name__ == '__main__':
    plot()
    print('We are done!')

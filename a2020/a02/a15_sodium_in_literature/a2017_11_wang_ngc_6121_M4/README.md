# Analysing data from [Wang et al. 2017](https://ui.adsabs.harvard.edu/abs/2017A%26A...607A.135W/abstract), NGC 6121 (M4)

We use [Na/H] LTE  abundances. For uncertainty of [Na/H] we use 0.16 from Table 9, Wang et al. 2017.

Code: [a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_6121_M4/code/model.py](a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_6121_M4/code/model.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_6121_M4/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_6121_M4/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.64^{+0.13}_{-0.15}`$  |   $`0.53 \pm 0.11`$   |    Yes     |    Yes     |
| $`r_{rgb}`$   |  $`0.72^{+0.06}_{-0.07}`$  |   $`0.76 \pm 0.06`$   |    Yes     |    Yes     |
| $`f`$         |  $`0.18^{+0.18}_{-0.24}`$  |   $`0.30 \pm 0.15`$   |    Yes     |    Yes     |

plot_frame_means = function(data, title) {
    data.means = lapply(data, mean)
    data.sd = lapply(data, sd)
    means = unlist(data.means, use.names=FALSE)
    sds = unlist(data.sd, use.names=FALSE)

    dotchart(means, labels=names(data.means),
             xlim=c(min(means) - max(sds), max(means) + max(sds)),
             main=title,
             pt.cex=1, pch=1)

    for (i in 1:length(means)) {
        lines(c(means[i] - sds[i], means[i] + sds[i]), c(i, i), lwd=2)
    }

    abline(v=0, lty=2)
}

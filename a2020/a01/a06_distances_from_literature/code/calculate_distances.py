# Calculate distance to NGC 288 and their uncertainties
# using distance moduli from literature
# -----------

from uncertainties import ufloat
from a2019.a12.a10_calculate_distance.code.distance import get_distance
from sigfig import round as round_sigfig
import numpy as np


def get_data():
    """
    Returns:

    Distance moduli to NGC 288 from literature.

    Array of tuples with following indexes:
        0 : distance modulus m - M (visual).
        1 : color excess E(B - V).
        2 : Name of publication.
    """

    return [
        (ufloat(14.9, 0.2), ufloat(0, 0), 'Penny 1984'),
        (ufloat(14.85, 0.10), ufloat(0.04, 0.02), 'Alcaino 1997'),
        (ufloat(14.57, 0.07), ufloat(0.018, 0.010), 'Chen 2000'),
        (ufloat(14.84, 0.1), ufloat(0.03, 0), 'Harris 2010'),
        (ufloat(14.768, 0.003), ufloat(0.03, 0), 'Arellano Ferro 2013')
    ]


def calculate_with_standard_uncertainties(data):
    distances = []

    for item in data:
        distance_modulus = item[0]
        color_excess = item[1]
        source = item[2]

        distance = get_distance(distance_modulus, color_excess=color_excess)
        distances.append(distance)
        print(f'Distance: {distance} pc {source}')

    # Calculate average distance

    values = (distance.nominal_value for distance in distances)
    values = np.array(list(values))
    uncertainties = (distance.std_dev for distance in distances)
    uncertainties = np.array(list(uncertainties))

    mean_distance = np.mean(values)
    total_uncertainty = np.sqrt(np.sum(uncertainties**2)) / len(uncertainties)

    print(f"Mean distance: {mean_distance} +- {total_uncertainty}")


def calculate_with_upper_lower_uncertainties(data):
    for item in data:
        distance_modulus = item[0]
        color_excess = item[1]
        source = item[2]

        distance_nominal = get_distance(
            distance_modulus.nominal_value,
            color_excess=color_excess.nominal_value)

        distance_upper = get_distance(
            distance_modulus.nominal_value + distance_modulus.std_dev,
            color_excess=color_excess.nominal_value)

        distance_lower = get_distance(
            distance_modulus.nominal_value - distance_modulus.std_dev,
            color_excess=color_excess.nominal_value)

        uncertainty_up = distance_upper - distance_nominal
        uncertainty_down = distance_nominal - distance_lower

        print(
            (
                f'Distance: {round_sigfig(distance_nominal, 2)}'
                f' [+{round_sigfig(uncertainty_up, 1)},'
                f' -{round_sigfig(uncertainty_down, 1)}] pc {source}'
            )
        )


if __name__ == '__main__':
    data = get_data()
    print('Approximate uncertainties')
    calculate_with_standard_uncertainties(data)
    print('\n\nAssymetric uncertainties')
    calculate_with_upper_lower_uncertainties(data)

from scipy import stats


def generate_priors(n):
    mu1_samples = stats.norm(-0.7, 0.5).rvs(size=n)
    mu2_samples = stats.norm(0.7, 0.5).rvs(size=n)
    sigma1_samples = stats.halfnorm(0, 0.3).rvs(size=n)
    sigma2_samples = sigma1_samples
    p_samples = stats.beta(2, 2).rvs(size=n)

    return dict(
        mu1=mu1_samples,
        mu2=mu2_samples,
        sigma1=sigma1_samples,
        sigma2=sigma2_samples,
        p=p_samples
    )


def distribution(x, p, mu1, mu2, sigma1, sigma2):
    return p * stats.norm.pdf(x, mu1, sigma1) + \
        (1 - p) * stats.norm.pdf(x, mu2, sigma2)

// Guassian mixture model
data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed values
  real uncertainties[N]; // uncertainties
}
parameters {
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  real<lower=0> sigma;       // spread of mixture components
  real z1;
  real z2;
}
transformed parameters {
  real mu1_mix = mu[1] + sigma*z1;
  real mu2_mix = mu[2] + sigma*z2;
}
model {
  sigma ~ normal(0, 0.1);
  mu[1] ~ normal(-1.4, 1);
  mu[2] ~ normal(-1.0, 1);
  p ~ beta(2, 2);
  z1 ~ std_normal();
  z2 ~ std_normal();

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y[n] | mu1_mix, uncertainties),
                      normal_lpdf(y[n] | mu2_mix, uncertainties));
  }
}

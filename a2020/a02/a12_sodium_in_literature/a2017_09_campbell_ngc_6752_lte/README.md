# Analysing data from Campbell et al. 2017, NGC 6752 LTE

We ran the statistical model using LTE data from [Campbell et al. 2017](https://arxiv.org/abs/1707.02840v1) paper for NGC 6752 globular cluster.

Code: [a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/code/model.py](a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/code/model.py)

Some stars had zero uncertainties for the abundances. We have replaced these uncertainties with `0.01`.

## Observations

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances.


## Results


### Parameter distributions

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     |                        r |                     mu.1 |                     mu.2 |
|----:|-------------------------:|-------------------------:|-------------------------:|
| AGB | $`0.63^{+0.21}_{-0.26}`$ | $`4.53^{+0.12}_{-0.03}`$ | $`4.78^{+0.05}_{-0.06}`$ |
| RGB | $`0.70^{+0.14}_{-0.16}`$ | $`4.68^{+0.15}_{-0.09}`$ | $`5.17^{+0.07}_{-0.06}`$ |

### AGB failure rate

We calculated failure rate f=$`0.1^{+0.4}_{-0.4}`$, which is defined as proportion of SP2 stars that fail to reach AGB:

```
f = 1 - r_agb / r_rgb
```



## Analysis

### RGB stars

Distribution of `r` agrees with previous $`r = 0.7 \pm 0.1`$ calculated from [Campbell et al. 2013 data](a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752).


### AGB stars

Posterior distribution show very weak evidence of two populations, unlike our previous result from [Campbell et al. 2013 data](a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752), which did not show evidence for two populations. The two populations are very close together and near the first population of RGB stars. In addition, distribution of parameter `r` is fairly flat and bimodal (see Fig. 3).

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/images/agb_r.png" width="700" alt='Distribution of parameter $r$ for AGB stars.'>

Figure 3: Distribution of parameter $r$ for AGB stars.


### AGB failure rate

The distribution of `f` is shown on Fig. 5 is bimodal, the parameter is very uncertain.

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte/images/failure_rate.png" width="700" alt='AGB failure rate'>

Figure 5: Distribution of AGB failure rate.

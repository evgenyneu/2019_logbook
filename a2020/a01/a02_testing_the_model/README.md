# The Gaussian mixture model

I used the following statistical model (see [Stan code](a2020/a01/a02_testing_the_model/code/calculate_sodium_ratio.stan)). The model takes in the list of measured sodium abundances $`Y_{\textrm{Obs}, i}`$ with their uncertainties $`Y_{\textrm{uncert}, i}`$. The model assumes two gaussian distributions and estimates parameter $`p`$ (proportion of low-sodium stars):

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma_1) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma_2) \\
\mu_1 \sim \textrm{Normal}(-1, 0.5) \\
\mu_2 \sim \textrm{Normal}(1, 0.5) \\
\sigma_1 \sim \textrm{LogNormal}(0, 1) \\
\sigma_2 \sim \textrm{LogNormal}(0, 1) \\
p \sim \textrm{Uniform}(0, 1).
```

Other variables are:

* $`Y_{\textrm{True}, i}`$ is the true abundance value the model estimates based on observed values with their uncertainties.

* $`\mu_1`$ and $`\mu_2`$ are the means of the two normal populations that model estimates.



## Testing the Gaussian mixture model

I want to test the gaussian mixture model from yesterday. I will simulate sodium abundances by randomly sampling from two normal populations. Then I will run the simulated observations through the model and see if the model recovers the parameters correctly.

## Simulate abundances

* Based on sodium abundances Chris calculated in his report ([see plot](a2019/a12/a30_use_data_with_uncertainties/data/sodium_density_2.png)), we generate abundances for 10 sodium-poor and 17 sodium-rich stars, with respective normal distributions centered at -1.1 amd -0.6 [Na/H].

* Then I generate 27 uncertainties (one for each 'true' abundance value), using normal distribution, centered at 0.13 with sd=0.06.

* Next, for each of 27 'true' abundance values I generate an observed abundace, by applying its uncertainty.

The 'true' and observed abundances are shown on Figures 1 and 2.

<img src="a2020/a01/a02_testing_the_model/plots/true_observed_333.png" width="700" alt='Simulated true and observed sodium abundances'>

Figure 1: Simulated true and observed sodium abundances.


<img src="a2020/a01/a02_testing_the_model/plots/density_333.png" width="700" alt='Density of simulated sodium abundances'>

Figure 2: Density of simulated sodium abundances.

## Fit model to the data

Next, I run the Stan model by supplying the list of simulated observed values. The model calculates estimates for the parameters (Table 1, Fig. 3).

Table 1: True and estimated parameters of the model

| Parameter | True value | Estimated value |
| :---:      |  :------:  | :---------:|
| p   | 0.37  | 0.39 ± 14  |
| mu1   | -1.1  | -1.07 ± 0.08  |
| mu2   | -0.6  | -0.63 ± 0.04  |
| sigma1   | 0.05  | 0.14 ± 0.08  |
| sigma2   | 0.05  | 0.09 ± 0.05  |


<img src="a2020/a01/a02_testing_the_model/plots/parameters_333.png" width="700" alt='True and estimated model parameters'>

Figure 3: True and estimated parameters of the model.






### Code

Code: [a2020/a01/a02_testing_the_model/code/simulate_sodium.r](a2020/a01/a02_testing_the_model/code/simulate_sodium.r)

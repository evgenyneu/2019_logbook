## Load distances to stars from Gaia's DR2 data

We load the data for the distances from [CDS/I/347/gaia2dis](https://ui.adsabs.harvard.edu/?#abs/2018AJ....156...58B) table using Aladin.

[a2019/a12/a04_plot_distances/data/ngc288_gaia_distances.csv](a2019/a12/a04_plot_distances/data/ngc288_gaia_distances.csv)

Code: [a2019/a12/a04_plot_distances/code/tsv_to_csv.py](a2019/a12/a04_plot_distances/code/tsv_to_csv.py)

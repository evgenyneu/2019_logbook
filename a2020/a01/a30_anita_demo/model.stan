// Model normally distributed population
data {
  int n;          // Number of measurements
  real y[n];      // Observed values
  real uncert[n]; // Ucertainties
}

parameters {
  real mu;        // Location of the population mean
  real y_true[n]; // Estimated "true" values for each observation
}

model {
  // Distribution: "true" values are normally distributed
  y_true ~ normal(mu, 1);

  // Distribution that takes into account measurement uncertainties
  y ~ normal(y_true, uncert);
}

import os
import pandas as pd
from uncertainties import ufloat

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def agb_data(data_dir, df_result):
    data_path = os.path.join(data_dir, "agb.txt")
    df = pd.read_table(data_path)

    for index, star in df.iterrows():
        star_id = star["ID (2MASS)"]
        star_type = 'AGB'
        na = star["$[\mathrm{Na}/\mathrm{Fe}]$"]
        e_na = star["rms/#.1"]
        e_na = float(e_na.split('/')[0])

        if float(e_na) == 0:
            print(f"Zero Na uncertainty for star ID={star_id}")
            e_na = 0.01

        df_result.loc[star_id] = [star_type, na, e_na]


def rgb_data(data_dir, df_result):
    data_path = os.path.join(data_dir, "rgb.txt")

    header = ["Star", "[Fe/HI]", "[O/Fe]", "[Na/Fe]", "[Mg/Fe]",
              "[Al/Fe]", "[Si/Fe]", "[Ca/Fe]", "[Ti/FeI]", "[Ti/FeII]",
              "[Cr/Fe]", "[Ni/Fe]", "[Ba/FeII]"]

    df = pd.read_table(data_path, names=header, delim_whitespace=True)

    for index, star in df.iterrows():
        star_id = star["Star"]
        star_type = 'RGB'
        na = star["[Na/Fe]"]
        e_na = 0.04
        df_result.loc[star_id] = [star_type, na, e_na]


def convert_data_to_csv(data_dir, csv_name):
    df_result = pd.DataFrame(columns=["Type", "[Na/Fe]", "e_[Na/Fe]"])
    agb_data(data_dir=data_dir, df_result=df_result)
    rgb_data(data_dir=data_dir, df_result=df_result)

    csv_path = os.path.join(data_dir, csv_name)
    df_result.to_csv(csv_path, index_label="ID")


def calculate_f_marino():
    f = 1 - ufloat(0.47, 0.13) / ufloat(0.58, 0.05)
    print(f"Marino f={f}")


def do_work():
    """Main function that does all the work"""

    calculate_f_marino()

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4",
        study_name="NGC 6121 (M4) Marino et al. 2008/2017",
        xlabel="Sodium abundance [Na/Fe]"
    )

    data_dir = os.path.join(settings.path, "data")
    csv_name = "2017_07_marino_ngc_6121_M4.csv"
    convert_data_to_csv(data_dir=data_dir, csv_name=csv_name)
    data_path = os.path.join(data_dir, csv_name)
    type_name = "Type"
    abundance_name = "[Na/Fe]"
    uncertainty_name = "e_[Na/Fe]"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

# Add id_yazan field

import pandas as pd
import os


def add_id_yazan():
    data_dir = 'a2019/a12/a01_stetson/data'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_stetson_update.csv')
    df = pd.read_csv(stars_path)

    # Insert new column
    df.insert(loc=df.columns.get_loc('ID_2MASS') + 1,
              column='id_yazan', value='')

    # Go over rows
    for index, star in df.iterrows():
        yazan_id = star['ID_Phot']

        if star['Cat'] != 'YAZ':
            continue

        df.loc[index, 'id_yazan'] = yazan_id

    # Remove ID_Phot column
    df = df.drop('ID_Phot', 1)

    # Save data frame
    filename = 'ngc288_stars_with_stetson_update__yazan_id.csv'
    output_path = os.path.join(data_dir, filename)
    df.to_csv(output_path, index=False)
    print(f'Successufully saved to {output_path}')


if __name__ == '__main__':
    add_id_yazan()
    print('We are done!')

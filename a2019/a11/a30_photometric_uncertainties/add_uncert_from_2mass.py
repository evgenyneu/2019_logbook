# Add uncertainties for J, H, K numbers from 2MASS catalog

import pandas as pd
import os


def combine():
    """
    Add uncertainties for J, H, K numbers from 2MASS catalog
    """

    data_dir = 'a2019/a11/a30_photometric_uncertainties/data/'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_uncert.csv')
    stars_df = pd.read_csv(stars_path)

    # Open 2mass data
    two_mass_path = os.path.join(data_dir, 'ngc288_2mass.csv')
    two_mass_df = pd.read_csv(two_mass_path)

    # Add new columns
    columns = ['J', 'H', 'K']

    for column in columns:
        stars_df.insert(loc=stars_df.columns.get_loc(column) + 1,
                        column=f'u_{column}', value='')

    # Go over rows
    for index, star in stars_df.iterrows():
        star_id = star['id']
        two_mass_id = star['ID_2MASS']

        star_2mass = two_mass_df.loc[(two_mass_df['2MASS'] == two_mass_id)]

        if star_2mass.shape[0] != 1:
            raise ValueError(f"Can't find star {star_id}")

        for column in columns:
            uncert_column_dest = f'u_{column}'
            uncert_column_src = f'e_{column}mag'
            uncertainty = star_2mass[uncert_column_src].values[0]
            stars_df.loc[index, uncert_column_dest] = uncertainty

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_uncert_2mass.csv')
    stars_df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    combine()
    print('We are done!')

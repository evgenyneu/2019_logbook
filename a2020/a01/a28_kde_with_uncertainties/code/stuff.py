import numpy as np

x = np.array([1, 2, 3])
b = x[:, np.newaxis]
c = x[np.newaxis, :]
print(b)
print(c)
print(b + c)

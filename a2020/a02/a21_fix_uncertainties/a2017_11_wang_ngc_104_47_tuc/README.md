# Analysing data from [Wang et al. 2017](https://ui.adsabs.harvard.edu/abs/2017A%26A...607A.135W/abstract), NGC 104 (47 Tuc)

We use [Na/H] LTE  abundances. For uncertainty of [Na/H] we use 0.13 from Table 9, Wang et al. 2017.

Code: [a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_104_47_tuc/code/model.py](a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_104_47_tuc/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_104_47_tuc/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.

## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_104_47_tuc/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.32^{+0.11}_{-0.09}`$  |   $`0.40 \pm 0.08`$   |    Yes     |    Yes     |
| $`r_{rgb}`$   |  $`0.56^{+0.14}_{-0.16}`$  |   $`0.67 \pm 0.10`$   |    Yes     |    Yes     |
| $`f`$         |  $`0.46^{+0.19}_{-0.30}`$  |   $`0.40 \pm 0.15`$   |    Yes     |    Yes     |

import pandas as pd
import os


def convert():
    data_dir = 'a2019/a12/a03_cmd_gaia/data'

    # Open photometry data
    data_dir = 'a2019/a12/a04_plot_distances/data'
    raw_path = os.path.join(data_dir, 'XMatch_gaia_distances.tsv')
    df = pd.read_csv(raw_path, sep='\t', skiprows=[1])

    output_path = os.path.join(data_dir, 'ngc288_gaia_distances.csv')
    df.to_csv(output_path, index=False)
    print(f'Saved to {output_path}')


if __name__ == '__main__':
    convert()

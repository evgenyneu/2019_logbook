# Analysing sodium abundances from literature

We analysed sodium abundances from literature using statistical model that has a mixture of two Gaussians. This model is superseded with [standardised model](a2020/a02/a15_sodium_in_literature).

| Paper   |  Cluster  | $`r_{agb}`$ |  $`r_{rgb}`$  |   f | Comments |
|:--------:|:-------:|:-------:|-------:|------:|:-------:|
| [Campbell et al. 2013](a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752) | NGC 6752 |  $`0.05^{+0.70}_{-0.05}`$ | $`0.7^{+0.1}_{-0.1}`$ |    $`0.9^{+0.1}_{-0.9}`$ | No evidence of two AGB populations. Agrees with the authors on r and f. |
| [Campbell et al. 2017 LTE](a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_lte) | NGC 6752 |  $`0.6^{+0.2}_{-0.3}`$ | $`0.7^{+0.14}_{-0.16}`$ |   $`0.13^{+0.4}_{-0.4}`$ | Very weak evidence of two AGB populations, f is uncertain. |
| [Campbell et al. 2017 NLTE](a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte) | NGC 6752 | $`0.05^{+0.60}_{-0.05}`$  | $`0.68^{+0.16}_{-0.18}`$ |  $`0.90^{+0.1}_{-0.8}`$  | Very weak evidence of two AGB populations, f is unconstrained. |
| [MacLean et al. 2018](a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397) | NGC 6397 | $`0.7^{+0.2}_{-0.3}`$  | $`0.7^{+0.16}_{-0.16}`$ | $`0.15^{+0.4}_{-0.4}`$  | Agrees with authors on r and f. Parameter f is uncertain.  |
| [MacLean et al. 2018](a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4) | NGC 6121 (M4) | $`0.65^{+0.12}_{-0.14}`$  | $`0.58^{+0.06}_{-0.05}`$ | $`-0.1^{+0.3}_{-0.2}`$  | $`r_{agb}`$ and $`f`$ disagree with authors, $`r_{rgb}`$ agree. |
| [Work in progress 2020](paper/model) | NGC 288 |  $`0.33^{+0.17}_{-0.16}`$ | $`0.65^{+0.10}_{-0.09}`$ |   $`0.5^{+0.3}_{-0.3}`$ | [Na/H] measurements are not final. |

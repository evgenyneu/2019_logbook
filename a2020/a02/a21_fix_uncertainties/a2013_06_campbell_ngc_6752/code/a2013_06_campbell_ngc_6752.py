import os

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def do_work():
    """Main function that does all the work"""

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2013_06_campbell_ngc_6752",
        study_name="NGC 6752 Campbell et al. 2013",
        xlabel="Sodium abundance [Na/Fe]"
    )

    data_path = os.path.join(settings.path, "data/2013_06_campbell_ngc_6752.csv")
    type_name = "Type"
    abundance_name = "[Na/Fe]"
    uncertainty_name = "e_[Na/Fe]"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

from uncertainties import ufloat
from uncertainties.umath import sqrt


def distance():
    x = ufloat(-0.084, 0.002)
    y = ufloat(0.046, 0.001)
    z = ufloat(-8.89, 0.21)

    distance = sqrt(x**2 + y**2 + z**2)

    print(distance)


if __name__ == '__main__':
    distance()
    print('We are done!')

from cmdstanpy import CmdStanModel
import pickle
import os
from scipy.stats import zscore
from tarpan.cmdstanpy.analyse import save_analysis
from tarpan.shared.info_path import InfoPath, get_info_path


def run_stan(data, sub_dir_name, stan_model_path, seed=None):
    """
    Run Stan to calculate parameters of the model.

    Parameters
    -----------

    data : dictionary
        Data for the stan sampler.


    Returns
    -------

    Panda's dataframe containing samples of parameters.
    """

    info_path = InfoPath(base_name="model", extension="pkl",
                         sub_dir_name=sub_dir_name)

    info_path.set_codefile()
    path_to_samples = get_info_path(info_path)

    if os.path.exists(path_to_samples):
        with open(path_to_samples, 'rb') as input:
            samples = pickle.load(input)
    else:
        model = CmdStanModel(stan_file=stan_model_path)

        fit = model.sample(data=data, seed=seed,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=15000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        info_path = InfoPath(sub_dir_name=sub_dir_name)
        save_analysis(fit, info_path=info_path)
        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        os.makedirs(os.path.dirname(path_to_samples), exist_ok=True)
        with open(path_to_samples, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)

    return samples


def standardize(values, uncertainties):
    """
    Standardize values and their uncertainties.

    Returns
    -------

    Tuple of standardized values and their uncertainties.
    """

    values_std = zscore(values, ddof=1)
    uncertainties_std = (uncertainties / values.std()).tolist()

    return (values_std, uncertainties_std)


def destandardize(samples, values_observed):
    """

    De-standardize samples of parameters.

    Parameters
    -----------

    samples : Panda's DataFrame
        Samples of parameters (normalized.)

    values_observed : list
        Observed values


    Returns : Panda's DataFrame
    --------

    Destandardized sample values.

    """
    std = values_observed.std()
    mean = values_observed.mean()
    result = samples.copy()
    result['mu.1'] = samples['mu.1'] * std + mean
    result['mu.2'] = samples['mu.2'] * std + mean

    if 'sigma' in samples:
        result['sigma'] = samples['sigma'] * std
    else:
        result['sigma.1'] = samples['sigma.1'] * std
        result['sigma.2'] = samples['sigma.2'] * std

    return result

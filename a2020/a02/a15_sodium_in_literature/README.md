# Analysing sodium abundances from literature

> This model has a bug, it did not account correctly for measurement uncertainties. Instead it used uncertainties that were multiple times smaller than the actual ones. This resulted in overconfident distributions of model parameters.

We analysed sodium abundances from literature using statistical model describing a mixture of two Gaussians (Table 1).

Table 1: Model parameters calculated with sodium abundance data from literature.

| Data source   |  Cluster  | $`r_{agb}`$ |  $`r_{rgb}`$  |   f | 🍎🍊Agreement with the authors |
|:--------:|:-------:|:-------:|-------:|------:|:-------:|
| [Campbell et al. 2013](a2020/a02/a15_sodium_in_literature/a2013_06_campbell_ngc_6752) | NGC 6752 | $`0.70^{+0.12}_{-0.14}`$  | $`0.71^{+0.10}_{-0.09}`$   |  $`0.06^{+0.22}_{-0.26}`$  | $`r_{agb}`$ and f disagree, $`r_{rgb}`$ agrees. |
| [Johnson et al. 2015](a2020/a02/a15_sodium_in_literature/a2015_01_johnson_ngc_104_47_tuc) | NGC 104 (47 Tuc) | $`0.59^{+0.10}_{-0.12}`$  | $`0.39^{+0.07}_{-0.06}`$   |  $`-0.40^{+0.27}_{-0.36}`$  | $`r_{agb}`$  disagree, $`r_{rgb}`$ and f agree. |
| [Lapenna et al. 2016](a2020/a02/a15_sodium_in_literature/a2016_07_lapenna_ngc_6752) | NGC 6752 | $`0.66^{+0.08}_{-0.13}`$  | $`0.71^{+0.10}_{-0.09}`$   |  $`0.11^{+0.20}_{-0.19}`$  | Agree. |
| [Marino et al. 2017](a2020/a02/a15_sodium_in_literature/a2017_07_marino_ngc_6121_M4) | NGC 6121 (M4) | $`0.54^{+0.12}_{-0.11}`$  | $`0.58^{+0.05}_{-0.05}`$   |  $`0.05^{+0.23}_{-0.20}`$  | Exceptional agreement. |
| [Campbell et al. 2017 LTE](a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_lte) | NGC 6752 |   $`0.62^{+0.10}_{-0.11}`$  |  $`0.67^{+0.11}_{-0.10}`$  | $`0.11^{+0.18}_{-0.25}`$ | Parameters are not reported. |
| [Campbell et al. 2017 NLTE](a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte) | NGC 6752 | $`0.60^{+0.13}_{-0.09}`$  | $`0.68^{+0.10}_{-0.11}`$  |  $`0.12^{+0.20}_{-0.24}`$  | Parameters are not reported. |
| [Wang et al. 2017](a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_6121_M4) | NGC 6121 (M4) | $`0.64^{+0.13}_{-0.15}`$  | $`0.72^{+0.06}_{-0.07}`$  |  $`0.18^{+0.18}_{-0.24}`$  |  Agree. |
| [Wang et al. 2017](a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_104_47_tuc) | NGC 104 (47 Tuc) | $`0.35^{+0.09}_{-0.07}`$  | $`0.55^{+0.11}_{-0.08}`$  |  $`0.43^{+0.13}_{-0.22}`$  |  Agree. |
| [MacLean et al. 2018](a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397) | NGC 6397 | $`0.64^{+0.13}_{-0.17}`$  | $`0.67^{+0.07}_{-0.09}`$ | $`0.07^{+0.27}_{-0.25}`$  |  Agree. |
| [MacLean et al. 2018](a2020/a02/a15_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4) | NGC 6121 (M4) |    $`0.65^{+0.10}_{-0.15}`$  | $`0.58^{+0.06}_{-0.06}`$  | $`-0.09^{+0.26}_{-0.22}`$ | $`r_{agb}`$ and f disagree, $`r_{rgb}`$ agrees. |
| [Work in progress 2020](paper/model_std) | NGC 288 |  $`0.39^{+0.12}_{-0.13}`$ | $`0.66^{+0.08}_{-0.11}`$ |   $`0.41^{+0.22}_{-0.21}`$ | [Na/H] measurements are not final. |

## 🍎🍊Apples to oranges

In the last column of the Table 1 we compare our model parameter with the literature. However, this comparison is "apple to oranges" because authors differ on how they locate the two populations and calculate parameters `r` and `f` .  For example, [Wang et al. 2017](https://ui.adsabs.harvard.edu/abs/2017A%26A...607A.135W/abstract) uses `[Na/Fe]cri = [Na/Fe]min + 0.3 dex` separation point between two population. In contrast, [MacLean et al. 2016](https://arxiv.org/abs/1604.05040v1) chooses the separation point to be at the minimum in the [Na/O] distribution in RGB sample from [Marino et al. 2008](https://ui.adsabs.harvard.edu/abs/2008A%26A...490..625M/abstract).

In comparison, the value `r` that we calculate is the proportion of stars belonging to the second population. Instead of choosing a separation points, the model finds distributions of two populations that are compatible with observations and returns the distribution of parameter `r`, which is the mixing ratio of the two populations.

### Important features of our model

The following are some important features of our model to consider when interpreting our model parameters and comparing them with the literature.

* Our `r` parameters are not calculated directly from the observed values. Instead the model uses probability theory to find distributions of `r` that are compatible with the observed values.

* Our model does not constrain the locations of two populations.

* The model will always find two populations, even if the data are also compatible with with a single or three populations. Therefore, it would be useful to make models with different number of populations and compare them using methods such as [WAIC](https://ui.adsabs.harvard.edu/abs/2012arXiv1208.6338W/abstract) (widely applicable information criterion) or [PSIS](https://ui.adsabs.harvard.edu/abs/2015arXiv150704544V/abstract) (Pareto-smoothed importance sampling cross-validation). These names of the method sound intimidating (like many other terms in statistics), but they are easy to implement.


## Parameter f

We define parameter f to be the fraction of RGB stars from the second population that would fail to reach AGB branch:

```
f = 1 - r_agb / r_rgb
```

## Model tests

* [One population](a2020/a02/a15_sodium_in_literature/test_single_population).

* [Two populations](a2020/a02/a15_sodium_in_literature/test_two_populations).

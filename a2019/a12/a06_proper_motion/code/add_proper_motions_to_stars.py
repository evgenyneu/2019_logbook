import pandas as pd
import os


def combine():
    data_dir = 'a2019/a12/a06_proper_motion/data/'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open gaia
    gaia_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(gaia_path)

    # Add new columns
    columns = ['pm_ra', 'u_pm_ra', 'pm_de', 'u_pm_de']
    columns_gaia = ['pmRA', 'e_pmRA', 'pmDE', 'e_pmDE']

    for column in columns:
        df.insert(loc=len(df.columns), column=column, value='')

    # Go over rows
    for index, star in df.iterrows():
        star_id = star['id']
        gaia_id = star['id_gaia_dr2']

        star_gaia = gaia_df.loc[(gaia_df['Source'] == gaia_id)]

        if star_gaia.shape[0] != 1:
            raise ValueError(f"Can't find star {star_id}")

        for i_column, column in enumerate(columns):
            gaia_column = columns_gaia[i_column]
            column_values = star_gaia[gaia_column].values

            if column_values.shape[0] != 1:
                raise ValueError(f"Too many values {star_id}")

            df.loc[index, column] = column_values[0]

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    combine()
    print('We are done!')

import os
import pandas as pd

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def convert_agb(data_dir, df_result):
    data_path = os.path.join(data_dir, "agb_stars_lapenna_2016.txt")
    df = pd.read_csv(data_path, sep='\t')

    for index, star in df.iterrows():
        star_id = star["ID"]
        star_type = "AGB"
        na_and_uncert = star["[Na/Fe]"]
        na_and_uncert = na_and_uncert.split(" +or- ")
        na = na_and_uncert[0]
        e_na = na_and_uncert[1]
        df_result.loc[star_id] = [star_type, na, e_na]


def convert_rgb(data_dir, df_result):
    data_path = os.path.join(data_dir, "2013_06_campbell_ngc_6752.csv")
    df = pd.read_csv(data_path)

    df = df[df["Type"] == "RGB"]

    for index, star in df.iterrows():
        star_id = star["ID"]
        star_type = "RGB"
        na = star["[Na/Fe]"]
        e_na = star["e_[Na/Fe]"]
        df_result.loc[star_id] = [star_type, na, e_na]


def convert_data_to_csv(data_dir, csv_name):
    df_result = pd.DataFrame(columns=["Type", "[Na/Fe]", "e_[Na/Fe]"])
    convert_agb(data_dir, df_result)
    convert_rgb(data_dir, df_result)
    csv_path = os.path.join(data_dir, csv_name)
    df_result.to_csv(csv_path, index_label="ID")


def do_work():
    """Main function that does all the work"""

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752",
        study_name="NGC 6752 Lapenna et al. 2016",
        xlabel="Sodium abundance [Na/Fe]"
    )

    data_dir = os.path.join(settings.path, "data")
    csv_name = "2016_07_lapenna_ngc_6752.csv"
    convert_data_to_csv(data_dir=data_dir, csv_name=csv_name)
    data_path = os.path.join(data_dir, csv_name)
    type_name = "Type"
    abundance_name = "[Na/Fe]"
    uncertainty_name = "e_[Na/Fe]"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

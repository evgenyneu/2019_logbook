import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a05_plot_distances/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia_distances.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    # Excluded
    # ---------------

    df_filtered = df.loc[df['selected'] == 0]
    x_values = df_filtered['v']
    y_values = df_filtered['rest']

    plt.scatter(x_values, y_values,
                c='black', marker='o', s=5, edgecolors='black',
                label='Excluded')

    # Selected AGB
    # ---------------

    df_filtered = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    x_values = df_filtered['v']
    y_values = df_filtered['rest']

    plt.scatter(x_values, y_values,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    df_filtered = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    x_values = df_filtered['v']
    y_values = df_filtered['rest']

    plt.scatter(x_values, y_values,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')


    # Error bars
    # --------------

    x_values = df['v']
    u_x_values = df['u_v']
    y_values = df['rest']

    # Calculate lower and upper uncertainties for the distance
    df['u_low_rest'] = df['rest'] - df['b_rest']
    df['u_high_rest'] = df['B_rest'] - df['rest']

    u_y_values = [list(df['u_low_rest']), list(df['u_high_rest'])]

    plt.errorbar(x=x_values, y=y_values,
                 xerr=u_x_values, yerr=u_y_values,
                 fmt='none', color='black', capsize=0,
                 ecolor='gray', elinewidth=1, zorder=-1)

    # Plot straight line
    # -----------

    ngc288_distance = 8900  # in Pc, taken from Chris' report. Source unknown

    plt.plot([df['v'].min(), df['v'].max()], [ngc288_distance, ngc288_distance],
             zorder=-1, linewidth=1, linestyle=':')

    plt.legend()
    plt.xlabel(r'V magnitude')
    plt.ylabel(r'Distance $d$ [pc]')
    plt.title('Distances to stars observed in NGC288')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

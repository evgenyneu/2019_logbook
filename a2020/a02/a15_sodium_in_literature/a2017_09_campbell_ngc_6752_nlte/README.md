# Analysing data from [Campbell et al. 2017](https://arxiv.org/abs/1707.02840v1) NLTE

Code: [a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/code/model.py](a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/code/model.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                     |   Author |  Agree 95% | Agree 68%  |
|:-------------:|-------------------------------:|---------:|:----------:|:----------:|
| $`r_{agb}`$   |    $`0.60^{+0.13}_{-0.09}`$    |    NA    |            |            |
| $`r_{rgb}`$   |    $`0.68^{+0.10}_{-0.11}`$    |    NA    |            |            |
| $`f`$         |    $`0.12^{+0.20}_{-0.24}`$    |    NA    |            |            |

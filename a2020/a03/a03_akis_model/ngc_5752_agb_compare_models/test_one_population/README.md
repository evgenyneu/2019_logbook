# Compare single vs two-gaussian models using data from synthetic data with one population

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations. Vertical dashed line indicate location of the synthesised population.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| One population  |  33.48 | 4.37 |         |       |    0.93 |   0.33 |     0.56 |
| Two populations |  33.93 | 3.75 |    0.45 |  0.76 |    0.89 |   0.37 |     0.44 |

|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| One population  |  33.45 | 4.37 |         |       |    0.92 |     0.56 |
| Two populations |  33.91 | 3.76 |    0.46 |  0.76 |    0.88 |     0.44 |


## Conclusion

Both models are equally compatible with the data.

import os
import pandas as pd

from a2020.a02.a15_sodium_in_literature.code.run_model import (
    do_analysis, load_data)


def convert_data_to_csv():
    data_dir = "a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/data"
    data_path = os.path.join(data_dir, "a2018_03_MacLean_NGC_6397_table_5.dat")
    df = pd.read_table(data_path)

    df_new = pd.DataFrame(columns=["Type", "[Na/H]", "e_[Na/H]"])

    for index, star in df.iterrows():
        star_id = star["Star"]
        star_type = star["Type"]
        na_split = star["[Na/H]"].split(' +/- ')
        na_over_h = na_split[0]
        e_na_over_h = na_split[1]

        if float(e_na_over_h) == 0:
            e_na_over_h = 0.01

        df_new.loc[star_id] = [star_type, na_over_h, e_na_over_h]

    csv_path = os.path.join(data_dir, "a2018_03_MacLean_NGC_6397_table_5.csv")
    df_new.to_csv(csv_path, index_label="Star")


def do_work():
    """Main function that does all the work"""

    convert_data_to_csv()
    data_path = "a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/data/a2018_03_MacLean_NGC_6397_table_5.csv"
    type_name = "Type"
    abundance_name = "[Na/H]"
    uncertainty_name = "e_[Na/H]"

    values_agb, uncert_agb = load_data('AGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    values_rgb, uncert_rgb = load_data('RGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    do_analysis(values_agb=values_agb, uncert_agb=uncert_agb,
                values_rgb=values_rgb, uncert_rgb=uncert_rgb,
                path="a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397",
                dir_name="model_info",
                study_name="NGC 6397 MacLean et al. 2018",
                xlabel="Sodium abundance [Na/H]")


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

# Programming a statistical model in Python and Stan

This is code and links for my demo in [ANITA 2020 workshop](https://asa-anita.github.io/workshop2020/).


## Setup

First, install Python 3.7 or newer, and then run:

```
pip install tarpan
```

If you have problems, try `pip install -Iv tarpan==0.3.7` instead.

Finally, install Stan:

```
install_cmdstan
```

## Usage

Save [stats.py](a2020/a01/a30_anita_demo/stats.py) and [model.stan](a2020/a01/a30_anita_demo/model.stan) to a directory and run

```
python stats.py
```

## Links

* [Video of this demo](https://youtu.be/WlFr_JAFKXA).

* [Statistical Rethinking textbook and lectures](https://xcelab.net/rm/statistical-rethinking/). That's the book and lectures I learned all this stats from.

* [Model with two Gaussian populations](https://gitlab.com/evgenyneu/2019_logbook/-/tree/master/paper/model_std). The actual model we use in our work.

* [Clipy app](https://clipy-app.com): the app I used on Mac to paste code for my demo.

* [ScreenBrush app](https://apps.apple.com/us/app/screenbrush/id1233965871?mt=12): Mac app I used to draw on screen.

* Short link to this page: http://bit.ly/anita2020_stats

# Analysing data from [Campbell et al. 2017](https://arxiv.org/abs/1707.02840v1) NLTE

Code: [a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_nlte/code/model.py](a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_nlte/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_nlte/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_nlte/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                     |   Author |  Agree 95% | Agree 68%  |
|:-------------:|-------------------------------:|---------:|:----------:|:----------:|
| $`r_{agb}`$   |    $`0.61^{+0.12}_{-0.16}`$    |    NA    |            |            |
| $`r_{rgb}`$   |    $`0.67^{+0.11}_{-0.13}`$    |    NA    |            |            |
| $`f`$         |    $`0.14^{+0.25}_{-0.33}`$    |    NA    |            |            |

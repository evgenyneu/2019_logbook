// Guassian mixture model
data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed values
  real uncertainties[N]; // uncertainties
}
parameters {
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  real<lower=0> sigma;       // spread of mixture components
}
model {
  sigma ~ normal(0, 0.1);
  mu[1] ~ normal(-1.4, 0.2);
  mu[2] ~ normal(-1.0, 0.2);
  p ~ beta(2, 2);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y[n] | mu[1], sqrt(sigma^2 + uncertainties[n]^2)),
                      normal_lpdf(y[n] | mu[2], sqrt(sigma^2 + uncertainties[n]^2)));
  }
}

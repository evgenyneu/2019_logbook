import pandas as pd
from cmdstanpy import CmdStanModel
from scipy.stats import zscore
import pickle
import os
import numpy as np
from tabulate import tabulate
import arviz as az
import seaborn as sns
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot
import math


def load_data(type):
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]

    # Standardize parameters
    zscores = zscore(df['na_over_h'].tolist(), ddof=1)

    model_data = {
        'y': zscores,
        'N': len(df)
    }

    return (model_data, df)


def load_data_with_uncertainties(type):
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]

    # Standardize parameters
    zscores = zscore(df['na_over_h'].tolist(), ddof=1)
    uncertainties = (df['uncertainty'] / df['na_over_h'].std()).tolist()

    model_data = {
        'y': zscores,
        'uncertainties': uncertainties,
        'N': len(df)
    }

    return (model_data, df)


def calculate_no_uncertainties(type):
    """
    Parameters
    -----------

    type : str
        Stat type: 'AGB' or 'RGB'
    """

    model_data, df = load_data(type)
    data_dir = 'a2020/a01/a08_changing_measurement_uncertainty/data'
    path_to_model = os.path.join(data_dir, f'samples_{type.lower()}_no_uncertainties.pkl')

    if os.path.exists(path_to_model):
        with open(path_to_model, 'rb') as input:
            samples = pickle.load(input)
    else:
        # Load model
        path = "a2020/a01/a08_changing_measurement_uncertainty/code/stan_model/sodium_ratio_model_no_uncertainties.stan"
        model = CmdStanModel(stan_file=path)

        fit = model.sample(data=model_data, seed=11421,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=12000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        print(fit.summary())
        print(fit.diagnose())

        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        with open(path_to_model, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)

    # De-standardize parameters
    std = df['na_over_h'].std()
    mean = df['na_over_h'].mean()
    samples['mu.1'] = samples['mu.1'] * std + mean
    samples['mu.2'] = samples['mu.2'] * std + mean
    samples['sigma.1'] = samples['sigma.1'] * df['na_over_h'].std()
    samples['sigma.2'] = samples['sigma.2'] * df['na_over_h'].std()
    return samples


def sample_summary(df, show=True):
    rows = []

    for column in df:
        values = df[column].to_numpy()
        mean = round(df[column].mean(), 2)
        std = round(df[column].std(), 2)
        hpdi5 = np.round(az.hpd(values, credible_interval=0.05), 2)
        mode = hpdi5[0] + (hpdi5[1] - hpdi5[0]) / 2
        hpdi68 = np.round(az.hpd(values, credible_interval=0.68), 2).tolist()
        uncert_plus = hpdi68[1] - mode
        uncert_minus = mode - hpdi68[0]
        hpdi95 = np.round(az.hpd(values, credible_interval=0.95), 2).tolist()
        rows.append([column, mean, std, mode, uncert_plus,
                     uncert_minus, hpdi68, hpdi95])

    headers = ['Name', 'Mean', 'Std', 'Mode', '+', '-',
               '68% HPDI', '95% HPDI']

    if show:
        table = tabulate(rows, headers=headers, floatfmt=".2f", tablefmt="pipe")
        print(table)

    return pd.DataFrame(rows, columns=headers)


def calculate_full_uncertainties(type):
    """
    Parameters
    -----------

    type : str
        Stat type: 'AGB' or 'RGB'
    """

    model_data, df = load_data_with_uncertainties(type)
    data_dir = 'a2020/a01/a08_changing_measurement_uncertainty/data'
    path_to_model = os.path.join(data_dir, f'samples_{type.lower()}_full_uncertainties.pkl')

    if os.path.exists(path_to_model):
        with open(path_to_model, 'rb') as input:
            samples = pickle.load(input)
    else:
        # Load model
        path = "a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan"
        model = CmdStanModel(stan_file=path)

        fit = model.sample(data=model_data, seed=11421,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=12000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        print(fit.summary())
        print(fit.diagnose())

        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        with open(path_to_model, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)


    # De-standardize parameters
    std = df['na_over_h'].std()
    mean = df['na_over_h'].mean()
    samples['mu.1'] = samples['mu.1'] * std + mean
    samples['mu.2'] = samples['mu.2'] * std + mean
    samples['sigma.1'] = samples['sigma.1'] * df['na_over_h'].std()
    samples['sigma.2'] = samples['sigma.2'] * df['na_over_h'].std()
    return samples


def tree_plot(groups, group_height=None, error_bar_cap_size=0.08,
              markersize=50,
              ylim=None, markers=None,
              marker_line_width=1,
              marker_colors=None, marker_edge_colors=None,
              error_bar_colors=None, labels=None):
    """
    Make a tree plot of parameters.

    Parameters
    -----------

    group_height : float

        The height of each group, a unmber between 0 and 1.
    """

    if markers is None:
        markers = ['x', 'o', 'v', 'D', 'X', '1']

    if marker_colors is None:
        marker_colors = ['#FF9922', '#009933', '#8888FF',
                         '#BBBB11', '#999900', '#33BB11']

    if marker_edge_colors is None:
        marker_edge_colors = ['#994400', '#003300', '#111177',
                              '#888800', '#333300', '#008800']

    if error_bar_colors is None:
        error_bar_colors = ["#0033BB33", '#7755FF55', '#22FF9955']

    for i_group, group in enumerate(groups):
        elements_in_group = len(group['values'])

        this_group_height = group_height

        if this_group_height is None:
            # Determine group height automatically
            x = len(group['values']) - 1
            this_group_height = 1 - math.exp(-x / 5)

        # Vertical separation between values in one group
        if len(group['values']) == 1:
            y_increment = 0
            this_group_height = 0
        else:
            y_increment = 1 / (elements_in_group - 1) * this_group_height

        # Plot the values for the group
        for i_value, value in enumerate(group['values']):
            y_coord = i_group + this_group_height/2 - y_increment * i_value

            value_label = '_nolegend_'

            if labels is not None and i_group == 0:
                value_label = labels[i_value]

            plt.scatter(value["position"], y_coord,
                        marker=markers[i_value], s=markersize,
                        facecolor=marker_colors[i_value],
                        edgecolor=marker_edge_colors[i_value],
                        linewidth=marker_line_width, zorder=5,
                        label=value_label)

            # Plot error bars
            n_error_bars = len(value["error_bars"])

            for i_error_bar, error_bar in enumerate(value["error_bars"]):
                color = error_bar_colors[i_error_bar]

                if i_error_bar == n_error_bars - 1:
                    color = marker_colors[i_value]

                plt.plot(error_bar, [y_coord, y_coord], zorder=i_error_bar + 1,
                         color=color)

                # Plot caps
                plt.plot([error_bar[0], error_bar[0]],
                         [y_coord - error_bar_cap_size/2,
                          y_coord + error_bar_cap_size/2],
                         zorder=i_error_bar + 1,
                         color=color)

                plt.plot([error_bar[1], error_bar[1]],
                         [y_coord - error_bar_cap_size/2,
                          y_coord + error_bar_cap_size/2],
                         zorder=i_error_bar + 1,
                         color=color)

    group_names = [group['name'] for group in groups]

    # Plot vertical line around zero if neeeded
    if plt.ylim()[0] < 0 and plt.ylim()[1] > 1:
        plt.axvline(x=0, linestyle='dashed')

    plt.yticks(range(0, len(groups)), group_names)
    plt.grid(axis='x', zorder=-10)

    if value_label is not None:
        plt.legend()

    plt.tight_layout()

    if ylim is not None:
        plt.ylim(ylim)


def extract_tree_plot_data(df, groups=[]):
    """
    Extract data used to for tree plot function from a dataframe.

    Parameters
    -----------

    df : Panda's data frame
        Each column is one parameter and it contains sample values from Stan.

    groups : list
        Tree plot data. If passed, the data frame's data will be added to it.
    """

    summary = sample_summary(df, show=False)

    for column_name in df:
        column_summary = summary[summary['Name'] == column_name].iloc[0]
        group_data = None

        # Check if `groups` already has the column
        for group in groups:
            if group['name'] == column_name:
                group_data = group
                break

        # Have not found group data - create one
        if group_data is None:
            group_data = {
                'name': column_name,
                'values': []
            }

            groups.append(group_data)

        # Add new value

        value = {}
        value['position'] = column_summary["Mode"]
        group_data['values'].append(value)

        value['error_bars'] = [
            column_summary["95% HPDI"],
            column_summary["68% HPDI"]
        ]

    return groups


def do_work():
    sns.set_style("ticks")

    # RGB
    # ---------

    df_no = calculate_no_uncertainties('RGB')
    df_full = calculate_full_uncertainties('RGB')

    # AGB
    # ---------

    df_no_agb = calculate_no_uncertainties('AGB')
    df_full_agb = calculate_full_uncertainties('AGB')

    tree_plot_data = extract_tree_plot_data(df_no)
    tree_plot_data = extract_tree_plot_data(df_full, groups=tree_plot_data)
    tree_plot_data = extract_tree_plot_data(df_no_agb, groups=tree_plot_data)
    tree_plot_data = extract_tree_plot_data(df_full_agb, groups=tree_plot_data)

    tree_plot(tree_plot_data,
              labels=[
                'RGB no uncertainties',
                'RGB with uncertainties',
                'AGB no uncertainties',
                'AGB with uncertainties'
              ])

    save_plot(plt=plt)

if __name__ == '__main__':
    do_work()
    print('We are done')

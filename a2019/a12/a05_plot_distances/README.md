## Plotting distances to stars

Code: [a2019/a12/a05_plot_distances/code/distances.py](a2019/a12/a05_plot_distances/code/distances.py)

<img src="a2019/a12/a05_plot_distances/code/plots/distances.png" width="700" alt='Distances to NGC288 stars'>

Figure 1: Distances to observed NGC288 stars ([Bailer-Jones at al. 2018](https://arxiv.org/abs/1804.10121)). The horizontal line is the distance to NGC288 we used in analysis.


### Stars selected for analysis

Code: [a2019/a12/a05_plot_distances/code/distances_selected.py](a2019/a12/a05_plot_distances/code/distances_selected.py)

<img src="a2019/a12/a05_plot_distances/code/plots/distances_selected.png" width="700" alt='Distances to NGC288 stars selected for analysis'>

Figure 2: Distances to observed NGC288 stars ([Bailer-Jones at al. 2018](https://arxiv.org/abs/1804.10121)) selected for analysis. The horizontal line is the distance to NGC288 we used in analysis.


### Distances to selected stars

**Distance**: 4900±600 pc.

This is mean distance to selected stars, standard deviation was used for uncertainty.

Code: [a2019/a12/a05_plot_distances/code/distances_boxplot.py](a2019/a12/a05_plot_distances/code/distances_boxplot.py)

<img src="a2019/a12/a05_plot_distances/code/plots/distances_boxplot.png" width="700" alt='Distribution of distances to NGC288 stars selected for analysis'>

Figure 3: Distances to observed NGC288 stars ([Bailer-Jones at al. 2018](https://arxiv.org/abs/1804.10121)) selected for analysis.


### Notes

We can see from Fig. 2 that distances to selected stars disagree with the d=8900 pc distance to NGC288 we used in analysis.

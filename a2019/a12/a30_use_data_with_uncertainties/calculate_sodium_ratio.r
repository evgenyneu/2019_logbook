# Use Gaussian mixture model to find out
# proportion of low sodium AGB/RGB stars in NGC 288
# --------------

library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

data_path = "a2019/a12/a30_use_data_with_uncertainties/data/sodium.csv"
data = read.csv(data_path, header = TRUE)
data = as.data.frame(data)
data = data[data$type == 'RGB',]

data = list(
    y = data$na_over_h,
    uncertainties = data$uncertainty,
    N=length(data$na_over_h)
)

# Run Stan
model_path = 'a2019/a12/a30_use_data_with_uncertainties/calculate_sodium_ratio.stan'
fit <- stan(file=model_path, data=data, iter=4000,
            control=list(adapt_delta=0.99, max_treedepth=15))

print(fit)

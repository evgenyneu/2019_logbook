# Analysing data from [MacLean et al. 2018](https://arxiv.org/abs/1712.03340v1), NGC 6397

Code: [a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/code/model.py](a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/code/model.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2018_03_MacLean_NGC_6397/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.64^{+0.13}_{-0.17}`$  | $`r_{agb} \sim 0.6`$  |    Yes     |    Yes     |
| $`r_{rgb}`$   |  $`0.67^{+0.07}_{-0.09}`$  | $`r_{rgb} \sim 0.6`$  |    Yes     |    Yes     |
| $`f`$         |  $`0.07^{+0.27}_{-0.25}`$  | $`f \sim 0`$          |    Yes     |    Yes     |

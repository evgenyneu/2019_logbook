data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed value
  real uncertainties[N]; // uncertainties
}
parameters {
  vector[N] y_true;          // true value
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  real<lower=0> sigma;  // spread of mixture components
}
model {
  sigma ~ normal(0, 0.3);
  mu[1] ~ normal(-0.7, 0.5);
  mu[2] ~ normal(0.7, 0.5);
  p ~ beta(2, 2);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y_true[n] | mu[1], sigma),
                      normal_lpdf(y_true[n] | mu[2], sigma));
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}

# 2019/2020 Summer project logbook

This is a logbook for Evgenii's 2019/2020 summer project. The logbook files are located in the years/months/days directories. For example, we have directory a2019/a11/a26. Letter 'a' here is added for Python, since it does not accept folders starting with numbers.


## Setup and run Python code

Follow [setup instructions](setup.md).


## Statistical model code for the paper

The statistical model code used in the paper and instructions for running are in [paper/model_std](paper/model_std) directory.


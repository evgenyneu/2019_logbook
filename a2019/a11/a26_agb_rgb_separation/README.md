# Separation of RGB/AGB stars

We want to separate the NGC288 stars into AGB and RGB categories.

<img src="a2019/a11/a26_agb_rgb_separation/images/chris_report_ngc288.png" alt="NGC 288 from Chris' report" width="500px">


##  Research

We look at papers to see how AGB/RGB stars are separated.


### [MacLean M4 arXiv:1604.05040](https://arxiv.org/abs/1604.05040)

> The RGB and AGB were separated in both V−(B−V) (see Figure 1) and U−(U−I), allowing for an accurately selected sample of AGB stars.

<img src="a2019/a11/a26_agb_rgb_separation/images/maclean_m4_agb_rgb.png" alt="M4 2018 arXiv:1604.05040 M4 AGB/RGB CMD" width="500px">



### [MacLean NGC 6397 arXiv:1712.03340v1](https://arxiv.org/abs/1712.03340)

> The RGB and AGB are separated in V−(B−V) and U−(U−I) space (Figure 1). AGB stars were conservatively selected – only early-AGB stars were included so as to avoid the mislabelling of stars since the AGB and RGB colours become similar at brighter magnitudes. We then cross-matched our selection with the 2MASS database to take advantage of the high quality astrometry and JHK photometry.

<img src="a2019/a11/a26_agb_rgb_separation/images/maclean_NGC_6397_agb_rgb.png" alt="NGC 6397 AGB/RGB CMD" width="500px">



### [MacLean M4 arXiv:1808.06735](https://arxiv.org/abs/1808.06735)

<img src="a2019/a11/a26_agb_rgb_separation/images/maclean_M4_2_agb_rgb.png" alt="M4 2 AGB/RGB CMD" width="500px">



### [Marino 2017 arXiv:1706.02278](https://arxiv.org/abs/1706.02278)

> Our target AGB stars were carefully selected from CMDs derived from both the ground-based and HST observations. Specifically, for NGC 2808 we have selected three stars on the three main AGB sequences identified by Milone et al.(2015a) by using the CF275W,F336W,F438W index from HST photometry. The remaining four stars of NGC 2808 have been selected from ground-based photometry by Stetson (2000). In the case of M 4, multi-wavelength HST photometry from Piotto et al. (2015) is available for six AGB stars, while the remaining eleven stars have been identified only on the CMD obtained from ground-based photometry.

<img src="a2019/a11/a26_agb_rgb_separation/images/marino_2017_m4.png" alt="Marino M4" width="700px">

<img src="a2019/a11/a26_agb_rgb_separation/images/marino_2017_NGC2808.png" alt="Marino NGC2808" width="700px">





### [Wang 2017 arXiv:1708.07634](https://arxiv.org/abs/1708.07634)

> To distinguish AGB from RGB stars, we used several CMDs with different combinations of color indices and magnitudes. We found that in the CMDs of, for example, (U-I)−U, (U-I)−I, and (B-I)−V, AGB and RGB stars can be separated efficiently, similarly to García-Hernández et al. (2015).

<img src="a2019/a11/a26_agb_rgb_separation/images/wang_2017.png" alt="Wang 2017" width="600px">


## Finding data for stars

### In Stetson catalog

Data comes from [Stetson 2019 calalog](https://arxiv.org/abs/1902.09925).

Table name: J/MNRAS/485/3042/table4

Here is how to get it:

* Click [NASA ADS](https://arxiv.org/abs/1902.09925)

* Under Data Product on the right, click [CDS](http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=J/MNRAS/485/3042).

* Click [J/MNRAS/485/3042/table4 (c) Final catalogue (4890955 rows)](http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=J/MNRAS/485/3042/table4)


### Find a star in catalogues


2massID: 00524297-2636140

RA/Dec: 13.179058, -26.603914



* Open [VizieR search](http://vizier.u-strasbg.fr/viz-bin/VizieR)

* In Search by Position, enter "13.179058, -26.603914"

* Change radius to 1 arcsec


### Load star data programmatically

```
conda install -c astropy astroquery
```

Access star from code: [query.py](query.py)

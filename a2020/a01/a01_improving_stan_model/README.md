# Fitting gaussian mixture abundance model

I was fiddling with the Stan model in order to improve the quality of the samples. In particular, I wanted to get rid of divergent transitions and reduce Rhat parameter to 1.

I did two things that helped:

* Standardised the data: (values - mean) / std.

* Changed the prior for the population means, to make it more separated from each other (before both were centered on zero):

```
mu[1] ~ normal(-1, 0.5);
mu[2] ~ normal(1, 0.5);
```

I use the data ([sodium.csv](a2020/a01/a01_improving_stan_model/data/sodium.csv)) from Chris' report and fit a Gaussian mixture model.

Code: [a2020/a01/a01_improving_stan_model/code/calculate_sodium_ratio.r](a2020/a01/a01_improving_stan_model/code/calculate_sodium_ratio.r).

## Results

I managed to improve the quality of sampling. The Stan model ran without any warnings and Rhat=1 for all parameters (see the [full output](a2020/a01/a01_improving_stan_model/code/output.txt)).


### RGB stars

```
        mean   sd  5.5% 94.5%       histogram
p       0.38 0.11  0.22  0.57      ▁▁▃▇▅▂▁▁▁▁
mu1    -1.15 0.07 -1.25 -1.02        ▁▁▃▇▂▁▁▁
mu2    -0.61 0.04 -0.67 -0.56    ▁▁▁▂▇▃▁▁▁▁▁▁
sigma1  0.15 0.07  0.07  0.28 ▁▅▇▃▂▁▁▁▁▁▁▁▁▁▁
sigma2  0.07 0.04  0.02  0.14  ▇▇▂▁▁▁▁▁▁▁▁▁▁▁
```


### AGB stars

```
        mean   sd  5.5% 94.5%  histogram
p       0.56 0.21  0.17  0.87 ▁▁▂▃▅▇▇▅▃▁
mu1    -1.37 0.07 -1.49 -1.26  ▁▁▁▁▃▇▂▁▁
mu2    -0.96 0.11 -1.14 -0.80  ▁▁▂▃▇▅▁▁▁
sigma1  0.14 0.14  0.03  0.30  ▇▁▁▁▁▁▁▁▁
sigma2  0.21 0.15  0.06  0.42  ▇▁▁▁▁▁▁▁▁
```

### What do parameters mean exactly?

* `p` is the mixing proportion, a fraction of low-sodium stars (i.e. 0.3 means 30% of stars are low sodium).
* `mu1`, `mu2` are the means for the Gaussian for the two populations.
* `sigma1`, `sigma2` are the standard deviations for the Gaussian for the two populations.


### Samples

Here are the samples returned by Stan:

* [a2020/a01/a01_improving_stan_model/data/sodium_model_agb.csv](a2020/a01/a01_improving_stan_model/data/sodium_model_agb.csv)

* [a2020/a01/a01_improving_stan_model/data/sodium_model_rgb.csv](a2020/a01/a01_improving_stan_model/data/sodium_model_rgb.csv)


On Fig. 1-2 we plot the distributions of the sample values.

<img src="a2020/a01/a01_improving_stan_model/plots/p_distribution.png" width="1000" alt='Proportion of low-sodium stars'>

Figure 1: Distribution of the mixing proportion `p` returned by the Stan model. The `p` value is the proportion of low sodium stars. The dashed lines indicate the mean values.


<img src="a2020/a01/a01_improving_stan_model/plots/mu_parameter_rgb.png" width="1000" alt='Means of two gaussian populations'>

Figure 2: Distribution of `mu1` and `mu2` parameters returned by Stan. These are the means of the normal distribution in the model.

### Plot posterior distributions

We create posterior distributions from first 1000 samples of parameters (Fig. 3).

<img src="a2020/a01/a01_improving_stan_model/plots/posterior_distributions.png" width="1000" alt='Posterior distributions'>

Figure 3: 1000 posterior distributions of sodium abundances. Mean distributions are shown in dark curves.

Code: [a2020/a01/a01_improving_stan_model/code/plot_posterior.r](a2020/a01/a01_improving_stan_model/code/plot_posterior.r)


## Constraining spreads of two populations

In my previous model, the sigma1 and sigma2 (spreads of the two normal populations) can take any values. It may produce fits that have very different sigma1 and sigma2 values. For example, one fit had sigma1=0.179600695 and sigma2=0.015654329. This means that the spreads of two populations vary by the factor of 10. Is this physical? I don't know. I created another model to constrain the spreads of the two populations. In this model the two sigmas can not differ by more than a factor of four:

Code: [a2020/a01/a01_improving_stan_model/code/calculate_sodium_ratio_limit_sigma.r](a2020/a01/a01_improving_stan_model/code/calculate_sodium_ratio_limit_sigma.r).


### Results of constrained spreads model

The new `k` parameter is the ratio of sigma2/sigma1.

#### RGB stars

```
        mean   sd  5.5% 94.5%     histogram
p       0.37 0.11  0.21  0.54    ▁▁▅▇▅▂▁▁▁▁
mu1    -1.17 0.06 -1.25 -1.06 ▁▁▁▁▅▇▃▁▁▁▁▁▁
mu2    -0.62 0.04 -0.67 -0.56 ▁▁▁▁▂▇▃▁▁▁▁▁▁
sigma1  0.11 0.05  0.06  0.20  ▁▇▅▂▁▁▁▁▁▁▁▁
sigma2  0.07 0.05  0.03  0.15  ▇▂▁▁▁▁▁▁▁▁▁▁
k       0.74 0.50  0.28  1.68      ▇▇▂▁▁▁▁▁
```


#### AGB stars

```
 mean   sd  5.5% 94.5%  histogram
p       0.57 0.20  0.21  0.86 ▁▁▂▃▅▇▇▅▃▁
mu1    -1.36 0.07 -1.47 -1.26  ▁▁▁▁▂▇▂▁▁
mu2    -0.96 0.10 -1.13 -0.80  ▁▁▂▃▇▅▁▁▁
sigma1  0.11 0.07  0.04  0.24  ▇▅▁▁▁▁▁▁▁
sigma2  0.20 0.13  0.05  0.42  ▇▃▁▁▁▁▁▁▁
k       2.02 1.01  0.53  3.69   ▂▇▇▇▇▇▅▃
```

#### Constrained spread model conclusion

We can see that the constrained spreads model give values of parameters that are almost identical to the previous unconstrained model. Therefore I would prefer to keep the simpler unconstrained model. Unconstrained model could be less physical, but it is easier to understand and communicate, and it gives almost the same results.

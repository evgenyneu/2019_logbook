# Code for model comparison question

This is the code for [model comparison question](https://discourse.mc-stan.org/t/comparing-two-models/13424) posted on Stan Forums.

## Setup

Download and install all required software with the following steps.

### 1. Install python libraries

First, install Python 3.7 or newer, and then install Python libraries:

```
pip install tarpan
pip install pyyaml
```

If you have issues, use `pip install -Iv tarpan==0.3.7` instead.

### 2. Install Stan

```
install_cmdstan
```

## Usage

Download this repository:

```bash
git clone https://gitlab.com/evgenyneu/2019_logbook.git
```

Run the code:

```bash
cd 2019_logbook/a2020/a03/a01_model_comparison_forum_question/code
python model.py
```

The plots and summary files will be created under  `model_info` directory, with PSIS/WAIC files in `compare` sub-directory.

# Analysing sodium abundances from literature

We analysed sodium abundances from literature using statistical model describing a mixture of two Gaussians (Table 1).

Table 1: Model parameters calculated with sodium abundance data from literature. Values correspond to the modes of the distributions. Uncertainties correspond to distances to 68% HPDI (highest posterior density interval).

| Data source   |  Cluster  | $`r_{agb}`$ |  $`r_{rgb}`$  |   f | 🍎🍊Agreement with the authors |
|:--------:|:-------:|:-------:|-------:|------:|:-------:|
| [Campbell et al. 2013](a2020/a02/a21_fix_uncertainties/a2013_06_campbell_ngc_6752) | NGC 6752 | $`0.6^{+0.2}_{-0.3}`$  | $`0.7^{+0.1}_{-0.1}`$   |  $`0.1^{+0.4}_{-0.3}`$  | $`r_{agb}`$ and f disagree, $`r_{rgb}`$ agrees. |
| [Johnson et al. 2015](a2020/a02/a21_fix_uncertainties/a2015_01_johnson_ngc_104_47_tuc) | NGC 104 (47 Tuc) | $`0.5^{+0.2}_{-0.2}`$  | $`0.3^{+0.3}_{-0.2}`$   |  $`0.2^{+0.4}_{-0.6}`$  | $`r_{agb}`$, $`r_{rgb}`$  agree, f disagree. |
| [Lapenna et al. 2016](a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752) | NGC 6752 | $`0.7^{+0.1}_{-0.1}`$  | $`0.7^{+0.1}_{-0.1}`$   |  $`0.1^{+0.2}_{-0.2}`$  | $`r_{agb}`$ agree, $`r_{rgb}`$ and f are not reported. |
| [Marino et al. 2017](a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4) | NGC 6121 (M4) | $`0.5^{+0.2}_{-0.2}`$  | $`0.6^{+0.1}_{-0.1}`$   |  $`0.2^{+0.3}_{-0.3}`$  | Agree. |
| [Campbell et al. 2017 LTE](a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte) | NGC 6752 |   $`0.6^{+0.1}_{-0.1}`$  |  $`0.7^{+0.1}_{-0.1}`$  | $`0.1^{+0.2}_{-0.3}`$ | Parameters are not reported. |
| [Campbell et al. 2017 NLTE](a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_nlte) | NGC 6752 | $`0.6^{+0.1}_{-0.2}`$  | $`0.7^{+0.1}_{-0.1}`$  |  $`0.1^{+0.3}_{-0.3}`$  | Parameters are not reported. |
| [Wang et al. 2017](a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_104_47_tuc) | NGC 104 (47 Tuc) | $`0.3^{+0.1}_{-0.1}`$  | $`0.6^{+0.1}_{-0.2}`$  |  $`0.5^{+0.2}_{-0.3}`$  |  Agree. |
| [Wang et al. 2017](a2020/a02/a21_fix_uncertainties/a2017_11_wang_ngc_6121_M4) | NGC 6121 (M4) | $`0.6^{+0.2}_{-0.3}`$  | $`0.7^{+0.1}_{-0.1}`$  |  $`0.2^{+0.4}_{-0.3}`$  |  Agree. |
| [MacLean et al. 2018](a2020/a02/a21_fix_uncertainties/a2018_03_MacLean_NGC_6397) | NGC 6397 | $`0.6^{+0.1}_{-0.2}`$  | $`0.7^{+0.1}_{-0.1}`$ | $`0.1^{+0.3}_{-0.3}`$  |  Agree. |
| [MacLean et al. 2018](a2020/a02/a21_fix_uncertainties/a2018_11_MacLean_NGC_6121_M4) | NGC 6121 (M4) |    $`0.6^{+0.1}_{-0.2}`$  | $`0.6^{+0.1}_{-0.1}`$  | $`-0.1^{+0.3}_{-0.3}`$ | $`r_{agb}`$ and f disagree, $`r_{rgb}`$ agrees. |
| [Work in progress 2020](a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288) | NGC 288 |  $`0.4^{+0.1}_{-0.1}`$ | $`0.7^{+0.1}_{-0.1}`$ |  $`0.5^{+0.2}_{-0.3}`$ | [Na/H] measurements are not final. |


## 🍎🍊Apples to oranges

In the last column of the Table 1 we compare our model parameter with the literature. However, this comparison is "apple to oranges" because authors differ on how they locate the two populations and calculate parameters `r` and `f` .  For example, [Wang et al. 2017](https://ui.adsabs.harvard.edu/abs/2017A%26A...607A.135W/abstract) uses `[Na/Fe]cri = [Na/Fe]min + 0.3 dex` separation point between two population. In contrast, [MacLean et al. 2016](https://arxiv.org/abs/1604.05040v1) chooses the separation point to be at the minimum in the [Na/O] distribution in RGB sample from [Marino et al. 2008](https://ui.adsabs.harvard.edu/abs/2008A%26A...490..625M/abstract).

In comparison, the value `r` that we calculate is the proportion of stars belonging to the second population. Instead of choosing a separation point, the model finds distributions of two populations that are compatible with observations and returns the distribution of parameter `r`, which is the mixing ratio of the two populations.

### Important features of our model

The following are some important features of our model to consider when interpreting our model parameters and comparing them with the literature.

* Our `r` parameters are not calculated directly from the observed values. Instead the model uses probability theory to find distributions of `r` that are compatible with the observed values.

* Our model does not constrain the locations of two populations and may produce distributions that are not useful for making predictions about existence of stellar populations in the globular cluster. For example, the model’s population locations mu1 and mu2 can be so close to each other that it might not be sensible to view these populations as separate but instead consider a single population model.

* The model will always find two populations, even if the data are also compatible with a single or three populations. Therefore, it would be useful to make models with different number of populations and compare them using methods such as [WAIC](https://ui.adsabs.harvard.edu/abs/2012arXiv1208.6338W/abstract) (widely applicable information criterion) or [PSIS](https://ui.adsabs.harvard.edu/abs/2015arXiv150704544V/abstract) (Pareto-smoothed importance sampling cross-validation). These names of the methods sound intimidating (like many other terms in statistics), but they are easy to implement.


## Parameter f

We define parameter f to be the fraction of RGB stars from the second population that would fail to reach AGB branch:

```
f = 1 - r_agb / r_rgb
```

## Statistical model

Mathematical description of the statistical model is shown in Listing 1.

Listing 1: Statical model describing a mixture of two Gaussian components.

<img src="a2020/a02/a21_fix_uncertainties/images/model.png" width=700 alt='Statistical model'>

This model describes a mixture of two Gaussian components. These are two distinct populations of stars from which we observed sodium abundances. The input data used in the model is transformed to standard values (i.e. z-scores) by subtracting mean and dividing by standard deviation. After Stan has finished processing the model, the values from posterior distributions are transformed back to sodium abundances.

### Input parameter of the model

The input parameters of the model are:

* $`y_i`$: observed value of sodium abundance of the i-th star, in [Na/H] units,

* $`e(y_i)`$: corresponding measurement uncertainty of the sodium abundance.

* $`\sigma_{\textrm{stdev}}`$: standard deviation of the population spread, in [Na/H] units. We used value of 0.05 dex.


### Estimated parameters of the model

With this code we want to estimate distributions of the following parameters:

* $`r`$: mixing proportion, which is the proportion of stars in the second population (the population with the higher average sodium abundance),

* $`\mu_1`$ and $`\mu_2`$: locations of two populations,

* $`\sigma`$: spreads of the populations.


## Model tests

* [One population](a2020/a02/a21_fix_uncertainties/test_single_population).

* [Two populations](a2020/a02/a21_fix_uncertainties/test_two_populations).

## Model parameter findings

* `sigma_stdev`: 0.05 is slightly better than 0.10 for two Gaussian data, same for single Gaussian.

* For [beta distribution](https://en.wikipedia.org/wiki/Beta_distribution) of r parameter, we found that beta(2, 2) produces about the same results as beta(1, 1). We choose beta(2, 2) because
    * Stan generates more effective samples and shows fewer warnings,
    * with beta(1, 1), r is uniform, meaning that we expect r values close to 0 and 1 to be equally probable, which describes a single Gaussian population. However, our model is for two populations, thus we don't expect r to be 0 or 1, which is achieved with `beta(2, 2)`. To handle the single population case, it would be better to create a separate single-gaussian model, and then compare it with the two-gaussian model using WAIC or PSIS.

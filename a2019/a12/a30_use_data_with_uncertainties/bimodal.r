library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

# Simulate observations of 5 two populations of stars:
# 5 sodiym poor and 10 sodium rich stars
# ----------------

n_poor = 5 # Number of Sodium poor stars
n_rich = 10 # Number of Sodium rich stars

# Generate random values from Normal distributions
samples_poor = rnorm(n_poor, 8, 0.2) # Poor stars have values around 8
samples_rich = rnorm(n_rich, 10, 0.2) # Rich stars have values around 10

# Compabine all samples in one list
observed_values = sample(c(samples_poor, samples_rich))

plot(observed_values, 1:length(observed_values))
plot(density(observed_values))

data = list(
    y = observed_values,
    N=length(observed_values)
)

# Run Stan
model_path = 'a2019/a12/a30_use_data_with_uncertainties/mixture.stan'
fit <- stan(file=model_path, data=data)

print(fit)

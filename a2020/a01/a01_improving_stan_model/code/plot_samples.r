# Plot parameters of Stan model
# ------------

data_rgb_path = "a2020/a01/a01_improving_stan_model/data/sodium_model_rgb.csv"
data_rgb = read.csv(data_rgb_path, header=TRUE)
data_rgb = as.data.frame(data_rgb)

data_agb_path = "a2020/a01/a01_improving_stan_model/data/sodium_model_agb.csv"
data_agb = read.csv(data_agb_path, header=TRUE)
data_agb = as.data.frame(data_agb)

agb_color = '#9999FF88'
agb_color_no_alpha = '#9999FF'
rgb_color = '#FF999988'
rgb_color_no_alpha = '#FF9999'

plot_path = "a2020/a01/a01_improving_stan_model/plots/"

# Plot parameter p
# --------------

png(filename=paste(plot_path, "p_distribution.png", sep=""),
width=7, height=5, units='in', res=300)

breaks = 70

# First distribution
hist(data_rgb$p, breaks=breaks, xlim=c(-0.2,1.2), col=rgb_color,
    xlab="Proportion of low-sodium stars, p",
    ylab="Samples",
    main="Proportion of low-sodium stars" )

# Second with add=T to plot on top
hist(data_agb$p, breaks=breaks, col=agb_color, add=T)

abline(v=mean(data_rgb$p), col=rgb_color_no_alpha, lty=3, lwd=2)
abline(v=mean(data_agb$p), col=agb_color_no_alpha, lty=3, lwd=2)

# Add legend
legend("topright", legend=c("AGB", "RGB"),
    col=c(agb_color, rgb_color), pt.cex=2, pch=15)

dev.off()


# Plot parameter mu (RGB)
# --------------

png(filename=paste(plot_path, "mu_parameter_rgb.png", sep=""),
    width=7, height=5, units='in', res=300)

breaks = 50

# First distribution
hist(data_rgb$mu1, breaks=breaks, xlim=c(-2,0), col=rgb_color,
    main="Means of normal populations of sodium abundances",
    ylab="Samples",
    xlab="Means of sodium abundance [Na/H]" )

# Second with add=T to plot on top
hist(data_rgb$mu2, breaks=breaks, col=rgb_color, add=T)

abline(v=mean(data_rgb$mu1), col=rgb_color_no_alpha, lty=3, lwd=2)
abline(v=mean(data_rgb$mu2), col=rgb_color_no_alpha, lty=3, lwd=2)

# Second with add=T to plot on top
hist(data_agb$mu1, breaks=breaks, col=agb_color, add=T)
hist(data_agb$mu2, breaks=breaks, col=agb_color, add=T)

abline(v=mean(data_agb$mu1), col=agb_color_no_alpha, lty=3, lwd=2)
abline(v=mean(data_agb$mu2), col=agb_color_no_alpha, lty=3, lwd=2)

# Add legend
legend("topright", legend=c("AGB", "RGB"),
    col=c(agb_color, rgb_color), pt.cex=2, pch=15)

dev.off()

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    plt.figure(figsize=(4, 3))
    data_dir = 'a2019/a12/a10_calculate_distance/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Remove rows with missing values
    # --------

    df = df[df.pm_speed_from_mean != ' ']
    df.pm_speed_from_mean = pd.to_numeric(df.pm_speed_from_mean)
    df = df[df.u_k != ' ']
    df.u_k = pd.to_numeric(df.u_k)

    # Show only stars with similar proper velocities
    # i.e. within 0.25 mas/a from the mean of the selected stars
    df = df.loc[df['pm_speed_from_mean'] < 0.25]

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    selected_other_b = stars_other_df['b']
    selected_other_b_minus_k = stars_other_df['b'] - stars_other_df['k']

    plt.scatter(selected_other_b_minus_k, selected_other_b,
                c='gray', marker='o', s=7, edgecolors='gray',
                label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    selected_agb_b = stars_agb_df['b']
    selected_ragb_b_minus_k = stars_agb_df['b'] - stars_agb_df['k']

    plt.scatter(selected_ragb_b_minus_k, selected_agb_b,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    selected_rgb_b = stars_rgb_df['b']
    selected_rgb_b_minus_k = stars_rgb_df['b'] - stars_rgb_df['k']

    plt.scatter(selected_rgb_b_minus_k, selected_rgb_b,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # Error bars
    # --------------

    all_selected_df = df

    uncertainty_b_df = all_selected_df['u_b']
    u_b = uncertainty_b_df.median()
    uncertainty_b_minus_k_df = np.sqrt(all_selected_df['u_b']**2 + all_selected_df['u_k']**2)
    u_b_minus_k = uncertainty_b_minus_k_df.median()

    print(f'u(u_b)={u_b}')
    print(f'u(u_b_minus_k)={u_b_minus_k}')

    x_error_bar = plt.xlim()[1] - u_b_minus_k
    y_error_bar = plt.ylim()[1] - u_b

    plt.errorbar([x_error_bar], [y_error_bar],
                 xerr=[u_b_minus_k], yerr=[u_b],
                 fmt='none', color='black', capsize=0.5,
                 ecolor='black', elinewidth=1)

    ax = plt.gca()
    ax.invert_yaxis()
    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    plt.legend()
    plt.xlabel('B-K color')
    plt.ylabel('B magnitude')
    plt.tight_layout(pad=0.05)
    save_plot(plt=plt,  extensions=['png', 'pdf'])
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

## Add uncertainties for V, B, I, U fields

Here we add uncertainties from Yazan's catalog for V, B, I, U fields.

[ngc288_stars_with_uncert.csv](a2019/a11/a30_photometric_uncertainties/data/ngc288_stars_with_uncert.csv)

Code: [add_uncert_from_yazan.py](add_uncert_from_yazan.py)


```
python -m a2019.a11.a30_photometric_uncertainties.add_uncert_from_yazan
```


## Add uncertainties for J, H, K fields

Here we add uncertainties from 2MASS catalog for J, H, K fields.

[ngc288_stars_with_uncert_2mass.csv](data/ngc288_stars_with_uncert_2mass.csv)

Code: [add_uncert_from_2mass.py](add_uncert_from_2mass.py)


```
python -m a2019.a11.a30_photometric_uncertainties.add_uncert_from_2mass
```


## Excluding two outliers

We exclude two stars BAGBYAZ005721 and RGBSYAZ003702. We think they are not members of the cluster based on radial velocity.


## Cross match with Stetson catalog

Cross match our stars with [Stetson catalog](https://ui.adsabs.harvard.edu/abs/2019MNRAS.485.3042S):

* [data/XMatch_stetson.txt](data/XMatch_stetson.txt)

* [data/ngc288_stetson.csv](data/ngc288_stetson.csv)

### Matching problem

* Records with Cat="STE", the ID_Phot does not match the Star number in Stetson catalog.

* V and B magnitudes also do not match those in Stetson. For example, our star RHBSSTE001843 with (ra, dec = 13.35582, -26.387657) star has V=15.398, B=15.932 in our file, but Stetson gives V=15.391 (-0.007 difference), B=15.941 (0.009 difference) instead.

* Another example, star RGBSSTE000213 has V=14.948, B=15.879 in our file, but Stetson gives V=14.948 (no difference), B=15.881 (0.002 difference) instead.

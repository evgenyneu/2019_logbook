import pandas as pd
import numpy as np
from scipy.stats import norm
from dataclasses import dataclass, field
from typing import List
import yaml

from tarpan.cmdstanpy.tree_plot import save_tree_plot
from tarpan.shared.tree_plot import TreePlotParams
from tarpan.shared.info_path import InfoPath

from tarpan.cmdstanpy.compare_parameters import (
    save_compare_parameters, CompareParametersType)


@dataclass
class SimulatedModelParameters:
    """Store parameters for the simulated model"""
    name: str
    sigma: float
    n: List[float]
    mu: List[float]

    def r(self, ix) -> str:
        return self.n[ix] / sum(self.n)


@dataclass
class SimulatedUncertainty:
    """Normally distributed uncertainty of observations"""
    mu: float
    sigma: float
    min: float  # Minimum value for the uncertainty


@dataclass
class SimulatedParameters:
    """Store parameters for the simulated models"""
    uncertainty: SimulatedUncertainty
    params: List[SimulatedModelParameters] = field(default_factory=list)

    def read_model_params(params_path):
        """
        Reads model parameters from configuration file.

        Returns
        -------
        SimulatedParameters
            Model parameters.
        """

        with open(params_path, 'r') as stream:
            data = yaml.safe_load(stream)
            uncertainty = data['uncertainty']

            simulated_uncertainty = SimulatedUncertainty(
                mu=uncertainty["mu"],
                sigma=uncertainty["sigma"],
                min=uncertainty["min"]
            )

            model_params = SimulatedParameters(
                uncertainty=simulated_uncertainty)

            for model in data['models']:
                single_model = SimulatedModelParameters(
                    name=model['name'],
                    sigma=model['sigma'],
                    n=model['n'],
                    mu=model['mu']
                )

                model_params.params.append(single_model)

            return model_params

    def __getitem__(self, index):
        return self.params[index]


def simulate_sample(seed, uncertainty: SimulatedUncertainty,
                    params: SimulatedModelParameters):
    """
    Simulate observations and return values and uncertainties.
    """

    np.random.seed(seed=seed)
    n = sum(params.n)  # Total sample size of observed stars
    values_true = []

    for i, mu in enumerate(params.mu):
        values = norm(mu, params.sigma).rvs(size=params.n[i])
        values_true += list(values)

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(uncertainty.mu, uncertainty.sigma).rvs(size=n))
    uncertainties = np.clip(uncertainties, uncertainty.min, None)

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)

    return values_observed, uncertainties


def generate_data(csv_path, seed, model_params: SimulatedParameters):
    """
    Simulate observations and write the values and uncertainties
    to a text file.
    """

    rows = []

    for params in model_params:
        values, uncertainties = simulate_sample(
            seed=seed, uncertainty=model_params.uncertainty, params=params)

        for value, uncert in zip(values, uncertainties):
            rows.append([params.name, value, uncert])

    df = pd.DataFrame(rows, columns=["Type", "[Na/H]", "e_[Na/H]"])
    df.to_csv(csv_path)


def compare_with_exact(fits,
                       path, dir_name,
                       model_params: SimulatedParameters):
    """Compare parameter distributions with simulated exact values"""
    tree_params = TreePlotParams()

    # Set legend names for Stan and exact markers
    # ---------

    names = [params.name for params in model_params]
    names_combined = names
    tree_params.labels = names_combined
    names_exact = [f'{name} exact' for name in names]
    tree_params.labels += names_exact

    default_tree_params = TreePlotParams()
    tree_params.marker_colors = default_tree_params.marker_colors[:len(fits)]
    tree_params.marker_colors += tree_params.marker_colors

    tree_params.marker_edge_colors = \
        default_tree_params.marker_edge_colors[:len(fits)]

    tree_params.marker_edge_colors += tree_params.marker_edge_colors

    data = []

    # Add exact values to the tree plot
    # ---------

    for params in model_params:
        model_data = {
            "sigma": params.sigma
        }

        if len(params.n) > 1:
            model_data["r"] = params.r(len(params.n) - 1)

        if ("mu.1" in fits[0].column_names):
            # Stan model include multiple mu parameters mu.1, mu.2 etc.
            for i, mu in enumerate(params.mu):
                model_data[f"mu.{i + 1}"] = mu
        else:
            # Stan model has only one mu parameter named 'mu'
            # -------

            if len(params.mu) == 1:
                # One exact mu
                model_data["mu"] = params.mu[0]
            else:
                # More than one exact
                for i, mu in enumerate(params.mu):
                    model_data[f"mu.{i + 1} exact"] = mu

                tree_params.labels = None  # Do not show legend

        data.append(model_data)

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="compare_with_exact",
                         extension="png")

    save_tree_plot(fits, extra_values=data,
                   param_names=['mu', 'r', 'sigma'],
                   tree_params=tree_params,
                   info_path=info_path)

    # Create text table
    # ----------

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="parameters_compared",
                         extension="txt")

    save_compare_parameters(fits,
                            labels=names_combined,
                            extra_values=data,
                            param_names=["r", "mu", "sigma"],
                            info_path=info_path,
                            type=CompareParametersType.GITLAB_LATEX)


def posterior_plot_fn(fig, axes, params):
    line_colors = ['#0060ff88', '#ff002188', '#8888FF88', '#BBBB1188']

    for model_params, color in zip(params.params, line_colors):
        for mu in model_params.mu:
            for ax in axes:
                ax.axvline(mu, linestyle="dashed", c=color, linewidth=1)

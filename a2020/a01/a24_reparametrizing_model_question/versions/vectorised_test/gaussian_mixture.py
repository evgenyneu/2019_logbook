"""
Run Gaussian mixture model

https://discourse.mc-stan.org/t/how-to-reparameterize-a-gaussian-mixture-model/12792
"""
from cmdstanpy import CmdStanModel
import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
from tarpan.cmdstanpy.tree_plot import save_tree_plot
from tarpan.shared.tree_plot import TreePlotParams
from tarpan.cmdstanpy.analyse import save_analysis
from tarpan.shared.info_path import InfoPath


def simulate_sample(seed):
    """
    Simulate observations of two populations of stars:
    sodium-poor and sodium-rich stars
    """

    np.random.seed(seed=seed)

    n_poor = 8  # Number of Sodium poor stars
    n_rich = 5  # Number of Sodium rich stars
    n = n_poor + n_rich  # Total sample size of observed stars

    # Generate random values from Normal distributions
    mu_poor = -1.4  # mean of sodium poor population
    mu_rich = -1  # mean of sodium rich population
    sigma = 0.05
    samples_poor = stats.norm(mu_poor, sigma).rvs(size=n_poor)
    samples_rich = stats.norm(mu_rich, sigma).rvs(size=n_rich)

    # Combine all samples in one list
    values_true = np.concatenate([samples_poor, samples_rich])

    # Generate uncertainties for all observations
    uncertainties = np.abs(stats.norm(0.12, 0.05).rvs(size=n))

    # Generate observed values from true using the uncertainties
    values_observed = stats.norm(values_true, uncertainties).rvs(size=n)

    p_true = n_poor / (n_poor + n_rich)

    true_parameters = {
        "p": p_true,
        "mu.1": mu_poor,
        "mu.2": mu_rich,
        "sigma": sigma
    }

    return (values_observed, uncertainties, true_parameters)


def plot_observed_values(values_observed, uncertainties, mu_poor, mu_rich):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()
    ax1.scatter(values_observed, range(0, len(values_observed)),
                color=color_observed, s=100, label='Observed')

    ax1.errorbar(values_observed, range(0, len(values_observed)),
                 xerr=uncertainties,
                 color=color_observed, fmt='none')

    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")

    ax2.axvline(mu_poor, color=color_observed, linestyle="dashed")
    ax2.axvline(mu_rich, color=color_observed, linestyle="dashed")

    # Show the plot
    # -------

    fig.suptitle(f"Observed sodium abundances")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    fig.savefig("simulated_values.png", dpi=300)


def compare_parameters(fits, true_params, labels):
    tree_params = TreePlotParams()

    tree_params.labels = labels

    save_tree_plot(fits,
                   extra_values=[true_params],
                   param_names=['p', 'mu', 'sigma'],
                   tree_params=tree_params,
                   info_path=InfoPath(sub_dir_name="compare_models"))


def run_stan(dir_name, data, seed, stan_model_path):
    print('\n\n\n-------------')
    print(f'Running model: {stan_model_path}')
    print('\n')

    model = CmdStanModel(stan_file=stan_model_path)

    fit = model.sample(
        data=data, seed=seed,
        adapt_delta=0.99, max_treedepth=10,
        sampling_iters=4000, warmup_iters=1000,
        chains=4, cores=4,
        show_progress=True)

    save_analysis(fit, param_names=["p", "mu", "sigma"],
                  info_path=InfoPath(sub_dir_name=dir_name))
    return fit


def sample():
    seed = 3

    values_observed, uncertainties, true_params = simulate_sample(seed)

    plot_observed_values(
        values_observed=values_observed,
        uncertainties=uncertainties,
        mu_poor=true_params['mu.1'],
        mu_rich=true_params['mu.2']
    )

    data = {
        "y": values_observed,
        "uncertainties": uncertainties,
        "N": len(values_observed)
    }

    model_original = run_stan(
        dir_name="01_original",
        data=data, seed=seed,
        stan_model_path="stan_model/original.stan")

    model_reparametrized = run_stan(
        dir_name="02_reparametrized",
        data=data, seed=seed,
        stan_model_path="stan_model/reparametrized.stan")

    model_vectorised = run_stan(
        dir_name="03_vectorised",
        data=data, seed=seed,
        stan_model_path="stan_model/vectorised.stan")

    fits = [
        model_original,
        model_reparametrized,
        model_vectorised
    ]

    legend_labels = [
        "Original",
        "Reparametrized",
        "Reparametrized vectorised",
        "Simulated 'true'"
    ]

    compare_parameters(fits, true_params, legend_labels)


if __name__ == '__main__':
    sample()
    print('We are done')

# NGC 288 Gaia DR2 data

Gaia data for our stars created by [cross-matching](a2019/a11/a28_organizing_our_data) Gaia catalog with the list of our stars.

### Gaia catalog

* Gaia data release 2 (Gaia DR2). (2018).

* CDS/I/345/gaia2

* https://arxiv.org/abs/1804.09365

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def set_plot_style():
    """Set global style"""

    plt.rcParams['font.family'] = 'serif'

    TINY_SIZE = 15
    SMALL_SIZE = 18
    NORMAL_SIZE = 22
    LARGE_SIZE = 25

    # Title size
    plt.rcParams['axes.titlesize'] = LARGE_SIZE

    # Axes label size
    plt.rcParams['axes.labelsize'] = NORMAL_SIZE

    # Tick label size
    plt.rcParams['xtick.labelsize'] = TINY_SIZE
    plt.rcParams['ytick.labelsize'] = TINY_SIZE

    # Legend text size
    plt.rcParams['legend.fontsize'] = SMALL_SIZE

    plt.rcParams['font.size'] = NORMAL_SIZE
    plt.rcParams['legend.fontsize'] = NORMAL_SIZE

    # Grid color
    plt.rcParams['grid.color'] = '#cccccc'

    # Define plot size
    plt.rcParams['figure.figsize'] = [10, 10]

    # Marker size
    plt.rcParams['lines.markersize'] = 13


def plot(marker_color_type=dict(AGB='#0033ff', RGB='#ff0000'),
         marker_type_type=dict(AGB='^', RGB='o')):

    set_plot_style()
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open stars data
    stars_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb.csv')

    df = pd.read_csv(stars_path)

    # Remove rows with missing values
    # --------

    df = df.dropna(subset=['pm_speed_from_mean', 'u_k'])
    df.pm_speed_from_mean = pd.to_numeric(df.pm_speed_from_mean)
    df = df[df.u_k != ' ']
    df.u_k = pd.to_numeric(df.u_k)

    # Show only stars with similar proper velocities
    # i.e. within 0.25 mas/a from the mean of the selected stars
    df = df.loc[df['pm_speed_from_mean'] < 0.25]

    # Excluded
    # ---------------

    stars_other_df = df[df['Teff'].isna()]
    selected_other_b = stars_other_df['b']
    selected_other_b_minus_k = stars_other_df['b'] - stars_other_df['k']

    fig, ax = plt.subplots(1, 1)

    ax.scatter(selected_other_b_minus_k, selected_other_b,
               c='gray', marker='o', s=7, edgecolors='gray',
               label='Excluded')


    # Plot stars with measured temperatures
    # ---------------

    # Drop rows without temperatures
    df = df.dropna(subset=['Teff'])

    for star_type in ['AGB', 'RGB']:
        marker_color = marker_color_type[star_type]
        marker_type = marker_type_type[star_type]

        type_df = df[df['agb_or_rgb'] == star_type]
        mag_b = type_df['b']
        mag_b_minus_k = type_df['b'] - type_df['k']

        ax.scatter(mag_b_minus_k, mag_b,
                   color=f"{marker_color}40",
                   marker=marker_type,
                   edgecolors=marker_color,
                   label=star_type)

    # Error bars
    # --------------

    all_selected_df = df
    uncertainty_b_df = all_selected_df['u_b']
    u_b = uncertainty_b_df.median()
    uncertainty_b_minus_k_df = np.sqrt(all_selected_df['u_b']**2 + all_selected_df['u_k']**2)
    u_b_minus_k = uncertainty_b_minus_k_df.median()

    print(f'u(u_b)={u_b}')
    print(f'u(u_b_minus_k)={u_b_minus_k}')

    x_error_bar = plt.xlim()[1] - u_b_minus_k
    y_error_bar = plt.ylim()[1] - u_b

    ax.errorbar([x_error_bar], [y_error_bar],
                xerr=[u_b_minus_k], yerr=[u_b],
                fmt='none', color='black', capsize=0.5,
                ecolor='black', elinewidth=1)

    ax.invert_yaxis()
    ax.legend()
    ax.grid()
    ax.set_xlabel('B-K color')
    ax.set_ylabel('B magnitude')
    fig.tight_layout()
    save_plot(plt=fig)


if __name__ == '__main__':
    plot()
    print('We are done')

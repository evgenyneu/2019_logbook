from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from run_stan import standardize, destandardize, run_stan
from code_dope.python.plot.plot import save_plot
import pandas as pd

from sample_utils import (
    save_posterior_plot, InfoPath, make_comparative_tree_plot,
    save_summary, make_tree_plot, TreePlotParams)


def simulate_sample(seed, multiplier):
    """
    Simulate observations of two populations of stars:
    sodium-poor and sodium-rich stars
    """

    np.random.seed(seed=seed)

    n_poor = int(8 * multiplier)  # Number of Sodium poor stars
    n_rich = int(16 * multiplier)  # Number of Sodium rich stars
    n = n_poor + n_rich  # Total sample size of observed stars

    # Generate random values from Normal distributions
    mu_poor = -1.1  # mean of sodium poor population
    mu_rich = -0.6  # mean of sodium rich population
    sigma = 0.05
    samples_poor = norm(mu_poor, sigma).rvs(size=n_poor)
    samples_rich = norm(mu_rich, sigma).rvs(size=n_rich)

    # Combine all samples in one list
    values_true = np.concatenate([samples_poor, samples_rich])

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(0.13, 0.06).rvs(size=n))

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)

    p_true = n_poor / (n_poor + n_rich)

    true_parameters = {
        "p": p_true,
        "mu.1": mu_poor,
        "mu.2": mu_rich,
        "sigma.1": sigma,
        "sigma.2": sigma
    }

    return (
        values_true, values_observed, uncertainties, mu_poor, mu_rich, n,
        true_parameters)


def plot_simulated_values(multiplier,
                          values_true, values_observed, uncertainties,
                          mu_poor, mu_rich, n_total):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()

    ax1.scatter(values_observed, range(0, len(values_true)),
                color="#0000ff44", s=100, label='Observed', zorder=5)

    ax1.errorbar(values_observed, range(0, len(values_true)),
                 xerr=uncertainties,
                 color="#0000ff22", fmt='none', zorder=5)

    ax1.scatter(values_true, range(0, len(values_true)),
                label='Simulated true', facecolors="none",
                edgecolors="#00000099", zorder=10)

    ax1.legend()
    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")
    ax2.axvline(mu_poor, color=color_observed, linestyle="dashed")
    ax2.axvline(mu_rich, color=color_observed, linestyle="dashed")

    # Show the plot
    # -------

    fig.suptitle(f"{n_total} simulated true and observed sodium abundances")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix=f"{multiplier:.2f}")


def run_model(dir_name, values_observed, uncertainties, seed):
    values_std, uncertainties_std = standardize(values_observed, uncertainties)

    data_for_model = {
        'y': values_std,
        'uncertainties': uncertainties_std,
        'N': len(values_std)
    }

    samples_std = run_stan(data_for_model, dir_name, seed=seed)
    samples = destandardize(samples_std, values_observed)
    return samples


def plot_posterior(samples, multiplier, n_total):
    dir_name = f'posterior_{multiplier:.2f}'
    info_path = InfoPath(sub_dir_name=dir_name)

    summary = save_summary(samples, info_path=info_path)

    # Make tree plot
    # --------

    tree_params = TreePlotParams(
        title=f"Posterior distributions of parameters for {n_total} measurements",
        xlim=[-1.25, 1]
    )

    make_tree_plot(summary['df'], info_path=info_path, tree_params=tree_params)

    # Posterior plot
    save_posterior_plot(samples, summary['df'], info_path=info_path)

    return summary['df']


def simulate(multiplier):
    seed = 333

    values_true, values_observed, uncertainties, \
        mu_poor, mu_rich, n_total, true_params = simulate_sample(
            seed=seed, multiplier=multiplier)

    plot_simulated_values(
        multiplier=multiplier,
        values_true=values_true,
        values_observed=values_observed,
        uncertainties=uncertainties,
        mu_poor=mu_poor,
        mu_rich=mu_rich,
        n_total=n_total)

    dir_name = f'simulated_{multiplier:.2f}'

    samples = run_model(dir_name, values_observed, uncertainties, seed=seed)

    summary = plot_posterior(samples, multiplier=multiplier, n_total=n_total)
    return (summary, n_total, true_params)


def plot_all(all_summaries, n_totals, true_params):
    info_path = InfoPath(
        dir_name="plots", sub_dir_name="compare_all", extension="png")

    title = (
        "Posterior distributions of parameters\n"
        "for different number of observations"
    )

    tree_params = TreePlotParams(title=title, markersize=25)

    tree_params.labels = n_totals
    tree_params.labels.insert(0, "True")

    true_df = pd.DataFrame.from_dict(
        true_params, orient="index", columns=["Mode"])

    all_summaries.insert(0, true_df)

    make_comparative_tree_plot(
        all_summaries, info_path=info_path,
        tree_params=tree_params)


def do_work():
    all_summaries = []
    n_totals = []
    all_true_params = []
    print("Running with multipliers:")

    multipliers = [0.25, 0.5, 1, 2, 5, 10, 20]

    for multiplier in multipliers:
        print(f'  {multiplier:.2f}')
        sumary, n_total, true_params = simulate(multiplier)
        all_summaries.append(sumary)
        n_totals.append(n_total)
        all_true_params.append(true_params)

    plot_all(all_summaries, n_totals, all_true_params[0])


if __name__ == '__main__':
    do_work()
    print('We are done')

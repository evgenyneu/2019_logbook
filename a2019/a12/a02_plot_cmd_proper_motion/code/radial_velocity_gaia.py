import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a02_plot_cmd_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    columns = ['RV', 'e_RV', 'e_pmRA', 'e_pmDE']

    for column in columns:
        df = df[df[column] != ' ']  # Remove rows with missing uncertainties
        df[column] = pd.to_numeric(df[column])

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]

    rv_gaia = stars_other_df['RV']
    rv_us = stars_other_df['vel_rad']

    plt.scatter(x=rv_gaia, y=rv_us,
                c='black', marker='.', s=20, edgecolors='black',
                label='Excluded')


    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    rv_gaia = stars_agb_df['RV']
    rv_us = stars_agb_df['vel_rad']

    plt.scatter(x=rv_gaia, y=rv_us,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    rv_gaia = stars_rgb_df['RV']
    rv_us = stars_rgb_df['vel_rad']

    plt.scatter(x=rv_gaia, y=rv_us,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # Error bars
    # --------------

    rv_gaia = df['RV']
    rv_us = df['vel_rad']
    u_rv_gaia = df['e_RV']
    u_rv_us = df['u_vel_rad']

    plt.errorbar(x=rv_gaia, y=rv_us,
                 xerr=u_rv_gaia, yerr=u_rv_us,
                 fmt='none', color='black', capsize=0,
                 ecolor='gray', elinewidth=1, zorder=-1)

    # Plot straight line
    # -----------

    plt.plot([-50, 50], [-50, 50], zorder=-1, linewidth=1, linestyle=':')
    plt.legend()
    plt.xlabel(r'Radial velocity from Gaia DR2 catalog $v_r$ [km/s]')
    plt.ylabel(r'Our radial velocity $v_r$ [km/s]')
    plt.title('Radial velocities of observed stars in NGC288')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

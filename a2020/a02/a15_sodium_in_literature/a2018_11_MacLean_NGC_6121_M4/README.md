# Analysing data from [MacLean et al. 2018](https://arxiv.org/abs/1808.06735), NGC 6121 (M4)

Code: [a2020/a02/a15_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/code/model.py](a2020/a02/a15_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/code/model.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.65^{+0.10}_{-0.15}`$  | $`\lt 0.2`$           |    No      |    No      |
| $`r_{rgb}`$   |  $`0.58^{+0.06}_{-0.06}`$  | $`\sim 0.55`$         |    Yes     |    Yes     |
| $`f`$         |  $`-0.09^{+0.26}_{-0.22}`$ | $`\gtrsim 0.65`$      |    No      |    No      |

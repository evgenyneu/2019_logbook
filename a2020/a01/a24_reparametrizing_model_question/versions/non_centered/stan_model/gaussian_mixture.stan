// Guassian mixture model
data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed values
  real uncertainties[N]; // uncertainties
}
parameters {
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;
  real<lower=0> sigma;       // spread of mixture components
  vector[N] z1;
  vector[N] z2;
}
transformed parameters {
  vector[N] y_mix1 = mu[1] + sigma*z1;
  vector[N] y_mix2 = mu[2] + sigma*z2; // I corrected this in an edit!!
}
model {
  sigma ~ normal(0, 0.1);
  mu[1] ~ normal(-1.4, 1);
  mu[2] ~ normal(-1.0, 1);
  p ~ beta(2, 2);

  z1 ~ std_normal();
  z2 ~ std_normal();

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y[n] | y_mix1, uncertainties),
                      normal_lpdf(y[n] | y_mix2, uncertainties));
  }
}

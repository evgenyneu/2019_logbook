from uncertainties import ufloat


def get_distance(distance_modulus_v, color_excess):
    """
    Calculates distance with uncertainty given distance modulus and its
    uncertainty


    Parameters
    -----------

    distance_modulus : float or ufloat

        Visual distance modulus (V - M_V)

    color_excess : float

        Color excess E_{B-V} = A_B - A_V


    Returns
    -------

    Distance with uncertainty in parsecs.
    """

    # Total-to-selective extinction ratio parameter
    # Harris uses value 3.1 in http://physwww.mcmaster.ca/%7Eharris/mwgc.ref
    # without uncertainty. This is ok for low values of color_excess.
    r_v = 3.1

    # Calculating extinction
    # Source: http://physwww.mcmaster.ca/%7Eharris/mwgc.ref
    a_v = r_v * color_excess

    # Calculate the distance
    distance = 10 * 10**((distance_modulus_v - a_v) / 5)
    return distance


if __name__ == '__main__':
    # Visual distance modulus for NGC 288
    # Source: http://physwww.mcmaster.ca/~harris/mwgc.dat
    #
    # W.E.Harris via email:
    #    "I would suggest using +-0.1 magnitude uncertainty
    #     for the distance modulus."
    distance_modulus_v = ufloat(14.84, 0.1)

    # Color excess for NGC 288
    # Source: http://physwww.mcmaster.ca/~harris/mwgc.dat
    color_excess = 0.03

    distance = get_distance(distance_modulus_v, color_excess)

    print(f'Distance: {distance}  pc')

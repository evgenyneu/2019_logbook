# Plot color magnitude diagram

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    """
    Plot color-magnitude diagram
    """

    data_dir = 'a2019/a12/a02_plot_cmd_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)
    df = df[df.u_k != ' ']  # Remove rows with missing uncertainties
    df.u_k = pd.to_numeric(df.u_k)

    # Error bars
    # --------------

    b_df = df['b']
    b_minus_k_df = df['b'] - df['k']

    uncertainty_b_df = df['u_b']
    uncertainty_b_minus_k_df = np.sqrt(df['u_b']**2 + df['u_k']**2)

    plt.errorbar(b_minus_k_df, b_df,
                 xerr=uncertainty_b_minus_k_df, yerr=uncertainty_b_df,
                 fmt='none', color='black', capsize=0,
                 ecolor='gray', elinewidth=1, zorder=-1)

    # plt.draw()

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    selected_other_b = stars_other_df['b']
    selected_other_b_minus_k = stars_other_df['b'] - stars_other_df['k']

    plt.scatter(selected_other_b_minus_k, selected_other_b,
                c='black', marker='.', s=3, edgecolors='black',
                label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    selected_agb_b = stars_agb_df['b']
    selected_ragb_b_minus_k = stars_agb_df['b'] - stars_agb_df['k']

    plt.scatter(selected_ragb_b_minus_k, selected_agb_b,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    selected_rgb_b = stars_rgb_df['b']
    selected_rgb_b_minus_k = stars_rgb_df['b'] - stars_rgb_df['k']

    plt.scatter(selected_rgb_b_minus_k, selected_rgb_b,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    ax = plt.gca()
    ax.invert_yaxis()
    plt.legend()
    plt.xlabel('B-K')
    plt.ylabel('B')
    plt.title('All observed stars in NGC288')
    save_plot(plt=plt, suffix=f'all_stars')
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a05_plot_distances/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia_distances.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    df = df.loc[df['selected'] == 1]

    plt.boxplot(df['rest'])

    print(f"Mean distance: {df['rest'].mean()} +- {df['rest'].std()} pc")

    ax = plt.gca()
    ax.set_ylabel(r'Distance $d$ [pc]')
    plt.xticks([1], [''])
    plt.title('Distances to stars in NGC288 selected for analysis')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from code_dope.python.plot.plot import save_plot, kde_with_uncerts
from scipy import stats


def do_work():
    sns.set(style='ticks')
    fig, ax = plt.subplots()

    x = np.linspace(-20, 20, 1000)

    # Plot exact
    y_exact = stats.norm.pdf(x, -10, 1.4) / 2 + stats.norm.pdf(x, 10, 1.8) / 2
    ax.plot(x, y_exact, label='Exact')

    # Plot kde
    y_kde = kde_with_uncerts(x, [-10, 10], [1.4, 1.8])
    ax.plot(x, y_kde, label="KDE", linestyle="dotted")

    ax.grid()
    ax.legend()
    fig.tight_layout()
    save_plot(fig)


if __name__ == '__main__':
    do_work()
    print('We are done')

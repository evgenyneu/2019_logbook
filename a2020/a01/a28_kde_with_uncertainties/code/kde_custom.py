import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from code_dope.python.plot.plot import save_plot, kde_with_uncerts
from data import get_data


def numpy_kde(eval_points, samples, bandwidths):
    # Source: https://numba.pydata.org/numba-examples/examples/density_estimation/kernel/results.html
    # This uses a lot of RAM and doesn't scale to larger datasets
    rescaled_x = (eval_points[:, np.newaxis] - samples[np.newaxis, :]) / bandwidths[np.newaxis, :]
    gaussian = np.exp(-0.5 * rescaled_x**2) / np.sqrt(2 * np.pi) / bandwidths[np.newaxis, :]
    return gaussian.sum(axis=1) / len(samples)


def plot(values):
    x = np.linspace(-2.25, 0, 100)
    sns.set(style='ticks')
    fig, ax = plt.subplots()

    # Numpy KDE
    scott_factor = np.std(values, ddof=1)*len(values)**(-1./(1 + 4))
    bandwidths = np.array([scott_factor] * len(values))
    y = numpy_kde(x, values, bandwidths)
    ax.plot(x, y, label="Custom")

    # Seaborn KDE
    ax = sns.kdeplot(values, ax=ax, label="Seaborn", linestyle="dashed")

    # Gaussian KDE
    y_gaussian = kde_with_uncerts(x, values, [0.159] * len(values))
    ax.plot(x, y_gaussian, label="Gaussian", linestyle="dotted")

    ax.set_xlim(-2.25, 0)
    ax.set_ylim(0, 1.5)
    ax.grid()
    ax.legend()
    fig.tight_layout()
    save_plot(fig)


def do_work():
    values, uncert = get_data()
    plot(values)


if __name__ == '__main__':
    do_work()
    print('We are done')

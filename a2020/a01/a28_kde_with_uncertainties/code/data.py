import numpy as np


def get_data():
    """Returns data from observations"""
    values =        [-0.99, -1.37, -1.38, -1.51, -1.29, -1.34, -1.50,
                     -0.93, -0.83, -1.46, -1.07, -1.28, -0.73]

    uncertainties = [ 0.12,  0.05,  0.11,  0.18,  0.03,  0.19,  0.18,
                      0.12,  0.19,  0.09,  0.11,  0.16,  0.08]

    return (np.array(values), np.array(uncertainties))

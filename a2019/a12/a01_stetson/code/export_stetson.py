# Export Stetson catalog from cross-matched file

import pandas as pd
import os


def rename_column(column):
    if column.endswith("_tab1"):
        return column[:-5]

    return column


def clean_data(df):
    # Remove columns that end with '_tab2'
    column_names = list(df)
    column_to_remove = [x for x in column_names if x.endswith('_tab2')]
    df = df.drop(columns=column_to_remove)

    # Remove _tab1 suffix from column names
    df = df.rename(columns=rename_column)

    return df


def export_stetson():
    """
    Export Gaia data from the cross-matched file and
    add Gaia's object ID to our combined data file.
    """

    data_dir = 'a2019/a12/a01_stetson/data'

    # Open stetson data
    stetson_path = os.path.join(data_dir, 'XMatch_stetson_0.5_arcsec.csv')
    df = pd.read_csv(stetson_path)

    df = clean_data(df=df)

    # Save data frame
    filename = 'ngc288_stetson.csv'
    output_path = os.path.join(data_dir, filename)
    df.to_csv(output_path, index=False)
    print(f'Successufully saved to {output_path}')


if __name__ == '__main__':
    export_stetson()
    print('We are done')

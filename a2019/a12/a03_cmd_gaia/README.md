## Plot color magnitude diagram from Gaia DR2

Code: [a2019/a12/a03_cmd_gaia/code/cmd_gaia.py](a2019/a12/a03_cmd_gaia/code/cmd_gaia.py)

```
python -m a2019.a12.a03_cmd_gaia.code.cmd_gaia
```

<img src="a2019/a12/a03_cmd_gaia/code/plots/cmd_gaia.png" width="700" alt='Color magnitude diagram from Gaia'>

Figure 1: Color magnitude diagram of observed stars in NGC288 using [Gaia DR2](https://arxiv.org/abs/1804.09365) photometry. The error bars are drawn from the maximum uncertainties for all observed stars.


## Save effective temperature to our table

Save Teff from file I received from Chris to our star table:

[ngc288_stars_with_temperatures.csv](a2019/a12/a03_cmd_gaia/data/ngc288_stars_with_temperatures.csv).

Code: [a2019/a12/a03_cmd_gaia/code/save_temperature.py](a2019/a12/a03_cmd_gaia/code/save_temperature.py)

```
python -m a2019.a12.a03_cmd_gaia.code.save_temperature
```


## Compare our effective temperatures with Gaia's

Code: [a2019/a12/a03_cmd_gaia/code/plot_temperature.py](a2019/a12/a03_cmd_gaia/code/plot_temperature.py)

```
python -m a2019.a12.a03_cmd_gaia.code.plot_temperature
```

<img src="a2019/a12/a03_cmd_gaia/code/plots/plot_temperature.png" width="700" alt="Compare our effective temperatures with Gaia's">

Figure 2: Comparison of effective temperatures of observed stars in NGC288 using our calculations and values from [Gaia DR2](https://arxiv.org/abs/1804.09365) catalog.

## Notes

We can see from Fig. 2 that for some stars, effective temperatures are not in very good agreement with Gaia's data. The list of worst offenders is shown in Table 1.


Table 1: Stars with effective temperatures that are more than one standard deviation apart from Gaia's temperatures ([a2019/a12/a03_cmd_gaia/data/temp_difference.csv](a2019/a12/a03_cmd_gaia/data/temp_difference.csv)).

| ID |	Temperature difference (K) |
| -- |   :---: |
| RGBSYAZ017382	| 443.18 |
| RGBSYAZ021904 | 428.79 |
| RGBSYAZ032345 | 389.33 |
| AGBSYAZ018498 | 337.18 |

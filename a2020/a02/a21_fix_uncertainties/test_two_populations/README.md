# Testing the model with two populations

We test the model by simulating two populations of AGB and RGB stars. Results are shown on Figures 1 and 2. We can see from Fig. 2 that all parameters (r, mu and sigma) agree with simulated exact values within 68% CI.

Code: [a2020/a02/a21_fix_uncertainties/test_two_populations/code/model.py](a2020/a02/a21_fix_uncertainties/test_two_populations/code/model.py)


<img src="a2020/a02/a21_fix_uncertainties/test_two_populations/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions. Dashed vertical lines correspond to the true locations of the populations' mu values.


<img src="a2020/a02/a21_fix_uncertainties/test_two_populations/images/compare_with_exact.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars, compared with exact values.


### Summary of model parameters

Table 1: Summaries of distributions of model parameters and their simulated true values.

|           |                        r |                      mu.1 |                      mu.2 |                    sigma |
|----------:|-------------------------:|--------------------------:|--------------------------:|-------------------------:|
|       AGB | $`0.25^{+0.32}_{-0.15}`$ | $`-1.49^{+0.05}_{-0.05}`$ | $`-1.38^{+0.12}_{-0.05}`$ | $`0.06^{+0.03}_{-0.04}`$ |
|       RGB | $`0.65^{+0.10}_{-0.10}`$ | $`-0.98^{+0.05}_{-0.04}`$ | $`-0.58^{+0.03}_{-0.03}`$ | $`0.03^{+0.02}_{-0.03}`$ |
| AGB exact |                 $`0.25`$ |                 $`-1.50`$ |                 $`-1.30`$ |                 $`0.05`$ |
| RGB exact |                 $`0.75`$ |                 $`-1.00`$ |                 $`-0.60`$ |                 $`0.05`$ |

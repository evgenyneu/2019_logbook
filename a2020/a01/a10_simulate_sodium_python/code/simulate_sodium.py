from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from run_stan import standardize, destandardize, run_stan
from code_dope.python.plot.plot import save_plot
from sample_utils import save_summary, make_tree_plot, TreePlotParams
from sample_utils import save_posterior_plot, InfoPath


def simulate_sample(seed):
    """
    Simulate observations of two populations of stars:
    sodium-poor and sodium-rich stars
    """

    np.random.seed(seed=seed)

    n_poor = 10  # Number of Sodium poor stars
    n_rich = 17  # Number of Sodium rich stars
    n = n_poor + n_rich  # Total sample size of observed stars

    # Generate random values from Normal distributions
    mu_poor = -1.1  # mean of sodium poor population
    mu_rich = -0.6  # mean of sodium rich population
    sigma = 0.05
    samples_poor = norm(mu_poor, sigma).rvs(size=n_poor)
    samples_rich = norm(mu_rich, sigma).rvs(size=n_rich)

    # Combine all samples in one list
    values_true = np.concatenate([samples_poor, samples_rich])

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(0.13, 0.06).rvs(size=n))

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)
    return values_true, values_observed, uncertainties, mu_poor, mu_rich


def plot_simulated_values(values_true, values_observed, uncertainties,
                          mu_poor, mu_rich):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()
    ax1.scatter(values_true, range(0, len(values_true)),
                label='Simulated true', facecolors="none", edgecolors='black')

    ax1.scatter(values_observed, range(0, len(values_true)),
                color="#0000ff44", s=100, label='Observed')

    ax1.errorbar(values_observed, range(0, len(values_true)),
                 xerr=uncertainties,
                 color="#0000ff22", fmt='none')

    ax1.legend()
    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")
    ax2.axvline(mu_poor, color=color_observed, linestyle="dashed")
    ax2.axvline(mu_rich, color=color_observed, linestyle="dashed")

    # Show the plot
    # -------

    fig.suptitle("Simulated true and observed sodium abundances")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix='simulated')


def run_model(values_observed, uncertainties, seed):
    values_std, uncertainties_std = standardize(values_observed, uncertainties)

    data_for_model = {
        'y': values_std,
        'uncertainties': uncertainties_std,
        'N': len(values_std)
    }

    samples_std = run_stan(data_for_model, seed=seed)
    samples = destandardize(samples_std, values_observed)
    return samples


def do_work():
    seed = 333

    values_true, values_observed, uncertainties, \
        mu_poor, mu_rich = simulate_sample(seed=seed)

    plot_simulated_values(
        values_true=values_true,
        values_observed=values_observed,
        uncertainties=uncertainties,
        mu_poor=mu_poor,
        mu_rich=mu_rich)

    samples = run_model(values_observed, uncertainties, seed=seed)

    summary = save_summary(samples)

    # Make tree plot
    # --------

    tree_params = TreePlotParams(
        xlabel="Sodium abundace [Na/H]",
        title="Posterior distributions of parameters"
    )

    info_path = InfoPath(extension="png")

    make_tree_plot(summary['df'], info_path=info_path, tree_params=tree_params)

    # Posterior plot
    save_posterior_plot(samples, summary['df'], info_path=info_path)


if __name__ == '__main__':
    do_work()
    print('We are done')

# Helper functions for dealing with plots

import os
import inspect
import numpy as np


def save_plot(plt, suffix=None, extensions=['png'], subdir='plots', dpi=300):
    """
    Saves a plot to an image file. The name of the
    the image file is constructed from file name of the python script
    that called `plot_to_image` with an added `suffix`.

    The plot is saved to a

    Parameters
    -----------

    plt :
        Matplotlib's plot object

    suffix : str
        File name suffix for the output image file name.
        No suffix is used if None.

    extensions : list of str
        The output image file extensions which will be used to save the plot.

    subdir : str
        Directory where the plot will be placed.
    """

    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__

    this_dir = os.path.dirname(codefile)
    plot_dir = os.path.join(this_dir, subdir)
    os.makedirs(plot_dir, exist_ok=True)
    code_file_without_extension = os.path.basename(codefile).rsplit('.', 1)[0]

    if suffix is None:
        suffix = ''
    else:
        suffix = f'_{suffix}'

    for extension in extensions:
        filename = f'{code_file_without_extension}{suffix}.{extension}'
        figure_path = os.path.join(plot_dir, filename)
        plt.savefig(figure_path, dpi=dpi)


def kde_with_uncerts(eval_points, values, uncertainties):
    """
    Computer KDE (kernel density estimator) using values and their
    uncertianties by adding PDFs of Normal distributions at each value that
    have mean equal to the value and standard deviation equal to value's
    uncertainty.

    Parameters
    ----------

    eval_points: list or numpy.ndarray
        Value at which KDE will be evaluated.
    values: list or numpy.ndarray
        Values for computing KDE.
    uncertainties: list or numpy.ndarray
        Corresponding uncertainties for the `values`.

    Returns
    -------
    numpy.ndarray
        Values of the KDE corresponding to `eval_points`.
    """

    x = np.array(eval_points)
    mu = np.array(values)
    sigma = np.array(uncertainties)

    if mu.shape[0] == 0:
        return np.array([])

    if mu.shape[0] != sigma.shape[0]:
        raise ValueError('Values and uncertainties\
are lists of different lengths')

    guassian = -0.5 * ((x[:, np.newaxis] - mu[np.newaxis, :]) / sigma)**2
    gaussian = np.exp(guassian)
    gaussian /= np.sqrt(2 * np.pi) * sigma
    return gaussian.sum(axis=1) / len(values)

import os

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def do_work():
    """Main function that does all the work"""

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte",
        study_name="NGC 6752 LTE Campbell et al. 2017",
        xlabel="Sodium abundance A(Na) LTE"
    )

    data_path = os.path.join(settings.path, "data/2017_09_campbell_ngc_6752.csv")
    type_name = "Type"
    abundance_name = "A(Na)_lte"
    uncertainty_name = "e_A(Na)_lte"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

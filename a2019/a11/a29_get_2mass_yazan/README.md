## Plot color-magnitude diagram

We plot color-magnitude diagram of all observed stars.


Code: [plot_cmd.py](plot_cmd.py)

```
python -m a2019.a11.a29_get_2mass_yazan.plot_cmd
```

## Download 2MASS catalog

Use Aladin to download 2MASS catalog for our stars:

* 2MASS All-Sky Catalog of Point Sources : II/246

* CDS/II/246/out

[data/ngc288_2mass.csv](data/ngc288_2mass.csv)


## Download Yazan's catalog

Simon emailed catalog from Yazan Momany. We need it to get uncertainties for U, V, B magnitudes.

[data/NGC288_yazan_from_simon.txt](data/NGC288_yazan_from_simon.txt)


### Add column headers

Yazan's catalog did not have column headers. Entering them manually.

[data/ngc288_yazan.csv](data/ngc288_yazan.csv)

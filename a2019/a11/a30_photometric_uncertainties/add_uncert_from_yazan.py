# Add uncertainties for V,B,I,U numbers from Yazan's catalog

import pandas as pd
import os


def combine():
    """
    Add uncertainties for V,B,I,U numbers from Yazan's catalog
    """

    data_dir = 'a2019/a11/a30_photometric_uncertainties/data/'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    stars_df = pd.read_csv(stars_path)

    # Open yazan data
    yazan_path = os.path.join(data_dir, 'ngc288_yazan.csv')
    yazan_df = pd.read_csv(yazan_path)

    # Add new columns
    columns = ['V', 'B', 'I', 'U']

    for column in columns:
        stars_df.insert(loc=stars_df.columns.get_loc(column) + 1,
                        column=f'u_{column}', value='')

    selected_df = stars_df.loc[(stars_df['Cat'] == 'YAZ')]

    # Go over rows
    for index, star in selected_df.iterrows():
        star_id = star['id']
        yazan_id = star['ID_Phot']

        star_yazan = yazan_df.loc[(yazan_df['ID'] == yazan_id)]

        if star_yazan.shape[0] != 1:
            raise ValueError(f"Can't find star {star_id}")

        for column in columns:
            uncert_column = f'u_{column}'
            uncertainty = star_yazan[uncert_column].values[0]
            stars_df.loc[index, uncert_column] = uncertainty

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_uncert.csv')
    stars_df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    combine()
    print('We are done!')

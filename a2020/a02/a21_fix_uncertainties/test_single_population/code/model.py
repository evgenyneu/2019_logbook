import os
from a2020.a02.a21_fix_uncertainties.code.synthetic_data import run_model

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    AnalysisSettings)


if __name__ == '__main__':
    print("Running the models...")

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/test_single_population",
        study_name="simulated data with single population",
        xlabel="Sodium abundance [Na/H]"
    )

    params_path = os.path.join(settings.path, "code/model_params.yaml")
    run_model(settings=settings, params_path=params_path, seed=333)

    print('We are done')

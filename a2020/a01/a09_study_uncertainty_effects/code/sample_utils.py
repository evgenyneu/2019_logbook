import numpy as np
from tabulate import tabulate
import arviz as az
import matplotlib.pyplot as plt
import pandas as pd
import math


def sample_summary(df, show=True):
    """
    Prints table showing statistical summary from the sample parameters:
    mean, std, mode, hpdi.

    Returns
    -------

    Panda's dataframe containing the summary for all parameters.
    """
    rows = []

    for column in df:
        values = df[column].to_numpy()
        mean = round(df[column].mean(), 2)
        std = round(df[column].std(), 2)
        hpdi5 = np.round(az.hpd(values, credible_interval=0.05), 2)
        mode = hpdi5[0] + (hpdi5[1] - hpdi5[0]) / 2
        hpdi68 = np.round(az.hpd(values, credible_interval=0.68), 2).tolist()
        uncert_plus = hpdi68[1] - mode
        uncert_minus = mode - hpdi68[0]
        hpdi95 = np.round(az.hpd(values, credible_interval=0.95), 2).tolist()
        rows.append([column, mean, std, mode, uncert_plus,
                     uncert_minus, hpdi68, hpdi95])

    headers = ['Name', 'Mean', 'Std', 'Mode', '+', '-',
               '68% HPDI', '95% HPDI']

    if show:
        table = tabulate(rows, headers=headers, floatfmt=".2f", tablefmt="pipe")
        print(table)

    return pd.DataFrame(rows, columns=headers)


def tree_plot(groups, group_height=None, error_bar_cap_size=0.08,
              markersize=50,
              ylim=None, markers=None,
              marker_line_width=1,
              marker_colors=None, marker_edge_colors=None,
              error_bar_colors=None, labels=None,
              title=None):
    """
    Make a tree plot of parameters.

    Parameters
    -----------

    group_height : float

        The height of each group, a unmber between 0 and 1.
    """

    if markers is None:
        markers = ['x', 'o', 'v', 'D', 'X', '1', '<', '>']

    if marker_colors is None:
        marker_colors = ['#FF9922', '#009933', '#8888FF',
                         '#BBBB11', '#3399AA', '#33BB11',
                         '#330077', '#BBAA22', '#55FFBB']

    if marker_edge_colors is None:
        marker_edge_colors = ['#994400', '#003300', '#111177',
                              '#888800', '#003355', '#008800',
                              '#000022', '#776600', '#008866']

    if error_bar_colors is None:
        error_bar_colors = ["#0033BB33", '#7755FF55', '#22FF9955']

    fig, ax = plt.subplots()

    for i_group, group in enumerate(groups):
        elements_in_group = len(group['values'])

        this_group_height = group_height

        if this_group_height is None:
            # Determine group height automatically
            x = len(group['values']) - 1
            this_group_height = 1 - math.exp(-x / 5)

        # Vertical separation between values in one group
        if len(group['values']) == 1:
            y_increment = 0
            this_group_height = 0
        else:
            y_increment = 1 / (elements_in_group - 1) * this_group_height

        # Plot the values for the group
        for i_value, value in enumerate(group['values']):
            y_coord = i_group + this_group_height/2 - y_increment * i_value

            value_label = '_nolegend_'

            if labels is not None and i_group == 0:
                value_label = labels[i_value]

            ax.scatter(value["position"], y_coord,
                        marker=markers[i_value], s=markersize,
                        facecolor=marker_colors[i_value],
                        edgecolor=marker_edge_colors[i_value],
                        linewidth=marker_line_width, zorder=5,
                        label=value_label)

            # Plot error bars
            n_error_bars = len(value["error_bars"])

            for i_error_bar, error_bar in enumerate(value["error_bars"]):
                color = error_bar_colors[i_error_bar]

                if i_error_bar == n_error_bars - 1:
                    color = marker_colors[i_value]

                ax.plot(error_bar, [y_coord, y_coord], zorder=i_error_bar + 1,
                         color=color)

                # Plot caps
                ax.plot([error_bar[0], error_bar[0]],
                         [y_coord - error_bar_cap_size/2,
                          y_coord + error_bar_cap_size/2],
                         zorder=i_error_bar + 1,
                         color=color)

                ax.plot([error_bar[1], error_bar[1]],
                         [y_coord - error_bar_cap_size/2,
                          y_coord + error_bar_cap_size/2],
                         zorder=i_error_bar + 1,
                         color=color)

    group_names = [group['name'] for group in groups]

    # Plot vertical line around zero if neeeded
    if ax.get_ylim()[0] < 0 and ax.get_ylim()[1] > 1:
        ax.axvline(x=0, linestyle='dashed')

    ax.set_yticks(range(0, len(groups)))
    ax.set_yticklabels(group_names)
    ax.grid(axis='x', zorder=-10)

    if labels is not None:
        ax.legend()

    if title is not None:
        ax.set_title(title)

    fig.tight_layout()

    if ylim is not None:
        ax.set_ylim(ylim)

    return (fig, ax)


def extract_tree_plot_data(df, groups):
    """
    Extract data used to for tree plot function from a dataframe.

    Parameters
    -----------

    df : Panda's data frame
        Each column is one parameter and it contains sample values from Stan.

    groups : list
        Tree plot data. If passed, the data frame's data will be added to it.
    """

    summary = sample_summary(df, show=False)

    for column_name in df:
        column_summary = summary[summary['Name'] == column_name].iloc[0]
        group_data = None

        # Check if `groups` already has the column
        for group in groups:
            if group['name'] == column_name:
                group_data = group
                break

        # Have not found group data - create one
        if group_data is None:
            group_data = {
                'name': column_name,
                'values': []
            }

            groups.append(group_data)

        # Add new value

        value = {}
        value['position'] = column_summary["Mode"]
        group_data['values'].append(value)

        value['error_bars'] = [
            column_summary["95% HPDI"],
            column_summary["68% HPDI"]
        ]

    return groups

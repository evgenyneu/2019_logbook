data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed value
  real uncertainties[N]; // uncertainties
  real mu1_prior;        // prior for mu of mu1
  real mu2_prior;        // prior for mu of mu2
  real mu_sigma_prior;   // prior for sigma of mu1 and mu2
  real sigma_prior;      // prior for sigmas of two populations
}
parameters {
  vector[N] y_true;          // true value
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  real<lower=0> sigma;       // spread of mixture components
}
model {
  sigma ~ normal(0, sigma_prior);
  mu[1] ~ normal(mu1_prior, mu_sigma_prior);
  mu[2] ~ normal(mu2_prior, mu_sigma_prior);
  p ~ beta(2, 2);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y_true[n] | mu[1], sigma),
                      normal_lpdf(y_true[n] | mu[2], sigma));
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}

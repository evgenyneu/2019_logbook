import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a06_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions_is_comoving.csv')
    df = pd.read_csv(stars_path)

    columns = ['pm_ra', 'u_pm_ra', 'pm_de', 'u_pm_de']

    for column in columns:
        df = df[df[column] != ' ']  # Remove rows with missing uncertainties
        df[column] = pd.to_numeric(df[column])

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    pm_ra = stars_other_df['pm_ra']
    pm_de = stars_other_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='black', marker='o', s=7, edgecolors='black',
                label='Excluded')


    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    pm_ra = stars_agb_df['pm_ra']
    pm_de = stars_agb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    pm_ra = stars_rgb_df['pm_ra']
    pm_de = stars_rgb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    plt.axis('equal')
    plt.xlim(4, 4.4)
    plt.ylim(-6.8, -4.5)

    plt.legend()
    plt.xlabel(r'Proper motion in right ascension $\mu_\alpha \ \cos \delta$ [mas $a^{-1}$]')
    plt.ylabel('Proper motion in declination $\mu_\delta$ [mas $a^{-1}$]')
    plt.title('Proper motions of all observed stars in NGC288')

    # Descriptive statistics for selected stars
    # -----------

    selected_df = df.loc[df['selected'] == 1]

    pm_ra_mean = selected_df['pm_ra'].mean()
    pm_ra_std = selected_df['pm_ra'].std()
    print(f'Proper motion RA={pm_ra_mean:.3f}±{pm_ra_std:.3f}')

    pm_de_mean = selected_df['pm_de'].mean()
    pm_de_std = selected_df['pm_de'].std()
    print(f'Proper motion DE={pm_de_mean:.3f}±{pm_de_std:.3f}')

    # Draw a circle with radius
    radius = 0.25
    circle = plt.Circle((pm_ra_mean, pm_de_mean), radius, color='#7181ff',
                        fill=False, linewidth=1)

    ax = plt.gca()
    ax.add_artist(circle)

    # Error bars
    # --------------

    selected_df = df.loc[(df['selected'] == 1)]

    pm_ra = selected_df['pm_ra']
    pm_de = selected_df['pm_de']

    u_pm_ra = selected_df['u_pm_ra']
    u_pm_de = selected_df['u_pm_de']

    max_u_ra = u_pm_ra.max()
    max_u_de = u_pm_de.max()
    print(f"X error: mean u(pmRA) = {max_u_ra}")
    print(f"Y error: mean u(pmDE) = {max_u_de}")

    plt.errorbar(x=[5.5], y=[-6.6],
                 xerr=[max_u_ra], yerr=[max_u_de],
                 fmt='none', color='black', capsize=2,
                 ecolor='black', eflinewidth=1)

    # --------------

    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    save_plot(plt=plt, suffix="3")
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

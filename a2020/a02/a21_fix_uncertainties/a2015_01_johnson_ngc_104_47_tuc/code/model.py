import os
import pandas as pd

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def agb_data(data_dir, df_result):
    data_path = os.path.join(data_dir, "agb_stars_2015_01_johnson_ngc_104_47_tuc.txt")
    df = pd.read_table(data_path)

    for index, star in df.iterrows():
        star_id = star["Star ID"]
        star_type = 'AGB'
        na = star["[Na/Fe]"]
        e_na = star["[Na/Fe] Error"]

        if float(e_na) == 0:
            print(f"Zero Na uncertainty for star ID={star_id}")
            e_na = 0.01

        df_result.loc[star_id] = [star_type, na, e_na]


def rgb_data(data_dir, df_result):
    data_path = os.path.join(data_dir, "rgb_stars_2014_cordero_ngc_104_47_tuc.txt")
    df = pd.read_table(data_path, delim_whitespace=True)

    for index, star in df.iterrows():
        star_id = star["id"]
        instrument = star["Instrument"]

        if "FLAMES" not in instrument:
            # Only interested in FLAMES instrument
            continue

        star_type = 'RGB'
        na = star["[Na/Fe]"]
        e_na = 0.14
        df_result.loc[star_id] = [star_type, na, e_na]


def convert_data_to_csv(data_dir, csv_name):
    df_result = pd.DataFrame(columns=["Type", "[Na/Fe]", "e_[Na/Fe]"])
    agb_data(data_dir=data_dir, df_result=df_result)
    rgb_data(data_dir=data_dir, df_result=df_result)

    csv_path = os.path.join(data_dir, csv_name)
    df_result.to_csv(csv_path, index_label="ID")


def do_work():
    """Main function that does all the work"""

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2015_01_johnson_ngc_104_47_tuc",
        study_name="NGC 104 (47 Tuc) Johnson et al. 2015",
        xlabel="Sodium abundance [Na/Fe]"
    )

    data_dir = data_path = os.path.join(settings.path, "data")
    csv_name = "johnson_cordero_ngc_104_47_tuc.csv"
    convert_data_to_csv(data_dir=data_dir, csv_name=csv_name)
    data_path = os.path.join(data_dir, csv_name)
    type_name = "Type"
    abundance_name = "[Na/Fe]"
    uncertainty_name = "e_[Na/Fe]"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

# Plot observed vs 'true' sodium abundances
# estimated by Stan model
# -----------------

library(rstan)
library(rethinking)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

seed_value = 333
set.seed(seed_value)

source("a2020/a01/a03_plot_true_vs_observed/code/calculate_sodium_ratio.r")

# Load observed abundaces
data = load_data()

model_name = 'a2020/a01/a03_plot_true_vs_observed/data/'
model_name = paste(model_name, 'rgb_', seed_value, '.rds', sep='')

if (file.exists(model_name)) {
    samples_rgb <- readRDS(model_name)
} else {
    samples_rgb = run_model(data, 'RGB')
    saveRDS(samples_rgb, file=model_name)
}

# Plot observed abundances
# ------

data_rgb = data[data$type == 'RGB',]
data_agb = data[data$type == 'AGB',]

color_observed = '#0000FF88'
color_true = 'black'
pch_observed = 16
pch_true = 1

plot(data_rgb$na_over_h, 1:length(data_rgb$na_over_h),
    cex=2, pch=pch_observed, col=color_observed,
    xlab="Sodium abundance [Na/H]",
    ylab="Star number",
    main="Observed and estimated 'true' sodium abundances")

# Plot centers of two populations
abline(v=mean(samples_rgb$mu1), lty=2, col=color_observed)
abline(v=mean(samples_rgb$mu2), lty=2, col=color_observed)


# Plot estimated 'true' abundances
# ------

rgb.true = apply(samples_rgb$y_true, 2, mean)
points(rgb.true, 1:length(data_rgb$na_over_h), cex=1, lty=2, col=color_true)

legend("topright", legend=c("True", "Observed"),
    col=c(color_true, color_observed),
    pt.cex=c(1,2), pch = c(pch_true, pch_observed))

for (i in 1:length(data_rgb$na_over_h)) {
    lines(c(data_rgb$na_over_h[i], rgb.true[i]), c(i,i))
}

# NGC 288 2MASS data

2MASS data for our stars [see logbook record](a2019/a11/a29_get_2mass_yazan).

### Catalog

* The 2MASS All-Sky Catalog of Point Sources (2003)

* CDS/II/246/out

* https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C/abstract

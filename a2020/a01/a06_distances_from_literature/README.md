# Distance modulus  from literature

Table 1 shows the distance moduli for NGC288 from literature. Using these data we calculated the distances, also shown in Table 1. We also calculated the mean distance to NGC 288 equal: 8800 ± 200 pc.

Table 1: NGC 288 distance moduli from literature and corresponding calculated distances that we calculated.

| m - M | E(B - V) | Distance [kpc] | Source |
| :---:   |  :------:  | :------:  | :---------:|
| 14.9 ± 0.2   | 0.0  | 9.5 ± 0.9| [Penny 1984](http://articles.adsabs.harvard.edu/pdf/1984MNRAS.208..559P)   |
| 14.85 ± 0.10 | 0.04±0.02 | 8.8 ± 0.5 | [Alcaino 1997](http://adsabs.harvard.edu/full/1997AJ....114.2626A)   |
| 14.57 ± 0.07 | 0.018 ± 0.010 | 8.0 ± 0.3 | [Chen 2000](https://iopscience.iop.org/article/10.1086/316808/pdf)   |
| 14.84 ± 0.1  | 0.03 | 8.9 ± 0.4 | [Harris 2010](http://physwww.mcmaster.ca/~harris/mwgc.dat)   |
| 14.768 ± 0.003 | 0.03 | 8.61 ± 0.01 | [Arellano Ferro 2013](https://arxiv.org/pdf/1401.5476.pdf)   |



## Distance calculation method

We calculated the distance using the following method. First, we calculate extinction parameter `A_V`:

```
A_V = R_V * color_excess,
```

where `R_V = 3.1` is total-to-selective extinction ratio parameter used in [Harris 2010](http://physwww.mcmaster.ca/%7Eharris/mwgc.ref), and `color_excess` (also known as excess reddening) is E(B - V).

Finally, we calculate the distance `d` to NGC288 in kpc:

```math
d = (10) \ 10^{((m - M)_V - A_V) / 5}
```

where $`(m - M)_V`$ is visual distance modulus. Note that we assumed that all distance moduli in Table 1 are given for the visual filter V.

Code: [a2020/a01/a06_distances_from_literature/code/calculate_distances.py](a2020/a01/a06_distances_from_literature/code/calculate_distances.py)


## Calculating uncertainties of distance d

The uncertainty of `d` is calculated using the [simplified error propagation method](https://en.wikipedia.org/wiki/Propagation_of_uncertainty):

```math
s_f = \sqrt{\Big(\frac{\partial f}{\partial x}\Big)^2 s^2_x + \Big(\frac{\partial f}{\partial y}\Big)^2 s^2_y},
```

where $`f = f(x, y)`$ is a multivariable function, $`s_f`$ is its uncertainty, and $`s_x`$, $`s_y`$ are uncertainties of the variables.

We also tried an alternative method of calculating uncertainties. We calculated three distances for distance moduli $`m - M`$, $`m - M + s`$ and $`m - M - s`$ where s is the uncertainty of m - M. Then we took the upper uncertainty to be the difference of calculated distances for $`m - M`$ and $`m - M + s`$. Similarly, the lower uncertainty was set to be the difference of calculated distances for $`m - M`$ and $`m - M - s`$. The results were identical to the previous simplified method for all values but one: the distance 8.8 ± 0.5 was instead calculated to be 8.8 ± 0.4. The upper and lower uncertainties were equal to one significant figure. We decided to use the simplified method, because it is standard, easy to communicate to colleagues and it gave nearly the same results as the alternative method.

Note that simplified uncertainty propagation was done automatically by `uncertainties` Python library.


## Mean distance calculation method

We calculate the mean distance by calculating the average of the distances given in Table 1. The uncertainty is calculate by taking the square root of the sum of squares of individual uncertainties and dividing the result by 5 (number of distances in Table 1).

## Distances and metallicities from older literature

There is a nice summary of distances and metallicity for NGC288 from older literature from [Chen 2000](https://iopscience.iop.org/article/10.1086/316808/pdf):

![Chen 2000 summary of NGC288 distance and metallicity from literature](a2020/a01/a06_distances_from_literature/images/chen_table_6_ngc_288.png)

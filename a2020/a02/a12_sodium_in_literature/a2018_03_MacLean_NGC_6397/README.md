# Analysing data from MacLean et al. 2018, NGC 6397

We ran the statistical model using data from [MacLean et al. 2018](https://arxiv.org/abs/1712.03340v1) for NGC 6397 globular cluster.

Code: [a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397/code/model.py](a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397/code/model.py)

Some stars had zero uncertainties for the abundances. We have replaced these uncertainties with `0.01`.


## Observations

<img src="a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances.


## Results


### Parameter distributions

<img src="a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     |                        r |                      mu.1 |                      mu.2 |
|----:|-------------------------:|--------------------------:|--------------------------:|
| AGB | $`0.65^{+0.20}_{-0.27}`$ | $`-2.22^{+0.11}_{-0.08}`$ | $`-1.95^{+0.05}_{-0.06}`$ |
| RGB | $`0.69^{+0.16}_{-0.16}`$ | $`-2.25^{+0.10}_{-0.08}`$ | $`-1.96^{+0.04}_{-0.06}`$ |


### AGB failure rate

We calculated failure rate f=$`0.15^{+0.4}_{-0.4}`$, which is defined as proportion of SP2 stars that fail to reach AGB:

```
f = 1 - r_agb / r_rgb
```



## Analysis

### RGB stars

Distribution of $`r_{rgb}`$ agrees with $`r_{rgb} \sim 0.6`$ calculated by [MacLean et al. 2018](https://arxiv.org/abs/1712.03340v1).


### AGB stars

Distribution of $`r_{agb}`$ agrees with  $`r_{agb} \sim 0.6`$ calculated by [MacLean et al. 2018](https://arxiv.org/abs/1712.03340v1). The data suggest presence of two distinct populations.

### AGB failure rate

The distribution of `f` is shown on Fig. 5 is bimodal. Our point estimate agrees with $`f \sim 0`$ calculated by [MacLean et al. 2018](https://arxiv.org/abs/1712.03340v1), although our `f` is fairly uncertain.

<img src="a2020/a02/a12_sodium_in_literature/a2018_03_MacLean_NGC_6397/images/agb_failure_rate.png" width="700" alt='AGB failure rate'>

Figure 5: Distribution of AGB failure rate.

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def save_temperature():
    data_dir = 'a2019/a12/a03_cmd_gaia/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open temperature data
    temperature_path = os.path.join(data_dir, 'sensitivity.csv')
    temperature_df = pd.read_csv(temperature_path)

    # Insert new columns
    df['t_eff'] = ''
    df['u_t_eff'] = 50

    # Go over rows
    for index, star in df.iterrows():
        star_id = star['id']

        star_from_temperature = temperature_df.loc[
            (temperature_df['Star Name'] == star_id)]

        if star_from_temperature.shape[0] != 1:
            continue

        t_eff = star_from_temperature['Teff (V-K)'].values[0]
        df.loc[index, 't_eff'] = t_eff

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_temperatures.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    save_temperature()
    print('We are done')

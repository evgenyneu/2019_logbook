# Changing measurement uncertainty

I want to see how measurement uncertainty influences the posterior distributions of parameters (p, mu, sigma) and compare it with [previous results](a2020/a01/a05_python_model) that had large uncertainties.

## Running the model without uncertainty

I run the model that does not have measurement uncertainties. It produced distributions of parameters very similar to those done with full uncertainties([previous results](a2020/a01/a05_python_model)), as shown in Fig. 1.

<img src="a2020/a01/a08_changing_measurement_uncertainty/code/plots/sodum_compare_with_no_uncertainties.png" alt='Posterior probability densities for AGB stars'>

Figure 1: Posterior distributions of parameters of the model. The markers are the modes (values of maximum probability). The error bars indicate 68% and 95% HPDI (highest posterior density interval).



Code: [a2020/a01/a08_changing_measurement_uncertainty/code/calculate_sodium_ratio_no_uncertainties.py](a2020/a01/a08_changing_measurement_uncertainty/code/calculate_sodium_ratio_no_uncertainties.py)

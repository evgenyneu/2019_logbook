// Single Gaussian population model
data {
  int<lower=1> n;        // Number of data points
  real y[n];             // Observed values
  real uncertainties[n]; // Uncertainties
  real sigma_stdev_prior;// Prior for standard deviation of population spread
}
parameters {
  real mu;                   // Population location
  real<lower=0> sigma;       // Population spread
  vector[n] y_true;          // Estimated "true" values
}
model {
  // Set priors
  sigma ~ normal(0, sigma_stdev_prior);
  mu ~ normal(0, 1);

  for (i in 1:n) {
    target += normal_lpdf(y_true[i] | mu, sigma);
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}
generated quantities{
  vector[n] lpd_pointwise;

  for (i in 1:n) {
    lpd_pointwise[i] = normal_lpdf(y[i] | y_true[i], uncertainties[i]);
  }
}

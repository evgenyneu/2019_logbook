import pandas as pd
import os


def mark_agb_rgb_stars():
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open data
    dest_path = os.path.join(data_dir, 'ngc288_stars_with_abundances_from_chris.csv')
    df = pd.read_csv(dest_path)

    # Add new column
    column_name = 'agb_or_rgb'
    df.insert(loc=4, column=column_name, value='')

    agb_types = ['AGBS', 'BAGS', 'BAGB']
    rgb_types = ['RGBS', 'BGBS']

    # Go over rows
    for index, star in df.iterrows():
        star_type = star['type']

        if star_type in agb_types:
            agb_or_rgb = 'AGB'
        elif star_type in rgb_types:
            agb_or_rgb = 'RGB'
        else:
            agb_or_rgb = None

        df.loc[index, column_name] = agb_or_rgb

    # Save data frame
    output_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb.csv')

    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    mark_agb_rgb_stars()
    print('We are done!')

from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from run_stan import standardize, destandardize, run_stan
from code_dope.python.plot.plot import save_plot

from sample_utils import (
    save_posterior_plot, InfoPath, make_comparative_tree_plot,
    save_summary, make_tree_plot, TreePlotParams, summary_from_dict)


def simulate_sample(mu_poor, mu_rich, seed):
    """
    Simulate observations of two populations of stars:
    sodium-poor and sodium-rich stars
    """

    np.random.seed(seed=seed)

    n_poor = 10  # Number of Sodium poor stars
    n_rich = 17  # Number of Sodium rich stars
    n = n_poor + n_rich  # Total sample size of observed stars

    # Generate random values from Normal distributions
    sigma = 0.05
    samples_poor = norm(mu_poor, sigma).rvs(size=n_poor)
    samples_rich = norm(mu_rich, sigma).rvs(size=n_rich)

    # Combine all samples in one list
    values_true = np.concatenate([samples_poor, samples_rich])

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(0.13, 0.06).rvs(size=n))

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)

    p_true = n_poor / n

    return (
        values_true, values_observed, uncertainties,
        p_true, sigma, sigma)


def plot_simulated_values(values_true, values_observed, uncertainties,
                          mu_poor, mu_rich, mu_difference):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    color_observed_dark = "#0000ffAA"
    ax1.grid()

    ax1.scatter(values_observed, range(0, len(values_true)),
                color=color_observed, edgecolors=color_observed_dark,
                s=100, label='Observed', zorder=5)

    ax1.errorbar(values_observed, range(0, len(values_true)),
                 xerr=uncertainties,
                 color="#0000ff44", fmt='none', zorder=5)

    ax1.scatter(values_true, range(0, len(values_true)),
                label='Simulated true', facecolors="none",
                edgecolors="#000000FF", zorder=10)

    ax1.legend()
    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")
    ax2.axvline(mu_poor, color=color_observed_dark, linestyle="dashed")
    ax2.axvline(mu_rich, color=color_observed_dark, linestyle="dashed")

    # Show the plot
    # -------

    title = (
        r"Simulated true and observed sodium abundances, $\Delta \mu$="
        f"{mu_difference:.1f} dex"
    )

    fig.suptitle(title)
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix=f"_apart_{mu_difference:.2f}")


def run_model(dir_name, values_observed, uncertainties, seed):
    values_std, uncertainties_std = standardize(values_observed, uncertainties)

    data_for_model = {
        'y': values_std,
        'uncertainties': uncertainties_std,
        'N': len(values_std)
    }

    samples_std = run_stan(data_for_model, dir_name, seed=seed)
    samples = destandardize(samples_std, values_observed)
    return samples


def plot_posterior(dir_name, samples, mu_poor, mu_rich):
    info_path = InfoPath(sub_dir_name=dir_name)
    summary = save_summary(samples, info_path=info_path)

    # Make tree plot
    # --------

    tree_params = TreePlotParams(
        title=f"Posterior distributions of parameters for mu1={mu_poor} and mu2={mu_rich}"
    )

    make_tree_plot(summary['df'], info_path=info_path, tree_params=tree_params)

    # Posterior plot
    save_posterior_plot(samples, summary['df'], info_path=info_path)

    return summary['df']


def simulate(mu_poor, mu_rich, mu_difference):
    seed = 12345

    values_true, values_observed, uncertainties, p_true, sigma1, sigma2 \
        = simulate_sample(
            mu_poor=mu_poor, mu_rich=mu_rich, seed=seed)

    plot_simulated_values(
        values_true=values_true,
        values_observed=values_observed,
        uncertainties=uncertainties,
        mu_poor=mu_poor,
        mu_rich=mu_rich,
        mu_difference=mu_difference)

    samples = run_model(
        f"mu_{mu_difference:.2f}_apart_std", values_observed, uncertainties,
        seed=seed)

    summary = plot_posterior(
        f"mu_{mu_difference:.2f}_apart", samples, mu_poor, mu_rich)

    return (summary, p_true, sigma1, sigma2)


def plot_all(all_summaries, legend_labels):
    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name="compare_all", extension="png")

    title = (
        "Posterior distributions of parameters\n"
        "for different separations between population means"
    )

    tree_params = TreePlotParams(title=title, markersize=30)
    tree_params.labels = legend_labels

    make_comparative_tree_plot(
        all_summaries, info_path=info_path,
        tree_params=tree_params)


def make_zero_separation_plot(summary, true_params):
    all_summaries = []

    all_summaries.append(summary_from_dict(true_params))
    all_summaries.append(summary)

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name="zero_separation", extension="png")

    tree_params = TreePlotParams(
        title=(
            "Posterior distributions for two simulated "
            "population with same means"
            )
        )

    tree_params.labels = [
        "Simulated true",
        "Posterior"
    ]

    make_comparative_tree_plot(
        all_summaries, info_path=info_path,
        tree_params=tree_params)


def do_work():
    # Contains pairs of mu_poor and mu_reach values
    means = [
        [-1.8, -0.6],
        [-1.1, -0.6],
        [-0.8, -0.6],
        [-0.6, -0.6]
    ]

    all_summaries = []
    legend_labels = []

    for i, (mu_poor, mu_rich) in enumerate(means):
        print(f'mu_poor={mu_poor} mu_rich={mu_rich}')
        mu_difference = round(abs(mu_poor - mu_rich), 2)

        summary, p_true, sigma1, sigma2 = simulate(
            mu_poor=mu_poor, mu_rich=mu_rich, mu_difference=mu_difference)

        # Add true values
        # ------------

        true_params = {
            'p': p_true,
            'mu.1': mu_poor,
            'mu.2': mu_rich,
            'sigma.1': sigma1,
            'sigma.2': sigma2,
        }

        all_summaries.append(summary_from_dict(true_params))

        label_values = (
            "$\Delta \mu$="
            f"{mu_difference:.1f}"
        )

        legend_labels.append(f"True {label_values}")

        all_summaries.append(summary)
        legend_labels.append(f"Posterior {label_values}")

        # Make a saparate plot for zero separation
        if mu_difference == 0:
            make_zero_separation_plot(summary, true_params)

    plot_all(all_summaries, legend_labels=legend_labels)


if __name__ == '__main__':
    do_work()
    print('We are done')

## Calculating distance uncertainties with Monte Carlo

I use Monte Carlo method to calculate uncertainties of the distance, given distance modulus (m - M) and color excess E(B - V) measurements from the literature (see Table 1 [from yesterday](a2020/a01/a06_distances_from_literature)). The calculated uncertainties were the same to one significant figure as we calculated yesterday using the standard uncertainty propagation method, shown in [Table 1](a2020/a01/a06_distances_from_literature).

Code: [a2020/a01/a07_distance_uncertainties_monte_carlo/code/distance_uncertainties_monte_carlo.py](a2020/a01/a07_distance_uncertainties_monte_carlo/code/distance_uncertainties_monte_carlo.py)

## Uncertainties calculation method

The method is best explained with an example. Suppose we have parameters:

```
(m - M) = 14.85 ± 0.1
E(B - V)  = 0.04 ± 0.02
```

The distance function depends on two parameters: `(m - M)` (distance modulus) and `E(B - V)` (color excess). First, we draw 1000 random samples from normal distributions for both parameters:

```python
from scipy.stats import norm

samples_distance_modulus = norm(14.85, 0.1).rvs(size=1000)
samples_color_excess = norm(0.04, 0.02).rvs(size=1000)
```

And then we calculate distances using the numbers from the two samples:

```python
from a2019.a12.a10_calculate_distance.code.distance import get_distance

distances = [
                get_distance(distance_mudulus, color_excess)
                for distance_mudulus, color_excess
                in zip(samples_distance_modulus, samples_color_excess)
            ]
```

The `distances` array will contain the distribution of distances. To find the uncertainty, we calculate the standard deviation of distances:

```python
import numpy as np

distance_uncertainty = np.std(distances)
```

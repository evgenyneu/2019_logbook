# Testing vectorised `log_mix` function

Compares vectorised version of `log_mix` function

```stan
target += log_mix(p,
                  normal_lpdf(y | y_mix1, uncertainties),
                  normal_lpdf(y | y_mix2, uncertainties));
```

with the non-vectorized one:

```stan
for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y[n] | y_mix1[n], uncertainties[n]),
                      normal_lpdf(y[n] | y_mix2[n], uncertainties[n]));
}
```

## Usage

Run

```
python gaussian_mixture.py
```

The script will create plots and summaries in `model_info` directory.

# Compare one and two-gaussian models for AGB data for NGC 6752

Here we compare one and two-gaussian models using sodium abundances for AGB stars from NGC 6752 cluster from difference sources. Our goal is to infer whether there is one or two populations of AGB stars in this globular cluster, **according to our models**. Results are shown in Table 1.

Table 1: Results of model comparison for AGB stars of NGC 6752

| Data source   | Conclusion  |
|:-------------:|:-----------:|
| [Campbell et al. 2013](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752) | No model preference. |
| [Lapenna et al. 2016](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752) | Preference for two-gaussian model. |
| [Campbell et al. 2017 LTE](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte) | No model preference. |
| [Campbell et al. 2017 NLTE](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte) | No model preference. |
| [Synthesised single population](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_one_population) | No model preference. |
| [Synthesised two populations](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations) | Very strong preference for two-gaussian model. |

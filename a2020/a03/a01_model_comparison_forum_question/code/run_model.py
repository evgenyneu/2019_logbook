import pandas as pd
from cmdstanpy import CmdStanModel
from tabulate import tabulate
import scipy.stats as stats
import numpy as np
import os
from dataclasses import dataclass
from tarpan.cmdstanpy.analyse import save_analysis
from tarpan.cmdstanpy.cache import run
from tarpan.shared.info_path import InfoPath, get_info_path
from tarpan.plot.kde import save_scatter_and_kde

from tarpan.plot.posterior import (
    save_posterior_scatter_and_kde, PosteriorKdeParams)


# Parameters for data analysys
@dataclass
class AnalysisSettings:
    # Path to directory where the data directory `dir_name` will be created.
    path: str

    # Name of the study to be shown in the plot title.
    study_name: str

    # X-label of the plot of observed data.
    xlabel: str

    # Name of the parent directory where the plots will be created.
    dir_name: str = "model_info"

    # [function(fig, axes, params), params]
    #     function:
    #         A function that can be used to add extra information to the
    #         scatter plot of observations before it is saved.
    #
    #         Parameters
    #         ----------
    #
    #         fig: Matplotlib's figure object
    #         axes: list of Matplotlib's axes objects
    #         params: custom parameters that are passed to the function
    #     params: parameters that will be passed to the function
    observations_plot_fn = None

    # function(x, row)
    #     function:
    #        A functino to plot posterior distribution
    posterior_pdf = None

    # [function(fig, axes, params), params]
    #     function:
    #         A function that can be used to add extra information to the
    #         posterior plot before it is saved. Same parameters
    #         as observations_plot_fn.
    posterior_plot_fn = None

    # Sodium abundances and their uncertainties for analysis.
    values_agb = None
    uncert_agb = None
    values_rgb = None
    uncert_rgb = None

    # Path to the .stan model file
    stan_model_path: str = "a2020/a02/a21_fix_uncertainties/code/stan_model/model_std.stan"

    # Prior for standard deviation of the population spread,
    # in measurement units (i.e. non-stsndardised).
    sigma_stdev_prior: float = 0.05

    # Stan's sampling parameter
    max_treedepth: float = 10

    # Location of plots and summaries
    info_path: InfoPath = None


def load_data(type, data_path, type_name, abundance_name, uncertainty_name):
    """
    Load sodium abundaces and their uncertainties

    Parameters
    ----------
    type : str
        Star type: RGB / RGB

    data_path : str
        Path to the CSV file.

    type_name, abundance_name, uncertainty_name : str
        Names of the columns in the CSV file

    Returns
    -------
    numpy.ndarray:
        Observed sodium abundance values.
    numpy.ndarray:
        Corresponding uncertainties for the observed values.
    """
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df[type_name] == type]

    return (df[abundance_name].to_numpy(), df[uncertainty_name].to_numpy())


def standardise(measurements):
    """
    Converts an array to standardised values
    (i.e. z-scores, values minus their means divided by standard deviation)

    Parameters
    ----------

    measurements: list of float
        Values to standardise

    Returns
    --------
    np.array:
        Standardised values.
    """
    measurements = np.array(measurements)
    mean = measurements.mean()
    std = measurements.std()

    return (measurements - mean) / std


def standardised_data_for_stan(measurements, uncertainties,
                               sigma_stdev_prior):
    """
    Returns data for the model.

    Parameters
    ----------

    measurements: list of float
        Measurements.
    uncertainties: list of float
        Measurement uncertainties.
    sigma_stdev_prior: float
        Prior for standard deviation of the population spread,
        in measurement units (i.e. non-stsndardised).

    Returns
    -------

    dict:
        Data that is supplied to Stan model.
    """

    measurements = np.array(measurements)
    std = measurements.std()
    measurements_std = standardise(measurements=measurements)
    uncertainties_std = uncertainties / std
    sigma_stdev_prior_std = sigma_stdev_prior / std

    return {
        "y": measurements_std,
        "uncertainties": uncertainties_std,
        "n": len(measurements_std),
        "sigma_stdev_prior": sigma_stdev_prior_std
    }


def destandardise_stan_output(fit, measurements):
    """
    Transform Stan output values from standardised form to the scale
    of observations.

    Parameters
    ----------

    fit: cmdstanpy.stanfit.CmdStanMCMC
        Stan'd output to standardise. The function transforms `fit.sample`
        array from standard form.

    measurements: list of float
        Measurements, non-standardised.
    """
    measurements = np.array(measurements)
    mean = measurements.mean()
    std = measurements.std()

    # Destandardise 'mu' parameters: 'mu.1', 'mu.2' etc.
    # -----------

    mu_columns = [name for name in fit.column_names if name.startswith('mu')]

    for column_name in mu_columns:
        column_id = fit.column_names.index(column_name)

        for chain_id in range(fit.chains):
            values = fit.sample[:, chain_id, column_id]
            fit.sample[:, chain_id, column_id] = values * std + mean


    # De-standardise `sigma`
    # ----------

    column_id = fit.column_names.index('sigma')

    for chain_id in range(fit.chains):
        values = fit.sample[:, chain_id, column_id]
        fit.sample[:, chain_id, column_id] = values * std


def run_stan(observed_values, uncertainties, output_dir,
             settings: AnalysisSettings):
    """
    Run Stan model and return the samples from posterior distributions.

    Parameters
    ----------
    observed_values: list
        Observed sodium abundance values.
    uncertainties: list
        Corresponding uncertainties for the observed values.
    mu_prior, sigma_prior : float
        Priors for the mean and standard deviations of two populations.

    Returns
    -------
    cmdstanpy.CmdStanMCMC
        Stan's output containing samples of posterior distribution
        of parameters.
    """

    data = standardised_data_for_stan(
        measurements=observed_values,
        uncertainties=uncertainties,
        sigma_stdev_prior=settings.sigma_stdev_prior)

    df_standard = pd.DataFrame(data)

    normalised_csv_path = os.path.join(
        settings.path, settings.dir_name, "normalised.csv")

    df_standard.to_csv(normalised_csv_path)

    model = CmdStanModel(stan_file=settings.stan_model_path)

    fit = model.sample(
        data=data, seed=333,
        adapt_delta=0.99, max_treedepth=settings.max_treedepth,
        sampling_iters=4000, warmup_iters=1000,
        chains=4, cores=4,
        show_progress=True,
        output_dir=output_dir)

    destandardise_stan_output(fit=fit, measurements=observed_values)

    # Make summaries and plots of parameter distributions
    save_analysis(fit, param_names=["r", "mu", "sigma"],
                  info_path=settings.info_path)

    return fit


def run_model(star_type, observed_values, uncertainties,
              settings: AnalysisSettings):
    """
    Run Stan to generate samples for the model and make plots
    and summaries of the results.

    Parameters
    ----------
    star_type: str
        Stat type: 'RGB' or 'AGB'

    observed_values : float
    uncertainties : float
        The values and corresponding uncertainties.

    Returns
    -------
    cmdstanpy.CmdStanMCMC
        Stan's output containing samples of posterior distribution
        of parameters.
    """
    info_path = InfoPath(path=settings.path,
                         dir_name=settings.dir_name,
                         sub_dir_name=star_type.lower())

    settings.info_path = info_path

    fit = run(info_path=info_path, func=run_stan,
              observed_values=observed_values, uncertainties=uncertainties,
              settings=settings)

    return fit


def proportion_positive_samples(df_delta, path, dir_name):
    """
    Calculate the proportino of samples for which parameters r, mu.1 and mu.2
    are greater in RGB stars.

    The result is saved in a text file.

    Parameters
    ----------

    df_delta : Pandas' dataframe
        Difference between RGB and AGB samples for r and mu parameters.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    n = df_delta["mu.1"].count()
    delta_r_positive = df_delta[df_delta["r"] > 0]["r"].count() / n
    delta_mu1_positive = df_delta[df_delta["mu.1"] > 0]["mu.1"].count() / n
    delta_mu2_positive = df_delta[df_delta["mu.2"] > 0]["mu.2"].count() / n

    table_data = [
        ["Number of samples", n],
        ["Delta r > 0", delta_r_positive],
        ["Delta mu 1 > 0", delta_mu1_positive],
        ["Delta mu 2 > 0", delta_mu2_positive],
    ]

    txt_table = tabulate(table_data,
                         headers=["Name", "Value"],
                         floatfmt=".2f", tablefmt="pipe")

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="compare_agb_rgb_parameters",
                         extension="txt")

    with open(get_info_path(info_path), "w") as text_file:
        print(txt_table, file=text_file)


def model_pdf_population_1(x, row):
    """
    Single population model. Calculate distribution from single posterior sample.
    """

    mu = row['mu']
    sigma = row['sigma']

    return stats.norm.pdf(x, mu, sigma)


def model_pdf_population_2(x, row):
    """
    Two population model. Calculate distribution from single posterior sample.
    """

    mu1 = row['mu.1']
    mu2 = row['mu.2']
    sigma = row['sigma']
    r = row['r']

    return (1 - r) * stats.norm.pdf(x, mu1, sigma) + \
        r * stats.norm.pdf(x, mu2, sigma)


def do_analysis(settings: AnalysisSettings):
    """
    Analyse the sodium abundances and create plots and summaries

    Parameters
    ----------

    settings: AnalysisSettings
        Setting used in analysis.
    """

    print(f'\n\n-----------------------------')
    print(f'Analysing {settings.study_name}...')

    # Plot observations
    # -----------

    info_path = InfoPath(path=settings.path, dir_name=settings.dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="observed_combined",
                         extension="png")

    save_scatter_and_kde(
        values=[settings.values_agb],
        uncertainties=[settings.uncert_agb],
        title=f"Abundances from {settings.study_name}",
        xlabel=settings.xlabel,
        ylabel=["Star number", "Probability density"],
        info_path=info_path,
        plot_fn=settings.observations_plot_fn)

    # Run Stan
    # ----------------

    fit_agb = run_model('AGB',
                        observed_values=settings.values_agb,
                        uncertainties=settings.uncert_agb,
                        settings=settings)

    info_path = InfoPath(path=settings.path, dir_name=settings.dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="posterior",
                         extension="png")

    ploterior_params = PosteriorKdeParams(
        kde_plot_count=50,
        kde_edgecolors=["#0060ff15", '#ff002115']
    )

    save_posterior_scatter_and_kde(
        fits=[fit_agb],
        pdf=settings.posterior_pdf,
        values=[settings.values_agb],
        uncertainties=[settings.uncert_agb],
        title=f"Observed/posterior, {settings.study_name}",
        xlabel=settings.xlabel,
        ylabel=["Star number", "Probability density"],
        info_path=info_path,
        plot_fn=settings.posterior_plot_fn,
        posterior_kde_params=ploterior_params)

    return fit_agb

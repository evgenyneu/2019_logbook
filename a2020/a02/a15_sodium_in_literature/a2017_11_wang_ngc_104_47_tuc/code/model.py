import os
import pandas as pd
from uncertainties import ufloat

from a2020.a02.a15_sodium_in_literature.code.run_model import (
    do_analysis, load_data)


def convert_data_to_csv(data_dir, csv_name):
    df_result = pd.DataFrame(columns=["Type", "[Na/H]", "e_[Na/H]"])

    data_path = os.path.join(data_dir, "table7.dat.txt")

    header = ["NGC", "StarID", "EvolPh", "[Na/H]LTE", "[Na/H]NLTE",
              "[Na/FeI]NLTE"]

    df = pd.read_table(data_path, names=header, delim_whitespace=True)

    df = df[df["NGC"] == 104]
    df = df[df["[Na/H]LTE"].notna()]

    for index, star in df.iterrows():
        star_id = star["StarID"]
        star_type = star["EvolPh"]
        na = star["[Na/H]LTE"]
        e_na = 0.13
        df_result.loc[star_id] = [star_type, na, e_na]

    csv_path = os.path.join(data_dir, csv_name)
    df_result.to_csv(csv_path, index_label="ID")


def calculate_f():
    f = 1 - ufloat(0.40, 0.08) / ufloat(0.67, 0.10)
    print(f"f={f}")


def do_work():
    """Main function that does all the work"""

    calculate_f()
    root_path = "a2020/a02/a15_sodium_in_literature/a2017_11_wang_ngc_104_47_tuc"
    data_dir = os.path.join(root_path, "data")
    csv_name = "2017_11_wang_ngc_104_47_tuc.csv"
    convert_data_to_csv(data_dir=data_dir, csv_name=csv_name)
    data_path = os.path.join(data_dir, csv_name)
    type_name = "Type"
    abundance_name = "[Na/H]"
    uncertainty_name = "e_[Na/H]"

    values_agb, uncert_agb = load_data('AGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    values_rgb, uncert_rgb = load_data('RGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    do_analysis(values_agb=values_agb, uncert_agb=uncert_agb,
                values_rgb=values_rgb, uncert_rgb=uncert_rgb,
                path=root_path,
                dir_name="model_info",
                study_name="NGC 104 (47 Tuc) Wang et al. 2017 LTE",
                xlabel="Sodium abundance [Na/H]")


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

import os
import shutil
from paper.model_std.code.model import do_work


def test_do_work():
    """Integration test that runs entire analysis"""

    outdir = "paper/model_std/test_output"

    if os.path.isdir(outdir):
        shutil.rmtree(outdir)

    do_work(dir_name="test_output")

    assert os.path.isfile(os.path.join(outdir, "observed_combined.pdf"))

    path_txt = os.path.join(outdir, "compare_agb_rgb_parameters.txt")
    assert os.path.isfile(path_txt)

    shutil.rmtree(outdir)

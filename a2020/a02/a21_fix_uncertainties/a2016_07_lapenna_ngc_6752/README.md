# Analysing data from [Lapenna et al. 2016](https://ui.adsabs.harvard.edu/abs/2016ApJ...826L...1L/abstract), NGC 6752

The authors measure [Na/Fe] abundances for AGB stars stars only.  Thus, we use RGB measurements from [Campbell et al. 2013](https://arxiv.org/abs/1305.7090). Lapenna et al. 2017 measures AGB abundances for the same stars as Campbell at al. 2013.


Code: [a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752/code/model.py](a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2016_07_lapenna_ngc_6752/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.65^{+0.09}_{-0.12}`$  |   $`0.65`$  (Lapenna) |    Yes     |    Yes     |
| $`r_{rgb}`$   |  $`0.71^{+0.09}_{-0.11}`$  |   Not reported        |    NA      |    NA      |
| $`f`$         |  $`0.12^{+0.18}_{-0.21}`$  |   Not reported        |    NA      |    NA      |

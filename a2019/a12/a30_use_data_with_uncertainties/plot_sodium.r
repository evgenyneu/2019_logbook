library(ggplot2)
library(grid)
library(gridExtra)

# Plot sodium abundances
# ------------

data_path = "a2019/a12/a30_use_data_with_uncertainties/data/sodium.csv"
data = read.csv(data_path, header = TRUE)
data = as.data.frame(data)
n = length(data$type)
data$num = sample(1:n)

# Make the histogram
# ---------

p1 = ggplot(data=data, aes(x=na_over_h, fill=type)) +
    geom_density(adjust=1.0, alpha=.6) +
    theme_minimal(base_size = 15) +
    scale_fill_manual(values=c('#9999FF', '#FF9999')) +
    # xlab("Sodium Abundance [Na/H]") +
    theme(axis.title.x = element_blank(), axis.text.x = element_blank(),
          plot.margin = unit(c(0.3,0,-0.3,0), "cm")) +
    ylab("Density") +
    xlim(-2,0) +
    labs(title="Sodium abundance in NGC 288 stars") +
    theme(legend.title=element_blank())



# Plot abundances
# -----------

p2 = ggplot(data, aes(x=na_over_h, y=num, color=type, shape=type, fill=type)) +
    theme_minimal(base_size = 15) +
    geom_errorbarh(aes(xmin=na_over_h-uncertainty, xmax=na_over_h+uncertainty),
                   show.legend = FALSE) +
    geom_point(size=6) +
    xlim(-2,0) +
    scale_shape_manual(values = c(24, 21)) +
    scale_color_manual(values=c('#0000FF', '#FF0000')) +
    scale_fill_manual(values=c('#BBBBFF', '#FFBBBB')) +
    xlab("Sodium Abundance [Na/H]") +
    ylab("Star number") +
    theme(legend.title=element_blank())

grid.newpage()
grid.draw(rbind(ggplotGrob(p1), ggplotGrob(p2), size = "last"))

g <- arrangeGrob(rbind(ggplotGrob(p1), ggplotGrob(p2), size = "last"))
image_path="a2019/a12/a30_use_data_with_uncertainties/data/sodium_density_2.png"
ggsave(file=image_path, plot=g)

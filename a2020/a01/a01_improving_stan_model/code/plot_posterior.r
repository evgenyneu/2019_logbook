# Plot parameters of Stan model
# ------------

data_rgb_path = "a2020/a01/a01_improving_stan_model/data/sodium_model_rgb.csv"
data_rgb = read.csv(data_rgb_path, header=TRUE)
data_rgb = as.data.frame(data_rgb)

data_agb_path = "a2020/a01/a01_improving_stan_model/data/sodium_model_agb.csv"
data_agb = read.csv(data_agb_path, header=TRUE)
data_agb = as.data.frame(data_agb)

agb_color = '#9999FF0B'
agb_color_no_alpha = '#9999FF'
rgb_color = '#FF99990B'
rgb_color_no_alpha = '#FF9999'

plot_path = "a2020/a01/a01_improving_stan_model/plots/"


png(filename=paste(plot_path, "posterior_distributions.png", sep=""),
    width=5, height=5, units='in', res=300)

n_cures = 1000

model = function(x, datum) {
    datum$p * dnorm(x, mean=datum$mu1, sd=datum$sigma1) +
    (1 - datum$p) * dnorm(x, mean=datum$mu2, sd=datum$sigma2)
}

for (i in 1:n_cures) {
    # i = sample(1:nrow(data_rgb),1)
    datum_rgb = data_rgb[i,]
    datum_agb = data_agb[i,]

    curve(model(x, datum_rgb), from=-2, to=0, add=i>1, col=rgb_color, ylim=c(0, 20),
        xlab='Sodium abundance [Na/H]',
        ylab='Density',
        main=paste(n_cures, "posterior distributions"))

    curve(model(x, datum_agb), from=-2, to=0, add=T, col=agb_color, ylim=c(0, 20))
}

mean_data_rgb <- data.frame(t(data.frame(colMeans(data_rgb))))
mean_data_agb <- data.frame(t(data.frame(colMeans(data_agb))))
curve(model(x, mean_data_rgb), from=-2, to=0, add=i>1, col='#FF5555', ylim=c(0, 20), main=i, lwd=1)
curve(model(x, mean_data_agb), from=-2, to=0, add=i>1, col='#5555FF', ylim=c(0, 20), main=i, lwd=1)

# Add legend
legend("topright", legend=c("AGB", "RGB"),
    col=c(agb_color_no_alpha, rgb_color_no_alpha), pt.cex=2, pch=15)

dev.off()

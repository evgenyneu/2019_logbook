from cmdstanpy import CmdStanModel
import pickle
import os
import inspect
from scipy.stats import zscore
from sample_utils import InfoPath, get_info_path
from cmdstanpy_utils import cmdtandpy_analyse


def run_stan(data, seed=None, cache_name=None, cache_path=None,
             cache_dir='model_info', cache_suffix=None):
    """
    Run Stan to calculate parameters of the model.

    Parameters
    -----------

    data : dictionary
        Data for the stan sampler.

    cache_name : str
        Name of the samples cache file (without extension).

    cache_path : str
        Path to the directory where the sample file cache will be saved.

    cache_dir : str
        Name of the directory where the cache file is placed

    cache_suffix : str
        Text that will added to the end of the file name for the cache.


    Returns
    -------

    Panda's dataframe containing samples of parameters.
    """

    info_path = InfoPath(base_name="model", extension="pkl",
                         sub_dir_name="run_stan")

    path_to_model = get_info_path(info_path)

    if os.path.exists(path_to_model):
        with open(path_to_model, 'rb') as input:
            samples = pickle.load(input)
    else:
        path = "a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan"

        model = CmdStanModel(stan_file=path)

        fit = model.sample(data=data, seed=seed,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=15000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        cmdtandpy_analyse(fit)
        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        os.makedirs(os.path.dirname(path_to_model), exist_ok=True)
        with open(path_to_model, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)

    return samples


def standardize(values, uncertainties):
    """
    Standardize values and their uncertainties.

    Returns
    -------

    Tuple of standardized values and their uncertainties.
    """

    values_std = zscore(values, ddof=1)
    uncertainties_std = (uncertainties / values.std()).tolist()

    return (values_std, uncertainties_std)


def destandardize(samples, values_observed):
    """

    De-standardize samples of parameters.

    Parameters
    -----------

    samples : Panda's DataFrame
        Samples of parameters (normalized.)

    values_observed : list
        Observed values


    Returns : Panda's DataFrame
    --------

    Destandardized sample values.

    """
    std = values_observed.std()
    mean = values_observed.mean()
    result = samples.copy()
    result['mu.1'] = samples['mu.1'] * std + mean
    result['mu.2'] = samples['mu.2'] * std + mean
    result['sigma.1'] = samples['sigma.1'] * std
    result['sigma.2'] = samples['sigma.2'] * std

    return result

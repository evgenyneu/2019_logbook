import os
import pandas as pd

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def convert_data_to_csv():
    data_dir = "a2020/a02/a21_fix_uncertainties/a2018_11_MacLean_NGC_6121_M4/data"
    data_path = os.path.join(data_dir, "a2018_11_MacLean_NGC_6121_M4_table5.dat")
    df = pd.read_table(data_path)
    df_new = pd.DataFrame(columns=["Type", "A(Na)", "e_A(Na)"])

    for index, star in df.iterrows():
        star_id = star["ID"]
        star_type = star["Type"]
        na_over_h = star["logε(Na)"]
        e_na_over_h = star["rms.3"]

        if float(e_na_over_h) == 0:
            print(f"Zero Na uncertainty for star ID={star_id}")
            e_na_over_h = 0.01

        df_new.loc[star_id] = [star_type, na_over_h, e_na_over_h]

    csv_path = os.path.join(data_dir, "a2018_11_MacLean_NGC_6121_M4_table5.csv")
    df_new.to_csv(csv_path, index_label="ID")


def do_work():
    """Main function that does all the work"""

    convert_data_to_csv()

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2018_11_MacLean_NGC_6121_M4",
        study_name="NGC 6121 (M4) MacLean et al. 2018",
        xlabel="Sodium abundance A(Na)"
    )

    data_path = os.path.join(settings.path, "data/a2018_11_MacLean_NGC_6121_M4_table5.csv")

    type_name = "Type"
    abundance_name = "A(Na)"
    uncertainty_name = "e_A(Na)"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.max_treedepth = 15

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

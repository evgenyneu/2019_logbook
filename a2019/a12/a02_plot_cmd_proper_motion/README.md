## Calculate uncertainties of B and B - K

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/cmd_uncertainties.py](a2019/a12/a02_plot_cmd_proper_motion/code/cmd_uncertainties.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.cmd_uncertainties
```

### 1. For entire sample

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/cmd_uncertainties_entire_sample.png" width="700" alt='Photometric uncertainties for entire sample'>

Figure 1: Uncertainties of photometric measurements for entire sample ([Yazan Momany](a2019/a11/a29_get_2mass_yazan), [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)).


### 2. For selected stars only

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/cmd_uncertainties_selected_stars.png" width="700" alt='Photometric uncertainties for selected stars'>

Figure 2: Uncertainties of photometric measurements for RGB/AGB stars selected for analysis ([Yazan Momany](a2019/a11/a29_get_2mass_yazan), [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)).


### 3. Descriptive statistics

The mean values of uncertainties and their sample standard deviations are the following.

### Entire sample

```
u(B)=0.0056+/-0.0018
u(B-K)=0.04+/-0.04
```

### Selected stars

```
u(B)=0.0055+/-0.0014
u(B-K)=0.029+/-0.008
```

### 4. Notes

From Fig. 1 we can wee many outliers for B-K with uncertainties up to 0.25. From Fig. 2 we can see that these outliers are mostly not present in the selected sample of RGB/AGB stars.


## Plot color magnitude diagram with error bars


### Entire sample

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_uncertainties.py](a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_uncertainties.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.plot_cmd_with_uncertainties
```

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/plot_cmd_with_uncertainties_all_stars.png" width="700" alt='Colour magnitude diagram of all observed stars. No extinction correction applied.'>

Figure 3: Colour magnitude diagram of all observed stars in NGC288 ([Yazan Momany](a2019/a11/a29_get_2mass_yazan), [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)). The "Excluded" stars were not used in analysis. No extinction correction applied.


### Zoomed into selected stars

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_uncertainties_zoomed_in.py](a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_uncertainties_zoomed_in.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.plot_cmd_with_uncertainties_zoomed_in
```

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/plot_cmd_with_uncertainties_zoomed_in_all_stars.png" width="700" alt='Colour magnitude diagram'>

Figure 4: Colour magnitude diagram of observed stars in NGC288 ([Yazan Momany](a2019/a11/a29_get_2mass_yazan), [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)). The "Excluded" stars were not used in analysis. No extinction correction applied.


### Mean error bars

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_mean_error_bars.py](a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_mean_error_bars.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.plot_cmd_with_mean_error_bars
```

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/plot_cmd_with_mean_error_bars_all_stars.png" width="700" alt='Colour magnitude diagram'>

Figure 5: Colour magnitude diagram of observed stars in NGC288. The error bars are drawn from the mean uncertainties for the selected RGB/AGB stars. The "Excluded" stars were not used in analysis. No extinction correction applied.



### Maximum error bars

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_max_error_bars.py](a2019/a12/a02_plot_cmd_proper_motion/code/plot_cmd_with_max_error_bars.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.plot_cmd_with_max_error_bars
```

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/plot_cmd_with_max_error_bars_selected_stars.png" width="700" alt='Colour magnitude diagram'>

Figure 5: Colour magnitude diagram of observed stars in NGC288 ([Yazan Momany](a2019/a11/a29_get_2mass_yazan), [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)). The error bars are drawn from the maximum uncertainties for the selected RGB/AGB stars. The "Excluded" stars were not used in analysis. No extinction correction applied.


## Plot proper motions

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/proper_motion.py](a2019/a12/a02_plot_cmd_proper_motion/code/proper_motion.py)

```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.proper_motion
```

<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/proper_motion_max_selected_error_bars.png" width="700" alt='Proper motions'>

Figure 6: Proper motions of observed stars in NGC288 ([Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G), robust fit). The error bars are drawn from the maximum uncertainties for the selected RGB/AGB stars. The "Excluded" stars were not used in analysis.


### Notes

We can see from Fig. 6 that all selected stars have proper velocities.



## Plot our vs Gaia's radial velocities

Code: [a2019/a12/a02_plot_cmd_proper_motion/code/radial_velocity_gaia.py](a2019/a12/a02_plot_cmd_proper_motion/code/radial_velocity_gaia.py)


```
python -m a2019.a12.a02_plot_cmd_proper_motion.code.radial_velocity_gaia
```


<img src="a2019/a12/a02_plot_cmd_proper_motion/code/plots/radial_velocity_gaia.png" width="700" alt='Radial velocities'>

Figure 7: Radial velocities of observed stars in NGC288. Our radial velocities calculated with IRAF's fxcor are plotted against [Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G) data.

### Notes on Fig. 7

* Few stars that we observed have radial velocities in Gaia's catalog.

* We can see that for some stars our data disagrees with Gaia's.

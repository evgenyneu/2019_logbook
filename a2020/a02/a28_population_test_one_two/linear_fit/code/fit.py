"""
Example of fitting a line through data using curve_fit and odr functions
from scipy. curve_fit takes into account uncertainties in y, while
odr uses uncertainties for both x and y.

Usage
------

    python fit.py

"""

import numpy as np
from scipy import stats, odr
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import math


def generate_data(slope=0.3, intercept=1.1,
                  n=10, uncert_mu=0.5, uncert_sigma=0.2, uncert_min=0.01):
    """
    Generate x and y values taking in account observational uncertainties.

    Values y are calculated from x using linear relation:

        y = slope * x + intercept

    Parameters
    ----------
    slope: float
        True slope of the linear relation.
    intercept: float
        True intercept of the linear relation.
    n: int
        Number of observations
    uncert_mu: float
        Mean of the normal distribution for generating uncertainties.
    uncert_sigma: float
        Spread of the normal distribution for generating uncertainties.
    uncert_sigma: float
        Minimum uncertainty.

    Returns
    -------
    (x_observed, y_observed, x_uncertainties, y_uncertainties)

    x_observed,
    y_observed: list of float
        Observed x and y values.

    x_uncertainties,
    y_uncertainties:
        Observational uncertainties of x and y.
    """

    np.random.seed(1)
    x_true = stats.uniform.rvs(loc=0, scale=10, size=n)
    y_true = intercept + slope * x_true

    # Generate observed values from true ones
    # ------------

    # Generate uncertainties for all observations
    x_uncertainties = np.abs(stats.norm(uncert_mu, uncert_sigma).rvs(size=n))
    x_uncertainties = np.clip(x_uncertainties, uncert_min, None)

    # Generate observed values from true using the uncertainties
    x_observed = stats.norm(x_true, x_uncertainties).rvs(size=n)

    y_uncertainties = np.abs(stats.norm(uncert_mu, uncert_sigma).rvs(size=n))
    y_uncertainties = np.clip(y_uncertainties, uncert_min, None)
    y_observed = stats.norm(y_true, y_uncertainties).rvs(size=n)

    return x_observed, y_observed, x_uncertainties, y_uncertainties


def model_line(x, slope, intercept):
    return intercept + slope * x


def fit_line_curve_fit(x, y, x_uncertainties, y_uncertainties):
    popt, pcov = curve_fit(model_line, x, y, sigma=y_uncertainties,
                           absolute_sigma=True)

    slope = popt[0]
    intercept = popt[1]

    return slope, intercept, pcov


def model_line_for_odr(coef, x):
    return coef[0] * x + coef[1]


def fit_line_odr(x, y, x_uncertainties, y_uncertainties):
    linear = odr.Model(model_line_for_odr)
    mydata = odr.RealData(x, y, sx=x_uncertainties, sy=y_uncertainties)
    myodr = odr.ODR(mydata, linear, beta0=[1.0, 0])
    out = myodr.run()
    slope = out.beta[0]
    intercept = out.beta[1]
    covariance = out.cov_beta

    return slope, intercept, covariance


def plot_data_and_model(x, y, x_uncertainties, y_uncertainties,
                        slope, intercept, covariance,
                        equation,
                        plot_path,
                        n_lines=300):

    # Set plot limits
    # -------

    x_range = max(x) - min(x)
    x_extra = x_range * 0.1
    xlim = [min(x) - x_extra, max(x) + x_extra]

    y_range = max(y) - min(y)
    y_extra = y_range * 0.1
    ylim = [min(y) - y_extra, max(y) + y_extra]

    # Plot possible models
    # ------

    x_fit = np.linspace(xlim[0], xlim[1], 100)

    # Generate random samples for slope and intercept using bivariate
    # normal probability distribution
    samples = stats.multivariate_normal.rvs([slope, intercept], covariance,
                                            size=n_lines)

    for slope_sample, intercept_sample in samples:
        y_fit = model_line(x=x_fit, slope=slope_sample,
                           intercept=intercept_sample)

        plt.plot(x_fit, y_fit, color="orange", alpha=0.05)

    # Plot data
    # ----------

    plt.errorbar(x, y,
                 xerr=x_uncertainties, yerr=y_uncertainties, fmt='none',
                 color="blue", zorder=10)

    plt.scatter(x, y, color="blue", zorder=10)

    plt.xlabel("x")
    plt.ylabel("y")
    plt.grid()
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.title(equation)
    plt.tight_layout()
    plt.savefig(fname=plot_path, dpi=300)
    plt.close()


def print_equation(slope, intercept, covariance):
    slope_uncertainty = math.sqrt(covariance[0][0])
    intercept_uncertainty = math.sqrt(covariance[1][1])

    return (
        f"y = ({intercept:.2f} ± {intercept_uncertainty:.2f})"
        f" + ({slope:.2f} ± {slope_uncertainty:.2f})x"
    )


def do_work():
    x, y, x_uncertainties, y_uncertainties = generate_data()
    fits = []

    slope, intercept, covariance = fit_line_curve_fit(
        x, y, x_uncertainties, y_uncertainties)

    fits.append(['curve_fit', slope, intercept, covariance])

    slope, intercept, covariance = fit_line_odr(
        x, y, x_uncertainties, y_uncertainties)

    fits.append(['odr', slope, intercept, covariance])

    for name, slope, intercept, covariance in fits:
        equation = print_equation(slope, intercept, covariance)
        equation = f"{name} fit: {equation}"
        print(equation)
        path = f'fit_{name}.png'

        plot_data_and_model(x=x, y=y,
                            x_uncertainties=x_uncertainties,
                            y_uncertainties=y_uncertainties,
                            slope=slope, intercept=intercept,
                            equation=equation,
                            covariance=covariance,
                            plot_path=path)


if __name__ == '__main__':
    do_work()
    print("We are done")

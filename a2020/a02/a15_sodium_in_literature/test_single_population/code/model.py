import os
from a2020.a02.a15_sodium_in_literature.code.synthetic_data import run_model

if __name__ == '__main__':
    print("Running the models...")

    path = "a2020/a02/a15_sodium_in_literature/test_single_population"
    params_path = os.path.join(path, "code/model_params.yaml")

    run_model(
        path=path,
        params_path=params_path,
        dir_name="model_info",
        data_file_name="data.csv",
        seed=333,
        title="simulated data with single population")

    print('We are done')

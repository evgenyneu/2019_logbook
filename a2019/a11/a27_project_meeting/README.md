# Project meeting with Chris/Simon at the 3rd floor lounge

## Idea for spectroscopic check of AGB/RGB stars

* Change mass of an AGB star from 0.6 to 0.8 Msun.

* Calculate Fe abundance

* If it is different from cluster's abundance, then this is indeed an AGB star and not RGB.

## Plots for me to make from GAIA data

1. Plot proper motions of our full samples. On the plot highlight the selected AGB (triangles) and RGB (circles) stars.

1. Plot GAIA CMD.

1. Plot Chris' temperature vs GAIA's.

1. Plot our radial velocities vs GAIA's

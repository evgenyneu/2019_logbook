# Model normally distributed population

from tarpan.plot.kde import save_scatter_and_kde
from tarpan.cmdstanpy.analyse import save_analysis
from cmdstanpy import CmdStanModel

# Measurements
abundances = [
    -1.22, -1.15, -0.97, -0.68, -0.37, -0.48, -0.73, -0.61, -1.32,
    -0.62, -1.13, -0.65, -0.90, -1.29, -1.19, -0.54, -0.64, -0.45,
    -1.21, -0.75, -0.66, -0.71, -0.61, -0.59, -1.07, -0.65, -0.59]

uncertainties = [
     0.13, 0.14, 0.17, 0.07, 0.11, 0.12, 0.23, 0.05, 0.04,
     0.30, 0.11, 0.13, 0.16, 0.03, 0.18, 0.20, 0.16, 0.16,
     0.11, 0.09, 0.20, 0.10, 0.08, 0.04, 0.04, 0.23, 0.19]

# Plot data
save_scatter_and_kde([abundances], [uncertainties], xlabel="Sodium abundance [Na/H]")

# Run the model
model = CmdStanModel("model.stan")
data = dict(n=len(abundances), y=abundances, uncert=uncertainties)
fit = model.sample(data=data)

# Generate summaries and plots in "model_info" directory
save_analysis(fit, param_names=["mu"])

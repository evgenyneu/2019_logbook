library(rethinking)


set.seed(42)
n_poor <- 10
n_rich <- 20

poor <- rnorm(n_poor, 0, .1)
rich <- rnorm(n_rich, 3, .1)

abund = append(poor, rich)

dens(abund)

d <- list(abund=abund)

model <- ulam( alist(
        abund ~ p*dnorm( mu1 , sigma ) + (1-p)*dnorm( mu1 , sigma ),
        mu1 ~ dnorm( 1, 5 ),
        mu1 ~ dnorm( 1, 5 ),
        p ~ dunif(0, 1),
        sigma ~ dexp( 1 )
    ),
    data=d, chains=2, cores=2 )


precis(model)
# stancode(model)
# traceplot(model)
# trankplot(model, n_cols=2)
# pairs(model@stanfit)

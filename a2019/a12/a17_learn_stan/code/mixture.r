library("rstan")
library("rethinking")
options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)


# Make a model of two normal populations of abundances
set.seed(42)
n_poor <- 5
n_rich <- 10
poor <- rnorm(n_poor, 0, .1)
rich <- rnorm(n_rich, 3, .1)
abundandes = sample(append(poor, rich))

# Plot the model
# ---------

dens(abundandes, main="Abundances", xlim=c(-2, 5))

plot(x=abundandes, y=1:length(abundandes), cex=2, pch=16,
     col=col.alpha("blue",0.4),
    ylab="Index")


# Prepare model and data for Stan
# -------------

model_path = 'a2019/a12/a17_learn_stan/code/mixture2.stan'

data <- list(K=2, # number of mixture components
             N=length(abundandes),
             y=abundandes)

# Run Stan
fit <- stan(file=model_path, data=data)

# Print output
print(fit)
pairs(fit)

# Extract posterior samples from Stan's output

post <- extract(fit)
precis(post, depth=2)

# Plot posterior predictive distribution

mu1 = post$mu[,1]
mu2 = post$mu[,2]

sigma1 = post$sigma[,1]
sigma2 = post$sigma[,2]

theta1 = post$theta[,1]
theta2 = post$theta[,2]

samples = 10

ratio = mean(theta1) / mean(theta2)

pop1_post = rnorm(n=samples * ratio, mean=mu1, sd=sigma1)
dens(pop1_post, xlim=c(-2, 5))

pop2_post = rnorm(n=samples, mean=mu2, sd=sigma2)
dens(pop2_post, xlim=c(-2, 5))

post_predict_distr = append(pop1_post, pop2_post)

dens(post_predict_distr,
     main="Posterior predictive distribution of abundances",
     xlim=c(-2, 5))

print("We are done")

import os

from a2020.a03.a03_akis_model.ngc_5752_agb_compare_models.code.compare import (
    AnalysisSettings, compare_models)


def do_work():
    path = "a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/\
a2017_09_campbell_ngc_6752_nlte"

    settings = AnalysisSettings(
        path=os.path.join(path, "model_info"),
        study_name="AGB NGC 6752 Campbell et al. 2017 NLTE",
        xlabel="Sodium abundance A(Na) NLTE",
        type_name="Type",
        abundance_name="A(Na)_nlte",
        uncertainty_name="e_A(Na)_nlte",
        data_path=os.path.join(path, 'data/2017_09_campbell_ngc_6752.csv')
    )

    compare_models(settings)


if __name__ == '__main__':
    print("Running the models...")

    do_work()

    print('We are done')

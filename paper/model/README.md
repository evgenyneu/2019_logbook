> This model is superseded with [standardised model](paper/model_std).

# Code for NGC288 sodium statistical model

This repository includes code used in [reference to paper] to calculate parameters of bimodal distributions of sodium abundances in AGB/RGB starts in NGC288 globular cluster. This code is for those who wish to rerun our calculations and reproduce our results.

Statistical model is written in [Stan](https://mc-stan.org) language. In addition, we used [cmdstanpy](https://github.com/evgenyneu/tarpan) library for running Stan from Python and [Tarpan](https://github.com/evgenyneu/tarpan) library for making data summaries and plots.


## Code description

The program code consists of two files:

1. Python script: [paper/model/code/run_model.py](paper/model/code/run_model.py)

1. Stan's model: [paper/model/code/stan_model/model.stan](paper/model/code/stan_model/model.stan)


## Statistical model

Mathematical description of the statistical model is shown in Listing 1.

Listing 1: Statical model describing a mixture of two Gaussian components.


<img src="paper/model/latex/images/model.png" width=700 alt='Statistical model'>


This model describes a mixture of two Gaussian components. These are two distinct populations of stars from which we observed sodium abundances.


### Input parameters of the model

The input parameters of the model are:

* $`Y_{\textrm{obs}, i}`$: observed value of sodium abundance in [Na/H] units of the i-th star,

* $`Y_{\textrm{uncert}, i}`$: corresponding measurement uncertainty of the sodium abundance.


### Estimated parameters of the model

With this code we want to estimate distributions of the following parameters:

* $`r`$: mixing proportion, which is the proportion of stars in the second population (the population with the higher average sodium abundance),

* $`\mu_1`$ and $`\mu_2`$: locations of two populations,

* $`\sigma`$: spreads of the populations.

* $`Y_{\textrm{true}, i}`$: estimated "true" abundance value for the i-th star, needed to take into account measurement uncertainties.


### Equivalent model with non-centered parameterization

When we ran model from Listing 1, Stan showed "divergent transitions" and "low E-BFMI value" warnings for this model for out smaller sample of AGB star abundances. These warning could indicate potential problems with sampling. With help of Max_Mantei user from [Stan's forums](https://discourse.mc-stan.org/t/how-to-reparameterize-a-gaussian-mixture-model/12792/22), we have made an equivalent model using the non-centered parameterization technique, shown in Listing 2, which samples posterior distribution with fewer Stan warnings. When we ran this model, we [found out](a2020/a01/a26_original_vs_reparametrized) that it generates almost exactly the same posterior distributions as the original model from Listing 1. This suggests that the issues with sampling do not significantly affect the results.

In our analysis we decided to use more complex model from Listing 2, since it produces smaller number of divergent transitions and is therefore more reliable for smaller samples.

Listing 2: Equivalent statistical model that uses non-centered parameterization.

<img src="paper/model/latex/images/reparametrised.png" width=600 alt='Reparametrised model'>



## Setup

Download and install all required software with the following steps.

### 1. Install Python and libraries

Download and install all required software by following [setup instructions](/setup.md).


### 2. Install Stan

```
install_cmdstan -v 2.21.0
```

## Usage

Change to the root of this repository:

```bash
cd 2019_logbook
```

Finally, run the Python script:

```bash
PYTHONPATH=. python paper/model/code/run_model.py
```

## Input data

The input values are plotted on Figure 1.

<img src="paper/model/code/ngc288_2020/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances of RGB and AGB stars in NGC288 globular cluster.


## Program output

The script will run the model code and generate plots and summaries of the distributions of model parameters in [paper/model/code/ngc288_2020](paper/model/code/ngc288_2020) directory. Specifically, the code makes the summary of parameter distributions, shown on Figure 2, which is saved in [compare_summaries.pdf](paper/model/code/ngc288_2020/compare_summaries.png) file.

<img src="paper/model/code/ngc288_2020/compare_summaries.png" width="700" alt='Posterior distributions of parameters'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1. This table is saved in [paper/model/code/ngc288_2020/parameters_compared.txt](paper/model/code/ngc288_2020/parameters_compared.txt) file.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     | r                        | mu.1                      | mu.2                      |
|:----|:-------------------------|:--------------------------|:--------------------------|
| AGB | $`0.33^{+0.17}_{-0.16}`$ | $`-1.33^{+0.06}_{-0.06}`$ | $`-0.88^{+0.10}_{-0.10}`$ |
| RGB | $`0.65^{+0.10}_{-0.09}`$ | $`-1.21^{+0.05}_{-0.04}`$ | $`-0.63^{+0.04}_{-0.03}`$ |


### Plot of posterior distribution

On Fig. 3 we plot input data along with 50 posterior distributions for AGB and RGB stars.

<img src="paper/model/code/ngc288_2020/posterior.png" width="700" alt='Posterior distributions of parameters'>

Figure 3: Observed and posterior distributions.



### Detailed summaries

The detailed summaries of distributions of model parameters are saved in [paper/model/code/ngc288_2020/agb/summary.txt](paper/model/code/ngc288_2020/agb/summary.txt) and [paper/model/code/ngc288_2020/rgb/summary.txt](paper/model/code/ngc288_2020/rgb/summary.txt) files. The summaries contain the following folumns:

*  **Name, Mean, Std** are the name of the parameter, its mean and standard deviation.

*  **68CI-, 68CI+, 95CI-, 95CI+** are the 68% and 95% HPDIs (highest probability density intervals).

* **Mode, +, -** is a mode of distribution with upper and lower uncertainties, which are calculated as distances to 68% HPDI. These are the values used in the paper.

* **N_Eff** is Stan's number of effective samples, the higher the better.

* **R_hat** is a Stan's parameter representing the quality of the sampling.


### AGB failure rate

The distribution of the "AGB failure rate" parameter is saved in [paper/model/code/ngc288_2020/failure_rate](paper/model/code/ngc288_2020/failure_rate) directory. Its summary is shown in Table 3. For example, the value `0.43 ± 0.30` is the proportion of RGB stars that belong to sub-population 2 that fail to reach AGB stage.

Table 3: Summary of AGB failure rate parameter.

| Name             |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-----------------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| AGB failure rate |   0.43 |  0.30 |   0.49 | 0.29 | 0.25 |    0.24 |    0.77 |   -0.16 |    1.00 |


### Parameter differences between AGB/RGB stars

The distributions of parameter difference (RGB minus AGB) between AGB and RGB stars are saved in [paper/model/code/ngc288_2020/delta_mu](paper/model/code/ngc288_2020/delta_mu). The summary is shown on Table 4 and it helps us compare r, mu1 and mu2 parameters between AGB and RGB stars. For example, `Delta r = 0.3 ± 0.2` means that the proportion of stars belonging to sub-population 2 is by `0.3 ± 0.2` larger in RGB stars than in AGB stars.

Table 4: Summary of distributions of parameter difference: RGB minus AGB stars.

| Name       |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-----------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| Delta r    |   0.29 |  0.20 |   0.31 | 0.19 | 0.19 |    0.12 |    0.50 |   -0.13 |    0.69 |
| Delta mu 1 |   0.14 |  0.09 |   0.14 | 0.08 | 0.08 |    0.06 |    0.21 |   -0.05 |    0.30 |
| Delta mu 2 |   0.27 |  0.13 |   0.26 | 0.11 | 0.11 |    0.15 |    0.37 |    0.03 |    0.58 |


#### Proportions

In Table 5 we can see the proportion of samples that have positive values of differences of parameters: RGB minus AGB. For example, `Delta r > 0 = 0.92` means that `92%` of RGB samples have higher values of parameter `r` than AGB samples. In other words, there is `0.92` probability that proportion of stars that belong to sub-population 2 is larger for RGB stars than for AGB stars.

The result is saved in [paper/model/code/ngc288_2020/compare_agb_rgb_parameters.txt](paper/model/code/ngc288_2020/compare_agb_rgb_parameters.txt) file.

Table 5: Number of samples and their proportions for positive values of parameter differences between AGB and RGB stars.

| Name              |    Value |
|:------------------|---------:|
| Number of samples | 16000.00 |
| Delta r > 0       |     0.92 |
| Delta mu 1 > 0    |     0.94 |
| Delta mu 2 > 0    |     0.98 |

# Compare single vs two-gaussian models using data from synthetic data with two populations

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations. Vertical dashed lines indicate locations of the synthesised populations.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/test_two_populations/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| Two populations |  25.10 | 7.10 |         |       |    2.27 |   0.25 |     1.00 |
| One population  |  91.20 | 4.28 |   66.10 |  5.47 |    3.83 |   0.19 |     0.00 |



|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| Two populations |  25.07 | 7.10 |         |       |    2.25 |     1.00 |
| One population  |  91.18 | 4.28 |   66.11 |  5.47 |    3.82 |     0.00 |


## Conclusion

Very strong preference for two population model, PSIS/WAIC values differ by more than ten standard errors.

from cmdstanpy import CmdStanModel
import pandas as pd

model_path = "a2020/a01/a04_cmdstanpy/code/stan_model/eight_schools.stan"
model = CmdStanModel(stan_file=model_path)

data = {
    "J": 8,
    "y": [28,  8, -3,  7, -1,  1, 18, 12],
    "sigma": [15, 10, 16, 11,  9, 11, 10, 18]
}

fit = model.sample(data=data)
print(fit.summary())
print("We are done")

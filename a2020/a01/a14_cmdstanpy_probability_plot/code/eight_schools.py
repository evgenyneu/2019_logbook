"""
Runs Stan via cmdstanpy and shows a live plot
of the log probability samples. Works both in terminal and Jupyter.

This is a proof of concept.

Dependencies
-----------

* ipycanvas
* numpy
* matplotlib


Usage
-----

Terminal:

    python eight_schools.py

Jupyter:


    from eight_schools import do_work
    do_work(is_jupyter=True)
"""

from cmdstanpy import CmdStanModel
from threading import Thread
from queue import Queue as ThreadQueue
from multiprocessing import Process, Event
from multiprocessing import Queue as ProcessQueue
from time import sleep
from pathlib import Path
import matplotlib.pyplot as plt
import os
import numpy as np
import time
from ipycanvas import Canvas, hold_canvas


def follow(file, quit):
    """
    Watch the file and return the lines that are being added.
    Similar to Linux "tail -f" command.


    Parameters
    ----------

    quit: Event

        The even for stopping the watch loop.

    Returns
    -------

    Generator of lines.
    """

    while not quit.is_set():
        line = file.readline()

        if not line:
            time.sleep(0.1)
            continue
        yield line


def run_model(csv_dir, is_jupyter):
    model = CmdStanModel(stan_file="stan_model/eight_schools.stan")

    data = {
        "J": 8,
        "y": [28,  8, -3,  7, -1,  1, 18, 12],
        "sigma": [15, 10, 16, 11,  9, 11, 10, 18]
    }

    os.makedirs(csv_dir, exist_ok=True)

    show_progress = True

    if is_jupyter:
        show_progress = "notebook"

    model.sample(data=data, show_progress=show_progress,
                 chains=1, cores=1,
                 sampling_iters=50000, warmup_iters=10000,
                 output_dir=csv_dir)


def get_csv_file(path):
    """
    Return path to the most recent CSV file in the `path` directory.
    """

    # Get CSV files sorted by modification time
    files = sorted(Path(path).glob("*.csv"), key=lambda t: t.stat().st_mtime)

    # Get only recently modified files
    files = [
        file for file in files
        if (time.time() - file.stat().st_mtime) < 1
    ]

    if len(files) > 0:
        # Return most recent file
        return files[-1]


def is_number(s):
    """
    Returns True is `s` is a number.
    """

    try:
        float(s)
        return True
    except ValueError:
        return False


class RepeatTimer(Thread):
    """
    Run `function` repeatedly after `interval` number of seconds.
    The `function` will be called with args and kwargs arguments.
    """

    def __init__(self, interval, stop_event, function, *args, **kwargs):
        Thread.__init__(self)
        self.interval = interval
        self.stopped = stop_event
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.function(*self.args, **self.kwargs)

        while not self.stopped.wait(self.interval):
            self.function(*self.args, **self.kwargs)


def read_probabilities(q, n_values, plotting_q):
    """
    Reads all probability values from the `q` queue.
    From those value, takes `n_values` evenly spaced ones and
    puts them into `plotting_q` queue for plotting.
    """

    # Get all items from the queue
    items = [q.get() for _ in range(q.qsize())]

    if len(items) == 0:
        return

    # Get `n_values` evenly spaced values from the prob array
    every_nth = int(len(items) / n_values) + 1
    items = items[::every_nth]
    plotting_q.put(items)


def data_reader(csv_dir, quit, plotting_q):
    """
    Scans `csv_dir` until it finds the a newly created CSV file.
    Watches new lines being added to the CSV file,
    extracts the probability values from them, and puts some of
    the values into the `plotting_q` queue for plotting.
    """

    while True:
        css_file_path = get_csv_file(csv_dir)

        if css_file_path is None:
            sleep(.1)
            continue

        with open(css_file_path) as file:
            # Get the first values before the commas from each line
            prob = (line.split(',', 1)[0] for line in follow(file, quit))

            # Convert to floats
            prob = (float(a) for a in prob if is_number(a))

            # Repeatedly call read_probabilities function
            # that will send some of the read probability numbers for plotting.
            # We can't send all the numbers, since the plotting will be slow.
            q = ThreadQueue()
            timer = RepeatTimer(.05, quit, read_probabilities, q, 50, plotting_q)
            timer.start()

            # Read the probabilities
            for i in prob:
                q.put(i)

        break


def empty_csv_dir(csv_dir):
    """
    Remove files from the CSV dir.
    """

    files_to_remove = list(Path(csv_dir).glob("*.csv"))
    files_to_remove += list(Path(csv_dir).glob("*.txt"))

    for file in files_to_remove:
        os.remove(file)


def jupyter_plotter(q):
    """
    Show the samples in Julyter notebook.

    Parameters
    ---------

    q : Queue

        Contains probability numbers or "DONE" text.
    """

    from IPython.display import display
    canvas_height = 200
    canvas_width = 800
    canvas = Canvas(size=(canvas_width, canvas_height))
    canvas.fill_style = 'white'
    canvas.stroke_style = 'blue'
    display(canvas)

    x = 1000000
    y = 0
    scale_min = 10000000
    scale_max = -10000000

    while True:
        msg = q.get()

        if (msg == 'DONE'):
            break

        msg = np.array(msg)

        if np.min(msg) < scale_min:
            scale_min = np.min(msg)

        if np.max(msg) > scale_max:
            scale_max = np.max(msg)

        # Map probability values to interval between 0 and 1
        msg = np.interp(msg, [scale_min, scale_max], [0, 1])
        msg = msg*0.6 + 0.2  # Add margins
        msg *= canvas_height  # Convert to canvas coordinates
        n_points = len(msg)

        if x + n_points > canvas_width:
            canvas.clear_rect(0, 0, canvas_width, canvas_height)
            x = 0
            y = np.mean(msg)

        with hold_canvas(canvas):
            canvas.begin_path()
            canvas.move_to(x, y)

            for i in range(n_points):
                x += 0.5
                y = msg[i]
                canvas.line_to(x, y)

            canvas.stroke()


def matplotlib_plotter(q):
    """
    Show the samples in a matplotlib window.

    Parameters
    ---------

    q : Queue

        Contains probability numbers or "DONE" text.
    """

    plt.rcParams['toolbar'] = 'None'
    fig, ax = plt.subplots()
    ax.set_ylim(-50, -30)
    xlim = 1000
    ax.set_xlim(0, xlim)
    fig.show()
    fig.canvas.draw()
    xdata = [0]
    ydata = [0]
    line, = ax.plot(xdata, ydata, color="black", animated=True)
    background = fig.canvas.copy_from_bbox(ax.bbox)

    while True:
        msg = q.get()

        if (msg == 'DONE'):
            break

        if len(msg) == 0:
            continue

        if len(xdata) + len(msg) > xlim:
            fig.canvas.restore_region(background)
            xdata = [0]
            ydata = [0]

        xdata += list(range(len(xdata), len(xdata) + len(msg)))
        ydata += msg
        line.set_data(xdata, ydata)
        ax.draw_artist(line)
        fig.canvas.blit(ax.bbox)
        plt.pause(0.001)


def plotter(q, is_jupyter):
    if is_jupyter:
        jupyter_plotter(q)
    else:
        # Runing from a Terminal, show a matplotlib plot
        matplotlib_plotter(q)


def model_runner(q, csv_dir, is_jupyter):
    """
    Run Stan
    """

    run_model(csv_dir=csv_dir, is_jupyter=is_jupyter)
    q.put('DONE')  # Model has finished, signal other processes to finish


def do_work(is_jupyter):
    """
    Main function that does all the hard work.
    """

    csv_dir = 'stan_model/csv_files'
    empty_csv_dir(csv_dir)
    quit = Event()
    plotting_q = ProcessQueue()
    process = Process(target=data_reader, args=(csv_dir, quit, plotting_q))
    model = Process(target=model_runner, args=(plotting_q, csv_dir, is_jupyter))
    process.start()
    model.start()
    plotter(plotting_q, is_jupyter)
    quit.set()
    plotting_q.put('DONE')
    print("Sampling finished")


if __name__ == '__main__':
    do_work(is_jupyter=False)

import pandas as pd
import os


def remove_columns():
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open data
    data_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb.csv')

    df = pd.read_csv(data_path)

    columns = [
        'fei_h',
        'u_fei_h',
        'feii_h',
        'u_feii_h',
        't_eff',
        'u_t_eff']

    df = df.drop(columns, axis=1)

    # Save data frame
    output_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb_columns_removed.csv')

    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    remove_columns()
    print('We are done!')

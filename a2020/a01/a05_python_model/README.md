# Python code for fitting Gaussian mixture abundance model

[Previously](a2020/a01/a01_improving_stan_model) I have used RStan to find parameters of the Guassian mixture model. Today I rewritten the program in Python, using cmdstanpy library. I have done this, since the model is compiles about 3 times faster with cmdstanpy (about 20 sec in cmdstanpy versus 70 seconds in RStan).

Code: [a2020/a01/a05_python_model/code/calculate_sodium_ratio.py](a2020/a01/a05_python_model/code/calculate_sodium_ratio.py)

## Results

The parameters are shown in Tables 1, 2 and Figures 1, 2.


### RGB stars

Table 1: Parameters of the model for RGB stars.

| Name    |   Mean |   Std |   Mode |    + |    - | 68% HPDI      | 95% HPDI      |
|:--------|-------:|------:|-------:|-----:|-----:|:--------------|:--------------|
| p       |   0.39 |  0.11 |   0.35 | 0.11 | 0.09 | [0.26 0.47]   | [0.18 0.61]   |
| mu.1    |  -1.14 |  0.08 |  -1.18 | 0.08 | 0.05 | [-1.23 -1.1 ] | [-1.28 -0.98] |
| mu.2    |  -0.61 |  0.04 |  -0.61 | 0.04 | 0.04 | [-0.65 -0.58] | [-0.69 -0.54] |
| sigma.1 |   0.15 |  0.08 |   0.11 | 0.07 | 0.05 | [0.06 0.18]   | [0.04 0.3 ]   |
| sigma.2 |   0.07 |  0.04 |   0.05 | 0.04 | 0.03 | [0.02 0.09]   | [0.01 0.16]   |


<img src="a2020/a01/a05_python_model/code/plots/calculate_sodium_ratio_rgb.png" alt='Posterior probability densities for RGB stars'>

Figure 1: Probability densities for posterior distributions of parameters of the models for RGB stars.


### AGB stars

Table 2: Parameters of the model for AGB stars.

| Name    |   Mean |   Std |   Mode |    + |    - | 68% HPDI      | 95% HPDI      |
|:--------|-------:|------:|-------:|-----:|-----:|:--------------|:--------------|
| p       |   0.57 |  0.21 |   0.56 | 0.25 | 0.15 | [0.41 0.81]   | [0.15 0.96]   |
| mu.1    |  -1.37 |  0.07 |  -1.35 | 0.05 | 0.07 | [-1.43 -1.3 ] | [-1.52 -1.22] |
| mu.2    |  -0.96 |  0.11 |  -0.94 | 0.10 | 0.11 | [-1.05 -0.84] | [-1.18 -0.76] |
| sigma.1 |   0.13 |  0.13 |   0.07 | 0.08 | 0.04 | [0.02 0.15]   | [0.01 0.31]   |
| sigma.2 |   0.21 |  0.15 |   0.15 | 0.11 | 0.09 | [0.06 0.26]   | [0.02 0.43]   |


<img src="a2020/a01/a05_python_model/code/plots/calculate_sodium_ratio_agb.png" alt='Posterior probability densities for AGB stars'>

Figure 2: Probability densities for posterior distributions of parameters of the models for AGB stars.

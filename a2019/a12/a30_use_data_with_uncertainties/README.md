# Detecting parameters of a distribution containing a mixture of two bell curves

I simulated two normally distributed populations and used Stan to find the parameters of the distribution.

Code: [a2019/a12/a30_use_data_with_uncertainties/bimodal.r](a2019/a12/a30_use_data_with_uncertainties/bimodal.r)

## Adding uncertainty for outcome variable in Stan

I used an example from [Statistical Rethinking textbook](a2019/a12/a30_use_data_with_uncertainties/play_with_uncert.r) and modified it to use `target +=` instead of `bla ~ normal`, in since this will be needed later.

Code: [a2019/a12/a30_use_data_with_uncertainties/play_with_uncert_stan.r](a2019/a12/a30_use_data_with_uncertainties/play_with_uncert_stan.r).


## Using uncertainties of measurements

I included uncertainties of measurements into the calculations.

Code [a2019/a12/a30_use_data_with_uncertainties/with_uncertainties.r](a2019/a12/a30_use_data_with_uncertainties/with_uncertainties.r)


## Plot sodium abundances

The plot is the same as in Chris' report.

Code: [a2019/a12/a30_use_data_with_uncertainties/plot_sodium.r](a2019/a12/a30_use_data_with_uncertainties/plot_sodium.r)

<img src="a2019/a12/a30_use_data_with_uncertainties/data/sodium_density_2.png" width="700" alt='Sodium abundance'>

## Calculate sodium ratio

We create a gaussian mixture model and use the data ([sodium.csv](a2019/a12/a30_use_data_with_uncertainties/data/sodium.csv)) for the sodium abundances from Chris' report.

I had some problems with sampling, it produces divergent transitions and Rhat parameter (indicator of the quality of sampling) is not smaller than 1.01 for some parameters. I will have to find how to solve this issues:

* Instead of absolute sodium abundances, use the standardized ones (values minus their mean divided by standard deviation).

* Change the Stan model by using the non-centered reparametrization technique.


### Results

Below are the results:

* p is the mixing proportion, a fraction of low-sodium stars (i.e. 0.3 means 30% of stars are low sodium).
* μ1, μ2 are the mean sodium abundances for the two populations.

#### RGB stars

* p = 0.39 ± 0.15
* μ1 = -1.14 ± 0.13
* μ2=-0.62 ± 0.07


#### AGB stars

* p = 0.57 ± 0.28
* μ1 = -1.34 ± 0.18
* μ2 = -0.95 ± 0.28


### Code

* [a2019/a12/a30_use_data_with_uncertainties/calculate_sodium_ratio.r](a2019/a12/a30_use_data_with_uncertainties/calculate_sodium_ratio.r)

* Stan model: [a2019/a12/a30_use_data_with_uncertainties/calculate_sodium_ratio.stan](a2019/a12/a30_use_data_with_uncertainties/calculate_sodium_ratio.stan)

# Distances to stars

Distances to stars from [CDS/I/347/gaia2dis](https://ui.adsabs.harvard.edu/?#abs/2018AJ....156...58B).

See [logbook record][a2019/a12/a04_plot_distances].

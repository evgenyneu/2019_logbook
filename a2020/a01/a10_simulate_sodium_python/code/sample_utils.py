import numpy as np
from tabulate import tabulate
import arviz as az
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gaussian_kde
import math
import seaborn as sns
import re
import inspect
import os
from dataclasses import dataclass


@dataclass
class InfoPath:
    # Path information for creating summaries
    path: str = None  # Full path to parent of info dir. Auto if None.
    dir_name: str = "model_info"  # Name of the info directory.
    sub_dir_name: str = None  # Name of the subdirectory. Auto if None.
    stack_depth: int = 3  # Used fo automatic `path` and `sub_dir_name`
    base_name: str = None  # Base name of the summary file or plot
    extension: str = None  # Extension of the summary or plot
    dpi: int = 300  # DPI setting using for plots


@dataclass
class TreePlotParams:
    """
    Parameters of the tree plot.
    """

    group_height: float = None
    error_bar_cap_size: float = None
    markersize: float = 50
    ylim: float = None
    markers = ['x', 'o', 'v', 'D', 'X', '1', '<', '>']
    marker_line_width: float = 1

    marker_colors = ['#FF9922', '#009933', '#8888FF',
                     '#BBBB11', '#3399AA', '#33BB11',
                     '#330077', '#BBAA22', '#55FFBB']

    marker_edge_colors = ['#994400', '#003300', '#111177',
                          '#888800', '#003355', '#008800',
                          '#000022', '#776600', '#008866']

    error_bar_colors = ["#0033BB33", '#7755FF55', '#22FF9955']

    labels = None
    title: str = None
    xlabel: str = None


@dataclass
class PosteriorPlotParams:
    title: str = None  # Plot's title
    max_plot_pages: int = 4  # Maximum number of plots to generate.
    num_plot_rows: int = 4  # Number of rows (subplots) in a plot.
    ncols: int = 2  # Number of columns in the plot.


# Default directory where the model summaries and plots are placed
default_model_info_dir = "model_info"


def get_info_path(info_path=InfoPath()):
    """
    Get full path to the plot ro summary file.

    Parameters
    ----------

    info_path : InfoPath

        Path information for creating summaries.

    """
    info_path = InfoPath(**info_path.__dict__)
    frame = inspect.stack()[info_path.stack_depth]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__

    if info_path.path is None:
        info_path.path = os.path.dirname(codefile)
    else:
        info_path.path = os.path.expanduser(info_path.path)

    full_path = os.path.join(info_path.path, info_path.dir_name)

    if info_path.sub_dir_name is None:
        info_path.sub_dir_name = os.path.basename(codefile).rsplit('.', 1)[0]

    full_path = os.path.join(full_path, info_path.sub_dir_name)
    os.makedirs(full_path, exist_ok=True)
    filename = f'{info_path.base_name}.{info_path.extension}'
    return os.path.join(full_path, filename)


def save_summary(samples, info_path=InfoPath()):
    """
    Generates and saves statistical summary of the samples using mean, std, mode, hpdi.

    Parameters
    ----------

    samples : Panda's dataframe

        Each column contains samples for a parameter.

    info_path : InfoPath

        Path information for creating summaries.
    """

    df_summary, table = sample_summary(samples)
    return save_summary_to_disk(df_summary, table, info_path)


def save_summary_to_disk(df_summary, txt_summary, info_path=InfoPath()):
    """
    Saves statistical summary of the samples using mean, std, mode, hpdi.

    Parameters
    ----------

    df_summary : cmdstanpy.stanfit.CmdStanMCMC

        Panda's dataframe containing the summary for all parameters.

    txt_summary : list of str

        Text of the summary table.

    info_path : InfoPath

        Path information for creating summaries.


    Returns
    --------

    Dict:

        "df" : Dataframe containing summary.

        "table" : text version of the summary.

        "path_txt": Path to txt summary file.

        "path_csv": Path to csv summary file.

    """
    info_path = InfoPath(**info_path.__dict__)
    info_path.base_name = info_path.base_name or "summary"
    info_path.extension = 'txt'
    path_to_summary_txt = get_info_path(info_path)
    info_path.extension = 'csv'
    path_to_summary_csv = get_info_path(info_path)

    with open(path_to_summary_txt, "w") as text_file:
        print(txt_summary, file=text_file)

    df_summary.to_csv(path_to_summary_csv, index_label='Name')

    return {
        "df": df_summary,
        "table": txt_summary,
        "path_txt": path_to_summary_txt,
        "path_csv": path_to_summary_csv,
    }


def get_mode(values):
    """
    Calculates mode of the gaussian distribution
    (value at which it is maximum)
    """

    kernel = gaussian_kde(values)

    # Evaluate the kernel at no more than `max_values` for performance
    max_values = 500
    take_every = int(len(values) / max_values)

    if take_every < 1:
        take_every = 1

    values = values[0::take_every].tolist()
    imax = np.argmax(kernel(values))
    return values[imax]


def sample_summary(df, extra_values=None):
    """
    Prints table showing statistical summary from the sample parameters:
    mean, std, mode, hpdi.

    Parameters
    ------------

    df : Panda's dataframe

        Contains parameter sample values: each column is a parameter.

    extra_values : Panda's dataframe

        Additional values to be shown for parameters. Indexes are
        parameter names, and columns contain additional values to
        be shown in summary.

    Returns
    -------

    Tuple:
        df: Panda's dataframe containing the summary for all parameters.

        summary: text of the summary table
    """
    rows = []

    for column in df:
        values = df[column].to_numpy()
        mean = df[column].mean()
        std = df[column].std()
        mode = get_mode(df[column])
        hpdi68 = az.hpd(values, credible_interval=0.68).tolist()
        uncert_plus = hpdi68[1] - mode
        uncert_minus = mode - hpdi68[0]
        hpdi95 = az.hpd(values, credible_interval=0.95).tolist()

        values = [column, mean, std, mode, uncert_plus,
                  uncert_minus, hpdi68[0], hpdi68[1], hpdi95[0], hpdi95[1]]

        if extra_values is not None:
            # Add extra columns
            values += extra_values.loc[column].values.tolist()

        rows.append(values)

    headers = ['Name', 'Mean', 'Std', 'Mode', '+', '-',
               '68CI-', '68CI+',
               '95CI-', '95CI+']

    if extra_values is not None:
        headers += extra_values.columns.values.tolist()

    formats = [".2f"] * len(headers)

    if 'N_Eff' in headers:
        formats[headers.index('N_Eff')] = ".0f"

    table = tabulate(rows, headers=headers, floatfmt=formats, tablefmt="pipe")

    df_summary = pd.DataFrame(rows, columns=headers, index=df.columns.values)
    df_summary.drop('Name', axis=1, inplace=True)
    return df_summary, table


def make_tree_plot(df_summary, param_names=None, info_path=InfoPath(),
                   tree_params: TreePlotParams = TreePlotParams()):

    info_path = InfoPath(**info_path.__dict__)
    tree_plot_data = extract_tree_plot_data(
        df_summary, param_names=param_names)

    fig, ax = tree_plot(tree_plot_data, params=tree_params)
    info_path.base_name = info_path.base_name or 'summary'
    info_path.extension = info_path.extension or 'pdf'
    the_path = get_info_path(info_path)
    fig.savefig(the_path, dpi=info_path.dpi)


def tree_plot(groups, params: TreePlotParams = TreePlotParams()):
    """
    Make a tree plot of parameters.

    Parameters
    -----------

    group_height : float

        The height of each group, a unmber between 0 and 1.
    """

    sns.set(style="ticks")

    if params.error_bar_cap_size is None:
        error_bar_cap_size = len(groups) / 100

    fig, ax = plt.subplots()

    for i_group, group in enumerate(groups):
        elements_in_group = len(group['values'])

        this_group_height = params.group_height

        if this_group_height is None:
            # Determine group height automatically
            x = len(group['values']) - 1
            this_group_height = 1 - math.exp(-x / 5)

        # Vertical separation between values in one group
        if len(group['values']) == 1:
            y_increment = 0
            this_group_height = 0
        else:
            y_increment = 1 / (elements_in_group - 1) * this_group_height

        # Plot the values for the group
        for i_value, value in enumerate(group['values']):
            y_coord = i_group + this_group_height/2 - y_increment * i_value

            value_label = '_nolegend_'

            if params.labels is not None and i_group == 0:
                value_label = params.labels[i_value]

            ax.scatter(value["position"], y_coord,
                       marker=params.markers[i_value], s=params.markersize,
                       facecolor=params.marker_colors[i_value],
                       edgecolor=params.marker_edge_colors[i_value],
                       linewidth=params.marker_line_width, zorder=5,
                       label=value_label)

            # Plot error bars
            n_error_bars = len(value["error_bars"])

            for i_error_bar, error_bar in enumerate(value["error_bars"]):
                color = params.error_bar_colors[i_error_bar]

                if i_error_bar == n_error_bars - 1:
                    color = params.marker_colors[i_value]

                ax.plot(error_bar, [y_coord, y_coord], zorder=i_error_bar + 1,
                        color=color)

                # Plot caps
                ax.plot([error_bar[0], error_bar[0]],
                        [y_coord - error_bar_cap_size/2,
                         y_coord + error_bar_cap_size/2],
                        zorder=i_error_bar + 1,
                        color=color)

                ax.plot([error_bar[1], error_bar[1]],
                        [y_coord - error_bar_cap_size/2,
                         y_coord + error_bar_cap_size/2],
                        zorder=i_error_bar + 1,
                        color=color)

    group_names = [group['name'] for group in groups]

    # Plot vertical line around zero if neeeded
    if ax.get_ylim()[0] < 0 and ax.get_ylim()[1] > 1:
        ax.axvline(x=0, linestyle='dashed')

    ax.set_yticks(range(0, len(groups)))
    ax.set_yticklabels(group_names)
    ax.grid(axis='x', zorder=-10)

    if params.labels is not None:
        ax.legend()

    if params.xlabel is not None:
        ax.set_xlabel(params.xlabel)

    if params.title is not None:
        ax.set_title(params.title)

    fig.tight_layout()

    if params.ylim is not None:
        ax.set_ylim(params.ylim)

    return (fig, ax)


def extract_tree_plot_data(df, param_names=None, groups=None):
    """
    Extract data used to for tree plot function from a dataframe.

    Parameters
    -----------

    param_names: list of str

        List of parameters to plot. If None, plot all.

    df : Panda's data frame
        Data frame containing summary

    groups : list
        Tree plot data. If passed, the data frame's data will be added to it.
    """

    if groups is None:
        groups = []

    for column_name, row in df.iterrows():
        if param_names is not None:
            # If param_names contains 'a', we will also plot
            # parameters named 'a.1', 'a.2' etc.
            if (column_name not in param_names
               and re.sub(r'\.[0-9]+\Z', '', column_name) not in param_names):
                continue

        column_summary = row
        group_data = None

        # Check if `groups` already has the column
        for group in groups:
            if group['name'] == column_name:
                group_data = group
                break

        # Have not found group data - create one
        if group_data is None:
            group_data = {
                'name': column_name,
                'values': []
            }

            groups.append(group_data)

        # Add new value

        value = {}
        value['position'] = column_summary["Mode"]
        group_data['values'].append(value)

        value['error_bars'] = [
            [column_summary["95CI-"], column_summary["95CI+"]],
            [column_summary["68CI-"], column_summary["68CI+"]]
        ]

    return groups


def save_posterior_plot(samples, summary, param_names=None,
                        info_path=InfoPath(),
                        posterior_plot_params=PosteriorPlotParams()):
    """
    Make histograms for the parameters from posterior destribution.

    Parameters
    -----------

    samples : Panda's DataFrame

        Each column contains samples from posterior distribution.

    summary : Panda's DataFrame

        Summary information about each column.

    param_names : list of str

        Names of the parameters for plotting. If None, all will be plotted.
    """

    info_path = InfoPath(**info_path.__dict__)

    figures_and_axes = plot_posterior(
        samples, summary, param_names=param_names,
        params=posterior_plot_params)

    base_name = info_path.base_name or "posterior"
    info_path.extension = info_path.extension or 'pdf'

    for i, figure_and_axis in enumerate(figures_and_axes):
        info_path.base_name = f'{base_name}_{i + 1:02d}'
        plot_path = get_info_path(info_path)
        figure_and_axis[0].savefig(plot_path, dpi=info_path.dpi)


def make_single_posterior_plot(i_start, samples, summary, param_names,
                               params: PosteriorPlotParams):

    nrows = math.ceil((len(param_names) - i_start) / params.ncols)

    if nrows > params.num_plot_rows:
        nrows = params.num_plot_rows

    fig_height = 4 * nrows

    fig, ax = plt.subplots(
        nrows=nrows,
        ncols=params.ncols, figsize=(12, fig_height),
        squeeze=False)

    axes = ax.flatten()

    for i_axis, ax in enumerate(axes):
        i_param = i_start + i_axis

        if i_param >= len(param_names):
            break

        parameter = param_names[i_param]
        param_samples = samples[parameter]
        data = summary.loc[parameter]

        # Exclude extreme outliers from the samples
        # to avoid the blow-up of the x-axis range
        inner_range = np.percentile(param_samples, [0.5, 99.5])

        samples_for_kde = param_samples[(param_samples > inner_range[0])
                                        & (param_samples < inner_range[1])]

        sns.distplot(samples_for_kde, kde=False, norm_hist=True, ax=ax)

        sns.kdeplot(samples_for_kde, shade=False,
                    clip=[data['95CI-'], data['95CI+']],
                    label='95% HPDI', ax=ax, legend=None,
                    color='#FF9922',
                    linestyle='dotted')

        sns.kdeplot(samples_for_kde, shade=False,
                    clip=[data['68CI-'], data['68CI+']],
                    label='68% HPDI', ax=ax, legend=None,
                    color='#6666FF')

        sns.kdeplot(samples_for_kde, shade=True,
                    clip=[data['68CI-'], data['68CI+']],
                    color='#6666FF', label='_nolegend_', alpha=0.2,
                    ax=ax, legend=None)

        ax.axvline(x=data['Mean'], label='Mean', linewidth=1.5,
                   linestyle='dashed', color='#33AA66')

        ax.axvline(x=data['Mode'], label='Mode', linewidth=1.5,
                   color='#FF66AA')

        ax.set_xlabel(parameter)

    # Do not draw the axes for non-existing plots
    for ax in axes[len(param_names):]:
        ax.axis('off')

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper center', mode='expand',
               ncol=len(labels))

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    return (fig, ax)


def plot_posterior(samples, summary, param_names=None,
                   params=PosteriorPlotParams()):
    """
    Make histograms for the parameters from posterior destribution.

    Parameters
    -----------

    samples : Panda's DataFrame

        Each column contains samples from posterior distribution.

    summary : Panda's DataFrame

        Summary information about each column.

    param_names : list of str

        Names of the parameters for plotting. If None, all will be plotted.
    """
    param_filtered = samples.columns

    if param_names is not None:
        # If param_names contains 'a', we will also plot
        # parameters named 'a.1', 'a.2' etc.
        param_filtered = [
            a for a in param_filtered
            if a in param_names
            or (re.sub(r'\.[0-9]+\Z', '', a) in param_names)
        ]

    param_names = param_filtered

    # Total number of plots
    n_plots = math.ceil(math.ceil(len(param_names) / params.ncols) / \
        params.num_plot_rows)

    if n_plots > params.max_plot_pages:
        print((
            f'Showing only first {params.max_plot_pages} '
            f'pages out of {n_plots} of posterior plots.'
            'Consider specifying "param_names".'))

        n_plots = params.max_plot_pages

    if n_plots < 1:
        n_plots = 1

    figures_and_axes = []

    # Make multople traceplots
    for i_plot in range(n_plots):
        fig, ax = make_single_posterior_plot(
            i_start=i_plot * params.num_plot_rows * params.ncols,
            samples=samples,
            summary=summary,
            param_names=param_names,
            params=params)

        figures_and_axes.append([fig, ax])

    return figures_and_axes

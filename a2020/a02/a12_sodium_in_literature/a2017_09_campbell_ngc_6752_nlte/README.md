# Analysing data from Campbell et al. 2017, NGC 6752 NLTE

We ran the statistical model using NLTE data from [Campbell et al. 2017](https://arxiv.org/abs/1707.02840v1) paper for NGC 6752 globular cluster.

Code: [a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/code/model.py](a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/code/model.py)

Some stars had zero uncertainties for the abundances. We have replaced these uncertainties with `0.01`.

## Observations

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances.


## Results


### Parameter distributions

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     |                        r |                     mu.1 |                     mu.2 |
|----:|-------------------------:|-------------------------:|-------------------------:|
| AGB | $`0.05^{+0.56}_{-0.05}`$ | $`4.59^{+0.02}_{-0.13}`$ | $`4.68^{+0.06}_{-0.07}`$ |
| RGB | $`0.68^{+0.16}_{-0.18}`$ | $`4.64^{+0.15}_{-0.11}`$ | $`5.10^{+0.06}_{-0.07}`$ |


### AGB failure rate

We calculated failure rate f=$`0.90^{+0.1}_{-0.8}`$, which is defined as proportion of SP2 stars that fail to reach AGB:

```
f = 1 - r_agb / r_rgb
```


## Analysis


### RGB stars

Distribution of `r` agrees with previous $`r = 0.7 \pm 0.1`$ calculated from [Campbell et al. 2013 data](a2020/a02/a12_sodium_in_literature/a2013_06_campbell_ngc_6752).


### AGB stars

The 68% HDPIs of two populations overlap. In addition, parameter `r` is uncertain (Fig. 3). We conclude there is very little evidence for presence of two populations.

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/agb_r.png" width="700" alt='Distribution of parameter $r$ for AGB stars.'>

Figure 3: Distribution of parameter $r$ for AGB stars.



### AGB failure rate

The distribution of `f` is shown on Fig. 5 is almost uniform. Therefore, the model does not help to constrain parameter `f`.

<img src="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/images/agb_failure_rate.png" width="700" alt='AGB failure rate'>

Figure 5: Distribution of AGB failure rate.

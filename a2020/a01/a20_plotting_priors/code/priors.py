import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from scipy import stats
from code_dope.python.plot.plot import save_plot


def distribution(x, p, mu1, mu2, sigma1, sigma2):
    return p * stats.norm.pdf(x, mu1, sigma1) + \
        (1 - p) * stats.norm.pdf(x, mu2, sigma2)


def do_work():
    np.random.seed(seed=4231)
    sns.set(style="ticks")
    n = 300
    mu1_samples = stats.norm(-1, 0.5).rvs(size=n)
    mu2_samples = stats.norm(1, 0.5).rvs(size=n)
    sigma1_samples = stats.lognorm(1).rvs(size=n)
    sigma2_samples = stats.lognorm(1).rvs(size=n)
    p_samples = stats.uniform(0, 1).rvs(size=n)

    x = np.linspace(-10, 10, 500)

    result = [
        distribution(i, p=p_samples, mu1=mu1_samples, mu2=mu2_samples,
                     sigma1=sigma1_samples, sigma2=sigma2_samples)
        for i in x
    ]

    result = np.array(result)

    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    mean = df["na_over_h"].mean()
    std = df["na_over_h"].std()
    x_transformed = x * std + mean
    fig, ax = plt.subplots()

    for i in range(200):
        ax.plot(x_transformed, result[:, i], color="#00000011")

        ax.set_xlabel("Sodium abundance [Na/H]")
        ax.set_ylabel("Probability density")
        ax.set_ylim(0, 2)
        fig.tight_layout()

    save_plot(plt=fig)

if __name__ == '__main__':
    do_work()
    print('We are done')

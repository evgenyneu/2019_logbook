// Guassian mixture model
data {
  int<lower=1> n;        // Number of data points
  real y[n];             // Observed values
  real uncertainties[n]; // Uncertainties
  real sigma_stdev_prior;// Prior for standard deviation of population spread
}
parameters {
  real<lower=0,upper=1> r;   // Mixing proportion
  ordered[2] mu;             // Locations of mixture components
  real<lower=0> sigma;       // Spread of mixture components
  vector[n] y_true;          // Estimated "true" values
}
model {
  // Set priors
  sigma ~ normal(0, sigma_stdev_prior);
  mu[1] ~ normal(0, 1);
  mu[2] ~ normal(0, 1);
  r ~ beta(2, 2);


  // Loop through observed values
  // and mix two Gaussian components accounting for uncertainty
  // of each measurent
  for (i in 1:n) {
    target += log_mix(1 - r,
                      normal_lpdf(y[i] | mu[1], sigma),
                      normal_lpdf(y[i] | mu[2], sigma));
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}
generated quantities{
  vector[n] lpd_pointwise;

  for (i in 1:n) {
    lpd_pointwise[i] = normal_lpdf(y[i] | y_true[i], uncertainties[i]);
  }
}

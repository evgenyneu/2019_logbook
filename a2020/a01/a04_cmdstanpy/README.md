## Reinstalling RStan

I had a problem with rstan - it did not show any error messages if the *.stan model had syntax errors. I reinstalled rstan following [instructions from stackoverflow](https://stackoverflow.com/questions/59572171/how-to-view-warnings-for-incorrect-stan-model-in-rstan) and now I can see the error messages.

```
install.packages("rstan", type = "source")
```

Code: [a2020/a01/a04_cmdstanpy/code/8schools.stan](a2020/a01/a04_cmdstanpy/code/8schools.stan)

## Installing cmdtanpy

I'm installing [cmdstanpy](https://cmdstanpy.readthedocs.io/en/latest/getting_started.html#installation) because Stan model compilation is faster.


```
pip install --upgrade cmdstanpy[all]
install_cmdstan
ls -F ~/.cmdstanpy
```

## Run cmdstanpy example`

Code: [a2020/a01/a04_cmdstanpy/code/cmdtanpy_test.py](a2020/a01/a04_cmdstanpy/code/cmdtanpy_test.py)

library(rethinking)
data(WaffleDivorce)
d <- WaffleDivorce
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

data <- list(
    D_obs = standardize( d$Divorce ),
    D_sd = d$Divorce.SE / sd( d$Divorce ),
    M = standardize( d$Marriage ),
    A = standardize( d$MedianAgeMarriage ),
    N = nrow(d)
)

# Run Stan
model_path = 'a2019/a12/a30_use_data_with_uncertainties/play_with_uncert.stan'
fit <- stan(file=model_path, data=data)

print(fit)

# Compare single vs two-gaussian models using data from NGC 6752 Lapenna et al. 2016 AGB

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2016_07_lapenna_ngc_6752/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| Two populations |  53.20 | 5.30 |         |       |    3.68 |   0.44 |     0.88 |
| One population  |  60.59 | 5.07 |    7.39 |  5.34 |    1.64 |   0.33 |     0.12 |

|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| Two populations |  53.03 | 5.24 |         |       |    3.59 |     0.89 |
| One population  |  60.55 | 5.05 |    7.52 |  5.28 |    1.62 |     0.11 |


## Conclusion

Preference for two-gaussian model since PSIS/WAIC values differ by about one standard error.

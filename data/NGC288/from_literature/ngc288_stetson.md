# NGC 288 Stetson+ data

Data from [Stetson+ 2019](https://ui.adsabs.harvard.edu/?#abs/2019MNRAS.485.3042S) catalog, table `CDS/J/MNRAS/485/3042/table4`.


See [logbook record](a2019/a12/a01_stetson).

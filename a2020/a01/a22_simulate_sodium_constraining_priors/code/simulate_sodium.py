from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from run_stan import standardize, destandardize, run_stan
from code_dope.python.plot.plot import save_plot
from tarpan.shared.summary import save_summary
from tarpan.shared.info_path import InfoPath
from tarpan.shared.histogram import save_histogram

from tarpan.shared.tree_plot import (
    TreePlotParams, make_comparative_tree_plot, summary_from_dict)


def simulate_sample(mu_poor, mu_rich, seed):
    """
    Simulate observations of two populations of stars:
    sodium-poor and sodium-rich stars
    """

    np.random.seed(seed=seed)

    n_poor = 10  # Number of Sodium poor stars
    n_rich = 17  # Number of Sodium rich stars
    n = n_poor + n_rich  # Total sample size of observed stars

    # Generate random values from Normal distributions
    sigma = 0.05
    samples_poor = norm(mu_poor, sigma).rvs(size=n_poor)
    samples_rich = norm(mu_rich, sigma).rvs(size=n_rich)

    # Combine all samples in one list
    values_true = np.concatenate([samples_poor, samples_rich])

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(0.13, 0.06).rvs(size=n))

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)

    p_true = n_poor / n

    return (
        values_true, values_observed, uncertainties,
        p_true, sigma, sigma)


def plot_simulated_values(values_true, values_observed, uncertainties,
                          mu_poor, mu_rich):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()
    ax1.scatter(values_true, range(0, len(values_true)),
                label='Simulated true', facecolors="none", edgecolors='black')

    ax1.scatter(values_observed, range(0, len(values_true)),
                color="#0000ff44", s=100, label='Observed')

    ax1.errorbar(values_observed, range(0, len(values_true)),
                 xerr=uncertainties,
                 color="#0000ff22", fmt='none')

    ax1.legend()
    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")
    ax2.axvline(mu_poor, color=color_observed, linestyle="dashed")
    ax2.axvline(mu_rich, color=color_observed, linestyle="dashed")

    # Show the plot
    # -------

    fig.suptitle("Simulated true and observed sodium abundances")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix='simulated')


def run_model(values_observed,
              uncertainties, seed, sub_dir_name, stan_model_path):
    values_std, uncertainties_std = standardize(values_observed, uncertainties)

    data_for_model = {
        'y': values_std,
        'uncertainties': uncertainties_std,
        'N': len(values_std)
    }

    samples_std = run_stan(
        data_for_model,
        sub_dir_name=sub_dir_name,
        stan_model_path=stan_model_path,
        seed=seed)

    samples = destandardize(samples_std, values_observed)

    info_path = InfoPath(sub_dir_name=f'{sub_dir_name}_destandardized')
    info_path.set_codefile()
    summary = save_summary(samples, info_path=info_path)
    return samples, summary


def make_tree_plot_compare_with_true(summary_weak,
                                     summary_informative, true_params):
    all_summaries = []
    all_summaries.append(summary_from_dict(true_params))
    all_summaries.append(summary_weak)

    # Use sigma.1 and sigma.2 in informative prior data
    summary_informative = summary_informative.rename(index={"sigma": "sigma.1"})
    sigma1_row = summary_informative.loc['sigma.1']
    summary_informative.loc['sigma.2'] = sigma1_row
    all_summaries.append(summary_informative)

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name="weak_and_informative_models", extension="png")

    info_path.set_codefile()

    tree_params = TreePlotParams(
        title="Posterior distributions for two simulated populations"
    )

    tree_params.labels = [
        "Simulated true",
        "Weak priors",
        "Informative priors"
    ]

    make_comparative_tree_plot(
        all_summaries, info_path=info_path,
        tree_params=tree_params)


def do_work():
    seed = 333
    mu_poor = -1.1
    mu_rich = -0.6

    values_true, values_observed, \
        uncertainties, p_true, sigma1, sigma2 = simulate_sample(
            mu_poor=mu_poor, mu_rich=mu_rich, seed=seed)

    plot_simulated_values(
        values_true=values_true,
        values_observed=values_observed,
        uncertainties=uncertainties,
        mu_poor=mu_poor,
        mu_rich=mu_rich)

    samples_weak, summary_weak = run_model(
        values_observed, uncertainties, seed=seed,
        sub_dir_name="model_weak",
        stan_model_path="a2020/a01/a05_python_model/code/stan_model/\
sodium_ratio_model.stan")

    samples_informative, summary_informative = run_model(
        values_observed, uncertainties, seed=seed,
        sub_dir_name="model_informative",
        stan_model_path="a2020/a01/a22_simulate_sodium_constraining_priors/\
code/stan_model/sodium_ratio_model.stan")

    # Pair plot
    # ----------

    pair = sns.pairplot(samples_weak, diag_kind='kde')
    save_plot(plt=pair, suffix="pair_weak", dpi=100)

    pair = sns.pairplot(samples_informative, diag_kind='kde')
    save_plot(plt=pair, suffix="pair_informative", dpi=100)

    # Make tree plot
    # --------

    true_params = {
        'p': p_true,
        'mu.1': mu_poor,
        'mu.2': mu_rich,
        'sigma.1': sigma1,
        'sigma.2': sigma1
    }

    make_tree_plot_compare_with_true(
        summary_weak['df'], summary_informative['df'], true_params)

    # Posterior plots
    # ------

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name="posterior_weak", extension="png")

    info_path.set_codefile()

    save_histogram(samples_weak, summary_weak['df'], info_path=info_path)

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name="posterior_informative", extension="png")

    info_path.set_codefile()

    save_histogram(samples_informative, summary_informative['df'],
                   info_path=info_path)


if __name__ == '__main__':
    do_work()
    print('We are done')

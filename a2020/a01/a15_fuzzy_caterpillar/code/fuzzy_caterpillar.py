"""
This is a proof of concept. Runs Stan via cmdstanpy and shows a live plot
of the log probability samples. Works both in terminal and Jupyter.

Videos
-------

Terminal: https://youtu.be/f6BpCAHXCvY

Jupyter: https://youtu.be/Ku9JsJAClgI


Usage
-----

Terminal:

    python fuzzy_caterpillar.py

Jupyter:

    from fuzzy_caterpillar import do_work
    do_work(is_jupyter=True)

Dependencies
-----------

* ipycanvas
* numpy
* tkinker

"""

import tkinter as tk
from cmdstanpy import CmdStanModel
from threading import Thread
from queue import Queue as ThreadQueue
from multiprocessing import Process, Event
from multiprocessing import Queue as ProcessQueue
import queue
from pathlib import Path
import matplotlib.pyplot as plt
import os
import collections
import numpy as np
import time
from ipycanvas import Canvas, hold_canvas
from itertools import cycle


END_OF_QUEUE = "We are done here"


def follow(file, quit):
    """
    Watch the file and return the lines that are being added.
    Similar to Linux "tail -f" command.


    Parameters
    ----------

    quit: Event

        The even for stopping the watch loop.

    Returns
    -------

    Generator of lines.
    """

    while not quit.is_set():
        line = file.readline()

        if not line:
            time.sleep(0.1)
            continue

        yield line


def run_model(csv_dir, is_jupyter):
    model = CmdStanModel(stan_file="stan_model/eight_schools.stan")

    data = {
        "J": 8,
        "y": [28,  8, -3,  7, -1,  1, 18, 12],
        "sigma": [15, 10, 16, 11,  9, 11, 10, 18]
    }

    os.makedirs(csv_dir, exist_ok=True)

    show_progress = True

    if is_jupyter:
        show_progress = "notebook"

    model.sample(data=data, show_progress=show_progress,
                 chains=4, cores=4,
                 sampling_iters=20000, warmup_iters=2000,
                 output_dir=csv_dir)


def is_number(s):
    """
    Returns True is `s` is a number.
    """

    try:
        float(s)
        return True
    except ValueError:
        return False


class RepeatTimer(Thread):
    """
    Run `function` repeatedly after `interval` number of seconds.
    The `function` will be called with args and kwargs arguments.
    """

    def __init__(self, interval, stop_event, function, *args, **kwargs):
        Thread.__init__(self)
        self.interval = interval
        self.stopped = stop_event
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.function(*self.args, **self.kwargs)

        while not self.stopped.wait(self.interval):
            self.function(*self.args, **self.kwargs)


def empty_csv_dir(csv_dir):
    """
    Remove files from the CSV dir.
    """

    files_to_remove = list(Path(csv_dir).glob("*.csv"))
    files_to_remove += list(Path(csv_dir).glob("*.txt"))

    for file in files_to_remove:
        os.remove(file)


def jupyter_plot_chains(canvas, chains, canvas_width, canvas_height):
    """
    Plotting all the chains at once to improve performance.
    """

    with hold_canvas(canvas):
        for chain_name, chain in chains.items():
            values = chain["values"]

            if values is None:
                continue

            chain["values"] = None
            x = chain["x"]
            y = chain["y"]
            n_values = len(values)

            if x + n_values > canvas_width:
                canvas.clear_rect(0, 0, canvas_width, canvas_height)

                for chain_name, chain in chains.items():
                    chain["x"] = 0

                x = 0
                y = np.mean(values)

            canvas.stroke_style = chain["color"]
            canvas.begin_path()
            canvas.move_to(x, y)

            for i in range(n_values):
                x += 1
                y = values[i]
                canvas.line_to(x, y)

            canvas.stroke()
            chain["x"] = x
            chain["y"] = y


def tkinker_plot_chains(canvas, chains, canvas_width, canvas_height):
    """
    Plotting all the chains at once to improve performance.
    """

    for chain_name, chain in chains.items():
        values = chain["values"]

        if values is None:
            continue

        chain["values"] = None
        x = chain["x"]
        y = chain["y"]
        n_values = len(values)

        if x + n_values > canvas_width:
            canvas.create_rectangle(0, 0, canvas_width, canvas_height,
                                    fill="#fff")

            for chain_name, chain in chains.items():
                chain["x"] = 0

            x = 0
            y = np.mean(values)

        x += 1
        xvalues = list(range(x, x + n_values))
        coord = [val for pair in zip(xvalues, values) for val in pair]

        if len(coord) >= 4:
            canvas.create_line(coord, fill=chain["color"])

        chain["x"] = x + n_values
        chain["y"] = values[-1]

    canvas.update()


def universal_plotter(q, is_jupyter):
    """
    Show the samples in a tkinker or jupyter.

    Parameters
    ---------

    q : Queue

        Contains 2-element arrays:

            0: Intex of the chain: 0, 1, 2, etc.

            1: probability values.

    is_jupyter : bool

        True if plotting from Jupyter.
    """

    canvas_height = 200
    canvas_width = 800

    if is_jupyter:
        from IPython.display import display
        canvas = Canvas(size=(canvas_width, canvas_height))
        canvas.fill_style = 'white'
        display(canvas)
        plot_chains = jupyter_plot_chains

    else:
        master = tk.Tk()
        canvas = tk.Canvas(master, width=canvas_width, height=canvas_height)
        canvas.pack()
        plot_chains = tkinker_plot_chains

    linecolors = ["#FF9922", "#3366FF", "#22FF22", "#FF0000", "#99BB77"]
    color_cycler = cycle(linecolors)
    chains_to_plot = []
    chains = {}
    scale_min = 10000000
    scale_max = -10000000

    while True:
        msg = q.get()

        if (msg == END_OF_QUEUE):
            break

        chain_name = msg[0]
        values = np.array(msg[1])

        if chain_name in chains_to_plot:
            plot_chains(canvas, chains, canvas_width, canvas_height)
            chains_to_plot = []

        chains_to_plot.append(chain_name)

        if chain_name not in chains:
            chain = {}
            chain["x"] = 0
            chain["y"] = canvas_height / 2
            chain["values"] = None
            chain["color"] = next(color_cycler)
            chains[chain_name] = chain

        chain = chains[chain_name]

        if np.min(values) < scale_min:
            scale_min = np.min(values)

        if np.max(values) > scale_max:
            scale_max = np.max(values)

        # Map probability values to interval between 0 and 1
        values = np.interp(values, [scale_min, scale_max], [0, 1])
        values = values * 0.6 + 0.2  # Add margins
        values *= canvas_height  # Convert to canvas coordinates
        chain["values"] = values

    if chains_to_plot != []:
        plot_chains(canvas, chains, canvas_width, canvas_height)


def matplotlib_plot_chains(ax, fig, chains, background):
    """
    Plotting all the chains at once to improve performance.
    """

    fig.canvas.restore_region(background)

    for chain_name, chain in chains.items():
        line = chain["line"]
        line.set_data(chain["xdata"], chain["ydata"])
        ax.draw_artist(line)

    fig.canvas.blit(ax.bbox)
    plt.pause(0.001)


def matplotlib_plotter(q):
    """
    Show the samples in a matplotlib window.

    Parameters
    ---------

    q : Queue

        Contains 2-element arrays:

            0: Intex of the chain: 0, 1, 2, etc.

            1: probability values.
    """

    plt.rcParams['toolbar'] = 'None'
    fig, ax = plt.subplots()
    ax.set_ylim(0, 1)
    plt.axis('off')
    plt.tight_layout()
    xlim = 1000
    ax.set_xlim(0, xlim)
    fig.show()
    fig.canvas.draw()
    chains = {}
    chains_to_plot = []
    zorder = 1
    background = fig.canvas.copy_from_bbox(ax.bbox)
    scale_min = 10000000
    scale_max = -10000000

    while True:
        msg = q.get()

        if (msg == END_OF_QUEUE):
            break

        chain_name = msg[0]
        values = msg[1]

        if chain_name not in chains:
            chain = {}
            chain["xdata"] = [0]
            chain["ydata"] = [0]
            line, = ax.plot([0], [0], animated=True, alpha=0.6, zorder=zorder)
            line.zorder = zorder
            zorder += 1
            chain["line"] = line
            chains[chain_name] = chain

        chain = chains[chain_name]

        xdata = chain["xdata"]
        ydata = chain["ydata"]
        line = chain["line"]

        values = np.array(values)
        min_values = np.min(values)

        if min_values < scale_min:
            scale_min = min_values

        max_values = np.max(values)

        if max_values > scale_max:
            scale_max = max_values

        values = np.interp(values, [scale_min, scale_max], [0, 1])
        values = list(values*0.6 + 0.2)  # Add margins

        if len(xdata) + len(values) > xlim:
            for chain_name, chain in chains.items():
                chain["xdata"] = [0]
                chain["ydata"] = [0]

            xdata = [0]
            ydata = [0]

        xdata += list(range(len(xdata), len(xdata) + len(values)))
        ydata += values
        chain["xdata"] = xdata
        chain["ydata"] = ydata

        if chain_name in chains_to_plot:
            matplotlib_plot_chains(ax, fig, chains, background)
            chains_to_plot = []
        else:
            chains_to_plot.append(chain_name)

    if chains_to_plot != []:
        matplotlib_plot_chains(ax, fig, chains, background)


def plotter(q, is_jupyter):
    universal_plotter(q, is_jupyter)
    # matplotlib_plotter(q)  #  This matplolib plotting is a bit slow


def model_runner(q, csv_dir, is_jupyter):
    """
    Run Stan
    """

    run_model(csv_dir=csv_dir, is_jupyter=is_jupyter)
    q.put(END_OF_QUEUE)  # Model has finished, signal other processes to finish


def young_csv_files(scan_dir, age):
    """
    Return paths to recently created CSV files in directory.

    Parameters
    ---------

    scan_dir : str

        Path to directory where to look for csv files.

    age : float

        Maximum age of the file in seconds, i.e. time since the
        file was created
    """

    # Get CSV files in directory
    files = Path(scan_dir).glob("*.csv")

    # Get only recently modified files
    return (
        file for file in files
        if (time.time() - file.stat().st_ctime) < age
    )


def track_young_csv_files(scan_dir, age, quit):
    """
    Scan for newly created csv files in directory.

    Parameters
    ---------

    scan_dir : str

        Path to directory where to look for csv files.

    age : float

        Maximum age of the file in seconds, i.e. time since the
        file was created

    quit : Event

        Event for stopping the scanning.

    Returns
    -------

    Generator of paths to the csv files
    """

    consumed = []  # CSV paths that have already been processed

    while not quit.is_set():
        paths = young_csv_files(scan_dir=scan_dir, age=age)
        paths = (path for path in paths if path not in consumed)

        for path in paths:
            consumed.append(path)
            yield path

        time.sleep(0.1)


def file_scanner(scan_dir, age, path_q, quit):
    """
    Scans for newly created CSV files and puts them to a queue.

    Parameters
    ---------

    scan_dir : str

        Path to directory where to look for csv files.

    age : float

        Maximum age of the file in seconds, i.e. time since the
        file was created

    quit : Event

        Event for stopping the scanning.


    path_q : Queue

        Queue where the paths to CSV files are placed.

    """

    for path in track_young_csv_files(scan_dir, age, quit):
        path_q.put(path)

    path_q.put(END_OF_QUEUE)


def clear_queue(q):
    """
    Removes all items form the queue.
    """

    collections.deque(current_items_in_queue(q), maxlen=0)


def current_items_in_queue(q):
    """
    Get all existing items from the queue without waiting for new ones.
    """

    try:
        while True:
            item = q.get_nowait()

            if item == END_OF_QUEUE:
                break

            yield item

    except queue.Empty:
        return


def read_probabilities(q, n_values, plotting_q, chain_id):
    """
    Reads all probability values from the `q` queue.
    From those value, takes `n_values` evenly spaced ones and
    puts them into `plotting_q` queue for plotting.

    The point of this is to avoid plotting probability values
    from all samples, since it will be very slow for large number
    of samples.

    Parameters
    ----------

    q : Queue

        Raw probability values first CSV's column

    n_values : int

        Maximum number of probability values to plot.

    plotting_q : Queue

        Queue containing values for plotting.

    chain_id : int

        Chain id: 0, 1, 2 etc.
    """

    items = list(current_items_in_queue(q))

    if len(items) == 0:
        return

    # Get `n_values` evenly spaced values from the prob array
    every_nth = int(len(items) / n_values) + 1
    items = items[::every_nth]
    plotting_q.put([int(chain_id), items])


def read_csv(chain_id, path, plotting_q, quit):
    """
    Reads probability values from a single CSV files and adds them to the
    plotting queue.

    Parameters
    ----------

    chain_id : int

        Chain id: 0, 1, 2 etc.

    path : str

        Path to the CSV file.

    plotting_q : Queue

        Queue containing values for plotting.

    quit : Event

        Event for stopping the reading of the files.
    """
    with open(path) as file:
        # Get the first values before the commas from each line
        prob = (line.split(',', 1)[0] for line in follow(file, quit))

        # Convert to floats
        prob = (float(a) for a in prob if is_number(a))

        # Repeatedly call read_probabilities function
        # that will send some of the read probability numbers for plotting.
        # We can't send all the numbers, since the plotting will be slow.
        q = ThreadQueue()

        timer = RepeatTimer(interval=.1,
                            stop_event=quit,
                            function=read_probabilities,
                            q=q, n_values=20, plotting_q=plotting_q, chain_id=chain_id)

        timer.start()

        # Read the probabilities
        for i in prob:
            q.put(i)

        q.put(END_OF_QUEUE)


def process_csvs(path_q, plotting_q, quit):
    """
    Reads paths to CSV files from `path_q` queue, creates new processes
    for reading them.

    Parameters
    ---------

    path_q : Queue

        Contains paths to CSV files.

    plotting_q : Queue

        Contains probability values for plotting.

    quit : Event

        Event for stopping the reading of the files.

    """

    chain_id = 0

    while True:
        msg = path_q.get()

        if (msg == END_OF_QUEUE):
            break

        reader = Process(target=read_csv, args=(chain_id, msg, plotting_q, quit))
        reader.start()
        chain_id += 1


def do_work(is_jupyter):
    """
    Main function that does all the hard work.
    """

    csv_dir = 'stan_model/csv_files'
    empty_csv_dir(csv_dir)
    quit = Event()

    path_q = ProcessQueue()
    files = Process(target=file_scanner, args=(csv_dir, 1, path_q, quit))
    files.start()

    plotting_q = ProcessQueue()
    csv_processor = Process(target=process_csvs, args=(path_q, plotting_q, quit))
    csv_processor.start()

    model = Process(target=model_runner, args=(plotting_q, csv_dir, is_jupyter))
    model.start()

    plotter(plotting_q, is_jupyter)
    quit.set()
    print("Sampling finished")


if __name__ == '__main__':
    do_work(is_jupyter=False)

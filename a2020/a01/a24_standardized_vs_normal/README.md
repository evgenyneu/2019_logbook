## Using standardized vs non-standardized data

I want to compare the model with non-standardized and standardised (i.e. z-score, which is value minus mean times standard deviation) values for the data (observed sodium abundances). I want to see what difference is makes on posterior distributions of model parameters.

Code [a2020/a01/a24_standardized_vs_normal/code/standardized_vs_normal.py](a2020/a01/a24_standardized_vs_normal/code/standardized_vs_normal.py).

### Observed data

In the model I'm using the data from Chris' report, shown in Figure 1.

<img src="a2020/a01/a24_standardized_vs_normal/code/plots/standardized_vs_normal_AGB_observed_data.png" width="400" alt='Sodium abundaces of AGB stars'>

<img src="a2020/a01/a24_standardized_vs_normal/code/plots/standardized_vs_normal_RGB_observed_data.png" width="400" alt='Sodium abundaces of AGB stars'>

Figure 1: Observed sodium abundances for AGB and RGB stars.

### Statistical model

We use the following models for AGB/RGB data.

#### AGB stars

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma) \\
\mu_1 \sim \textrm{Normal}(-1.4, 0.15) \\
\mu_2 \sim \textrm{Normal}(-1, 0.15) \\
\sigma \sim \textrm{HalfNoormal}(0, 0.1) \\
p \sim \textrm{Beta}(2, 2).
```

#### RGB stars

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma) \\
\mu_1 \sim \textrm{Normal}(-1.1, 0.15) \\
\mu_2 \sim \textrm{Normal}(-0.6, 0.15) \\
\sigma \sim \textrm{HalfNoormal}(0, 0.1) \\
p \sim \textrm{Beta}(2, 2).
```

### Posterior distributions of parameters

On Figure 2 we can see the comparison of posterior distributions of parameters.

<img src="a2020/a01/a24_standardized_vs_normal/code/plots/AGB_normal_and_standardized_models.png" width="400" alt='Comparing posterior distributions of standardized vs non-standardized values for AGB stars'>

<img src="a2020/a01/a24_standardized_vs_normal/code/plots/RGB_normal_and_standardized_models.png" width="400" alt='Comparing posterior distributions of standardized vs non-standardized values for RGB stars'>

Figure 2: Comparing posterior distributions of models that use standardized vs non-standardized observation values.


### Summaries of posterior distributions

The summaries of posterior distributions for the model with non-standardized values are shown in Tables 1 and 2.

Table 1: Summary of posterior distributions of parameters for a AGB observations

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| p      |   0.60 |  0.14 |   0.61 | 0.14 | 0.15 |    0.47 |    0.75 |    0.31 |    0.87 |
| mu.1   |  -1.35 |  0.06 |  -1.34 | 0.04 | 0.06 |   -1.40 |   -1.30 |   -1.47 |   -1.24 |
| mu.2   |  -0.92 |  0.09 |  -0.91 | 0.07 | 0.08 |   -0.99 |   -0.84 |   -1.12 |   -0.76 |
| sigma  |   0.09 |  0.05 |   0.07 | 0.05 | 0.05 |    0.02 |    0.12 |    0.01 |    0.19 |


Table 2: Summary of posterior distributions of parameters for a RGB observations

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| p      |   0.36 |  0.09 |   0.34 | 0.10 | 0.08 |    0.26 |    0.45 |    0.19 |    0.55 |
| mu.1   |  -1.19 |  0.04 |  -1.19 | 0.04 | 0.04 |   -1.23 |   -1.15 |   -1.28 |   -1.10 |
| mu.2   |  -0.62 |  0.04 |  -0.62 | 0.04 | 0.03 |   -0.65 |   -0.58 |   -0.69 |   -0.55 |
| sigma  |   0.08 |  0.03 |   0.07 | 0.03 | 0.02 |    0.05 |    0.10 |    0.03 |    0.14 |


### Sampling problems

During sampling I got the following warnings about divergent transitions and E-BFMI for AGB data for both standardized and non-standardized values:

```
Checking sampler transitions treedepth.
Treedepth satisfactory for all transitions.

Checking sampler transitions for divergences.
252 of 60000 (0.42%) transitions ended with a divergence.
These divergent transitions indicate that HMC is not fully able to explore the posterior distribution.
Try increasing adapt delta closer to 1.
If this doesn't remove all divergences, try to reparameterize the model.

Checking E-BFMI - sampler transitions HMC potential energy.
The E-BFMI, 0.28, is below the nominal threshold of 0.3 which suggests that HMC may have trouble exploring the target distribution.
If possible, try to reparameterize the model.

Effective sample size satisfactory.

Split R-hat values satisfactory all parameters.

Processing complete.
```

The sampling with RGB sapling went well, with `1 of 60000 (0.0017%) transitions` warning for non-standardized values.


### Discussion

We can see from Figure 2 that distributions of both types of models (standardized and non-standardized) are in close agreement. Since a model with natural, non-standardized data is easier to work with and communicate, we will use non-standardized model from now on.

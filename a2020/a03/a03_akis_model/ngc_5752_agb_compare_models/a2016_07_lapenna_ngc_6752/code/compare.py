import os

from a2020.a03.a03_akis_model.ngc_5752_agb_compare_models.code.compare import (
    AnalysisSettings, compare_models)


def do_work():
    path = "a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/\
a2016_07_lapenna_ngc_6752"

    settings = AnalysisSettings(
        path=os.path.join(path, "model_info"),
        study_name="AGB NGC 6752 Lapenna et al. 2016",
        xlabel="Sodium abundance [Na/Fe]",
        type_name="Type",
        abundance_name="[Na/Fe]",
        uncertainty_name="e_[Na/Fe]",
        data_path=os.path.join(path, 'data/2016_07_lapenna_ngc_6752.csv')
    )

    compare_models(settings)


if __name__ == '__main__':
    print("Running the models...")

    do_work()

    print('We are done')

import pandas as pd
import os


def combine():
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open photometry data
    dest_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df_dest = pd.read_csv(dest_path)

    # Open elements from Chris
    souce_path = os.path.join(data_dir, 'elements_final.csv')
    df_source = pd.read_csv(souce_path)

    # Add new columns
    columns = list(df_source)
    columns.remove('star')

    for column in columns:
        df_dest.insert(loc=len(df_dest.columns), column=column, value='')

    # Go over rows
    for index_source, star_source in df_source.iterrows():
        star_id = star_source['star']
        star_dest = df_dest.loc[(df_dest['id'] == star_id)]
        index_dest = star_dest.index

        if star_dest.shape[0] != 1:
            raise ValueError(f"Can't find star {star_id}")

        # Check the star is selected for observations
        # ----------

        is_selected = star_dest["selected"].values[0]

        if is_selected != 1:
            raise ValueError(f"Star {star_id} is not selected for observations")

        for i_column, column in enumerate(columns):
            df_dest.loc[index_dest, column] = star_source[column]

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_abundances_from_chris.csv')
    df_dest.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    combine()
    print('We are done!')

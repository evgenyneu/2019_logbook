## Calculating distance with uncertainty from distance modulus

Example of calculating distance to NGC 288 using visual distance modulus $`V - M_V = 14.84 \pm 0.1`$ and color excess of 0.03.

```Python
from uncertainties import ufloat
from a2019.a12.a10_calculate_distance.code.distance import get_distance

distance = get_distance(ufloat(14.84, 0.1), color_excess=0.03)
print(f'Distance: {distance} pc')

# (8.9+/-0.4)e+03  pc
```

Code: [a2019/a12/a10_calculate_distance/code/distance.py](a2019/a12/a10_calculate_distance/code/distance.py)

### Notes

* The above code requires `uncertainties` Python package.

* Values for $`V - M_V = 14.84`$ and $`E_{B - V}=0.03`$ are taken from [Harris catalog 2010](http://physwww.mcmaster.ca/~harris/mwgc.dat).

* Uncertainty for $`V - M_V`$ for NGC 288 was given by W.E. Harris via email:

> I would suggest using +-0.1 magnitude uncertainty for the distance modulus.


## Color magnitude

### B vs B-K


Code: [a2019/a12/a10_calculate_distance/code/cmd_b_minus_k.py](a2019/a12/a10_calculate_distance/code/cmd_b_minus_k.py)


<img src="a2019/a12/a10_calculate_distance/code/plots/cmd_b_minus_k.png" width="700" alt='Colour magnitude diagram of comoving stars in NGC288'>

Figure 1: Colour magnitude diagram of stars in NGC288 (B magnitudes from [Yazan Momany](a2019/a11/a29_get_2mass_yazan), K magnitudes from [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)). The ARG/RGB stars used in the analysis are shown as colored triangles and circles. All stars have proper speeds within 0.25 mas/a from the mean of the selected AGB/RGB stars ([Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G)). The small error bars in bottom-right corner are drawn from the median uncertainties. The "Excluded" stars, shown as small black circles, are the ones that were not used in analysis. Extinction correction was not applied for this diagram.


### $`G`$ vs $`G_{BP} - G_{RP}`$


Code: [a2019/a12/a10_calculate_distance/code/cmd_gaia.py](a2019/a12/a10_calculate_distance/code/cmd_gaia.py)


<img src="a2019/a12/a10_calculate_distance/code/plots/cmd_gaia.png" width="700" alt='Colour magnitude diagram of comoving stars in NGC288'>

Figure 2: Colour magnitude diagram of stars in NGC288 in [Gaia DR2](https://arxiv.org/abs/1804.09365). The  error bars are too small to be seen at this scale. The RGB/AGB stars used in the analysis are shown as colored triangles and circles. The shown stars have proper speeds within 0.25 mas/a from the mean of the selected RGB/AGB stars. The "Excluded" stars, shown as small black circles, are the ones that were not used in analysis. Extinction correction was not applied for this diagram.


### Proper motion plot


Code: [a2019/a12/a10_calculate_distance/code/proper_motion.py](a2019/a12/a10_calculate_distance/code/proper_motion.py)


<img src="a2019/a12/a10_calculate_distance/code/plots/proper_motion.png" width="700" alt='Proper motion of observed stars in NGC 288'>

Figure 3: Proper motions of observed stars from Gaia DR2, robust fit ([Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G)). The circle marks the stars that move with a speed smaller than 0.25 mas/a relative to the mean of the selected RGB/AGB stars. The error bars are drawn from the median uncertainties for the selected RGB/AGB stars. The "Excluded" stars were not used in analysis.

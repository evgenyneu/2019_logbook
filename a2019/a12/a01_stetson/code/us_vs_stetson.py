# Plot color-magnitude diagram

import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot_mag(stetson_df, filter):
    delta_V = stetson_df[f'{filter}mag_tab1'] - stetson_df[f'{filter}_tab2']
    vMag = stetson_df[f'{filter}_tab2']
    uV = stetson_df[f'e_{filter}mag_tab1']

    plt.errorbar(vMag, delta_V, yerr=uV, fmt='o', color='black', capsize=5,
                 ecolor='gray', elinewidth=1)

    plt.xlabel(f'{filter}' + '$_{our}$')
    plt.ylabel(f'{filter}' + '$_{stetson}$ - ' + f'{filter}' + '$_{us}$')
    plt.title(f"Comparing {filter} magnitudes between our and Stetson's 2019 data")
    plt.tight_layout()
    save_plot(plt=plt, suffix=f'{filter.lower()}_mag')
    plt.show()


def plot():
    data_dir = 'a2019/a12/a01_stetson/data'

    # Open gaia data
    data_path = os.path.join(data_dir, 'XMatch_stetson_1_arcsec.csv')
    df = pd.read_csv(data_path)

    # Excluded
    # ---------------

    # Find Stetson's stars only
    stetson_df = df.loc[
        (df['Cat_tab2'] == 'STE')
    ]

    plot_mag(stetson_df, filter="V")
    plot_mag(stetson_df, filter="B")
    plot_mag(stetson_df, filter="I")


if __name__ == '__main__':
    plot()
    print('We are done')

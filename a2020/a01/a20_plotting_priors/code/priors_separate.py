import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from scipy import stats
from code_dope.python.plot.plot import save_plot


def distribution(x, p, mu1, mu2, sigma1, sigma2):
    return p * stats.norm.pdf(x, mu1, sigma1) + \
        (1 - p) * stats.norm.pdf(x, mu2, sigma2)


def do_work():
    np.random.seed(seed=4231)
    sns.set(style="ticks")
    n = 100
    mu1_samples = stats.norm(-1, 0.5).rvs(size=n)
    mu2_samples = stats.norm(1, 0.5).rvs(size=n)
    sigma1_samples = stats.lognorm(1).rvs(size=n)
    sigma2_samples = stats.lognorm(1).rvs(size=n)
    p_samples = stats.uniform(0, 1).rvs(size=n)

    x = np.linspace(-5, 5, 1000)

    result = [
        distribution(i, p=p_samples, mu1=mu1_samples, mu2=mu2_samples,
                     sigma1=sigma1_samples, sigma2=sigma2_samples)
        for i in x
    ]

    result_left = [
        stats.norm.pdf(i, mu1_samples, sigma1_samples)
        for i in x
    ]

    result_right = [
        stats.norm.pdf(i, mu2_samples, sigma2_samples)
        for i in x
    ]

    result = np.array(result)
    result_left = np.array(result_left)
    result_right = np.array(result_right)

    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    mean = df["na_over_h"].mean()
    std = df["na_over_h"].std()
    x_transformed = x * std + mean

    textbox = dict(boxstyle='round', facecolor='white',
                   alpha=0.8, edgecolor='0.7')

    for i in range(20):
        fig, ax = plt.subplots()
        ax.plot(x_transformed, result[:, i], linewidth=2, label="Combined")

        ax.plot(x_transformed, result_left[:, i], linestyle='dashed',
                linewidth=2, label="First")

        ax.plot(x_transformed, result_right[:, i], linestyle='dotted',
                linewidth=2, label="Second")

        text = (
            f"p={p_samples[i]:.2f}\n"
            r"$\mu_1$="
            f"{mu1_samples[i]*std + mean:.2f}, "
            r"$\sigma_1$="
            f"{sigma1_samples[i]*std:.2f}\n"
            r"$\mu_2$="
            f"{mu2_samples[i]*std + mean:.2f}, "
            r"$\sigma_2$="
            f"{sigma2_samples[i]*std:.2f}\n\n"

            f"Standardized:\n"
            r"$\mu_1$="
            f"{mu1_samples[i]:.2f}, "
            r"$\sigma_1$="
            f"{sigma1_samples[i]:.2f}\n"
            r"$\mu_2$="
            f"{mu2_samples[i]:.2f}, "
            r"$\sigma_2$="
            f"{sigma2_samples[i]:.2f}"
        )

        x = ax.set_xlim()[0] + abs(ax.set_xlim()[1] - ax.set_xlim()[0]) / 20
        y = ax.set_ylim()[1] - abs(ax.set_ylim()[1] - ax.set_ylim()[0]) / 20

        ax.text(
            x, y, text, fontsize=10,
            horizontalalignment='left',
            verticalalignment='top',
            bbox=textbox)

        ax.set_xlabel("Sodium abundance [Na/H]")
        ax.set_ylabel("Probability density")
        ax.set_title(f"Distribution {i+1}")
        fig.tight_layout()
        ax.legend(loc="upper right")
        save_plot(plt=fig, suffix=f"_{i:02d}", dpi=150)


if __name__ == '__main__':
    do_work()
    print('We are done')

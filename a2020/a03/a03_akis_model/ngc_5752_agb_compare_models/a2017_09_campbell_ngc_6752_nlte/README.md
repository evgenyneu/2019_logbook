# Compare single vs two-gaussian models using data from Campbell et al. 2017 NLTE AGB

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_nlte/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| Two populations |  57.03 | 5.15 |         |       |    3.22 |   0.43 |     0.55 |
| One population  |  57.50 | 5.31 |    0.48 |  2.48 |    1.63 |   0.27 |     0.45 |


|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| Two populations |  56.92 | 5.14 |         |       |    3.16 |     0.56 |
| One population  |  57.47 | 5.30 |    0.55 |  2.47 |    1.61 |     0.44 |


## Conclusion

Both models are equally compatible with the data.

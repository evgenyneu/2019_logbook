"""
Run Gaussian mixture model

Posted as a question:

https://discourse.mc-stan.org/t/how-to-reparameterize-a-gaussian-mixture-model/12792

Usage
------

Instal cmdstanpy and run:

    python gaussian_mixture.py
"""
from cmdstanpy import CmdStanModel
from tarpan.cmdstanpy.analyse import save_analysis


def get_data():
    """Returns data from observations"""
    values =        [-0.99, -1.37, -1.38, -1.51, -1.29, -1.34, -1.50,
                     -0.93, -0.83, -1.46, -1.07, -1.28, -0.73]

    uncertainties = [ 0.12,  0.05,  0.11,  0.18,  0.03,  0.19,  0.18,
                      0.12,  0.19,  0.09,  0.11,  0.16,  0.08]

    return {
        "y": values,
        "uncertainties": uncertainties,
        "N": len(values)
    }


def sample():
    model = CmdStanModel(stan_file="stan_model/gaussian_mixture.stan")

    fit = model.sample(data=get_data(), seed=2,
                       adapt_delta=0.99, max_treedepth=10,
                       sampling_iters=4000, warmup_iters=1000,
                       chains=4, cores=4,
                       show_progress=True)

    fit.diagnose()
    print(fit.summary())
    save_analysis(fit)


if __name__ == '__main__':
    sample()
    print('We are done')

import matplotlib.pyplot as plt
import seaborn as sns
from code_dope.python.plot.plot import save_plot
from tarpan.cmdstanpy.analyse import save_analysis
from tarpan.cmdstanpy.tree_plot import save_tree_plot
from tarpan.shared.info_path import InfoPath
from tarpan.shared.tree_plot import TreePlotParams
import pandas as pd
from cmdstanpy import CmdStanModel


def load_data(type):
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]

    return (df['na_over_h'].to_numpy(), df['uncertainty'].to_numpy())


def plot_observed_values(values_observed, uncertainties, star_type):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()
    ax1.scatter(values_observed, range(0, len(values_observed)),
                color=color_observed, s=100, label='Observed')

    ax1.errorbar(values_observed, range(0, len(values_observed)),
                 xerr=uncertainties,
                 color=color_observed, fmt='none')

    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")

    # Show the plot
    # -------

    fig.suptitle(f"Observed sodium abundances of {star_type} stars")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix=f'{star_type}_observed_data')


def run_stan(dir_name, values_observed, uncertainties, seed, stan_model_path):
    print('\n\n\n-------------')
    print(f'Running model: {stan_model_path}')
    print('\n')

    data = {
        "y": values_observed,
        "uncertainties": uncertainties,
        "N": len(values_observed)
    }

    model = CmdStanModel(stan_file=stan_model_path)

    fit = model.sample(
        data=data, seed=seed,
        adapt_delta=0.99, max_treedepth=10,
        sampling_iters=4000, warmup_iters=1000,
        chains=4, cores=4,
        show_progress=True)

    save_analysis(fit, param_names=["p", "mu", "sigma"],
                  info_path=InfoPath(sub_dir_name=dir_name))

    return fit


def compare_parameters(star_type, fits, labels):
    tree_params = TreePlotParams()
    tree_params.labels = labels
    basename = f"{star_type}_compare_models"

    save_tree_plot(fits,
                   param_names=['p', 'mu', 'sigma'],
                   tree_params=tree_params,
                   info_path=InfoPath(
                        dir_name="plots",
                        sub_dir_name=InfoPath.DO_NOT_CREATE,
                        base_name=basename,
                        extension="png"))


def run_model(star_type):
    values_observed, uncertainties = load_data(star_type)

    plot_observed_values(
        values_observed=values_observed,
        uncertainties=uncertainties,
        star_type=star_type)

    seed = 333

    fit_reparametrised = run_stan(
        values_observed=values_observed,
        uncertainties=uncertainties, seed=seed,
        dir_name=f"{star_type}_reparametrised",
        stan_model_path="a2020/a01/a26_original_vs_reparametrized/code/\
stan_model/reparametrised_model.stan")

    fit_original = run_stan(
        values_observed=values_observed,
        uncertainties=uncertainties, seed=seed,
        dir_name=f"{star_type}_original",
        stan_model_path="a2020/a01/a26_original_vs_reparametrized/\
code/stan_model/original_model.stan")

    fits = [fit_original, fit_reparametrised]
    compare_parameters(star_type=star_type, fits=fits, labels=["Original", "Reparametrised"])


def do_work():
    run_model('AGB')
    run_model('RGB')


if __name__ == '__main__':
    do_work()
    print('We are done')

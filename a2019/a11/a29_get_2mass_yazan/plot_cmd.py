# Plot color-magnitude diagram

import pandas as pd
import os
import matplotlib.pyplot as plt


def plot():
    """
    Plot color-magnitude diagram
    """

    data_dir = 'a2019/a11/a29_get_2mass_yazan/data'

    # Open gaia data
    gaia_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(gaia_path)

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    stars_df = pd.read_csv(stars_path)


    # Excluded
    # ---------------

    stars_other_df = stars_df.loc[
        (stars_df['selected'] == 0)
    ]

    selected_other_b = stars_other_df['B']
    selected_other_b_minus_k = stars_other_df['B'] - stars_other_df['K']

    plt.scatter(selected_other_b_minus_k, selected_other_b,
                c='black', marker='.', edgecolors='black', label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = stars_df.loc[
        (stars_df['selected'] == 1) &
        (
            (stars_df['TYPE_tab1'] == 'AGBS') |
            (stars_df['TYPE_tab1'] == 'BAGS')
        )
    ]

    selected_agb_b = stars_agb_df['B']
    selected_ragb_b_minus_k = stars_agb_df['B'] - stars_agb_df['K']

    plt.scatter(selected_ragb_b_minus_k, selected_agb_b,
                c='blue', marker='^', edgecolors='black', label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = stars_df.loc[
        (stars_df['selected'] == 1) &
        (
            (stars_df['TYPE_tab1'] == 'RGBS') |
            (stars_df['TYPE_tab1'] == 'BGBS')
        )
    ]

    selected_rgb_b = stars_rgb_df['B']
    selected_rgb_b_minus_k = stars_rgb_df['B'] - stars_rgb_df['K']

    plt.scatter(selected_rgb_b_minus_k, selected_rgb_b,
                c='red', marker='o', edgecolors='black', label='RGB')

    ax = plt.gca()
    ax.invert_yaxis()
    plt.legend()
    plt.xlabel('B-K')
    plt.ylabel('B')
    plt.title('Complete sample of observed stars')
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

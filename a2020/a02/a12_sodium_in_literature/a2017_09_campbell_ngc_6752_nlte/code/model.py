from paper.model.code.run_model import do_analysis, load_data


def do_work():
    """Main function that does all the work"""

    data_path = "a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte/data/2017_09_campbell_ngc_6752.csv"
    type_name = "Type"
    abundance_name = "A(Na)_nlte"
    uncertainty_name = "e_A(Na)_nlte"

    values_agb, uncert_agb = load_data('AGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    values_rgb, uncert_rgb = load_data('RGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    do_analysis(values_agb=values_agb, uncert_agb=uncert_agb,
                values_rgb=values_rgb, uncert_rgb=uncert_rgb,
                path="a2020/a02/a12_sodium_in_literature/a2017_09_campbell_ngc_6752_nlte",
                dir_name="model_info",
                study_name="NGC 6752 NLTE Campbell et al. 2017",
                xlabel="Sodium abundance A(Na) NLTE",
                mu_prior=5,
                sigma_prior=0.5)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

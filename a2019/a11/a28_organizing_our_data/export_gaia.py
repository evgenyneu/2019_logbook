# Export Gaia data from cross-matched file

import pandas as pd
import os


def rename_gaia_column(column):
    if column.endswith("_tab1"):
        return column[:-5]

    return column


def clean_gaia_data(cross_df):
    """
    Clean gaia data: removes non-gaia columns and removes _tab1 suffix
    from column names
    """

    cross_column_names = list(cross_df)
    column_to_remove = [x for x in cross_column_names if x.endswith('_tab2')]
    cross_df = cross_df.drop(columns=column_to_remove)

    # Remove _tab1 suffix from column names
    cross_df = cross_df.rename(columns=rename_gaia_column)

    return cross_df


def save_stars_data(df):
    dest_dir = 'a2019/a11/a28_organizing_our_data/'
    output_path = os.path.join(dest_dir, 'ngc288_stars_with_gaia_id.csv')
    df.to_csv(output_path, index=False)
    print(f'Added Gaia id to stars data in {output_path}')


def save_gaia_data(df):
    dest_dir = 'a2019/a11/a28_organizing_our_data/gaia'
    output_path = os.path.join(dest_dir, 'ngc288_gaia.csv')
    df.to_csv(output_path, index=False)
    print(f'Saved Gaia data to {output_path}')


def export_gaia():
    """
    Export Gaia data from the cross-matched file and
    add Gaia's object ID to our combined data file.
    """

    # Open photometry data
    gaia_dir = 'a2019/a11/a28_organizing_our_data/gaia/'
    raw_path = os.path.join(gaia_dir, 'XMatch.tsv')
    cross_df = pd.read_csv(raw_path, sep='\t', skiprows=[1])

    # Open stars data
    stars_dir = 'a2019/a11/a28_organizing_our_data/'
    raw_path = os.path.join(stars_dir, 'ngc288_stars.csv')
    stars_df = pd.read_csv(raw_path)

    # Add new gaia id column
    stars_df.insert(loc=7, column='id_gaia_dr2', value='')

    for index, star in cross_df.iterrows():
        gaia_id = star['Source_tab1']
        our_id = star['id_tab2']

        # Locate the star in our data
        star = stars_df.loc[
            (stars_df['id'] == our_id)]

        # Add Gaia ID to stars data
        stars_df.loc[star.index.values[0], 'id_gaia_dr2'] = gaia_id

    save_stars_data(df=stars_df)
    cross_df = clean_gaia_data(cross_df=cross_df)
    save_gaia_data(df=cross_df)


if __name__ == '__main__':
    export_gaia()

library(rethinking)
data(WaffleDivorce)
d <- WaffleDivorce

dlist <- list(
    D_obs = standardize( d$Divorce ),
    D_sd = d$Divorce.SE / sd( d$Divorce ),
    M = standardize( d$Marriage ),
    A = standardize( d$MedianAgeMarriage ),
    N = nrow(d)
)

model_description = alist(
    D_obs ~ dnorm( D_true , D_sd ),
    vector[N]:D_true ~ dnorm( mu , sigma ),
    mu <- a + bA*A + bM*M,
    a ~ dnorm(0,0.2),
    bA ~ dnorm(0,0.5),
    bM ~ dnorm(0,0.5),
    sigma ~ dexp(1)
)

model_name = 'a2019/a12/a30_use_data_with_uncertainties/m15.01.rds'

if (file.exists(model_name)) {
    model_description <- readRDS(model_name)
    m15.1 = model_description
} else {
    m15.1 <- ulam(model_description, data=dlist, chains=2, cores=2)
    saveRDS(m15.1, file=model_name)
}

precis( m15.1 , depth=2 )
stancode(m15.1)

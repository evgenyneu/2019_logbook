# Compare single vs two-gaussian models using data from Campbell et al. 2013 NGC 6752 AGB

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2013_06_campbell_ngc_6752/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| Two populations |  59.38 | 5.02 |         |       |    1.45 |   0.24 |     0.54 |
| One population  |  59.80 | 5.86 |    0.42 |  0.94 |    1.59 |   0.41 |     0.46 |

|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| Two populations |  59.36 | 5.01 |         |       |    1.43 |     0.54 |
| One population  |  59.77 | 5.86 |    0.41 |  0.94 |    1.58 |     0.46 |


## Conclusion

Both models are equally compatible with the data.

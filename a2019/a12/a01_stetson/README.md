## Comparing STE stars with Stetson's catalog

We compare J, H, K magnitudes in [our data](data/ngc288_stars.csv) with those in Stetson's catalog:

* In Aladin, File > Load local file, choose [ngc288_stars.csv](data/ngc288_stars.csv).

* Load [Stetson's catalog](https://ui.adsabs.harvard.edu/?#abs/2019MNRAS.485.3042S), table `CDS/J/MNRAS/485/3042/table4`.

* Cross match with 1 arcsecond radius: [data/XMatch_stetson_1_arcsec.tsv](a2019/a12/a01_stetson/data/XMatch_stetson_1_arcsec.tsv).

## Plotting V, B difference

Code: [a2019/a12/a01_stetson/code/us_vs_stetson.py](a2019/a12/a01_stetson/code/us_vs_stetson.py)


```
python -m a2019.a12.a01_stetson.code.us_vs_stetson
```

### V magnitude

<img src="a2019/a12/a01_stetson/code/plots/us_vs_stetson_v_mag.png" width=600 alt='V magnitude, us vs Stetson'>

Figure 1: V magnitudes in our data vs [Stetson+'s catalog](https://ui.adsabs.harvard.edu/abs/2019MNRAS.485.3042S). Error bars are uncertainties in Stetson+'s catalog, our data did not have uncertainties.

### I magnitude

<img src="a2019/a12/a01_stetson/code/plots/us_vs_stetson_i_mag.png" width=600 alt='I magnitude, us vs Stetson'>

Figure 2: I magnitudes in our data vs [Stetson+'s catalog](https://ui.adsabs.harvard.edu/abs/2019MNRAS.485.3042S). Error bars are uncertainties in Stetson+'s catalog, our data did not have uncertainties.

### B magnitude

<img src="a2019/a12/a01_stetson/code/plots/us_vs_stetson_b_mag.png" width=600 alt='B magnitude, us vs Stetson'>

Figure 3: B magnitudes in our data vs [Stetson+'s catalog](https://ui.adsabs.harvard.edu/abs/2019MNRAS.485.3042S). Error bars are uncertainties in Stetson+'s catalog, our data did not have uncertainties.

### Notes

1. We can see from Fig. 1 - 3 that our magnitudes are different from Stetson's magnitudes. This means our data came from a different catalog.

1. Two stars in Stetson+ catalog did not have U magnitudes.

1. In our data we did not have values for U and I magnitudes for most stars.


## Using Stetson's V, B, I, U magnitudes

* We replace the V, B, I, U magnitudes for stars marked as "STE" with those from Stetson's catalog.

* We add id_stetson field.

Code: [a2019/a12/a01_stetson/code/stetson_uvbi.py](a2019/a12/a01_stetson/code/stetson_uvbi.py)

```
python -m a2019.a12.a01_stetson.code.stetson_uvbi
```


## Add id_yazan

* We add id_yazan field to our [star list](a2019/a12/a01_stetson/data/ngc288_stars_with_stetson_update__yazan_id.csv)

* Remove ID_Phot column. It is no longer needed since we added separate columns for yazan and stetson IDs.

Code: [a2019/a12/a01_stetson/code/id_yazan.py](a2019/a12/a01_stetson/code/id_yazan.py)

```
python -m a2019.a12.a01_stetson.code.id_yazan
```


## Rename columns

* Rename TYPE_tab1 to type.

* Make all column lowercase.

* Replace spaces with underscores.

* Replace "/" with underscores.

Code: [a2019/a12/a01_stetson/code/rename_columns.py](a2019/a12/a01_stetson/code/rename_columns.py)


```
python -m a2019.a12.a01_stetson.code.rename_columns
```


## Export Stetson catalog

We export Stetson's catalog into a CSV file:

[a2019/a12/a01_stetson/data/ngc288_stetson.csv](a2019/a12/a01_stetson/data/ngc288_stetson.csv)

Code: [a2019/a12/a01_stetson/code/export_stetson.py](a2019/a12/a01_stetson/code/export_stetson.py)


```
python -m a2019.a12.a01_stetson.code.export_stetson
```

## Distance in Harris catalog

I want to find how 8.9 kps distance was calculated in [Harris catalog](http://physwww.mcmaster.ca/~harris/mwgc.dat) and what's the uncertainty.

The sources for the numbers are explained in [Catalog Bibliography, Harris, 2010](http://physwww.mcmaster.ca/%7Eharris/mwgc.ref):

> The primary distance indicator used here is the mean V magnitude of the horizontal branch, V_HB. In all but a few extreme cases, the HB level is measured directly from a color-magnitude diagram or from the mean magnitude of the RR Lyrae stars.

Harris catalog uses data from [Bellazinni at al. 2001](https://ui.adsabs.harvard.edu/abs/2001AJ....122.2569B) to calculate the distance to NGC 288. However, the Bellazinni paper does provide, nor calculate the distance to NGC288. Instead it looks at photometric measurements of stars in horizontal branches of NGC 288 and NGC 362 and estimates the age difference between the two clusters.

### [Bellazinni at al. 2001](https://ui.adsabs.harvard.edu/abs/2001AJ....122.2569B) on uncertainties of distance determination

> Of course, if significant differences in helium content, core rotation and/or any other “second parameter” that affects HB luminosity do exist and effectively change the HB luminosity of GCs with similar metallicity, our whole distance and age scales of GCs may be in error, since both are mostly based on the use of HB stars as standard candles (e.g., Pritzl et al. 2000)...

### On Harris distance estimate

The distance of d=8.9 kps from [Harris 2010](http://physwww.mcmaster.ca/~harris/mwgc.dat) does not have uncertainties. Thus it is impossible to compare this number to other calculations.


## Distance to NGC288 from Gaia Collaboration 2018

Source: https://arxiv.org/abs/1804.09381

The parallax calculated in the paper is:

Parallax: 0.1401 ± 0.0021 mas.

From this parallax we [calculate](a2019/a12/a09_distance/code/distance_from_parallax.py) distance:

Distance: 7.14 ± 0.11 kpc.

Compare with 4.900 ± 0.600 kpc [we calculated earlier](a2019/a12/a05_plot_distances).

The values disagree with each other within 2 standard deviations. The values also disagree with d=8.9 kps estimate from [Harris 2010](http://physwww.mcmaster.ca/~harris/mwgc.dat) (unless Harris' value has uncertainty larger than 2 kpc).


## Mark stars that were actually observed

In [our star list](a2019/a12/a09_distance/data/ngc288_stars.csv) we want to mark the stars that were actually observed.

### Get list of observed stars from FITS file

```
dfinfo 27oct10016.fits fibres > 27oct10016.fibres.txt
```

Output: [a2019/a12/a09_distance/data/27oct10016.fibres.txt](a2019/a12/a09_distance/data/27oct10016.fibres.txt)

### Mark observed stars

We add a field `observed`, which has value `1` for stars that were present in the FITS file.

Code: [a2019/a12/a09_distance/code/observed.py](a2019/a12/a09_distance/code/observed.py)

Output: [a2019/a12/a09_distance/data/ngc288_stars_observed.csv](a2019/a12/a09_distance/data/ngc288_stars_observed.csv)

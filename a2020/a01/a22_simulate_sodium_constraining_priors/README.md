# Testing a model with informative priors

I make a model with informative priors and test it with simulated observations:

```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma) \\
\mu_1 \sim \textrm{Normal}(-0.7, 0.5) \\
\mu_2 \sim \textrm{Normal}(0.7, 0.5) \\
\sigma \sim \textrm{HalfNoormal}(0, 0.3) \\
p \sim \textrm{Beta}(2, 2).
```

Code: [a2020/a01/a22_simulate_sodium_constraining_priors/code/simulate_sodium.py](a2020/a01/a22_simulate_sodium_constraining_priors/code/simulate_sodium.py)



## Weak vs informative priors

On Figure 1 we can see the comparison of posterior distributions of parameters for [weak](a2020/a01/a22_informative_priors/) and informative priors. We can see that the model with informative priors has narrower spread of all parameters distributions.

<img src="a2020/a01/a22_simulate_sodium_constraining_priors/code/plots/weak_and_informative_models.png" width="700" alt='Comparing posterior distributions'>

Figure 1: Comparing posterior distributions of  the model with weak and informative priors.


## Posterior distributions summary

The summary of the posterior distributions for the informative prior model are shown in Table 1.

Table 1: Posterior parameter distributions for the model with informative priors.

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| p      |   0.36 |  0.10 |   0.35 | 0.11 | 0.10 |    0.25 |    0.46 |    0.18 |    0.58 |
| mu.1   |  -1.06 |  0.05 |  -1.06 | 0.05 | 0.06 |   -1.12 |   -1.02 |   -1.17 |   -0.95 |
| mu.2   |  -0.61 |  0.04 |  -0.61 | 0.03 | 0.04 |   -0.65 |   -0.58 |   -0.69 |   -0.54 |
| sigma  |   0.08 |  0.03 |   0.07 | 0.03 | 0.02 |    0.05 |    0.10 |    0.03 |    0.14 |


### Parameter p comparison

The parameter p summaries for informative and weak priors are the following:

* Informative: 0.35 (+0.11, -0.10)
* Weak: 0.36 (+0.16, -0.14)

The values are modes and uncertainties are distances to 68 HPDIs.


### Pair plot (informative priors)

On Fig. 2 we show the pair plot of parameter distributions for the informative priors model. We can see no correlations between parameters.

<img src="a2020/a01/a22_simulate_sodium_constraining_priors/code/plots/simulate_sodium_pair_informative.png" alt='Pair plot'>

Figure 2: Pair plot of parameter distributions for model with informative priors.


#### Pair plot (weak priors)

On Fig. 3 shows a pair plot for weak priors model. We can see that plots for sigma parameter show non-circular pattern, probably due to presence of outliers.

<img src="a2020/a01/a22_simulate_sodium_constraining_priors/code/plots/simulate_sodium_pair_weak.png" alt='Pair plot'>

Figure 3: Pair plot of parameter distributions for model with weak priors.

# Analysing data from [Campbell et al. 2017](https://arxiv.org/abs/1707.02840v1), NGC 6752 LTE

Code: [a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte/code/model.py](a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_09_campbell_ngc_6752_lte/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                     |   Author |  Agree 95% | Agree 68%  |
|:-------------:|-------------------------------:|---------:|:----------:|:----------:|
| $`r_{agb}`$   |    $`0.63^{+0.11}_{-0.14}`$    |    NA    |            |            |
| $`r_{rgb}`$   |    $`0.69^{+0.10}_{-0.13}`$    |    NA    |            |            |
| $`f`$         |    $`0.13^{+0.22}_{-0.27}`$    |    NA    |            |            |

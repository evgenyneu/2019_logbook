# Analysing data from [Johnson et al. 2015](https://arxiv.org/abs/1412.4108v1), NGC 104 (47 Tuc)



## RGB data sample problem

The authors calculate sodium abundances for AGB stars only. For RGB stars they used abundances from [Cordero et al. 2014](https://arxiv.org/abs/1311.1541).

Note that data from Cordero was captured with different instruments: Hydra and  Flames. We only used stars from Flames instrument, since Johnson mention this instrument in their paper. However, we counted 103 RGB Flames stars, while Johnson said they had 113. Therefore, is likely that we are not using the same sample as Johnson for RGB stars.

For uncertainty of sodium abundance for RGB stars we use 0.14 (Table 3, Cordero et al. 2014).

Code: [a2020/a02/a15_sodium_in_literature/a2015_01_johnson_ngc_104_47_tuc/code/model.py](a2020/a02/a15_sodium_in_literature/a2015_01_johnson_ngc_104_47_tuc/code/model.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2015_01_johnson_ngc_104_47_tuc/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2015_01_johnson_ngc_104_47_tuc/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.59^{+0.10}_{-0.12}`$  |     $`0.37`$            |    Yes     |     No     |
| $`r_{rgb}`$   |  $`0.39^{+0.07}_{-0.06}`$  |     $`0.55`$            |    Yes     |    Yes     |
| $`f`$         |  $`-0.40^{+0.27}_{-0.36}`$ |    $`\lesssim 0.2`$    |     Yes    |     Yes    |

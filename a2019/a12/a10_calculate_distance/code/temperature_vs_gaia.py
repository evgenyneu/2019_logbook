import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    plt.figure(figsize=(4.4, 3.2))
    data_dir = 'a2019/a12/a10_calculate_distance/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    df = df.loc[df['selected'] == 1]

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    t_gaia = stars_agb_df['Teff']
    t_us = stars_agb_df['t_eff']

    plt.scatter(x=t_gaia, y=t_us,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    t_gaia = stars_rgb_df['Teff']
    t_us = stars_rgb_df['t_eff']

    plt.scatter(x=t_gaia, y=t_us,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # Error bars
    # --------------

    # Source: https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G
    u_t_gaia = 324
    u_t_us = df['u_t_eff'].median()

    plt.errorbar(x=5400, y=4100,
                 xerr=u_t_gaia, yerr=u_t_us,
                 fmt='none', color='black', capsize=5,
                 ecolor='black', elinewidth=1)

    # Plot straight line
    # -----------

    plt.plot([4000, 5600], [4000, 5600], zorder=-1, linewidth=1, linestyle=':')
    plt.legend()
    ax = plt.gca()
    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    plt.xlabel(r'Effective temperature from Gaia DR2 catalog $T_{eff}$ [K]')
    plt.ylabel('Effective temperature\nfrom this study $T_{eff}$ [K]')
    plt.tight_layout(pad=0.05)
    save_plot(plt=plt, extensions=['png', 'pdf'])
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

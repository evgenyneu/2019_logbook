import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a06_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions_is_comoving.csv')
    df = pd.read_csv(stars_path)

    # Remove rows with missing values
    # --------

    df = df[df.pm_speed_from_mean != ' ']
    df.pm_speed_from_mean = pd.to_numeric(df.pm_speed_from_mean)

    # Show only stars with similar proper velocities
    # i.e. within 0.25 mas/a from the mean of the selected stars
    df = df.loc[df['pm_speed_from_mean'] < 0.25]

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    selected_other_b = stars_other_df['b']
    selected_other_b_minus_k = stars_other_df['b'] - stars_other_df['v']

    plt.scatter(selected_other_b_minus_k, selected_other_b,
                c='black', marker='o', s=7, edgecolors='black',
                label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    selected_agb_b = stars_agb_df['b']
    selected_ragb_b_minus_k = stars_agb_df['b'] - stars_agb_df['v']

    plt.scatter(selected_ragb_b_minus_k, selected_agb_b,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    selected_rgb_b = stars_rgb_df['b']
    selected_rgb_b_minus_k = stars_rgb_df['b'] - stars_rgb_df['v']

    plt.scatter(selected_rgb_b_minus_k, selected_rgb_b,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # Error bars
    # --------------

    all_selected_df = df

    uncertainty_b_df = all_selected_df['u_b']
    mean_u_b = uncertainty_b_df.max()
    uncertainty_b_minus_k_df = np.sqrt(all_selected_df['u_b']**2 + all_selected_df['u_v']**2)
    mean_u_b_minus_k = uncertainty_b_minus_k_df.max()
    print(f"Y error: mean u(B - K) = {mean_u_b_minus_k}")
    print(f"X error: mean u(B) () = {mean_u_b}")

    x_error_bar = plt.xlim()[1] - mean_u_b_minus_k
    y_error_bar = plt.ylim()[1] - mean_u_b

    plt.errorbar([x_error_bar], [y_error_bar],
                 xerr=[mean_u_b_minus_k], yerr=[mean_u_b],
                 fmt='none', color='black', capsize=1,
                 ecolor='black', elinewidth=1)

    ax = plt.gca()
    ax.invert_yaxis()
    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    plt.legend()
    plt.xlabel('B-V magnitude')
    plt.ylabel('B magnitude')
    plt.title('Observed stars in NGC288 with similar proper velocities')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

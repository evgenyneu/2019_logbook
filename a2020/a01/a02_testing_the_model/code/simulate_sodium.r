library(rstan)
library(rethinking)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

seed_value = 333
set.seed(seed_value)

# Simulate observations of 5 two populations of stars:
# 5 sodium-poor and 10 sodium-rich stars
# ----------------

n_poor = 10 # Number of Sodium poor stars
n_rich = 17 # Number of Sodium rich stars
n = n_poor + n_rich # Total sample size of observed stars

# Generate random values from Normal distributions
mu_poor = -1.1 # mean of sodium poor population
mu_rich = -0.6 # mean of sodium rich population
sigma = 0.05
samples_poor = rnorm(n_poor, mu_poor, sigma)
samples_rich = rnorm(n_rich, mu_rich, sigma)

# Combine all samples in one list
values_true = sample(c(samples_poor, samples_rich))


# Generate uncertainties for all observations
uncertainties = abs(rnorm(n, 0.13, 0.06))

# Generate observed values from true using the uncertainties
values_observed = rnorm(n, values_true, uncertainties)

color_true = 'black'
pch_true = 1

color_observed = '#0000FF88'
pch_observed = 16

# Plot the true and observed values
# --------

plot_path = "a2020/a01/a02_testing_the_model/plots/"

png(filename=paste(plot_path, "true_observed_", seed_value, ".png", sep=""),
    width=6, height=5, units='in', res=300)

xlim = c(-1.5, 0)

plot(values_true, 1:n,
    xlim=xlim,
    ylim=c(1, n+6), cex=2, pch=pch_true, col=color_true,
    xlab="Sodium abundance [Na/H]",
    ylab="Star number",
    main="Simulated sodium abundances")

points(values_observed, 1:n, cex=2, pch=pch_observed, col=color_observed)

abline(v=c(mu_poor, mu_rich), lty=2)

legend("topright", legend=c("True", "Observed"),
    col=c(color_true, color_observed),
    pt.cex=2, pch = c(pch_true, pch_observed))


for (i in 1:n) {
    u = uncertainties[i]
    lines(c(values_observed[i] - u, values_observed[i] + u), c(i, i),
        col=color_observed)
}

dev.off()

png(filename=paste(plot_path, "density_", seed_value, ".png", sep=""),
    width=6, height=5, units='in', res=300)

# Make a density plot
plot(density(values_observed),
     xlim=xlim,
     xlab="Sodium abundance [Na/H]",
     ylab="Density",
     main="Simulated sodium abundances")

dev.off()

# Run model
# --------

source("a2020/a01/a02_testing_the_model/code/calculate_sodium_ratio.r")

data = data.frame(
    na_over_h = values_observed,
    uncertainty = uncertainties
)


model_name = 'a2020/a01/a02_testing_the_model/data/'
model_name = paste(model_name, 'simulate_sodium_', seed_value, '.rds', sep='')

if (file.exists(model_name)) {
    post <- readRDS(model_name)
} else {
    post = run_model(data)
    saveRDS(post, file=model_name)
}

# Plot estimated and true parameters
# ---------

print("True parameters")
p_true = round(n_poor / (n_poor + n_rich), 2)
print(paste("p =", p_true))
print(paste("mu1 = ", mu_poor, ", sigma1 = ", sigma, sep=""))
print(paste("mu2 = ", mu_rich, ", sigma2 = ", sigma, sep=""))

print("--------------")
print("Estimated parameters")

precis(post)

# Plot
# -----

png(filename=paste(plot_path, "parameters_", seed_value, ".png", sep=""),
    width=6, height=5, units='in', res=300)

source("code_dope/r/plot/plot_utils.r")

title = "Estimated and true parameters of the model"
plot_frame_means(post, title)

true_values = c(p_true, mu_poor, mu_rich, sigma, sigma)
points(true_values, 1:length(true_values), pch=4, cex=2, lwd=2, col="#0000FFAA")

legend("topleft", legend=c("True", "Estimate"),
    pt.cex=c(2, 1), pch=c(4, 1), col=c("#0000FFAA", "black"))

dev.off()

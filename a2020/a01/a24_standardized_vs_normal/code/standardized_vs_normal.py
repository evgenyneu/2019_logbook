import matplotlib.pyplot as plt
import seaborn as sns
from run_stan import standardize, destandardize, run_stan
from code_dope.python.plot.plot import save_plot
from tarpan.shared.summary import save_summary
from tarpan.shared.info_path import InfoPath
from tarpan.shared.histogram import save_histogram
import pandas as pd

from tarpan.shared.tree_plot import (
    TreePlotParams, make_comparative_tree_plot)


def load_data(type):
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]

    return (df['na_over_h'].to_numpy(), df['uncertainty'].to_numpy())


def plot_observed_values(values_observed, uncertainties, star_type):
    sns.set(style="ticks")

    # Creates two subplots and unpacks the output array immediately
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(6, 6),
                                   gridspec_kw={'wspace': 0, 'hspace': 0})

    # Make scatter plots of observed and simulated true values
    # -----------

    color_observed = "#0000ff44"
    ax1.grid()
    ax1.scatter(values_observed, range(0, len(values_observed)),
                color="#0000ff44", s=100, label='Observed')

    ax1.errorbar(values_observed, range(0, len(values_observed)),
                 xerr=uncertainties,
                 color="#0000ff22", fmt='none')

    ax1.set_ylabel("Star number")

    # Plot the KDE of observed values
    # ----------

    ax2.grid()
    sns.kdeplot(values_observed, ax=ax2, color=color_observed, shade=True)
    ax2.set_xlabel("Sodium abundance [Na/H]")
    ax2.set_ylabel("Probability density")

    # Show the plot
    # -------

    fig.suptitle(f"Observed sodium abundances of {star_type} stars")
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    save_plot(plt=fig, suffix=f'{star_type}_observed_data')


def run_model_std(values_observed,
                  uncertainties, seed,
                  sub_dir_name, stan_model_path):

    values_std, uncertainties_std = standardize(values_observed, uncertainties)

    data_for_model = {
        'y': values_std,
        'uncertainties': uncertainties_std,
        'N': len(values_std)
    }

    samples_std = run_stan(
        data_for_model,
        sub_dir_name=sub_dir_name,
        stan_model_path=stan_model_path,
        seed=seed)

    samples = destandardize(samples_std, values_observed)

    info_path = InfoPath(sub_dir_name=f'{sub_dir_name}_destandardized')
    info_path.set_codefile()
    summary = save_summary(samples, info_path=info_path)
    return samples, summary


def run_model_normal(values_observed,
                     uncertainties, seed,
                     sub_dir_name, stan_model_path, data_for_model):

    data_for_model['y'] = values_observed
    data_for_model['uncertainties'] = uncertainties
    data_for_model['N'] = len(values_observed)

    samples = run_stan(
        data_for_model,
        sub_dir_name=sub_dir_name,
        stan_model_path=stan_model_path,
        seed=seed)

    info_path = InfoPath(sub_dir_name=f'{sub_dir_name}_destandardized')
    info_path.set_codefile()
    summary = save_summary(samples, info_path=info_path)
    return samples, summary


def compare_tree_plots(summary, summary_std, star_type):
    all_summaries = []
    all_summaries.append(summary)
    all_summaries.append(summary_std)

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name=f"{star_type}_normal_and_standardized_models",
        extension="png")

    info_path.set_codefile()

    tree_params = TreePlotParams(
        title=(
            "Posterior distributions using non-standardized\n"
            "and standardized observations of RGB stars"
        )
    )

    tree_params.labels = [
        "Non-standardized",
        "Standardized"
    ]

    make_comparative_tree_plot(
        all_summaries, info_path=info_path,
        tree_params=tree_params)


def run_model(star_type, data_for_model):
    values_observed, uncertainties = load_data(star_type)

    print(f'{star_type} mean={values_observed.mean()}')
    print(f'{star_type} std={values_observed.std()}')

    plot_observed_values(
        values_observed=values_observed,
        uncertainties=uncertainties,
        star_type=star_type)

    # Run model
    # -----------

    seed = 333

    samples_std, summary_std = run_model_std(
        values_observed, uncertainties, seed=seed,
        sub_dir_name=f"{star_type}_model_std",
        stan_model_path="a2020/a01/a22_simulate_sodium_constraining_priors/\
code/stan_model/sodium_ratio_model.stan")

    samples, summary = run_model_normal(
        values_observed, uncertainties, seed=seed,
        sub_dir_name=f"{star_type}_model_normal",
        stan_model_path="a2020/a01/a24_standardized_vs_normal/code/stan_model/\
sodium_ratio_model_destandardized.stan",
        data_for_model=data_for_model)

    # Make tree plot
    # --------

    compare_tree_plots(summary=summary['df'], summary_std=summary_std['df'],
                       star_type=star_type)

    # Posterior plots
    # ------

    # Standardized

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name=f"{star_type}_posterior_std", extension="png")

    info_path.set_codefile()

    save_histogram(samples_std, summary_std['df'],
                        info_path=info_path)

    # Non-stamdardized

    info_path = InfoPath(
        dir_name="plots", sub_dir_name=InfoPath.DO_NOT_CREATE,
        base_name=f"{star_type}_posterior", extension="png")

    info_path.set_codefile()

    save_histogram(samples, summary['df'],
                        info_path=info_path)


def do_work():
    data_for_model = {
        'mu1_prior': -1.4,
        'mu2_prior': -1,
        'mu_sigma_prior': 0.15,
        'sigma_prior': 0.1
    }

    run_model('AGB', data_for_model=data_for_model)

    data_for_model = {
        'mu1_prior': -1.1,
        'mu2_prior': -0.6,
        'mu_sigma_prior': 0.15,
        'sigma_prior': 0.1
    }
    run_model('RGB', data_for_model=data_for_model)


if __name__ == '__main__':
    do_work()
    print('We are done')

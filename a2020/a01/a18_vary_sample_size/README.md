# Varying sample size

I simulate sodium abundances and vary the number of observations. I want to see how it affects the posterior distribution of parameters p, mu and sigma.

## Simulated samples

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_0.25.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_0.50.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_1.00.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_2.00.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_5.00.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_10.00.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_vary_sample_size/code/plots/simulate_sodium_20.00.png" width="400" alt='Simulated sodium abundances'>

Figure 1: Simulated sodium abundances for different sample sizes. Vertical dashed lines indicate simulated true values mu1 and mu2 parameters.


## Results

The posterior distributions of the parameters are shown on Figure 2. We can see that increasing the number of observations reduces the spread of the posterior distributions of parameters and make them closer to the simulated true values.

<img src="a2020/a01/a18_vary_sample_size/code/plots/compare_all/summary.png" alt='Parameter distributions'>

Figure 2: Posterior distributions of parameters for different number of observations. The top marker (orange "x") in each group shows the simulated true values of the parameters.

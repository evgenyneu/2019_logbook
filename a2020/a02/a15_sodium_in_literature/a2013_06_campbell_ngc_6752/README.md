# Analysing data from [Campbell et al. 2013](https://arxiv.org/abs/1305.7090), NGC 6752

Code: [a2020/a02/a15_sodium_in_literature/a2013_06_campbell_ngc_6752/code/a2013_06_campbell_ngc_6752.py](a2020/a02/a15_sodium_in_literature/a2013_06_campbell_ngc_6752/code/a2013_06_campbell_ngc_6752.py)


## Posterior distributions

<img src="a2020/a02/a15_sodium_in_literature/a2013_06_campbell_ngc_6752/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a15_sodium_in_literature/a2013_06_campbell_ngc_6752/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                     |   Author |  Agree 95% | Agree 68%  |
|:-------------:|-------------------------------:|---------:|:----------:|:----------:|
| $`r_{agb}`$   |    $`0.70^{+0.12}_{-0.14}`$    |    0     |     No     |     No     |
| $`r_{rgb}`$   |    $`0.71^{+0.10}_{-0.09}`$    |    0.7   |     Yes    |     Yes    |
| $`f`$         |    $`0.06^{+0.22}_{-0.26}`$    |    1     |     No     |     No     |

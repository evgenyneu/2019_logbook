# Comparing original and reparametrized models

[Previously](a2020/a01/a24_standardized_vs_normal) the model showed divergent transitions warnings for the AGB stars samples. People from [Stan's forum](https://discourse.mc-stan.org/t/how-to-reparameterize-a-gaussian-mixture-model/12792/22) helped me to reparametrize the model to get rid of Stan's warnings.

Code: [a2020/a01/a26_original_vs_reparametrized/code/original_vs_reparametrised.py](a2020/a01/a26_original_vs_reparametrized/code/original_vs_reparametrised.py)

## Previous model


```math
Y_{\textrm{Obs}, i} \sim \textrm{Normal}(Y_{\textrm{True}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{True}, i} \sim p \ \textrm{Normal}(\mu_1, \sigma) + (p - 1) \ \textrm{Normal}(\mu_2, \sigma) \\
\mu_1 \sim \textrm{Normal}(-1, 0.5) \\
\mu_2 \sim \textrm{Normal}(-1, 0.5) \\
\sigma \sim \textrm{HalfNormal}(0, 0.1) \\
p \sim \textrm{Uniform}(0, 1) \\
```

## New reparametrised model

```math
Y_{i} \sim p \ \textrm{Normal}(Y_{\textrm{mix1}, i}, Y_{\textrm{uncert}, i}) + (p - 1) \ \textrm{Normal}(Y_{\textrm{mix2}, i}, Y_{\textrm{uncert}, i}) \\
Y_{\textrm{mix1}, i} = \mu_1 + \sigma * Z_{1, i} \\
Y_{\textrm{mix2}, i} = \mu_2 + \sigma * Z_{2, i} \\
\mu_1 \sim \textrm{Normal}(-1, 0.5) \\
\mu_2 \sim \textrm{Normal}(-1, 0.5) \\
\sigma \sim \textrm{HalfNormal}(0, 0.1) \\
p \sim \textrm{Uniform}(0, 1) \\
Z_{1, i} \sim \textrm{Normal}(0, 1) \\
Z_{2, i} \sim \textrm{Normal}(0, 1).
```


### Posterior distributions of parameters

We can from Figure 1 that the two models produce nearly identical distributions of parameters. This suggests that the two models are equivalent.

<img src="a2020/a01/a26_original_vs_reparametrized/code/plots/AGB_compare_models.png" width="400" alt='Comparing posterior distributions of original vs reparametrised values for AGB stars'>

<img src="a2020/a01/a26_original_vs_reparametrized/code/plots/RGB_compare_models.png" width="400" alt='Comparing posterior distributions of original vs reparametrised values for AGB stars'>

Figure 1: Comparing posterior distributions of original and reparametrised models.


### Summaries of posterior distributions for reparametrised model

The summaries of posterior distributions are shown in Tables 1 and 2.

Table 1: Summary of posterior distributions of parameters for AGB stars.

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |   N_Eff |   R_hat |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|--------:|--------:|
| p      |   0.61 |  0.15 |   0.64 | 0.13 | 0.16 |    0.48 |    0.77 |    0.31 |    0.90 |    3794 |    1.00 |
| mu.1   |  -1.33 |  0.06 |  -1.33 | 0.05 | 0.06 |   -1.39 |   -1.28 |   -1.46 |   -1.20 |    4270 |    1.00 |
| mu.2   |  -0.91 |  0.11 |  -0.89 | 0.10 | 0.09 |   -0.98 |   -0.79 |   -1.18 |   -0.69 |    1675 |    1.00 |
| sigma  |   0.09 |  0.06 |   0.07 | 0.05 | 0.06 |    0.01 |    0.12 |    0.00 |    0.20 |    1756 |    1.00 |


Table 2: Summary of posterior distributions of parameters for RGB stars.

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |   N_Eff |   R_hat |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|--------:|--------:|
| p      |   0.36 |  0.09 |   0.35 | 0.09 | 0.10 |    0.25 |    0.44 |    0.18 |    0.54 |   15716 |    1.00 |
| mu.1   |  -1.19 |  0.05 |  -1.20 | 0.04 | 0.04 |   -1.24 |   -1.15 |   -1.29 |   -1.10 |    3608 |    1.00 |
| mu.2   |  -0.62 |  0.04 |  -0.63 | 0.04 | 0.03 |   -0.66 |   -0.59 |   -0.70 |   -0.55 |    5736 |    1.00 |
| sigma  |   0.08 |  0.03 |   0.07 | 0.03 | 0.02 |    0.05 |    0.10 |    0.03 |    0.14 |    2739 |    1.00 |


## Sampling quality

The sampling using the new reparametrised warnings went without any Stan warnings for both AGB and RGB values.

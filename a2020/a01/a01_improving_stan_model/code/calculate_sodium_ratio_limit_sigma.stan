data {
  int<lower=1> N;        // number of data points
  real y[N];             // observed value
  real uncertainties[N]; // uncertainties
}
parameters {
  vector[N] y_true;          // true value
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  real<lower=0> sigma1;      // spread of mixture components
  real<lower=0.25,upper=4> k; // ratio of sigma2 / sigma1
}
transformed parameters {
    real<lower=0> sigma2;  // spread of mixture components
    sigma2 = k * sigma1;
}
model {
  sigma1 ~ lognormal(0, 1);
  mu[1] ~ normal(-1, 0.5);
  mu[2] ~ normal(1, 0.5);
  //k ~ uniform(0.5, 1.5);
  //p ~ beta(5, 5);
  k ~ uniform(0.25, 4);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y_true[n] | mu[1], sigma1),
                      normal_lpdf(y_true[n] | mu[2], sigma2));
  }

  y ~ normal(y_true, uncertainties);  // Estimate true values
}

import pandas as pd
from cmdstanpy import CmdStanModel
from scipy.stats import zscore
import pickle
import os
import seaborn as sns
from code_dope.python.plot.plot import save_plot
from sample_utils import extract_tree_plot_data, tree_plot


def load_data(type, uncertainty_multiplier):
    """
    Loads abundace data.

    Parameters
    ----------

    type: str
        Star type: 'AGB' or 'RGB'

    uncertainty_multiplier : float
        A number that is used to multiply the measurement uncertainties
        of the abundances.

    Returns
    -------

    Tuple (model_data, abundanced):

        model_data : standardized data that will be supplied to Stan.

        abundances : the unstandardized abundaces with uncertainties.
    """

    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df['type'] == type]
    df['uncertainty'] *= uncertainty_multiplier

    # Standardize parameters
    zscores = zscore(df['na_over_h'].tolist(), ddof=1)
    uncertainties = (df['uncertainty'] / df['na_over_h'].std()).tolist()

    model_data = {
        'y': zscores,
        'uncertainties': uncertainties,
        'N': len(df)
    }

    return (model_data, df)


def destandardize(samples, data):
    """

    De-standardize samples of parameters.

    Parameters
    -----------

    samples : Panda's DataFrame
        Samples of parameters (normalized.)

    data : Panda's DataFrame
        The initial abundance data (non-normalized)

    """
    std = data['na_over_h'].std()
    mean = data['na_over_h'].mean()
    samples['mu.1'] = samples['mu.1'] * std + mean
    samples['mu.2'] = samples['mu.2'] * std + mean
    samples['sigma.1'] = samples['sigma.1'] * data['na_over_h'].std()
    samples['sigma.2'] = samples['sigma.2'] * data['na_over_h'].std()


def run_stan(type, data, uncertainty_multiplier):
    """
    Run Stan to calculate parameters of the model.

    Parameters
    -----------

    type: str
        Star type: 'AGB' or 'RGB'.

    data : dictionary
        Data for the stan sampler.

    Returns
    -------

    Panda's dataframe containing samples of parameters.
    """

    data_dir = 'a2020/a01/a09_study_uncertainty_effects/data'

    path_to_model = os.path.join(
        data_dir,
        f'samples_{type.lower()}_{uncertainty_multiplier:.2f}.pkl')

    if os.path.exists(path_to_model):
        with open(path_to_model, 'rb') as input:
            samples = pickle.load(input)
    else:
        path = "a2020/a01/a05_python_model/code/stan_model/sodium_ratio_model.stan"

        if uncertainty_multiplier == 0:
            path = "a2020/a01/a08_changing_measurement_uncertainty/code/stan_model/sodium_ratio_model_no_uncertainties.stan"

        model = CmdStanModel(stan_file=path)

        fit = model.sample(data=data, seed=11421,
                           adapt_delta=0.99, max_treedepth=10,
                           sampling_iters=12000, warmup_iters=3000,
                           chains=1, cores=1,
                           show_progress=True)

        print(fit.summary())
        print(fit.diagnose())

        samples = fit.get_drawset(params=['p', 'mu', 'sigma'])

        # Save samples to disk
        os.makedirs(os.path.dirname(path_to_model), exist_ok=True)
        with open(path_to_model, 'wb') as output:
            pickle.dump(samples, output, protocol=pickle.HIGHEST_PROTOCOL)

    return samples


def calculate(type, uncertainty_multiplier):
    """
    Run Stan model and calculate distributions of parameters.

    Parameters
    ----------

    type: str
        Star type: 'AGB' or 'RGB'.

    uncertainty_multiplier : float
        A number that is used to multiply the measurement uncertainties
        of the abundances.

    Returns
    --------

    Distirbutions of parameters of the model.
    """

    model_data, df = load_data(type, uncertainty_multiplier)
    samples = run_stan(type, model_data, uncertainty_multiplier)
    destandardize(samples, df)
    return samples


def do_work():
    sns.set_style("ticks")

    for type in ["AGB", "RGB"]:
        samples_0_00 = calculate(type, 0)
        samples_0_05 = calculate(type, 0.5)
        samples_1_00 = calculate(type, 1)
        samples_2_00 = calculate(type, 2)
        samples_4_00 = calculate(type, 4)
        samples_10_00 = calculate(type, 10)

        tree_plot_data = extract_tree_plot_data(samples_0_00, groups=[])
        tree_plot_data = extract_tree_plot_data(samples_0_05, groups=tree_plot_data)
        tree_plot_data = extract_tree_plot_data(samples_1_00, groups=tree_plot_data)
        tree_plot_data = extract_tree_plot_data(samples_2_00, groups=tree_plot_data)
        tree_plot_data = extract_tree_plot_data(samples_4_00, groups=tree_plot_data)
        tree_plot_data = extract_tree_plot_data(samples_10_00, groups=tree_plot_data)

        fig, ax = tree_plot(
            tree_plot_data,
            labels=[
              'Uncertainty 0.0x',
              'Uncertainty 0.5x',
              'Uncertainty actual',
              'Uncertainty 2.0x',
              'Uncertainty 4.0x',
              'Uncertainty 10.0x'
            ],
            title=f"Posterior distributions of parameters for {type} stars")

        save_plot(plt=fig, suffix=type)


if __name__ == '__main__':
    do_work()
    print('We are done')

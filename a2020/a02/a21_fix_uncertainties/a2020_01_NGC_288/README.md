# Analysing data from work in progress 2020 NGC 288

Code: [a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288/code/model.py](a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |    Author      |  Agree 95%     | Agree 68%      |
|:-------------:|---------------------------:|---------------:|:--------------:|:--------------:|
| $`r_{agb}`$   |  $`0.38^{+0.13}_{-0.14}`$  | Not Applicable | Not Applicable | Not Applicable |
| $`r_{rgb}`$   |  $`0.66^{+0.08}_{-0.10}`$  | Not Applicable | Not Applicable | Not Applicable |
| $`f`$         |  $`0.46^{+0.18}_{-0.26}`$  | Not Applicable | Not Applicable | Not Applicable |

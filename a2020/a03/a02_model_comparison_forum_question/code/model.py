import os

from tarpan.cmdstanpy.compare import save_compare
from tarpan.cmdstanpy.psis import ParetoKPlotParams
from tarpan.shared.info_path import InfoPath

from synthetic_data import (
    SimulatedParameters, generate_data, compare_with_exact,
    posterior_plot_fn)

from run_model import (
    AnalysisSettings, do_analysis, load_data,
    model_pdf_population_1, model_pdf_population_2)


def do_work():
    path = "./"

    # Generate data
    # --------

    data_dir = os.path.join(path, "data")

    if not os.path.isdir(data_dir):
        os.makedirs(data_dir, exist_ok=True)

    # Load model parameters
    params_path = os.path.join(path, "model_params.yaml")
    model_params = SimulatedParameters.read_model_params(params_path)
    data_path = os.path.join(data_dir, "data.csv")
    seed = 10

    # Generate data
    generate_data(csv_path=data_path, seed=seed, model_params=model_params)

    # Analyse data with Stan
    # ---------

    type_name = "Type"
    abundance_name = "[Na/H]"
    uncertainty_name = "e_[Na/H]"

    settings = AnalysisSettings(
        path=os.path.join(path, "model_info"),
        study_name="simulated data with two populations",
        xlabel="Sodium abundance [Na/H]"
    )

    settings.values_agb, settings.uncert_agb = load_data(
        'Simulated', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.observations_plot_fn = [posterior_plot_fn, model_params]
    settings.posterior_plot_fn = settings.observations_plot_fn

    # One population
    # ---------

    settings.stan_model_path = os.path.join(path,
                                            "stan_model/populations_1.stan")

    settings.dir_name = "population1"
    settings.posterior_pdf = model_pdf_population_1

    fit1 = do_analysis(settings=settings)

    compare_with_exact(fits=[fit1],
                       path=settings.path, dir_name=settings.dir_name,
                       model_params=model_params)

    # Two populations
    # ---------

    settings.stan_model_path = os.path.join(path,
                                            "stan_model/populations_2.stan")

    settings.dir_name = "population2"
    settings.posterior_pdf = model_pdf_population_2

    fit2 = do_analysis(settings=settings)

    compare_with_exact(fits=[fit2],
                       path=settings.path, dir_name=settings.dir_name,
                       model_params=model_params)

    # Compare models
    # ----------

    models = {
        "One population": fit1,
        "Two populations": fit2
    }

    pareto_plot_params = ParetoKPlotParams(
        xlabel="[Na/H]",
        x_values=settings.values_agb)

    info_path = InfoPath(sub_dir_name='compare')

    save_compare(models=models, pareto_k_plot_params=pareto_plot_params,
                 info_path=info_path)


if __name__ == '__main__':
    print("Running the models...")

    do_work()

    print('We are done')

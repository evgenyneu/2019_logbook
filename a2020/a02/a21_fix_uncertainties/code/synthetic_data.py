import os
import pandas as pd
import numpy as np
from scipy.stats import norm
from dataclasses import dataclass, field
from typing import List
import yaml

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)

from tarpan.cmdstanpy.tree_plot import save_tree_plot
from tarpan.shared.tree_plot import TreePlotParams
from tarpan.shared.info_path import InfoPath

from tarpan.cmdstanpy.compare_parameters import (
    save_compare_parameters, CompareParametersType)


@dataclass
class SimulatedModelParameters:
    """Store parameters for the simulated model"""
    name: str
    sigma: float
    n: List[float]
    mu: List[float]

    def r(self, ix) -> str:
        return self.n[ix] / sum(self.n)


@dataclass
class SimulatedUncertainty:
    """Normally distributed uncertainty of observations"""
    mu: float
    sigma: float
    min: float  # Minimum value for the uncertainty


@dataclass
class SimulatedParameters:
    """Store parameters for the simulated models"""
    uncertainty: SimulatedUncertainty
    params: List[SimulatedModelParameters] = field(default_factory=list)

    def read_model_params(params_path):
        """
        Reads model parameters from configuration file.

        Returns
        -------
        SimulatedParameters
            Model parameters.
        """

        with open(params_path, 'r') as stream:
            data = yaml.safe_load(stream)
            uncertainty = data['uncertainty']

            simulated_uncertainty = SimulatedUncertainty(
                mu=uncertainty["mu"],
                sigma=uncertainty["sigma"],
                min=uncertainty["min"]
            )

            model_params = SimulatedParameters(
                uncertainty=simulated_uncertainty)

            for model in data['models']:
                single_model = SimulatedModelParameters(
                    name=model['name'],
                    sigma=model['sigma'],
                    n=model['n'],
                    mu=model['mu']
                )

                model_params.params.append(single_model)

            return model_params

    def __getitem__(self, index):
        return self.params[index]


def simulate_sample(seed, uncertainty: SimulatedUncertainty,
                    params: SimulatedModelParameters):
    """
    Simulate observations and return values and uncertainties.
    """

    np.random.seed(seed=seed)
    n = sum(params.n)  # Total sample size of observed stars
    values_true = []

    for i, mu in enumerate(params.mu):
        values = norm(mu, params.sigma).rvs(size=params.n[i])
        values_true += list(values)

    # Generate uncertainties for all observations
    uncertainties = np.abs(norm(uncertainty.mu, uncertainty.sigma).rvs(size=n))
    uncertainties = np.clip(uncertainties, uncertainty.min, None)

    # Generate observed values from true using the uncertainties
    values_observed = norm(values_true, uncertainties).rvs(size=n)

    return values_observed, uncertainties


def generate_data(csv_path, seed, model_params: SimulatedParameters):
    """
    Simulate observations and write the values and uncertainties
    to a text file.
    """

    rows = []

    for params in model_params:
        values, uncertainties = simulate_sample(
            seed=seed, uncertainty=model_params.uncertainty, params=params)

        for value, uncert in zip(values, uncertainties):
            rows.append([params.name, value, uncert])

    df = pd.DataFrame(rows, columns=["Type", "[Na/H]", "e_[Na/H]"])
    df.to_csv(csv_path)


def compare_with_exact(fits,
                       path, dir_name,
                       model_params: SimulatedParameters):
    """Compare parameter distributions with simulated exact values"""
    tree_params = TreePlotParams()
    names = [params.name for params in model_params]
    names_exact = [f'{name} exact' for name in names]
    names_combined = names
    tree_params.labels = names_combined
    tree_params.labels += names_exact

    default_tree_params = TreePlotParams()
    tree_params.marker_colors = default_tree_params.marker_colors[:len(fits)]
    tree_params.marker_colors += tree_params.marker_colors

    tree_params.marker_edge_colors = \
        default_tree_params.marker_edge_colors[:len(fits)]

    tree_params.marker_edge_colors += tree_params.marker_edge_colors

    data = []

    for params in model_params:
        model_data = {
            "sigma": params.sigma
        }

        if len(params.n) > 1:
            model_data["r"] = params.r(len(params.n) - 1)

        for i, mu in enumerate(params.mu):
            model_data[f"mu.{i + 1}"] = mu

        data.append(model_data)

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="compare_with_exact",
                         extension="png")

    save_tree_plot(fits, extra_values=data,
                   param_names=['mu', 'r', 'sigma'],
                   tree_params=tree_params,
                   info_path=info_path)

    # Create text table
    # ----------

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="parameters_compared",
                         extension="txt")

    save_compare_parameters(fits,
                            labels=names_combined,
                            extra_values=data,
                            param_names=["r", "mu", "sigma"],
                            info_path=info_path,
                            type=CompareParametersType.GITLAB_LATEX)


def posterior_plot_fn(fig, axes, params):
    line_colors = ['#0060ff88', '#ff002188', '#8888FF88', '#BBBB1188']

    for model_params, color in zip(params.params, line_colors):
        for mu in model_params.mu:
            for ax in axes:
                ax.axvline(mu, linestyle="dashed", c=color, linewidth=1)


def run_model(settings: AnalysisSettings, params_path, seed):
    """
    Run model with synthetic data

    Parameters
    ---------

    settings: AnalysisSettings
        Anaylsis settings
    params_path: str
        Path to the yaml file with parameters used for generating observations.
    seed: int
        Seed for initializing random number generator.
    """

    data_dir = os.path.join(settings.path, "data")

    if not os.path.isdir(data_dir):
        os.makedirs(data_dir, exist_ok=True)

    # Load model parameters
    model_params = SimulatedParameters.read_model_params(params_path)
    data_path = os.path.join(data_dir, "data.csv")

    # Generate data
    generate_data(csv_path=data_path, seed=seed, model_params=model_params)

    type_name = "Type"
    abundance_name = "[Na/H]"
    uncertainty_name = "e_[Na/H]"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.observations_plot_fn = [posterior_plot_fn, model_params]
    settings.posterior_plot_fn = settings.observations_plot_fn
    settings.xlabel = "Sodium abundance [Na/H]"

    fit_agb, fit_rgb = do_analysis(settings=settings)

    compare_with_exact(fits=[fit_agb, fit_rgb],
                       path=settings.path, dir_name=settings.dir_name,
                       model_params=model_params)

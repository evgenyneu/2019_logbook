# Calculate distance uncertainties with Monte Carlo
# ---------

from uncertainties import ufloat
from a2019.a12.a10_calculate_distance.code.distance import get_distance
from scipy.stats import norm
import matplotlib.pyplot as plt
import seaborn as sns
from sigfig import round as round_sigfig
import numpy as np


def get_data():
    """
    Returns:

    Distance moduli to NGC 288 from literature.

    Array of tuples with following indexes:
        0 : distance modulus m - M (visual).
        1 : color excess E(B - V).
        2 : Name of publication.
    """

    return [
        (ufloat(14.9, 0.2), ufloat(0, 0), 'Penny 1984'),
        (ufloat(14.85, 0.10), ufloat(0.04, 0.02), 'Alcaino 1997'),
        (ufloat(14.57, 0.07), ufloat(0.018, 0.010), 'Chen 2000'),
        (ufloat(14.84, 0.1), ufloat(0.03, 0), 'Harris 2010'),
        (ufloat(14.768, 0.003), ufloat(0.03, 0), 'Arellano Ferro 2013')
    ]


def calculate_one(item):
    """
    Calculates distance and its uncertainty
    using only uncertainty for the distance modulus.

    Returns: tuple

    (distance, uncertainty, source)
    """
    n_samples = 10000
    distance_modulus = item[0]
    color_excess = item[1].nominal_value
    source = item[2]

    distance_modulus_samples = norm(
        distance_modulus.nominal_value, distance_modulus.std_dev) \
        .rvs(size=n_samples)

    distances = [
                    get_distance(dm, color_excess=color_excess) for
                    dm in distance_modulus_samples
                ]

    distance = get_distance(distance_modulus.nominal_value, color_excess=color_excess)
    std = np.std(distances)
    return (distance, float(std), source)


def calculate_all_with_distance_modulus_uncertainty(data):
    print("\n\nWith distance modulus uncertainty only")

    for datum in data:
        mean, std, source = calculate_one(datum)
        std = round_sigfig(std, 1)
        sig_figs = len(str(int(mean / std)))
        mean = round_sigfig(mean, sig_figs)
        print(f'{mean:g} ± {std:g} pc {source}')


def calculate_one_both_uncertainties(item):
    """
    Calculates distance and its uncertainty
    using only uncertainties for both (m - M) and E(B - V)

    Returns: tuple

    (distance, uncertainty, source)
    """

    n_samples = 10000
    distance_modulus = item[0]
    color_excess = item[1]
    source = item[2]

    distance_modulus_samples = norm(
        distance_modulus.nominal_value, distance_modulus.std_dev) \
        .rvs(size=n_samples)

    color_excess_samples = norm(
        color_excess.nominal_value, color_excess.std_dev) \
        .rvs(size=n_samples)

    distances = [
                    get_distance(distance_modulus, color_excess)
                    for distance_modulus, color_excess
                    in zip(distance_modulus_samples, color_excess_samples)
                ]

    distance = get_distance(
        distance_modulus.nominal_value,
        color_excess=color_excess.nominal_value)

    std = np.std(distances)
    return (distance, float(std), source)


def calculate_all_with_complete_uncertainty(data):
    print("\n\nWith uncertainty for both (m - M) and E(B - V)")

    for datum in data:
        mean, std, source = calculate_one_both_uncertainties(datum)
        std = round_sigfig(std, 1)
        sig_figs = len(str(int(mean / std)))
        mean = round_sigfig(mean, sig_figs)
        print(f'{mean:g} ± {std:g} pc {source}')


if __name__ == '__main__':
    data = get_data()
    # calculate_all_with_distance_modulus_uncertainty(data)
    calculate_all_with_complete_uncertainty(data)
    print("We are done :)")

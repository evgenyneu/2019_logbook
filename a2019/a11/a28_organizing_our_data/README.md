# Getting star data

I want to get the data from Chris and organize for later use.

## Data from Chris

* [rawphotometry.csv](a2019/a11/a28_organizing_our_data/original_data_from_chris/rawphotometry.csv) The initial raw photometry data containing all the stars

* [finalsample_photometry_velocities_abundances.csv](a2019/a11/a28_organizing_our_data/original_data_from_chris/finalsample_photometry_velocities_abundances.csv) Final sample containing all the stars for analysis.

* [rawvelocities.csv](a2019/a11/a28_organizing_our_data/original_data_from_chris/rawvelocities.csv)

## Combine Chris' data into single CSV file

[ngc288_stars.csv](data/NGC288/ngc288_stars.csv)

[ngc288_stars.md](data/NGC288/ngc288_stars.md)

*Code*:

```
python -m a2019.a11.a28_organizing_our_data.combine_data
```

## Learning Aladin


* File > Load local file: `data/NGC288/ngc288_stars.csv`


Click "Filter" icon on right menu:

```
# Obj.Type
# We draw a different symbol according to
# the object type (value of the column with UCD "src.class")
${selected}=1 {draw red square}
${selected}=0 {draw blue rhomb}
# etc ...
```

## Cross match with GAIA

* Open Aladin

* Load our stars: File > Load local file: [ngc288_stars.csv](a2019/a11/a28_organizing_our_data/ngc288_stars.csv)

* Click 2MASS link on the top to show the star markers.

* In the botton-left search box, enter "DR2", search and click on "GaiaSource DR2 data (gaia2)"

* Click "Load" button in the popup. Gaia stars should be loaded.

* Click "Cross" icon on the right.

* Change the upper "Threshold in source separation" from 4 to 0.5.

* Click "Perform cross-match".

A new layer called "XMatch" will appear on the right. If you hover mouse over it, it should show 185 objects.

### Save cross match to a File

* Click on XMatch layer on the right.

* Select File > Export Planes...

* Press "Export" button.

An [XMatch.tsv](a2019/a11/a28_organizing_our_data/gaia/XMatch.tsv) file will be created.


## Exporting Gaia data

Here we export Gaia data from the cross-matched file [XMatch.tsv](a2019/a11/a28_organizing_our_data/gaia/XMatch.tsv) and add Gaia's object ID to our data file [ngc288_stars.csv](a2019/a11/a28_organizing_our_data/ngc288_stars.csv):

```
python -m a2019.a11.a28_organizing_our_data.export_gaia
```

[ngc288_stars.csv](data/NGC288/ngc288_gaia.csv)

[ngc288_stars.md](data/NGC288/ngc288_gaia.md)

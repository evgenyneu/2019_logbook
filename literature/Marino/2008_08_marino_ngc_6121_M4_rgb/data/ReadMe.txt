J/A+A/490/625       Abundances of NGC 6121 red giants        (Marino+, 2008)
================================================================================
Spectroscopic and photometric evidence of two stellar populations in the
Galactic globular cluster NGC 6121 (M4).
    Marino A.F., Villanova S., Piotto G., Milone A.P., Momany Y., Bedin L.R.,
    Medling A.M.
   <Astron. Astrophys. 490, 625 (2008)>
   =2008A&A...490..625M
================================================================================
ADC_Keywords: Clusters, globular ; Spectroscopy ; Abundances
Keywords: star: abundances - Galaxy: globular cluster: individual: NGC 6121

Abstract:
    We present abundance analysis based on high resolution spectra of 105
    isolated red giant branch (RGB) stars in the Galactic Globular Cluster
    NGC 6121 (M4). The data have been collected with FLAMES+UVES, at the
    ESO/VLT@UT2 telescope. Spectroscopic data were coupled with high
    precision wide-field UBVI_C photometry from WFI@2.2m telescope and
    infrared JHK photometry from 2MASS. We derived an average
    [Fe/H]=-1.07+/-0.01, and an {alpha} enhancement of
    [{alpha}/Fe]=+0.39+/-0.05dex (internal errors). We confirm the
    presence of an extended Na-O anticorrelation, and find two distinct
    groups of stars with significantly different Na and O content. We find
    no evidence of a Mg-Al anticorrelation. By coupling our results with
    previous studies on the CN band strength, we find that the CN strong
    stars have higher Na and Al content and are more O depleted than the
    CN weak ones. The two groups of Na-rich, CN-strong and Na-poor,
    CN-weak stars populate two different regions along the RGB. In the U
    vs. U-B color magnitude diagram the RGB spread is present from the
    base of the RGB to the RGB-tip. Apparently, both spectroscopic and
    photometric results imply the presence of two stellar populations 
    in M4. We briefly discuss the possible origin of these populations.

Description:
    Table 8 contains the equatorial coordinates, the atmospheric
    parameters measured on UVES spectra, UBVI_C photometry from WFI@2.2m
    telescope and infrared JHK photometry from 2MASS for the analysed
    stars.
    Table 9 contains the measured chemical abundances.

File Summary:
--------------------------------------------------------------------------------
 FileName   Lrecl  Records   Explanations
--------------------------------------------------------------------------------
ReadMe         80        .   This file
table8.dat     88      105   Identification, equatorial coordinates,
                              atmospheric parameters and photometry
table9.dat     70      105   Chemical abundances
--------------------------------------------------------------------------------

See also:
  J/ApJ/443/124  : Radial Velocities of Stars in M4 (Peterson+ 1995)
  J/PAZh/22/847  : Identification and coordinates in M4 (Shokin+, 1996)
  J/AJ/114/189   : M 4 CCD photometry (Alcaino+, 1997)
  J/ApJS/120/265 : HST photometry of M4 (Ibata+, 1999)
  J/AJ/127/2771  : HST proper-motions of M4 main-sequence stars (Richer+, 2004)

Byte-by-byte Description of file: table8.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label     Explanations
--------------------------------------------------------------------------------
   1-  5  I5    ---     Star      [5359/45895] Star identification number
   7- 18  F12.8 deg     RAdeg     Right ascension in decimal degrees (J2000)
  20- 31  F12.8 deg     DEdeg     Declination in decimal degrees (J2000)
  33- 36  I4    K       Teff      Effective temperature
  38- 41  F4.2  [cm/s2] logg      Surface gravity
  43- 46  F4.2  km/s    Vt        Microturbulent velocity
  48- 52  F5.2  mag     Umag      ?=- Johnson U magnitude
  54- 58  F5.2  mag     Bmag      ?=- Johnson B magnitude
  60- 64  F5.2  mag     Vmag      ?=- Johnson V magnitude
  66- 70  F5.2  mag     Icmag     ?=- Cousisn I magnitude
  72- 76  F5.2  mag     Jmag      ?=- 2MASS J magnitude
  78- 82  F5.2  mag     Hmag      ?=- 2MASS H magnitude
  84- 88  F5.2  mag     Kmag      ?=- 2MASS K magnitude
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table9.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label      Explanations
--------------------------------------------------------------------------------
   1-  5  I5    ---     Star       [5359/45895] Star identification number
   7- 11  F5.2  [Sun]   [Fe/HI]    Abundance ratio [Fe/HI]
  13- 16  F4.2  [Sun]   [O/Fe]     ?=- Abundance ratio [O/Fe]
  18- 22  F5.2  [Sun]   [Na/Fe]    Abundance ratio [Na/Fe]
  24- 27  F4.2  [Sun]   [Mg/Fe]    Abundance ratio [Mg/Fe]
  29- 33  F5.2  [Sun]   [Al/Fe]    ?=- Abundance ratio [Al/Fe]
  35- 38  F4.2  [Sun]   [Si/Fe]    Abundance ratio [Si/Fe]
  40- 43  F4.2  [Sun]   [Ca/Fe]    Abundance ratio [Ca/Fe]
  45- 48  F4.2  [Sun]   [Ti/FeI]   Abundance ratio [Ti/FeI]
  50- 53  F4.2  [Sun]   [Ti/FeII]  Abundance ratio [Ti/FeII]
  55- 59  F5.2  [Sun]   [Cr/Fe]    Abundance ratio [Cr/Fe]
  61- 65  F5.2  [Sun]   [Ni/Fe]    Abundance ratio [Ni/Fe]
  67- 70  F4.2  [Sun]   [Ba/FeII]  ?=- Abundance ratio [Ba/FeII]
--------------------------------------------------------------------------------

Acknowledgements:
    Anna Marino, anna.marino(at)unipd.it
================================================================================
(End)          Anna Marino [Univ. Padova], Patricia Vannier [CDS]    02-Sep-2008

import pandas as pd
import os


def extract_abundaces():
    data_dir = 'a2020/a07/a06_abundances_from_chris/data'

    # Open data
    data_path = os.path.join(
        data_dir,
        'ngc288_stars_with_abundances_from_chris_agb_or_rgb_columns_removed.csv')

    df = pd.read_csv(data_path)
    columns = ['agb_or_rgb', 'NaI/H', 'errNaI/H']
    df = df.dropna(subset=columns)
    df = df[columns]

    # Save data frame
    output_path = os.path.join(data_dir, 'sodium.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    extract_abundaces()
    print('We are done!')

import os

from a2020.a03.a03_akis_model.ngc_5752_agb_compare_models.code.compare import (
    AnalysisSettings, compare_models)

from a2020.a03.a03_akis_model.ngc_5752_agb_compare_models.code.synthetic_data import (
    SimulatedParameters, generate_data, compare_with_exact,
    posterior_plot_fn)


def do_work():
    path = "a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/\
test_one_population"

    data_dir = os.path.join(path, 'data')

    settings = AnalysisSettings(
        path=os.path.join(path, "model_info"),
        study_name="synthetic data",
        xlabel="Sodium abundance [Na/H]",
        type_name="Type",
        abundance_name="[Na/H]",
        uncertainty_name="e_[Na/H]",
        data_path=os.path.join(path, 'data/data.csv')
    )

    # Generate data
    # --------

    if not os.path.isdir(data_dir):
        os.makedirs(data_dir, exist_ok=True)

    params_path = os.path.join(path, "code/model_params.yaml")
    model_params = SimulatedParameters.read_model_params(params_path)
    generate_data(csv_path=settings.data_path, seed=10, model_params=model_params)

    settings.observations_plot_fn = [posterior_plot_fn, model_params]
    settings.posterior_plot_fn = settings.observations_plot_fn

    fit1, fit2 = compare_models(settings)

    compare_with_exact(fits=[fit1],
                       path=settings.path, dir_name="one_population",
                       model_params=model_params)

    compare_with_exact(fits=[fit2],
                       path=settings.path, dir_name="two_populations",
                       model_params=model_params)


if __name__ == '__main__':
    print("Running the models...")

    do_work()

    print('We are done')

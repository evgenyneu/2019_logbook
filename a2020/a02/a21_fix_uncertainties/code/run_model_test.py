from pytest import approx
from unittest.mock import Mock
import numpy as np

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    standardise, standardised_data_for_stan, destandardise_stan_output)


def test_standardise():
    measurements = [1, 2.2, 4.4, -1, 0, 8, 12]

    result = standardise(measurements=measurements)

    assert len(result) == 7
    assert result[0] == approx(-0.642074462492175, rel=1e-15)
    assert result[5] == approx(0.9631116937382623, rel=1e-15)
    assert result.mean() == approx(0, rel=1e-15)
    assert result.std() == approx(1, rel=1e-15)


def test_standardised_data_for_stan():
    measurements = [1, 2, 3, 4]
    uncertainties = [0.5, 0.3, 0.2, 0.6]

    data = standardised_data_for_stan(measurements=measurements,
                                      uncertainties=uncertainties,
                                      sigma_stdev_prior=0.1)

    assert len(data["y"]) == 4
    assert data["y"][0] == approx(-1.3416407864998738, rel=1e-15)
    assert data["y"].mean() == approx(0, rel=1e-15)
    assert data["y"].std() == approx(1, rel=1e-15)

    assert len(data["uncertainties"]) == 4
    assert data["uncertainties"][0] == approx(0.4472135954999579, rel=1e-15)

    assert data["n"] == 4

    assert data["sigma_stdev_prior"] == approx(0.08944271909999159, rel=1e-15)

    # Ensure supplied arrays are unchanged
    assert measurements == [1, 2, 3, 4]
    assert uncertainties == [0.5, 0.3, 0.2, 0.6]


def test_destandardise_stan_output():
    measurements = [1, 2, 3, 4]
    fit_mock = Mock()
    fit_mock.column_names = ['r', 'mu.1', 'mu.2', 'sigma']
    fit_mock.chains = 2
    fit_mock.sample = np.zeros(shape=[4, 2, 4])

    for i_column in range(4):
        for i_chain in range(2):
            values = np.array([1., 2., 3., 4.])
            values += i_column
            values *= i_chain * 1.2
            fit_mock.sample[:, i_chain, i_column] = values

    destandardise_stan_output(fit=fit_mock, measurements=measurements)

    assert fit_mock.sample[:, 0, 1].tolist() == [2.5, 2.5, 2.5, 2.5]
    assert fit_mock.sample[:, 0, 2].tolist() == [2.5, 2.5, 2.5, 2.5]
    assert fit_mock.sample[:, 0, 3].tolist() == [0, 0, 0, 0]

    assert fit_mock.sample[0, 1, 1] == approx(5.183281572999748, rel=1e-15)
    assert fit_mock.sample[0, 1, 2] == approx(6.524922359499621, rel=1e-15)
    assert fit_mock.sample[0, 1, 3] == approx(5.366563145999495, rel=1e-15)

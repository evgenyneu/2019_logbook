# Calculate uncertainties of CMD axes

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot
from uncertainties import ufloat


def uncertainties():
    data_dir = 'a2019/a12/a02_plot_cmd_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)
    df = df[df.u_k != ' ']  # Remove rows with missing uncertainties
    df.u_k = pd.to_numeric(df.u_k)

    # Entire sample
    # ----------

    uncertainty_b_df = df['u_b']
    uncertainty_b_minus_k_df = np.sqrt(df['u_b']**2 + df['u_k']**2)

    print("\n\n")
    print("All stars")
    print("--------------------------")

    u_b_with_uncert = ufloat(uncertainty_b_df.mean(), uncertainty_b_df.std())

    u_b_minus_k_with_uncert = ufloat(uncertainty_b_minus_k_df.mean(),
                                     uncertainty_b_minus_k_df.std())

    print(f"u(B)={u_b_with_uncert}")
    print(f"u(B-K)={u_b_minus_k_with_uncert}")

    data = [
        uncertainty_b_df,
        uncertainty_b_minus_k_df
    ]

    plt.boxplot(data, labels=["B", "B - K"])

    title = (
        'Distibution of uncertainties of photometric\n'
        'measurements in entire sample'
    )
    plt.title(title)
    save_plot(plt=plt, suffix=f'entire_sample')
    plt.show()

    # Selected stars
    # ----------

    df = df.loc[df['selected'] == 1]

    uncertainty_b_df = df['u_b']
    uncertainty_b_minus_k_df = np.sqrt(df['u_b']**2 + df['u_k']**2)

    print("\n")
    print("Selected stars")
    print("--------------------------")
    u_b_with_uncert = ufloat(uncertainty_b_df.mean(), uncertainty_b_df.std())

    u_b_minus_k_with_uncert = ufloat(uncertainty_b_minus_k_df.mean(),
                                     uncertainty_b_minus_k_df.std())

    print(f"u(B)={u_b_with_uncert}")
    print(f"u(B-K)={u_b_minus_k_with_uncert}")

    data = [
        uncertainty_b_df,
        uncertainty_b_minus_k_df
    ]

    plt.boxplot(data, labels=["B", "B - K"])

    title = (
        'Distibution of uncertainties of photometric\n'
        'measurements in selected stars only'
    )
    plt.title(title)
    save_plot(plt=plt, suffix=f'selected_stars')
    plt.show()


if __name__ == '__main__':
    uncertainties()
    print('We are done')

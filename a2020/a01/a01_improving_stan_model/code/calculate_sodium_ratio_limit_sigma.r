# Use Gaussian mixture model to find out
# proportion of low sodium AGB/RGB star in NGC 288
# --------------

library(rstan)
library(rethinking)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

# Load the abundance data
load_data <- function() {
    data_path = "a2020/a01/a01_improving_stan_model/data/sodium.csv"
    data = read.csv(data_path, header = TRUE)
    as.data.frame(data)
}

# Fit the model to the data
#
# Parameters
# -----------
#
# data : the input data frame
#
# star_type : 'RGB' or 'AGB'
#
run_model <- function(data, star_type, seed) {
    print("-------------------")
    print(paste("Fitting", star_type, 'stars data'))
    print("-------------------")

    data = data[data$type == star_type,]

    # Standardize the sodium abundance
    # --------

    d_st = list(
        y = standardize(data$na_over_h),
        uncertainties = data$uncertainty /  sd(data$na_over_h),
        N=length(data$na_over_h)
    )

    # Run Stan
    model_path = 'a2020/a01/a01_improving_stan_model/code/calculate_sodium_ratio_limit_sigma.stan'

    fit <- stan(file=model_path, data=d_st, iter=10000, warmup=2000, chains=1,
                control=list(adapt_delta=0.99, max_treedepth=10),
                seed=seed)

    print(fit)
    traceplot(fit, par=c('p', 'mu'))
    samples = extract(fit)

    # De-standardise the variables
    # ------------

    d_destand = list(
        p = samples$p,
        mu1 = samples$mu[,1] * sd(data$na_over_h) + mean(data$na_over_h),
        mu2 = samples$mu[,2] * sd(data$na_over_h) + mean(data$na_over_h),
        sigma1 = samples$sigma1 * sd(data$na_over_h),
        sigma2 = samples$sigma2 * sd(data$na_over_h),
        k = samples$k
    )

    file_name = paste("sodium_model", tolower(star_type), "sigma_constrained", sep="_")
    path = paste("a2020/a01/a01_improving_stan_model/data/", file_name, ".csv", sep="")
    write.csv(file=path, x=d_destand)

    precis(d_destand)
}

data = load_data()
run_model(data, 'RGB', seed=75675)
run_model(data, 'AGB', seed=53452)

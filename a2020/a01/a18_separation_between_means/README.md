# Varying separation between population means

Here I want to change the separation between the population means mu1 and mu2 and see how it affects the posterior distributions of parameters of the model.

## Simulating observations

I generate series of observations of sodium abundance from two normally distributed populations. The means of the populations are separated by 0, 0.2, 0.5 and 1.2 dex. The simulated values are shown on Fig. 1.


<img src="a2020/a01/a18_separation_between_means/code/plots/simulate_sodium__apart_1.20.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_separation_between_means/code/plots/simulate_sodium__apart_0.50.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_separation_between_means/code/plots/simulate_sodium__apart_0.20.png" width="400" alt='Simulated sodium abundances'>

<img src="a2020/a01/a18_separation_between_means/code/plots/simulate_sodium__apart_0.00.png" width="400" alt='Simulated sodium abundances'>

Figure 1: Simulated sodium abundances for different separations between the means of two normally distributed populations, shown as vertical dashed lines.


## Posterior distributions

The posterior distributions of parameters are shown on Fig. 2. We can see that as separation between means decreases, parameter p becomes more uncertain.

<img src="a2020/a01/a18_separation_between_means/code/plots/compare_all.png" width="700" alt='Posterior distributions of parameters'>

Figure 2: Posterior distributions of model parameters, along with locations of the simulated true values. The samples were generated from simulated sodium abundances with different separations between the means.


### Distribution of p for small separations between means

We have found that for small separation between means, parameter p becomes more uncertain. We can see that from Fig. 3, distribution of p values look almost uniform.

<img src="a2020/a01/a18_separation_between_means/code/plots/p_separation_0.2.png" width="700" alt='Posterior distributions of parameter p'>

Figure 3: Posterior distributions of parameter `p` for $`\Delta \mu`$=0.2.


### Special case: zero separation of the means

When two population have the same means, this means that this is a single population. This special case is plotted separately on Fig. 4. We can see that the distributions of mu1 and mu2 parameters now partially overlap within 95% HPDI. Interestingly, the two distributions do not overlap completely. This is probably happens because in our model the priors for mu1 and mu2 are well-separated. In other words, our model assumes that there are two well-separated populations, so it detected those with large uncertainty.

<img src="a2020/a01/a18_separation_between_means/code/plots/zero_separation.png" width="700" alt='Posterior distributions of parameters'>

Figure 4: Posterior distributions of model parameters for no separation between two population means.

import pandas as pd
import os
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a06_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions_is_comoving.csv')
    df = pd.read_csv(stars_path)

    columns = ['pm_ra', 'u_pm_ra', 'pm_de', 'u_pm_de', 'pm_speed_from_mean']

    for column in columns:
        df = df[df[column] != ' ']  # Remove rows with missing uncertainties
        df[column] = pd.to_numeric(df[column])

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    pm_ra = stars_other_df['pm_ra']
    pm_de = stars_other_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='black', marker='o', s=7, edgecolors='black',
                label='Excluded')


    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    pm_ra = stars_agb_df['pm_ra']
    pm_de = stars_agb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    pm_ra = stars_rgb_df['pm_ra']
    pm_de = stars_rgb_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # Excluded stars
    # ---------------

    stars_df = df.loc[df['id'].isin(['BAGBYAZ005721', 'RGBSYAZ003702'])]

    pm_ra = stars_df['pm_ra']
    pm_de = stars_df['pm_de']

    plt.scatter(x=pm_ra, y=pm_de,
                c='orange', marker='p', edgecolors='black',
                linewidth=1, label='Outliers', s=70)

    plt.legend()
    plt.xlabel(r'Proper motion in right ascension $\mu_\alpha \ \cos \delta$ [mas $a^{-1}$]')
    plt.ylabel('Proper motion in declination $\mu_\delta$ [mas $a^{-1}$]')
    plt.title('Proper motions of all observed stars in NGC288\nwith two removed outliers')

    # --------------

    plt.axis('equal')
    plt.xlim(-20, 60)
    plt.ylim(-40, 20)
    ax = plt.gca()
    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

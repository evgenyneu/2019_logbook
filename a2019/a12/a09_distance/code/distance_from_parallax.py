from uncertainties import ufloat



def distance():
    parallax = ufloat(0.1401, 0.0021)
    distance = 1 / parallax
    print(distance)


if __name__ == '__main__':
    distance()
    print('We are done!')

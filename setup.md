# Set up

* Download the project:

```
git clone https://gitlab.com/evgenyneu/2019_logbook.git
```

* Change working directory:

```
cd 2019_logbook
```

* Install [Miniconda](https://docs.conda.io/en/latest/miniconda.html). It is used for creating isolated Python environments that use specific version of Python and the libraries for this project (listed in [requirements.txt](requirements.txt)). This also avoids polluting system-wide python installation and interfering with other Python projects.

* Create a Conda environment with Python 3.8:

```
conda create --name evgenii_2019_logbook python=3.8
```

Answer YES to when asked "The following NEW packages will be INSTALLED".

* Activate the environment:

```
conda activate evgenii_2019_logbook
```

* Install Python libraries listed in [requirements.txt](requirements.txt) file:

```
pip install -r requirements.txt
```

It will install specific versions of Python libraries used in this logbook. Now you are ready to run the code.


## Running Python code

Example of running the code from [a2019/a12/a10_calculate_distance/code/cmd_gaia.py](a2019/a12/a10_calculate_distance/code/cmd_gaia.py) file.

1. All code is supposed to be run **from the root 2019_logbook directory** of this repository. In other words, do not change to the subdirectory containing the python file because the code might use Python modules located in other directories of the logbook.

2. Run the code:

```
PYTHONPATH=. python a2019/a12/a10_calculate_distance/code/cmd_gaia.py
```

### Cleaning up

To remove the `evgenii_2019_logbook` environment completely and free-up the disk space:

```
conda deactivate
conda env remove -n evgenii_2019_logbook
```

import os

from paper.model_std.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def do_work(dir_name):
    """
    Main function that does all the work.

    Parameters
    ----------

    dir_name: str
        Directory where plots and summaries will be placed.
    """

    settings = AnalysisSettings(
        path="paper/model_std",
        dir_name=dir_name,
        study_name="NGC 288",
        xlabel="Sodium abundance [Na/H]",
        stan_model_path="paper/model_std/code/stan_model/two_populations.stan"
    )

    data_path = os.path.join(settings.path, "data/sodium.csv")
    type_name = "agb_or_rgb"
    abundance_name = "NaI/H"
    uncertainty_name = "errNaI/H"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work(dir_name="model_info")
    print('We are done')

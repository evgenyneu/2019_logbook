# Analysing data from [Marino et al. 2017](https://ui.adsabs.harvard.edu/abs/2017ApJ...843...66M/abstract), NGC 6121 (M4)


## RGB sample

The authors calculate sodium abundances for AGB stars only. For RGB stars they used abundances from [Marino et al. 2008](https://ui.adsabs.harvard.edu/abs/2008A%26A...490..625M/abstract).

For uncertainty of sodium abundance for RGB stars we use 0.04 given by $`\sigma_{tot}`$ from Table 3, Marino et al. 2008.

Code: [a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4/code/model.py](a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4/code/model.py)


## Posterior distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions.


## Parameter distributions

<img src="a2020/a02/a21_fix_uncertainties/a2017_07_marino_ngc_6121_M4/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 3: Posterior distributions of model parameters for RGB and AGB stars.


## Apples to oranges comparison with the authors


|   Parameter   |         Us                 |        Author         |  Agree 95% | Agree 68%  |
|:-------------:|---------------------------:|----------------------:|:----------:|:----------:|
| $`r_{agb}`$   |  $`0.51^{+0.16}_{-0.16}`$  |   $`0.47 \pm 0.13`$   |    Yes     |    Yes     |
| $`r_{rgb}`$   |  $`0.58^{+0.05}_{-0.05}`$  |   $`0.58 \pm 0.05`$   |    Yes     | Yes (OMG)  |
| $`f`$         |  $`0.16^{+0.26}_{-0.31}`$  |   $`0.19 \pm 0.23`$   |    Yes     |    Yes     |

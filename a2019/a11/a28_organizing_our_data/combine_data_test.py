from combine_data import make_star_id


def test_make_star_id():
    result = make_star_id(type='AGBS', catalog='YAZ', id=16610)

    assert result == 'AGBSYAZ016610'

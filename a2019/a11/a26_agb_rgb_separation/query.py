from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord

catalog_name = 'J/MNRAS/485/3042/table4'

catalog_list = Vizier.get_catalogs(catalog_name)

coord = coord.SkyCoord(ra=13.179058,
                       dec=-26.603914,
                       unit=(u.deg, u.deg),
                       frame='icrs')

result = Vizier(row_limit=50).query_region(coord,
                                           radius=1*u.arcsec,
                                           catalog=[catalog_name])

print(result[0].info)

print(result[0][['Vmag', 'Bmag', 'Imag']])

## Learning how to run Stan

Programming Gaussian mixture model in Stan:

https://mc-stan.org/docs/2_21/stan-users-guide/summing-out-the-responsibility-parameter.html


## Data analysis

We simulate 15 observations, create a statistical model of two Gaussians and run Stan to generate samples:

Code: [a2019/a12/a17_learn_stan/code/mixture.r](a2019/a12/a17_learn_stan/code/mixture.r)

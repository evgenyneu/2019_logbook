# Testing the model with one population

We test the model by simulating single population of AGB and RGB stars. Results are shown on Figures 1 and 2. We can see from Fig. 2 that distributions for mu1 and mu2 for both AGB and RGB overlap, and their 95% CI agree with simulated exact values.

Code: [a2020/a02/a21_fix_uncertainties/test_single_population/code/model.py](a2020/a02/a21_fix_uncertainties/test_single_population/code/model.py)


<img src="a2020/a02/a21_fix_uncertainties/test_single_population/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions. Dashed vertical lines correspond to the true locations of the populations' mu values.


<img src="a2020/a02/a21_fix_uncertainties/test_single_population/images/compare_with_exact.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars, compared with exact values.


### Summary of model parameters

Table 1: Summaries of distributions of model parameters and their simulated true values.

|           |                        r |                      mu.1 |                      mu.2 |                    sigma |
|----------:|-------------------------:|--------------------------:|--------------------------:|-------------------------:|
|       AGB | $`0.28^{+0.33}_{-0.14}`$ | $`-1.31^{+0.04}_{-0.04}`$ | $`-1.26^{+0.11}_{-0.05}`$ | $`0.01^{+0.03}_{-0.01}`$ |
|       RGB | $`0.41^{+0.25}_{-0.24}`$ | $`-1.06^{+0.03}_{-0.03}`$ | $`-1.01^{+0.05}_{-0.04}`$ | $`0.01^{+0.03}_{-0.01}`$ |
| AGB exact |                          |                 $`-1.30`$ |                           |                 $`0.05`$ |
| RGB exact |                          |                 $`-1.00`$ |                           |                 $`0.05`$ |

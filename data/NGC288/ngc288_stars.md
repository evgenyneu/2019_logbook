# NGC 288 stars data

The main data file of this project that contains all data about individual stars.

## Columns


### id

The star ID we use internally


### observed

Contains 1 if the star was observed with the telescope (i. e. its data present in the FITS files).


### selected

Contains 1 if the star was selected for analysis (based on radial velocity, Fe). Otherwise, 0.


### type

The type of the star.


### cat

The catalog form which U, V, B, I values and their uncertainties are from:

* 'YAS': [Yazan Momany](data/NGC288/from_literature/ngc288_yazan.csv) catalog.

* 'STE': [Stetson+ 2019](https://ui.adsabs.harvard.edu/?#abs/2019MNRAS.485.3042S) catalog.


### raj2000_2mass

Right ascension J2000 epoch coordinate from [2MASS catalog](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C).


### dej2000_2mass

Declination J2000 epoch coordinate from [2MASS catalog](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C).


### id_gaia_dr2

The value of the Source field in [Gaia DR2 2018](https://arxiv.org/abs/1804.09365) catalog, table `CDS/I/345/gaia2`.


### id_2mass

Record ID in the [2MASS catalog](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C), table `CDS/II/246/out`.


### id_yazan

Record ID in the [Yazan Momany](data/NGC288/from_literature/ngc288_yazan.csv) catalog.


### id_stetson

The 'Star' id in the [Stetson+](https://ui.adsabs.harvard.edu/?#abs/2019MNRAS.485.3042S) catalog.


### v, u_v, b, u_b, i, u_i, u, u_u

The magnitudes with uncertainties. The sources are:

* For Cat="YAS" stars, data are from [Yazan Momany's]((data/NGC288/from_literature/ngc288_yazan.csv)) catalog.

* For Cat="STE", the data is from [Stetson+](https://ui.adsabs.harvard.edu/?#abs/2019MNRAS.485.3042S) catalog.


### j, u_j, h, u_h, k, u_k

The magnitudes and their uncertainties from [2MASS catalog](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C).


### vel_rad, u_vel_rad

Radial velocity of the star in km/s and its uncertainty.



### pm_ra, u_pm_ra

Proper motions in right ascension direction and its uncertainty from [Gaia DR2 2018](https://arxiv.org/abs/1804.09365) catalog (pmRA * cos(DE)).

Units: mas/a.


### pm_de, u_pm_de

Proper motions in declination direction and its uncertainty from [Gaia DR2 2018](https://arxiv.org/abs/1804.09365) catalog.

Units: mas/a.


### pm_speed_from_mean

Proper speed relative to the average proper motion of the selected AGB/RGB stars. The value is used to determine if the star is in globular cluster.

Units: mas/a.


### Teff, logg, micro

Effective temperature, log(g) and microgravity that we calculated.

#### Uncertainties

Teff: 70 K

logg: 0.2

micro: 0.2


### rawElementI, rawElementstdev,

Abundances and their standard deviations that we calcualated. Standard deviation are the standard deviations of abundaces of individual lines.


### Element/H, errElement/H, Element/Fe, errElement/Fe

Abundances with respect to solar [Element/H] and their uncertainties that we calculated.

# Code for NGC288 sodium statistical model

This repository includes code used in [reference to paper] to calculate parameters of bimodal distributions of sodium abundances in AGB/RGB starts in NGC288 globular cluster. This code is for those who wish to rerun our calculations and reproduce our results.

Statistical model is written in [Stan](https://mc-stan.org) language. In addition, we used [cmdstanpy](https://github.com/evgenyneu/tarpan) library for running Stan from Python and Tarpan library for making data summaries and plots.


## Code description

The program code includes:

1. Python script: [paper/model_std/code/model.py](paper/model_std/code/model.py),

1. Stan's model: [paper/model_std/code/stan_model/two_populations.stan](paper/model_std/code/stan_model/two_populations.stan).


## Statistical model

Mathematical description of the statistical model is shown in Listing 1.

Listing 1: Statical model describing a mixture of two Gaussian components.


<img src="paper/model_std/latex/images/model.png" width=700 alt='Statistical model'>


This model describes a mixture of two Gaussian components. These are two distinct populations of stars from which we observed sodium abundances. The input data used in the model is transformed to standard values (i.e. z-scores) by subtracting mean and dividing by standard deviation. After Stan has finished processing the model, the values from posterior distributions are transformed back to sodium abundances.


### Input parameter of the model

The input parameters of the model are:

* $`y_i`$: observed value of sodium abundance of the i-th star, in [Na/H] units,

* $`e(y_i)`$: corresponding measurement uncertainty of the sodium abundance.

* $`\sigma_{\textrm{stdev}}`$: standard deviation of the population spread, in [Na/H] units. We used value of 0.05 dex.


### Estimated parameters of the model

With this code we want to estimate distributions of the following parameters:

* $`r`$: mixing proportion, which is the proportion of stars in the second population (the population with the higher average sodium abundance),

* $`\mu_1`$ and $`\mu_2`$: locations of two populations,

* $`\sigma`$: spreads of the populations.



## Setup

### 1. Install Python and libraries

Download and install all required software by following [setup instructions](/setup.md).


### 2. Install Stan

```
install_cmdstan -v 2.21.0
```

## Usage

Change to the root of this repository:

```bash
cd 2019_logbook
```

Finally, run the Python script:

```bash
PYTHONPATH=. python paper/model_std/code/model.py
```

The script will create plots and summaries in `paper/model_std/model_info` directory.


## Input data

The input values are plotted on Figure 1.

<img src="paper/model_std/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances of RGB and AGB stars in NGC288 globular cluster.


## Model parameters

Summaries of parameter distributions are shown on Figure 2.

<img src="paper/model_std/images/compare_summaries.png" width="700" alt='Posterior distributions of parameters'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars. Error bars corresponds to 68% and 95% HPDIs (highest posterior density intervals).


### Text summary

The text version of model parameters is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPDIs.

|     |                        r |                      mu.1 |                      mu.2 |
|----:|-------------------------:|--------------------------:|--------------------------:|
| AGB | $`0.34^{+0.14}_{-0.12}`$ | $`-1.34^{+0.03}_{-0.05}`$ | $`-0.86^{+0.08}_{-0.08}`$ |
| RGB | $`0.50^{+0.14}_{-0.11}`$ | $`-1.15^{+0.05}_{-0.05}`$ | $`-0.65^{+0.06}_{-0.06}`$ |



### Posterior distribution

On Fig. 3 we plot posterior distributions from first 50 iterations.

<img src="paper/model_std/images/posterior.png" width="700" alt='Posterior distributions of parameters'>

Figure 3: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations.


### Detailed summaries

The detailed summaries of distributions of model parameters are shown in Tables 2 and 3. The summaries contain the following folumns:

*  **Name, Mean, Std** are the name of the parameter, its mean and standard deviation.

*  **68CI-, 68CI+, 95CI-, 95CI+** are the 68% and 95% HPDIs (highest probability density intervals).

* **Mode, +, -** is a mode of distribution with upper and lower uncertainties, which are calculated as distances to 68% HPDI. These are the values used in the paper.

* **N_Eff** is Stan's number of effective samples, the higher the better.

* **R_hat** is a Stan's parameter representing the quality of the sampling.

Table 2: Summaries of model parameters for AGB data.

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |   N_Eff |   R_hat |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|--------:|--------:|
| r      |   0.36 |  0.13 |   0.34 | 0.14 | 0.12 |    0.22 |    0.48 |    0.12 |    0.62 |    9182 |    1.00 |
| mu.1   |  -1.35 |  0.04 |  -1.34 | 0.03 | 0.05 |   -1.38 |   -1.30 |   -1.44 |   -1.27 |    6255 |    1.00 |
| mu.2   |  -0.86 |  0.08 |  -0.86 | 0.08 | 0.08 |   -0.94 |   -0.79 |   -1.03 |   -0.70 |    9269 |    1.00 |
| sigma  |   0.05 |  0.03 |   0.05 | 0.02 | 0.04 |    0.01 |    0.07 |    0.00 |    0.11 |    6195 |    1.00 |



Table 3: Summaries of model parameters for RGB data.

| Name   |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |   N_Eff |   R_hat |
|:-------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|--------:|--------:|
| r      |   0.51 |  0.12 |   0.50 | 0.14 | 0.11 |    0.39 |    0.64 |    0.26 |    0.74 |    9171 |    1.00 |
| mu.1   |  -1.15 |  0.05 |  -1.15 | 0.05 | 0.05 |   -1.20 |   -1.10 |   -1.25 |   -1.03 |    5253 |    1.00 |
| mu.2   |  -0.65 |  0.06 |  -0.65 | 0.06 | 0.06 |   -0.71 |   -0.59 |   -0.77 |   -0.52 |   12177 |    1.00 |
| sigma  |   0.08 |  0.03 |   0.08 | 0.03 | 0.02 |    0.05 |    0.10 |    0.04 |    0.14 |    7125 |    1.00 |



### AGB failure rate

The distribution of the "AGB failure rate" parameter is shown in Table 4. AGB failure rate f is proportion of RGB stars from sub-population 2 that would fail to reach AGB stage:

```math
f = 1 - \frac{r_{\text{AGB}}}{r_{\text{RGB}}}
```

Table 4: Summary of AGB failure rate parameter.

| Name             |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-----------------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| AGB failure rate |   0.25 |  0.33 |   0.41 | 0.24 | 0.36 |    0.05 |    0.65 |   -0.45 |    0.82 |





### Parameter differences between AGB/RGB stars

The distributions of parameter difference (RGB minus AGB) between AGB and RGB stars are shown in Table 5. These data are used to compare r, mu1 and mu2 parameters between AGB and RGB stars. For example, `Delta r = 0.15 (+0.19, -0.17)` means that the proportion of stars belonging to sub-population 2 is by `0.15 (+0.19, -0.17)` larger in RGB stars than in AGB stars.

Table 5: Summary of distributions of parameter difference: RGB minus AGB stars.


| Name       |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:-----------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| Delta r    |   0.14 |  0.18 |   0.15 | 0.19 | 0.17 |   -0.01 |    0.34 |   -0.22 |    0.48 |
| Delta mu 1 |   0.20 |  0.07 |   0.19 | 0.07 | 0.06 |    0.12 |    0.26 |    0.06 |    0.34 |
| Delta mu 2 |   0.21 |  0.10 |   0.21 | 0.11 | 0.09 |    0.12 |    0.31 |    0.01 |    0.42 |




#### Proportions

In Table 6 we can see the proportion of samples that have positive values of differences of parameters: RGB minus AGB. For example, `Delta r > 0 = 0.79` means that `79%` of RGB samples have higher values of parameter `r` than AGB samples. In other words, there is `0.79` probability that proportion of stars that belong to sub-population 2 is larger for RGB stars than for AGB stars.


Table 6: Number of samples and their proportions for positive values of parameter differences between AGB and RGB stars.

| Name              |    Value |
|:------------------|---------:|
| Number of samples | 16000.00 |
| Delta r > 0       |     0.79 |
| Delta mu 1 > 0    |     1.00 |
| Delta mu 2 > 0    |     0.98 |



## Special thanks

We would like to thank Richard McElreath, who wrote [Statistical Rethinking textbook](https://xcelab.net/rm/statistical-rethinking/) and taught us all we know about data analysis used here. In addition we would like to thank Aki Vehtari, Emir and Max Mantei from [Stan](https://discourse.mc-stan.org) forums, who helped us to write the Stan model code.

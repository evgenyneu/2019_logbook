data {
  int<lower=1> N;          // number of data points
  real y[N];               // observations
}
parameters {
  real<lower=0,upper=1> p;   // mixing proportion
  ordered[2] mu;             // locations of mixture components
  vector<lower=0>[2] sigma;  // spread of mixture components
}
model {
  sigma ~ exponential(1);
  mu ~ normal(9, 3);

  for (n in 1:N) {
    target += log_mix(p,
                      normal_lpdf(y[n] | mu[1], sigma[1]),
                      normal_lpdf(y[n] | mu[2], sigma[2]));
  }
}

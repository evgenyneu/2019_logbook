J/A+A/607/A135 NGC 104, 6121 & 6809 AGB and RGB stars Na abundance (Wang+, 2017)
================================================================================
Sodium abundances of AGB and RGB stars in Galactic globular clusters
II. Analysis and results of NGC 104, NGC 6121, and NGC 6809.
    Wang Y., Primas F., Charbonnel C., Van der Swaelmen M., Bono G.,
    Chantereau W., Zhao G.
    <Astron. Astrophys. 607, A135 (2017)>
    =2017A&A...607A.135W        (SIMBAD/NED BibCode)
================================================================================
ADC_Keywords: Clusters, globular ; Stars, giant ; Abundances ; Photometry ;
              Radial velocities
Keywords: stars: abundances - globular clusters: general -
          globular clusters: individual: NGC 104 -
          globular clusters: individual: NGC 6121 -
          globular clusters: individual: NGC 6809

Abstract:
    We analyze high-resolution spectra of a large sample of asymptotic
    giant branch AGB and red giant branch (RGB) stars in the Galactic GCs
    NGC 104, NGC 6121, and NGC 6809 obtained with FLAMES/GIRAFFE at
    ESO/VLT, and determine their Na abundances. This is the first time
    that the AGB stars in NGC 6809 are targeted. We find that NGC 104 and
    NG 6809 have comparable AGB and RGB Na abundance distributions, while
    NGC 6121 shows a lack of very Na-rich AGB stars. Moreover, to
    investigate the dependence of AGB Na abundance dispersion on GC
    parameters, we compare the AGB [Na/H] distributions of a total of nine
    GCs, with five determined by ourselves with homogeneous method and
    four from literature, covering a wide range of GC parameters. Their Na
    abundances and multiple populations of AGB stars form complex picture.
    In some GCs, AGB stars have similar Na abundances and/or
    second-population fractions as their RGB counterparts, while some GCs
    do not have Na-rich second-population AGB stars, and various cases
    exist between the two extremes. In addition, the fitted relations
    between fractions of the AGB second population and GC global
    parameters show that the AGB second-population fraction slightly
    anticorrelates with GC central concentration, while no robust
    dependency can be confirmed with other GC parameters.

Description:
    The spectra of our sample of AGB and RGB stars in the Galactic
    globular clusters NGC 104, NGC 6121 and NGC 6809 were obtained with
    the high-resolution multi-object spectrograph FLAMES, mounted on
    ESO/VLT-UT2, taking advantage of GIRAFFE for the majority of our
    sample stars and used the UVES fibres for the brightest objects of
    each cluster. The basic information of our sample stars are listed in
    Table 3, including the evolutionary phase, instrument used for
    observation, coordinates, photometry and barycentric radial velocity.
    Our Fe abundances were derived from the equivalent widths of Fe lines,
    while the Na abundances were determined with spectra synthesis. Both
    FeI and Na abundances have been corrected for the non-LTE effect. In
    Table 5 we show the derived stellar parameters of our sample stars,
    and the Na abundances are shown in Table 7.

File Summary:
--------------------------------------------------------------------------------
 FileName      Lrecl  Records   Explanations
--------------------------------------------------------------------------------
ReadMe            80        .   This file
table3.dat       176      282   Basic information of our sample stars
table5.dat       101      282   Stellar parameters of our sample stars
table7.dat        50      282   Na abundances of our sample stars
--------------------------------------------------------------------------------

See also:
   J/A+A/592/A66 : NGC 2808 AGB and RGB stars Na abundance (Wang+, 2016)

Byte-by-byte Description of file: table3.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label     Explanations
--------------------------------------------------------------------------------
   2-  5  A4    ---     NGC       Globular cluster identification
   9- 17  A9    ---     StarID    Star identification (1)
  22- 24  A3    ---     EvolPh    Evolutionary phase
  27- 38  A12   ---     Inst      Instrument used to obtain the spectrum
  43- 44  I2    h       RAh       Right Ascension J2000 (hours)
  46- 47  I2    min     RAm       Right Ascension J2000 (minutes)
  49- 53  F5.2  s       RAs       Right Ascension J2000 (seconds)
      56  A1    ---     DE-       Declination J2000 (sign)
  57- 58  I2    deg     DEd       Declination J2000 (degrees)
  60- 61  I2    arcmin  DEm       Declination J2000 (minutes)
  63- 67  F5.2  arcsec  DEs       Declination J2000 (seconds)
  72- 77  F6.3  mag     Bmag      Apparent B magnitude (2)
  80- 85  F6.4  mag   e_Bmag      Error of B magnitude (2)
  89- 94  F6.3  mag     Vmag      Apparent V magnitude (2)
  97-102  F6.4  mag   e_Vmag      Error of V magnitude (2)
 106-111  F6.3  mag     Imag      Apparent I magnitude (2)
 114-119  F6.4  mag   e_Imag      Error of I magnitude (2)
 123-128  F6.3  mag     Jmag      ? J magnitude (3)
 131-135  F5.3  mag   e_Jmag      ? Error of J magnitude (3)
 139-144  F6.3  mag     Hmag      ? H magnitude (3)
 147-151  F5.3  mag   e_Hmag      ? Error of H magnitude (3)
 155-160  F6.3  mag     Kmag      ? K magnitude (3)
 163-167  F5.3  mag   e_Kmag      ? Error of K magnitude (3)
 171-176  F6.2  km/s    RV        Barycentric radial velocity
--------------------------------------------------------------------------------
Note (1): The 'StarID' reports the original ID from the photometric catalogue,
 to which we added the suffix AGB/RGB to ease our own data handling.
Note (2): The optical photometry (B, V and I) were obtained from the
 Johnson-Morgan photometric database which is part of the project
 described in Stetson (2000PASP..112..925S, 2005PASP..117.1325S).
Note (3): The infrared J, H and K magnitudes were obtained from the 2MASS
 catalogue (Skrutskie et al., 2006, Cat. VII/233) by coordinates cross-matching.
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table5.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label        Explanations
--------------------------------------------------------------------------------
   2-  5  A4    ---     NGC          Globular cluster identification
   9- 17  A9    ---     StarID       Star identification
  21- 23  A3    ---     EvolPh       Evolutionary phase
  28- 31  I4    K       Teff         Effective temperature
  34- 38  F5.1  K     s_Teff         Scatter of the temperature (1)
  44- 47  F4.2  ---     logg         Surface gravity
  53- 56  F4.2  km/s    Vturb        Microturbulence velocity
  61- 65  F5.2  [-]     [FeI/H]LTE   LTE [FeI/H] ratio
  71- 74  F4.2  [-]   s_FeIline      Abundance dispersion of FeI lines
  79- 83  F5.2  [-]     [FeII/H]     Line ratio [FeII/H]
  89- 92  F4.2  [-]   s_FeIIline     Abundance dispersion of FeII lines
  97-101  F5.2  [-]     [FeI/H]NLTE  NLTE [FeI/H] ratio
--------------------------------------------------------------------------------
Note (1): The s_Teff ({sigma}_Teff_) is the scatter of the temperatures derived
  from the five colors we considered.
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table7.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label         Explanations
--------------------------------------------------------------------------------
   2-  5  A4    ---     NGC           Globular cluster identification
   9- 17  A9    ---     StarID        Star identification
  21- 23  A3    ---     EvolPh        Evolutionary phase
  28- 32  F5.2  [-]     [Na/H]LTE     ? LTE [Na/H] (1)
  37- 41  F5.2  [-]     [Na/H]NLTE    ? NLTE [Na/H] (1)
  46- 50  F5.2  [-]     [Na/FeI]NLTE  ? NLTE [Na/FeI] (1)
--------------------------------------------------------------------------------
Note (1): The abundance value is left blank for the stars whose Na line/lines
 are of poor quality or saturate, and no reliable abundance could be derived.
--------------------------------------------------------------------------------

Acknowledgements:
    Yue Wang, ywang(at)nao.cas.cn

References:
    Wang et al., Paper I    2016A&A...592A..66W, Cat. J/A+A/592/A66

================================================================================
(End)          Yue Wang [NAOC, China], Patricia Vannier [CDS]       26-Sep-2017

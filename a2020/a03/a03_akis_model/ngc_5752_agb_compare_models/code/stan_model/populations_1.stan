// Single Gaussian population model
data {
  int<lower=1> n;        // Number of data points
  real y[n];             // Observed values
  real uncertainties[n]; // Uncertainties
  real sigma_stdev_prior;// Prior for standard deviation of population spread
}
parameters {
  real mu;               // Population location
  real<lower=0> sigma;   // Population spread
}
model {
  // Set priors
  sigma ~ normal(0, sigma_stdev_prior);
  mu ~ normal(0, 1);

  // Loop through observed values and calculate their probabilities
  // for a Gaussian distribution, accounting for uncertainty of each measurment
  for (i in 1:n) {
    target += normal_lpdf(y[i] | mu, sqrt(sigma^2 + uncertainties[i]^2));
  }
}
generated quantities{
  vector[n] lpd_pointwise;

  for (i in 1:n) {
    lpd_pointwise[i] = normal_lpdf(y[i] | mu, sqrt(sigma^2 + uncertainties[i]^2));
  }
}

# Analysing data from MacLean et al. 2018, NGC 6121 (M4)

We ran the statistical model using data from [MacLean et al. 2018](https://arxiv.org/abs/1808.06735) for NGC 6121 (M4) globular cluster.

Code: [a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/code/model.py](a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/code/model.py)

Some stars had zero uncertainties for the abundances. We have replaced these uncertainties with `0.01`.


## Observations

<img src="a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/images/observed_combined.png" width="700" alt='Observed sodium abundances'>

Figure 1: Observed sodium abundances.


## Results


### Parameter distributions

<img src="a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/images/compare_summaries.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars.


### Model parameter values

The numerical version of summary from Figure 2 is shown in Table 1.

Table 1: Summary of posterior distributions of model parameters for AGB and RGB stars. The values are the modes of the distributions and uncertainties are distances from the modes to the 68% HPD intervals.

|     |                        r |                mu.1 (std) |                mu.2 (std) |
|----:|-------------------------:|--------------------------:|--------------------------:|
| AGB | $`0.65^{+0.12}_{-0.14}`$ | $`-1.75^{+0.19}_{-0.13}`$ | $`-0.52^{+0.10}_{-0.10}`$ |
| RGB | $`0.58^{+0.06}_{-0.05}`$ | $`-0.85^{+0.10}_{-0.08}`$ |  $`0.83^{+0.06}_{-0.09}`$ |


### AGB failure rate

We calculated failure rate f=$`-0.1^{+0.3}_{-0.2}`$, which is defined as proportion of SP2 stars that fail to reach AGB:

```
f = 1 - r_agb / r_rgb
```



## Analysis

### RGB stars

Distribution of $`r_{rgb}`$ agrees with $`r_{rgb} \sim 0.55`$ estimated by [MacLean et al. 2018](https://arxiv.org/abs/1808.06735).

### AGB stars

Distribution of $`r_{agb}`$ **disagrees** with  $`r_{agb} \lt 0.2`$ calculated by [MacLean et al. 2018](https://arxiv.org/abs/1808.06735). The data suggest presence of two distinct populations.


### AGB failure rate

The distribution of `f` is shown on Fig. 5 is bimodal. Our point estimate **disagrees** with $`f \gtrsim 0.65`$ calculated by [MacLean et al. 2018](https://arxiv.org/abs/1808.06735).

<img src="a2020/a02/a12_sodium_in_literature/a2018_11_MacLean_NGC_6121_M4/images/agb_failure_rate.png" width="700" alt='AGB failure rate'>

Figure 5: Distribution of AGB failure rate.

## Save proper motions to star table

Code: [a2019/a12/a06_proper_motion/code/add_proper_motions_to_stars.py](a2019/a12/a06_proper_motion/code/add_proper_motions_to_stars.py)

Output: [a2019/a12/a06_proper_motion/data/ngc288_stars_with_proper_motions.csv](a2019/a12/a06_proper_motion/data/ngc288_stars_with_proper_motions.csv)


## Calculate proper motion speed relative to the mean

We add column 'pm_speed_from_mean' to the list of stars that is the proper speed of the star relative to the mean proper speed of the selected AGB/RGB stars:


* $`\mu_{\alpha} \cos \delta = 4.19 \pm 0.07`$ mas/a

* $`\mu_{\delta} = -5.67 \pm 0.06`$ mas/a

The values agree with [Gaia Collaboration, et al.](https://arxiv.org/abs/1804.09381): $`\mu_{\alpha} \cos \delta = 4.2385 `$ mas/a, $`\mu_{\delta} = -5.6470`$ mas/a.


Code: [a2019/a12/a06_proper_motion/code/comoving.py](a2019/a12/a06_proper_motion/code/comoving.py)

Output: [a2019/a12/a06_proper_motion/data/ngc288_stars_with_proper_motions_is_comoving.csv](a2019/a12/a06_proper_motion/data/ngc288_stars_with_proper_motions_is_comoving.csv)



## Make proper motion plot with a circle

Code: [a2019/a12/a06_proper_motion/code/proper_motion_with_circle.py](a2019/a12/a06_proper_motion/code/proper_motion_with_circle.py)

<img src="a2019/a12/a06_proper_motion/code/plots/proper_motion_with_circle_3.png" width="700" alt='Proper motions of observed stars in NGC288'>

Figure 1: Proper motions of observed NGC288 stars ([Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G), robust fit). The circle marks the stars that move with a speed smaller than 0.25 mas/a relative to the mean of the selected RGB/AGB stars. The error bars are drawn from the maximum uncertainties for the selected RGB/AGB stars. The "Excluded" stars were not used in analysis.


## Plot removed outliers

Code: [a2019/a12/a06_proper_motion/code/plot_excluded_stars.py](a2019/a12/a06_proper_motion/code/plot_excluded_stars.py)

<img src="a2019/a12/a06_proper_motion/code/plots/plot_excluded_stars.png" width="700" alt='Proper motions of observed stars in NGC288 with two removed outliers'>

Figure 2: Proper motions of observed NGC288 stars ([Gaia DR2](https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G), robust fit) with two removed outliers. The error bars are not visible at this scale.

### Notes

We can see from Fig. 2 that the two removed outliers are not moving with the same proper motions velocity as the stars selected for analysis.



## Color magnitude diagram of co-moving (in proper motion) stars


### In B-K color

Code: [a2019/a12/a06_proper_motion/code/cmd_comoving.py](a2019/a12/a06_proper_motion/code/cmd_comoving.py)


<img src="a2019/a12/a06_proper_motion/code/plots/cmd_comoving.png" width="700" alt='Colour magnitude diagram fo comoving stars in NGC288'>

Figure 3: Colour magnitude diagram of observed stars in NGC288 (B magnitudes from [Yazan Momany](a2019/a11/a29_get_2mass_yazan), K magnitudes from [Cutri+ 2003](https://ui.adsabs.harvard.edu/abs/2003yCat.2246....0C)) that have proper speeds within 0.25 mas/a from the mean of the selected AGB/RGB stars. The error bars are drawn from the maximum uncertainties. The "Excluded" stars were not used in analysis. No extinction correction applied.


### In B-V color

Code: [a2019/a12/a06_proper_motion/code/cmd_comoving_b_minus_v.py](a2019/a12/a06_proper_motion/code/cmd_comoving_b_minus_v.py)


<img src="a2019/a12/a06_proper_motion/code/plots/cmd_comoving_b_minus_v.png" width="700" alt='Colour magnitude diagram fo comoving stars in NGC288 in B-V color'>

Figure 3: Colour magnitude diagram of observed stars in NGC288 ([Yazan Momany](a2019/a11/a29_get_2mass_yazan)) that have proper speeds within 0.25 mas/a from the mean of the selected AGB/RGB stars. The error bars are drawn from the maximum uncertainties. The "Excluded" stars were not used in analysis. No extinction correction applied.


### Missing stars

We can see from Figures 2 and 3 that there are three co-moving stars in the AGB/RGB region that were not included in analysis (shown as small black dots).

#### Missing AGB stars

BAGBYAZ018557, AGBSYAZ025015 and AGBSYAZ022288.

These stars were not observed (missing in the FITS files).



Code: [a2019/a12/a06_proper_motion/code/cmd_comoving_star_names.py](a2019/a12/a06_proper_motion/code/cmd_comoving_star_names.py)

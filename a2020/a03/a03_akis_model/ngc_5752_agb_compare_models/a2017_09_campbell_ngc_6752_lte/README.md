# Compare single vs two-gaussian models using data from Campbell et al. 2017 LTE AGB

Code: [a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte/code/compare.py](a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte/code/compare.py)

## Posterior distributions

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte/images/posterior_one.png" width="500" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions for single gaussian model. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions from first 50 iterations.

<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte/images/posterior_two.png" width="500" alt='Parameter distributions'>

Figure 2: Observed and posterior distributions for model that describes a mixture of two gaussians.

## Comparison results


<img src="a2020/a03/a03_akis_model/ngc_5752_agb_compare_models/a2017_09_campbell_ngc_6752_lte/images/compare_psis.png" width="700" alt='PSIS'>

Figure 3: Comparison of two models with PSIS method.

|                 |   PSIS |   SE |   dPSIS |   dSE |   pPSIS |   MaxK |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|-------:|---------:|
| Two populations |  55.85 | 5.45 |         |       |    3.53 |   0.62 |     0.67 |
| One population  |  57.76 | 5.27 |    1.91 |  3.46 |    1.68 |   0.25 |     0.33 |


|                 |   WAIC |   SE |   dWAIC |   dSE |   pWAIC |   Weight |
|:----------------|-------:|-----:|--------:|------:|--------:|---------:|
| Two populations |  55.73 | 5.45 |         |       |    3.47 |     0.67 |
| One population  |  57.72 | 5.26 |    1.99 |  3.45 |    1.66 |     0.33 |


## Conclusion

Both models are equally compatible with the data.

# Combining star data into a since CSV file

import pandas as pd
import os


def make_star_id(type, catalog, id):
    """
    Create an ID for the star that we are using internally

    Parameters
    ----------

    type : str
        Type of the star.

    catalog : str
        A catalog the star data came from

    id : int
        ID of the star in its original catalog


    Returns : str
    --------

    ID of the star for intenal usage
    """

    return f'{type}{catalog}{id:06d}'


def combine():
    """
    Combine star data into one CSV file
    """

    # Open photometry data
    original_dir = 'a2019/a11/a28_organizing_our_data/original_data_from_chris/'
    raw_path = os.path.join(original_dir, 'rawphotometry.csv')
    photometry_df = pd.read_csv(raw_path)

    # Open final sample photometry data
    raw_path = os.path.join(original_dir, 'finalsample_photometry_velocities_abundances.csv')
    final_df = pd.read_csv(raw_path)

    # Open velcities data
    velocities_path = os.path.join(original_dir, 'rawvelocities.csv')
    velocities_df = pd.read_csv(velocities_path)

    # Add new columns to photometry
    photometry_df.insert(loc=0, column='id', value='')
    photometry_df.insert(loc=1, column='selected', value=0)

    abundance_column_names = ['FeI/H', 'u FeI/H', 'FeII/H', 'u FeII/H']

    for column_name in abundance_column_names:
        photometry_df[column_name] = ''

    velocity_columns = ['vel_rad', 'u_vel_rad']

    for column_name in velocity_columns:
        photometry_df[column_name] = ''

    # Go over all rows in final data
    for index, star in photometry_df.iterrows():
        type = star['TYPE_tab1']
        catalog = star['Cat']
        id_phot = star['ID_Phot']

        # Make star ID
        star_id = make_star_id(type=type, catalog=catalog, id=id_phot)

        # Asseing the star ID
        photometry_df.loc[index, 'id'] = star_id

        # Locate the star in the photometry data
        selected_star = final_df.loc[
            (final_df['TYPE_tab1'] == type) &
            (final_df['Cat'] == catalog) &
            (final_df['ID_Phot'] == id_phot)]

        if selected_star.shape[0] > 1:
            raise ValueError(f'Too many selected stars {star_id}')

        # The star is selected
        if selected_star.shape[0] == 1:
            # Mark the star as selected
            photometry_df.loc[index, 'selected'] = 1

            # Copy abundance columns
            for column_name in abundance_column_names:
                value = selected_star[column_name].values[0]
                photometry_df.loc[index, column_name] = value

        # Locate the star in the velocities data
        velocities_star = velocities_df.loc[
            (velocities_df['Star Name'] == star_id)]

        if velocities_star.shape[0] == 1:
            # Copy velocity columns
            value = velocities_star['average vel corr'].values[0]
            photometry_df.loc[index, 'vel_rad'] = value

            value = velocities_star['aver uvel'].values[0]
            photometry_df.loc[index, 'u_vel_rad'] = value

        if velocities_star.shape[0] > 1:
            print(f"Too many velocities for {star_id}")

    # Save combined original_data_from_chris
    dest_dir = 'a2019/a11/a28_organizing_our_data/'
    output_path = os.path.join(dest_dir, 'ngc288_stars.csv')
    photometry_df.to_csv(output_path, index=False)
    print(f'Successufully combined data to {output_path}')


if __name__ == '__main__':
    combine()

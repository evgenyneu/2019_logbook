# Meeting with Simon, Chris and Lorenzo

* Check 'quality number' in Daospec output (Lorenzo).

* Avoid measuring deep lines, since their shapes are not exactly Gaussian.

* Measuring for single lines: manually measure multiple times and find standard deviation.

* Plot NGC288 stars on color-magnitude diagram, try different color to distinguish ABG and RGB stars. Use [VizieR](https://vizier.u-strasbg.fr/viz-bin/VizieR) to find data. Example: [Stetson's photometry data](http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=J/MNRAS/485/3042/table2&-out.max=50&-out.form=HTML%20Table&-oc.form=sexa).

* Calculate uncertainties for R and F parameters (AGB avoidance) using [this](https://stats.stackexchange.com/questions/238571/taking-into-account-the-uncertainty-of-p-when-estimating-the-mean-of-a-binomial).

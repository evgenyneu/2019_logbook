# Use Gaussian mixture model to find out
# proportion of low sodium AGB/RGB star in NGC 288
# --------------

library(rstan)
library(rethinking)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

# Fit the model to the data
#
# Parameters
# -----------
#
# data : the input data frame
#
run_model <- function(data) {
    print("-------------------")
    print("Fitting stars data")
    print("-------------------")

    # Standardize the sodium abundance
    # --------

    d_st = list(
        y = standardize(data$na_over_h),
        uncertainties = data$uncertainty / sd(data$na_over_h),
        N=length(data$na_over_h)
    )

    # Run Stan
    model_path = 'a2020/a01/a02_testing_the_model/code/calculate_sodium_ratio.stan'

    fit <- stan(file=model_path, data=d_st, iter=15000, warmup=3000, chains=1,
                control=list(adapt_delta=0.99, max_treedepth=10),
                seed=11421)

    print(fit)
    traceplot(fit, par=c('p', 'mu'))
    samples = extract(fit)

    # De-standardise the variables
    # ------------

    d_destand = list(
        p = samples$p,
        mu1 = samples$mu[,1] * sd(data$na_over_h) + mean(data$na_over_h),
        mu2 = samples$mu[,2] * sd(data$na_over_h) + mean(data$na_over_h),
        sigma1 = samples$sigma[,1] * sd(data$na_over_h),
        sigma2 = samples$sigma[,2] * sd(data$na_over_h)
    )

    d_destand
}

import os

from a2020.a02.a21_fix_uncertainties.code.run_model import (
    do_analysis, load_data, AnalysisSettings)


def do_work():
    """Main function that does all the work"""

    settings = AnalysisSettings(
        path="a2020/a02/a21_fix_uncertainties/a2020_01_NGC_288",
        study_name="work in progress NGC 288 2020",
        xlabel="Sodium abundance [Na/H]"
    )

    data_path = os.path.join(settings.path, "data/sodium.csv")
    type_name = "type"
    abundance_name = "abundance"
    uncertainty_name = "uncertainty"

    settings.values_agb, settings.uncert_agb = load_data(
        'AGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    settings.values_rgb, settings.uncert_rgb = load_data(
        'RGB', data_path=data_path,
        type_name=type_name,
        abundance_name=abundance_name,
        uncertainty_name=uncertainty_name)

    do_analysis(settings=settings)


if __name__ == '__main__':
    print("Running the models...")
    do_work()
    print('We are done')

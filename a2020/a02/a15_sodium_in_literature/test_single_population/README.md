# Testing the model with one population

We test the model by simulating single population of AGB and RGB stars. Results are shown on Figures 1 and 2.

Code: [a2020/a02/a15_sodium_in_literature/test_single_population/code/model.py](a2020/a02/a15_sodium_in_literature/test_single_population/code/model.py)


<img src="a2020/a02/a15_sodium_in_literature/test_single_population/images/posterior.png" width="700" alt='Parameter distributions'>

Figure 1: Observed and posterior distributions. Thick lines on the bottom plot correspond to observations, thinner lines are posterior distributions. Dashed vertical lines correspond to the true locations of the populations' mu values.


<img src="a2020/a02/a15_sodium_in_literature/test_single_population/images/compare_with_exact.png" width="700" alt='Parameter distributions'>

Figure 2: Posterior distributions of model parameters for RGB and AGB stars, compared with exact values.


### Summary of model parameters

Table 1: Summaries of distributions of model parameters and their simulated true values.

|           |                        r |                      mu.1 |                      mu.2 |                    sigma |
|----------:|-------------------------:|--------------------------:|--------------------------:|-------------------------:|
|       AGB | $`0.27^{+0.16}_{-0.12}`$ | $`-1.31^{+0.02}_{-0.03}`$ | $`-1.02^{+0.05}_{-0.05}`$ | $`0.06^{+0.01}_{-0.01}`$ |
|       RGB | $`0.25^{+0.16}_{-0.11}`$ | $`-1.08^{+0.01}_{-0.02}`$ | $`-0.90^{+0.04}_{-0.03}`$ | $`0.05^{+0.01}_{-0.00}`$ |
| AGB exact |                          |                 $`-1.30`$ |                           |                 $`0.05`$ |
| RGB exact |                          |                 $`-1.00`$ |                           |                 $`0.05`$ |

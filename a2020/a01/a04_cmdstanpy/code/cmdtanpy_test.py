import os
from cmdstanpy import cmdstan_path, CmdStanModel
import pandas as pd

# Set model
bernoulli_stan = os.path.join(cmdstan_path(), 'examples', 'bernoulli', 'bernoulli.stan')
bernoulli_model = CmdStanModel(stan_file=bernoulli_stan)

print(bernoulli_model.name)
print(bernoulli_model.stan_file)
print(bernoulli_model.exe_file)
print(bernoulli_model.code())

# Run Stan
bernoulli_data = { "N" : 10, "y" : [0,1,0,0,0,0,0,0,0,1] }
bern_fit = bernoulli_model.sample(data=bernoulli_data)

# Get samples
print(bern_fit.sample.shape)
print(bern_fit.get_drawset(params=['theta'])) # From all chains

# Or from single chain
chain_1 = bern_fit.sample[:,0,:]
print(chain_1)

# Print diagnostic
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(bern_fit.summary())
    print(bern_fit.diagnose())


# Save CSV files
# -----------

# stan_output_dir = 'a2020/a01/a04_cmdstanpy'
# bern_fit.save_csvfiles(dir=stan_output_dir)

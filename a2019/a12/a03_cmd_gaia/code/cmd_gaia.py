import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a03_cmd_gaia/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    # Error bars
    # --------------

    all_selected_df = df

    uncertainty_u_bp = all_selected_df['e_BPmag']

    uncertainty_u_bp_minus_rp = np.sqrt(
        all_selected_df['e_BPmag']**2 + all_selected_df['e_RPmag']**2)

    uncertainty_x = uncertainty_u_bp.max()
    uncertainty_y = uncertainty_u_bp_minus_rp.max()

    print(f"Y error: mean u(BP) = {uncertainty_x}")
    print(f"X error: mean u(BP-RP) = {uncertainty_y}")

    plt.errorbar([1.65], [15.8],
                 xerr=[uncertainty_x], yerr=[uncertainty_y],
                 fmt='none', color='black', capsize=2,
                 ecolor='black', elinewidth=1, zorder=-1)

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    # stars_other_df = df
    selected_other_x = stars_other_df['BP-RP']
    selected_other_y = stars_other_df['BPmag']

    plt.scatter(selected_other_x, selected_other_y,
                c='black', marker='.', s=3, edgecolors='black',
                label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    selected_agb_x = stars_agb_df['BP-RP']
    selected_agb_y = stars_agb_df['BPmag']

    plt.scatter(selected_agb_x, selected_agb_y,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    selected_rgb_x = stars_rgb_df['BP-RP']
    selected_rgb_y = stars_rgb_df['BPmag']

    plt.scatter(selected_rgb_x, selected_rgb_y,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    plt.xlim(0.85, 1.75)
    plt.ylim(12.5)
    ax = plt.gca()
    ax.invert_yaxis()
    plt.legend(loc="upper left")
    plt.xlabel('BP-RP color')
    plt.ylabel('BP magnitude')
    plt.title('NGC288 stars in Gaia DR2 photometry')
    save_plot(plt=plt)
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    data_dir = 'a2019/a12/a02_plot_cmd_proper_motion/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')


    columns = ['pmRA', 'pmDE', 'e_pmRA', 'e_pmDE']

    for column in columns:
        df = df[df[column] != ' ']  # Remove rows with missing uncertainties
        df[column] = pd.to_numeric(df[column])

    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    pm_ra = stars_other_df['pmRA']
    pm_de = stars_other_df['pmDE']

    plt.scatter(x=pm_ra, y=pm_de,
                c='black', marker='.', s=3, edgecolors='black',
                label='Excluded')


    # Error bars
    # --------------

    selected_df = df.loc[(df['selected'] == 1)]

    pm_ra = selected_df['pmRA']
    pm_de = selected_df['pmDE']

    u_pm_ra = selected_df['e_pmRA']
    u_pm_de = selected_df['e_pmDE']

    max_u_ra = u_pm_ra.max()
    max_u_de = u_pm_de.max()
    print(f"X error: mean u(pmRA) = {max_u_ra}")
    print(f"Y error: mean u(pmDE) = {max_u_de}")

    plt.errorbar(x=[4.323], y=[-5.84],
                 xerr=[max_u_ra], yerr=[max_u_de],
                 fmt='none', color='black', capsize=5,
                 ecolor='black', elinewidth=1, zorder=-1)

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    pm_ra = stars_agb_df['pmRA']
    pm_de = stars_agb_df['pmDE']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    pm_ra = stars_rgb_df['pmRA']
    pm_de = stars_rgb_df['pmDE']

    plt.scatter(x=pm_ra, y=pm_de,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    plt.xlim(4, 4.4)
    plt.ylim(-5.9, -5.4)

    plt.legend()
    plt.xlabel(r'Proper motion in right ascension $\mu_\alpha \ \cos \delta$ [mas $a^{-1}$]')
    plt.ylabel('Proper motion in declination $\mu_\delta$ [mas $a^{-1}$]')
    plt.title('Proper motions of all observed stars in NGC288')
    save_plot(plt=plt, suffix=f'max_selected_error_bars')
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

import pandas as pd
import os
import numpy as np


def comoving():
    """
    Select stars that have similar proper motions
    """

    data_dir = 'a2019/a12/a06_proper_motion/data/'

    # Open photometry data
    stars_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions.csv')
    df = pd.read_csv(stars_path)

    df = df[df.pm_ra != ' ']  # Remove rows with missing uncertainties
    df.pm_ra = pd.to_numeric(df.pm_ra)
    df.pm_de = pd.to_numeric(df.pm_de)

    # Calculate the mean proper velocities of selected stars
    selected_df = df.loc[df['selected'] == 1]
    pm_ra_mean = selected_df['pm_ra'].mean()
    pm_ra_std = selected_df['pm_ra'].std()
    print(f'Proper motion RA={pm_ra_mean:.3f}±{pm_ra_std:.3f}')

    pm_de_mean = selected_df['pm_de'].mean()
    pm_de_std = selected_df['pm_de'].std()
    print(f'Proper motion DE={pm_de_mean:.3f}±{pm_de_std:.3f}')

    # Mark comoving stars: stars that are within the circle of `radius`
    # --------------

    df = pd.read_csv(stars_path)
    df.insert(loc=len(df.columns), column='pm_speed_from_mean', value='')

    # Go over rows
    for index, star in df.iterrows():
        pm_ra = star['pm_ra']

        if pm_ra == " ":
            continue

        pm_ra = pd.to_numeric(star['pm_ra'])
        pm_de = pd.to_numeric(star['pm_de'])

        # Calcualate the speed 'distance' from center of the circle
        # (mean speed of selected stars)
        distance = np.sqrt((pm_ra - pm_ra_mean)**2 + (pm_de - pm_de_mean)**2)
        df.loc[index, 'pm_speed_from_mean'] = distance

    # Save data frame
    output_path = os.path.join(data_dir, 'ngc288_stars_with_proper_motions_is_comoving.csv')
    df.to_csv(output_path, index=False)
    print(f'Successufully save to {output_path}')


if __name__ == '__main__':
    comoving()
    print('We are done!')

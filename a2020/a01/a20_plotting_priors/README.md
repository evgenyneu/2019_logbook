# Plotting priors

We want to plot prior distribution for the model to see if they are sensible.

## Plotting all priors

On Fig. 1 we plot multiple prior distributions of sodium abundances. We can see that most distributions look flat, we can try model with more constrained spreads, to make most values between -2 and 0.

Code: [a2020/a01/a20_plotting_priors/code/priors.py](a2020/a01/a20_plotting_priors/code/priors.py)

<img src="a2020/a01/a20_plotting_priors/code/plots/priors.png" width="700" alt='Prior distribution'>

Figure 1: Prior distributions of sodium abundances.


## Plotting individual priors

On Fig. 2 we plot some individual prior distributions.

Code: [a2020/a01/a20_plotting_priors/code/priors_separate.py](a2020/a01/a20_plotting_priors/code/priors_separate.py)

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__00.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__01.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__02.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__03.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__04.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__05.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__06.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__07.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__08.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__09.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__10.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__11.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__12.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__13.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__14.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__15.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__16.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__17.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__18.png" width="400" alt='Prior distribution'>

<img src="a2020/a01/a20_plotting_priors/code/plots/priors_separate__19.png" width="400" alt='Prior distribution'>


Figure 2: Prior distributions of sodium abundances. The dotted and dashed lines show distributions of two populations, and the solid lines show the combined distribution mixed with parameter p.


### Things to note on Fig. 2

* Some distributions do not constrain the spreads of population very much. For example, in [distribution 4])(https://gitlab.com/evgenyneu/2019_logbook/raw/master/a2020/a01/a20_plotting_priors/code/plots/priors_separate__03.png), first population has large spread, that make it considerably overlap with the second population. Therefore, instead of lognormal(0, 1) we can try more limiting prior, exponential, or normal with small, sigma, for example. We can try standard deviation of 0.1 in [Na/H] units (or 0.3 standardised).

* In [distribution 4])(https://gitlab.com/evgenyneu/2019_logbook/raw/master/a2020/a01/a20_plotting_priors/code/plots/priors_separate__03.png), the spread of the first population is about 10 times larger than that of the second population. However, I would expect the spreads of two population to be similar. Therefore, we can try a model with identical values of sigmas for both populations.

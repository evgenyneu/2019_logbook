library(rstan)
library(rethinking)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

set.seed(42)

# Simulate observations of 5 two populations of stars:
# 5 sodium-poor and 10 sodium-rich stars
# ----------------

n_poor = 5 # Number of Sodium poor stars
n_rich = 10 # Number of Sodium rich stars
n = n_poor + n_rich # Total sample size of observed stars

# Generate random values from Normal distributions
mu_poor = 8 # Poor stars have values around 8
mu_rich = 10 # Rich stars have values around 10
samples_poor = rnorm(n_poor, mu_poor, 0.2)
samples_rich = rnorm(n_rich, mu_rich, 0.2)

# Combine all samples in one list
values_true = sample(c(samples_poor, samples_rich))

# Generate uncertainties for all observations
uncertainties = abs(rnorm(n, 0.3, 0.1))

# Generate observed values from true using the uncertainties
values_observed = rnorm(n, values_true, uncertainties)

# Plot the true and observed values
plot(values_true, xlim=c(7, 11), ylim=c(1, n+2), 1:n, cex=2, pch=16, col='#0000FF88')
points(values_observed, 1:n, cex=2)
abline(v=c(mu_poor, mu_rich), lty=2, col='#0000FF88')

legend("top", pch = c(16, 1),
        pt.cex=c(2, 2),
        col = c('#0000FF88', "black"),
        legend = c("True", "Observed"))

for (i in 1:n) {
    u = uncertainties[i]
    lines(c(values_observed[i] - u, values_observed[i] + u), c(i, i))
}

# Make a density plot
plot(density(values_observed))


data = list(
    y = values_observed,
    uncertainties = uncertainties,
    N=length(values_observed)
)

# Run Stan
model_path = 'a2019/a12/a30_use_data_with_uncertainties/with_uncertainties.stan'
fit <- stan(file=model_path, data=data, iter=1000, control=list(adapt_delta=0.95))

print(fit)
# traceplot(fit)

# Plot observed and estimates true values
# ----------------------------

post <- extract(fit)
# precis(post, depth=2)

post.y_true.mean = apply(post$y_true, 2, mean)

# Plot the true and observed values
plot(values_true, xlim=c(7, 11), ylim=c(1, n+2), 1:n, cex=2, pch=16, col='#0000FF88')
points(values_observed, 1:n, cex=2)
points(post.y_true.mean, 1:n, cex=2, pch=18, col='#FF00FF88')
abline(v=c(mu_poor, mu_rich), lty=2, col='#0000FF88')

legend("top", pch = c(16, 1, 18),
        pt.cex=c(2, 2, 2),
        col = c('#0000FF88', "black", '#FF00FF88'),
        legend = c("True", "Observed", "Predicted"))

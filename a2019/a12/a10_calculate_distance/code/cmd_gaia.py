import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from code_dope.python.plot.plot import save_plot


def plot():
    plt.figure(figsize=(4, 3))
    data_dir = 'a2019/a12/a10_calculate_distance/data'

    # Open stars data
    stars_path = os.path.join(data_dir, 'ngc288_stars.csv')
    df = pd.read_csv(stars_path)

    # Open Gaia data
    stars_path = os.path.join(data_dir, 'ngc288_gaia.csv')
    gaia_df = pd.read_csv(stars_path)

    df = pd.merge(df, gaia_df, how='inner',
                  left_on='id_gaia_dr2', right_on='Source',
                  suffixes=('_l', '_r'),
                  validate='one_to_one')

    # Show only stars with similar proper velocities
    # i.e. within 0.25 mas/a from the mean of the selected stars
    df = df.loc[df['pm_speed_from_mean'] < 0.25]


    # Excluded
    # ---------------

    stars_other_df = df.loc[df['selected'] == 0]
    # stars_other_df = df
    selected_other_x = stars_other_df['BP-RP']
    selected_other_y = stars_other_df['Gmag']

    plt.scatter(selected_other_x, selected_other_y,
                c='gray', marker='o', s=7, edgecolors='gray',
                label='Excluded')

    # Selected AGB
    # ---------------

    stars_agb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'AGBS') |
            (df['type'] == 'BAGS') |
            (df['type'] == 'BAGB')
        )
    ]

    selected_agb_x = stars_agb_df['BP-RP']
    selected_agb_y = stars_agb_df['Gmag']

    plt.scatter(selected_agb_x, selected_agb_y,
                c='#7181ff', marker='^', edgecolors='black',
                linewidth=1, label='AGB')

    # Selected RGB
    # ---------------

    stars_rgb_df = df.loc[
        (df['selected'] == 1) &
        (
            (df['type'] == 'RGBS') |
            (df['type'] == 'BGBS')
        )
    ]

    selected_rgb_x = stars_rgb_df['BP-RP']
    selected_rgb_y = stars_rgb_df['Gmag']

    plt.scatter(selected_rgb_x, selected_rgb_y,
                c='#ff5f4c', marker='o', edgecolors='black',
                linewidth=1, label='RGB')

    # plt.xlim(0.85, 1.75)
    # plt.ylim(12.5)

    # Error bars
    # --------------

    all_selected_df = df

    uncertainty_u_g = all_selected_df['e_Gmag']

    uncertainty_u_bp_minus_rp = np.sqrt(
        all_selected_df['e_BPmag']**2 + all_selected_df['e_RPmag']**2)

    uncertainty_x = uncertainty_u_bp_minus_rp.median()
    uncertainty_y = uncertainty_u_g.median()

    x_margin = 0.05 * abs(plt.xlim()[1] - plt.xlim()[0])
    y_margin = 0.05 * abs(plt.ylim()[1] - plt.ylim()[0])
    x_error_bar = plt.xlim()[1] - uncertainty_x - x_margin
    y_error_bar = plt.ylim()[1] - uncertainty_y - y_margin

    plt.errorbar([x_error_bar], [y_error_bar],
                 xerr=[uncertainty_x], yerr=[uncertainty_y],
                 fmt='none', color='black', capsize=0.5,
                 ecolor='black', elinewidth=1, zorder=1)

    ax = plt.gca()
    ax.invert_yaxis()
    ax.set_axisbelow(True)
    plt.grid(color='#cccccc', linestyle='dashed')
    plt.legend(loc="upper left")
    plt.xlabel('$G_{BP}-G_{RP}$ color')
    plt.ylabel('G magnitude')
    plt.tight_layout(pad=0.05)
    save_plot(plt=plt, extensions=['png', 'pdf'])
    plt.show()


if __name__ == '__main__':
    plot()
    print('We are done')

import pandas as pd
from cmdstanpy import CmdStanModel
from tabulate import tabulate
import scipy.stats as stats
from tarpan.cmdstanpy.analyse import save_analysis
from tarpan.cmdstanpy.tree_plot import save_tree_plot
from tarpan.cmdstanpy.cache import run
from tarpan.cmdstanpy.compare_parameters import save_compare_parameters
from tarpan.shared.compare_parameters import CompareParametersType
from tarpan.shared.info_path import InfoPath, get_info_path
from tarpan.shared.tree_plot import TreePlotParams
from tarpan.shared.summary import save_summary
from tarpan.shared.histogram import save_histogram
from tarpan.shared.tree_plot import save_tree_plot as shared_save_tree_plot
from tarpan.plot.kde import save_scatter_and_kde
from tarpan.plot.posterior import save_posterior_scatter_and_kde


COLOR_AGB = '#7181ff'
COLOR_AGB_EDGE = '#373e7c'
COLOR_RGB = '#ff5f4c'
COLOR_RGB_EDGE = '#7b2d24'


def load_data(type, data_path, type_name, abundance_name, uncertainty_name):
    """
    Load sodium abundaces and their uncertainties

    Parameters
    ----------
    type : str
        Star type: RGB / RGB

    data_path : str
        Path to the CSV file.

    type_name, abundance_name, uncertainty_name : str
        Names of the columns in the CSV file

    Returns
    -------
    numpy.ndarray:
        Observed sodium abundance values.
    numpy.ndarray:
        Corresponding uncertainties for the observed values.
    """
    df = pd.read_csv(data_path, skipinitialspace=True)
    df = df[df[type_name] == type]

    return (df[abundance_name].to_numpy(), df[uncertainty_name].to_numpy())


def run_stan(observed_values, uncertainties, mu_prior, sigma_prior,
             output_dir, info_path2):
    """
    Run Stan model and return the samples from posterior distributions.

    Parameters
    ----------
    observed_values: list
        Observed sodium abundance values.
    uncertainties: list
        Corresponding uncertainties for the observed values.
    mu_prior, sigma_prior : float
        Priors for the mean and standard deviations of two populations.

    Returns
    -------
    cmdstanpy.CmdStanMCMC
        Stan's output containing samples of posterior distribution
        of parameters.
    """

    data = {
        "y": observed_values,
        "uncertainties": uncertainties,
        "N": len(observed_values),
        "mu_prior": mu_prior,
        "sigma_prior": sigma_prior
    }

    model = CmdStanModel(stan_file="paper/model/code/stan_model/model.stan")

    fit = model.sample(
        data=data, seed=333,
        adapt_delta=0.99, max_treedepth=10,
        sampling_iters=4000, warmup_iters=1000,
        chains=4, cores=4,
        show_progress=True,
        output_dir=output_dir)

    # Make summaries and plots of parameter distributions
    save_analysis(fit, param_names=["r", "mu", "sigma"], info_path=info_path2)

    return fit


def run_model(star_type, observed_values, uncertainties, mu_prior, sigma_prior,
              path, dir_name):
    """
    Run Stan to generate samples for the model and make plots
    and summaries of the results.

    Parameters
    ----------
    star_type: str
        Stat type: 'RGB' or 'AGB'

    observed_values : float
    uncertainties : float
        The values and corresponding uncertainties.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.

    mu_prior, sigma_prior : float
        Priors for the mean and standard deviations of two populations.

    Returns
    -------
    cmdstanpy.CmdStanMCMC
        Stan's output containing samples of posterior distribution
        of parameters.
    """
    info_path = InfoPath(path=path,
                         dir_name=dir_name, sub_dir_name=star_type.lower())

    fit = run(info_path=info_path, func=run_stan,
              observed_values=observed_values, uncertainties=uncertainties,
              mu_prior=mu_prior, sigma_prior=sigma_prior,
              info_path2=info_path)

    return fit


def compare_parameters(fit_agb, fit_rgb, path, dir_name, title):
    """
    Save a table that compares parameters for AGB and RGB values.

    Parameters
    ----------
    fit_agb:
    fit_rgb:
        cmdstanpy.CmdStanMCMC
            Stan's output containing samples of posterior distribution
            of parameters generated using observations of AGB and RGB stars.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE)

    save_compare_parameters([fit_agb, fit_rgb],
                            labels=["AGB", "RGB"],
                            param_names=["r", "mu"],
                            info_path=info_path,
                            type=CompareParametersType.GITLAB_LATEX)


def plot_compare(fit_agb, fit_rgb, path, dir_name, title):
    """
    Show summaries of posterior distributions of model parameters
    for both AGB and RGB on the same plot for comparison.

    Parameters
    ----------
    fit_agb:
    fit_rgb:
        cmdstanpy.CmdStanMCMC
            Stan's output containing samples of posterior distribution
            of parameters generated using observations of AGB and RGB stars.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    tree_params = TreePlotParams()
    tree_params.title = title
    tree_params.labels = ["AGB", "RGB"]
    tree_params.markers = ["^", "o"]
    tree_params.marker_colors = [COLOR_AGB, COLOR_RGB]
    tree_params.marker_edge_colors = [COLOR_AGB_EDGE, COLOR_RGB_EDGE]

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="compare_summaries",
                         extension="png")

    save_tree_plot([fit_agb, fit_rgb],
                   param_names=["r", "mu", "sigma"],
                   info_path=info_path,
                   tree_params=tree_params)


def calculate_agb_failure_rate(fit_rgb, fit_agb, path, dir_name):
    """
    Saves summary and histogram of AGB failure rate distribution

        f = 1 - r_agb / r_rgb

    Saves result to `failure_rate` directory.

    Parameters
    ----------
    fit_rgb, fit_agb:
        cmdstanpy.CmdStanMCMC
            Stan's output containing samples of posterior distribution
            of parameters generated using observations of AGB and RGB stars.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    df_agb = fit_agb.get_drawset(params="r")
    df_rgb = fit_rgb.get_drawset(params="r")
    df_f = 1 - df_agb / df_rgb  # Failure rate

    # Exclude values way outside [0, 1] interval
    df_f = df_f[df_f['r'].between(-1, 2)]

    df_f.columns = ["AGB failure rate"]

    # Save summary
    info_path = InfoPath(path=path,
                         dir_name=dir_name, sub_dir_name="failure_rate")

    save_summary(df_f, info_path=info_path)
    shared_save_tree_plot([df_f], info_path=info_path)

    info_path.extension = 'png'
    save_histogram(df_f, info_path=info_path)


def proportion_positive_samples(df_delta, path, dir_name):
    """
    Calculate the proportino of samples for which parameters r, mu.1 and mu.2
    are greater in RGB stars.

    The result is saved in a text file.

    Parameters
    ----------

    df_delta : Pandas' dataframe
        Difference between RGB and AGB samples for r and mu parameters.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    n = df_delta["mu.1"].count()
    delta_r_positive = df_delta[df_delta["r"] > 0]["r"].count() / n
    delta_mu1_positive = df_delta[df_delta["mu.1"] > 0]["mu.1"].count() / n
    delta_mu2_positive = df_delta[df_delta["mu.2"] > 0]["mu.2"].count() / n

    table_data = [
        ["Number of samples", n],
        ["Delta r > 0", delta_r_positive],
        ["Delta mu 1 > 0", delta_mu1_positive],
        ["Delta mu 2 > 0", delta_mu2_positive],
    ]

    txt_table = tabulate(table_data,
                         headers=["Name", "Value"],
                         floatfmt=".2f", tablefmt="pipe")

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="compare_agb_rgb_parameters",
                         extension="txt")

    with open(get_info_path(info_path), "w") as text_file:
        print(txt_table, file=text_file)


def calculate_deltas(fit_rgb, fit_agb, path, dir_name):
    """
    Calculate differences of two populations of sodium abundances
    between rgb and agb stars.

    Parameters
    ----------
    fit_rgb, fit_agb:
        cmdstanpy.CmdStanMCMC
            Stan's output containing samples of posterior distribution
            of parameters generated using observations of AGB and RGB stars.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    df_agb = fit_agb.get_drawset(params=["r", "mu"])
    df_rgb = fit_rgb.get_drawset(params=["r", "mu"])
    df_delta = df_rgb - df_agb

    proportion_positive_samples(df_delta=df_delta, path=path,
                                dir_name=dir_name)

    df_delta.rename(
        columns={"mu.1": "Delta mu 1", "mu.2": "Delta mu 2", "r": "Delta r"},
        inplace=True)

    # Save summary
    info_path = InfoPath(path=path, dir_name=dir_name, sub_dir_name="delta_mu")
    save_summary(df_delta, info_path=info_path)
    shared_save_tree_plot([df_delta], info_path=info_path)
    save_histogram(df_delta, info_path=info_path)


def model_pdf(x, row):
    """
    Calculate distribution from single posterior sample.
    """

    mu1 = row['mu.1']
    mu2 = row['mu.2']
    sigma = row['sigma']
    r = row['r']

    return (1 - r) * stats.norm.pdf(x, mu1, sigma) + \
        r * stats.norm.pdf(x, mu2, sigma)


def do_analysis(values_agb, uncert_agb, values_rgb, uncert_rgb,
                path, dir_name, study_name, xlabel,
                mu_prior, sigma_prior):
    """
    Analyse the sodium abundances and create plots and summaries

    Parameters
    ----------

    values_agb, uncert_agb,
    values_rgb, uncert_rgb : list of float
        Sodium abundances and their uncertainties for analysis.

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.

    study_name : str
        Name of the study to be shown in the plot title.

    xlabel : str
        X-label of the plot of observed data.

    mu_prior, sigma_prior : float
        Priors for the mean and standard deviations of two populations.
    """

    print(f'\n\n-----------------------------')
    print(f'Analysing {study_name}...')

    # Plot observations
    # -----------

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="observed_combined",
                         extension="png")

    save_scatter_and_kde(
        values=[values_agb, values_rgb],
        uncertainties=[uncert_agb, uncert_rgb],
        title=f"Abundances from {study_name}",
        xlabel=xlabel,
        ylabel=["Star number", "Probability density"],
        legend_labels=["AGB", "RGB"],
        info_path=info_path)

    # Run Stan
    # ----------------

    print("\nRGB stars...")

    fit_rgb = run_model('RGB',
                        observed_values=values_rgb,
                        uncertainties=uncert_rgb,
                        path=path,
                        dir_name=dir_name,
                        mu_prior=mu_prior,
                        sigma_prior=sigma_prior)

    print("\nAGB stars....")

    fit_agb = run_model('AGB',
                        observed_values=values_agb,
                        uncertainties=uncert_agb,
                        path=path,
                        dir_name=dir_name,
                        mu_prior=mu_prior,
                        sigma_prior=sigma_prior)

    info_path = InfoPath(path=path, dir_name=dir_name,
                         sub_dir_name=InfoPath.DO_NOT_CREATE,
                         base_name="posterior",
                         extension="png")

    save_posterior_scatter_and_kde(
        fits=[fit_agb, fit_rgb],
        pdf=model_pdf,
        values=[values_agb, values_rgb],
        uncertainties=[uncert_agb, uncert_rgb],
        title=f"Observed and posterior abundances from {study_name}",
        xlabel=xlabel,
        ylabel=["Star number", "Probability density"],
        legend_labels=["AGB", "RGB"],
        info_path=info_path)

    title = f"Model parameters for {study_name}"

    plot_compare(fit_agb=fit_agb, fit_rgb=fit_rgb, path=path,
                 dir_name=dir_name, title=title)

    compare_parameters(fit_agb=fit_agb, fit_rgb=fit_rgb, path=path,
                       dir_name=dir_name, title=title)

    calculate_agb_failure_rate(fit_rgb=fit_rgb, fit_agb=fit_agb,
                               path=path, dir_name=dir_name)

    calculate_deltas(fit_rgb=fit_rgb, fit_agb=fit_agb,
                     path=path, dir_name=dir_name)


def do_work(path, dir_name):
    """
    Main function that does all the work

    Parameters
    ----------

    path : str
        Path to directory where the data directory `dir_name` will be created.

    dir_name : str
        Name of the parent directory where the plots will be created.
    """

    data_path = "paper/model/data/sodium.csv"
    type_name = "type"
    abundance_name = "abundance"
    uncertainty_name = "uncertainty"

    values_agb, uncert_agb = load_data('AGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    values_rgb, uncert_rgb = load_data('RGB', data_path=data_path,
                                       type_name=type_name,
                                       abundance_name=abundance_name,
                                       uncertainty_name=uncertainty_name)

    do_analysis(values_agb=values_agb, uncert_agb=uncert_agb,
                values_rgb=values_rgb, uncert_rgb=uncert_rgb,
                path=path,
                dir_name=dir_name,
                study_name="NGC 288",
                xlabel="Sodium abundance [Na/H]",
                mu_prior=-1,
                sigma_prior=0.5)


if __name__ == '__main__':
    print("Running the models...")
    do_work(path="paper/model/code", dir_name="ngc288_2020")
    print('We are done')

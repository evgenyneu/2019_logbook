# Simulate sodium

I simulate two populations of sodium abundances based on those from Chris' report, as shown in Fig. 1.

```
n1 = 10
mu1 = -1.1
sigma1 = 0.05

n2 = 17
mu2 = -0.6
sigma2 = 0.05
```

For measurement uncertainties I generated random numbers from normal distributions with mu=0.13 and sigma=0.06.

Code: [a2020/a01/a10_simulate_sodium_python/code/simulate_sodium.py](a2020/a01/a10_simulate_sodium_python/code/simulate_sodium.py)


<img src="a2020/a01/a10_simulate_sodium_python/code/plots/simulate_sodium_simulated.png" width="700" alt='Simulated sodium abundances'>

Figure 1: Simulated sodium abundances.


## Results

Posterior distributions of parameters are shown below in Table 1 and Figures 2 and 3.

Table 1: Posterior distribution of parameters.

| Name    |   Mean |   Std |   Mode |    + |    - |   68CI- |   68CI+ |   95CI- |   95CI+ |
|:--------|-------:|------:|-------:|-----:|-----:|--------:|--------:|--------:|--------:|
| p       |   0.40 |  0.16 |   0.36 | 0.16 | 0.14 |    0.22 |    0.52 |    0.11 |    0.73 |
| mu.1    |  -1.03 |  0.09 |  -1.06 | 0.09 | 0.07 |   -1.14 |   -0.97 |   -1.18 |   -0.85 |
| mu.2    |  -0.60 |  0.05 |  -0.60 | 0.04 | 0.05 |   -0.65 |   -0.56 |   -0.70 |   -0.50 |
| sigma.1 |   0.15 |  0.09 |   0.10 | 0.11 | 0.05 |    0.05 |    0.20 |    0.01 |    0.29 |
| sigma.2 |   0.10 |  0.06 |   0.08 | 0.04 | 0.03 |    0.05 |    0.12 |    0.02 |    0.19 |



<img src="a2020/a01/a10_simulate_sodium_python/code/plots/summary.png" width="700" alt='Posterior distributions of parameters'>

Figure 2: Posterior distributions of parameters.


### Histograms of posterior distributions of parameters

<img src="a2020/a01/a10_simulate_sodium_python/code/plots/posterior_01.png" alt='Posterior distributions of parameters'>

Figure 3: Histograms of posterior distributions of parameters.

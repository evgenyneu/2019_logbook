from cmdstanpy import CmdStanModel

model = CmdStanModel(stan_file="eight_schools.stan")

data = {
    "J": 8,
    "y": [28,  8, -3,  7, -1,  1, 18, 12],
    "sigma": [15, 10, 16, 11,  9, 11, 10, 18]
}

fit = model.sample(data=data, show_progress=True,
                   chains=4, cores=2,
                   sampling_iters=15000, warmup_iters=16000)


print("Sampling finished")
# print(fit.summary())
